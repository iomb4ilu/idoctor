package com.staarlab.idoctor.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by PHAMHUNG on 12/26/2016.
 */

/**
 * Danh mục kho xuất
 * "MEDI_STOCK_ID":124,"ROOM_ID":225,"PRIORITY":null,"MEDI_STOCK_CODE":"KVT","MEDI_STOCK_NAME":"Kho vật tư","ROOM_CODE":"BBKKB","ROOM_NAME":"BĐT-Khoa khám bệnh 106","DEPARTMENT_ID":22,"ROOM_TYPE_ID":4,"ROOM_TYPE_CODE":"GI","ROOM_TYPE_NAME":"Buồng điều trị","DEPARTMENT_CODE":"KKB","DEPARTMENT_NAME":"Khoa Khám Bệnh","IS_PAUSE":null,"G_CODE":null
 */
@DatabaseTable(tableName = "MEDI_STOCK")
public class MedicalStock implements Parcelable, Serializable {
    public static final String FIELD_NAME_STOCK_NAME = "MEDI_STOCK_NAME";
    public static final String FIELD_NAME_STOCK_CODE = "MEDI_STOCK_CODE";
    public static final String FIELD_NAME_STOCK_SEARCH = "MEDI_STOCK_SEARCH";

    @DatabaseField(columnName = "_id", id = true)
    public long ID;
    @DatabaseField(columnName = "CREATE_TIME")
    public String CREATE_TIME;
    @DatabaseField(columnName = "MODIFY_TIME")
    public String MODIFY_TIME;
    @DatabaseField(columnName = "IS_ACTIVE")
    public int IS_ACTIVE;
    @DatabaseField(columnName = "IS_DELETE")
    public int IS_DELETE;
    @DatabaseField(columnName = "GROUP_CODE")
    public String GROUP_CODE;
    @DatabaseField(columnName = FIELD_NAME_STOCK_CODE)
    public String MEDI_STOCK_CODE;
    @DatabaseField(columnName = FIELD_NAME_STOCK_NAME)
    public String MEDI_STOCK_NAME;
    @DatabaseField(columnName = FIELD_NAME_STOCK_SEARCH, columnDefinition = "TEXT COLLATE NOCASE")
    public String MEDI_STOCK_SEARCH;
    @DatabaseField(columnName = "ROOM_ID")
    public long ROOM_ID;
    @DatabaseField(columnName = "IS_BUSINESS")
    public long IS_BUSINESS;

    public Long MEDI_STOCK_ID;


    public MedicalStock() {
    }

    protected MedicalStock(Parcel in) {
        ID = in.readLong();
        CREATE_TIME = in.readString();
        MODIFY_TIME = in.readString();
        IS_ACTIVE = in.readInt();
        IS_DELETE = in.readInt();
        GROUP_CODE = in.readString();
        MEDI_STOCK_CODE = in.readString();
        MEDI_STOCK_NAME = in.readString();
        MEDI_STOCK_SEARCH = in.readString();
        ROOM_ID = in.readLong();
        IS_BUSINESS = in.readLong();
    }

    public static final Creator<MedicalStock> CREATOR = new Creator<MedicalStock>() {
        @Override
        public MedicalStock createFromParcel(Parcel in) {
            return new MedicalStock(in);
        }

        @Override
        public MedicalStock[] newArray(int size) {
            return new MedicalStock[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(ID);
        parcel.writeString(CREATE_TIME);
        parcel.writeString(MODIFY_TIME);
        parcel.writeInt(IS_ACTIVE);
        parcel.writeInt(IS_DELETE);
        parcel.writeString(GROUP_CODE);
        parcel.writeString(MEDI_STOCK_CODE);
        parcel.writeString(MEDI_STOCK_NAME);
        parcel.writeString(MEDI_STOCK_SEARCH);
        parcel.writeLong(ROOM_ID);
        parcel.writeLong(IS_BUSINESS);
    }
}
