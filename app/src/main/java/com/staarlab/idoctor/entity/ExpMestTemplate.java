package com.staarlab.idoctor.entity;

/**
 * Created by PHAMHUNG on 3/5/2017.
 */

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * {"ID":1743,"CREATE_TIME":20170224154951,"MODIFY_TIME":20170224154951,"CREATOR":"simnt","MODIFIER":"simnt","APP_CREATOR":"MOS","APP_MODIFIER":"MOS","IS_ACTIVE":1,"IS_DELETE":0,"GROUP_CODE":null,
 * "EXP_MEST_TEMPLATE_CODE":"m2017","EXP_MEST_TEMPLATE_NAME":"m2017","DESCRIPTION":"jgjhfjhgjh","HIS_EMTE_MATERIAL_TYPE":[],"HIS_EMTE_MEDICINE_TYPE":[]}
 */
@DatabaseTable(tableName = "EXP_MEST_TEMPLATE")
public class ExpMestTemplate {
    public static final String FIELD_NAME_EXP_TEMPLATE_NAME = "TEMPLATE_NAME";
    public static final String FIELD_NAME_EXP_TEMPLATE_CODE = "TEMPLATE_CODE";
    public static final String FIELD_NAME_EXP_TEMPLATE_DESCRIPTION= "TEMPLATE_DESCRIPTION";
    public static final String FIELD_NAME_EXP_TEMPLATE_SEARCH = "TEMPLATE_SEARCH";

    @DatabaseField(columnName = "_id", id = true)
    public long ID;
    @DatabaseField(columnName = FIELD_NAME_EXP_TEMPLATE_CODE)
    public String EXP_MEST_TEMPLATE_CODE;
    @DatabaseField(columnName = FIELD_NAME_EXP_TEMPLATE_NAME)
    public String EXP_MEST_TEMPLATE_NAME;
    @DatabaseField(columnName = FIELD_NAME_EXP_TEMPLATE_DESCRIPTION)
    public String DESCRIPTION;
    @DatabaseField(columnName = FIELD_NAME_EXP_TEMPLATE_SEARCH)
    public String EXP_MEST_TEMPLATE_SEARCH;
}
