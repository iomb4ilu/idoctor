package com.staarlab.idoctor.entity;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by ducln on 02/07/2016.
 */
@DatabaseTable(tableName = "User")
public class User implements Serializable {

    @DatabaseField(dataType = DataType.STRING)
    @SerializedName("LoginName")
    private String loginName;

    @DatabaseField(dataType = DataType.STRING)
    @SerializedName("UserName")
    private String userName;

    @DatabaseField(dataType = DataType.STRING)
    @SerializedName("ApplicationCode")
    private String applicationCode;

    @DatabaseField(dataType = DataType.STRING)
    @SerializedName("GCode")
    private String gCode;

    @DatabaseField(dataType = DataType.STRING)
    @SerializedName("Email")
    private String email;

    @DatabaseField(dataType = DataType.STRING)
    @SerializedName("Mobile")
    private String mobile;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    public String getGCode() {
        return gCode;
    }

    public void setGCode(String GCode) {
        this.gCode = GCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
