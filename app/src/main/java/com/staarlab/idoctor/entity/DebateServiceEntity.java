package com.staarlab.idoctor.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DebateServiceEntity extends CommonServiceEntity {
    @SerializedName("DEBATE_TIME")
    private String debateTime;

    @SerializedName("REQUEST_CONTENT")
    private String requestContent;

    @SerializedName("CREATOR")
    private String creator;

    @SerializedName("CREATE_TIME")
    private String createTime;

    @SerializedName("MODIFIER")
    private String modifier;

    @SerializedName("MODIFY_TIME")
    private String modifyTime;

    public String getDebateTime() {
        return debateTime;
    }

    public void setDebateTime(String debateTime) {
        this.debateTime = debateTime;
    }

    public String getRequestContent() {
        return requestContent;
    }

    public void setRequestContent(String requestContent) {
        this.requestContent = requestContent;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }
}
