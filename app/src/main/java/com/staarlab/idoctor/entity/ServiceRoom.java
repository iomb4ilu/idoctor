package com.staarlab.idoctor.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Gun on 2/17/17.
 */
@DatabaseTable(tableName = "SERVICE_ROOM")
public class ServiceRoom {

    public static final String FIELD_NAME_SERVICE_TYPE_ID = "SERVICE_TYPE_ID";
    public static final String FIELD_NAME_SERVICE_TYPE_CODE = "SERVICE_TYPE_CODE";
    public static final String FIELD_NAME_SERVICE_ROOM_ID = "ROOM_ID";

    @DatabaseField(columnName = "_id", id = true)
    public long ID;
    @DatabaseField(columnName = "CREATE_TIME")
    public String CREATE_TIME;
    @DatabaseField(columnName = "MODIFY_TIME")
    public String MODIFY_TIME;
    @DatabaseField(columnName = "IS_ACTIVE")
    public int IS_ACTIVE;
    @DatabaseField(columnName = "IS_DELETE")
    public int IS_DELETE;

    @DatabaseField(columnName = "GROUP_CODE")
    public String GROUP_CODE;
    @DatabaseField(columnName = FIELD_NAME_SERVICE_TYPE_CODE)
    public String SERVICE_TYPE_CODE;
    @DatabaseField(columnName = "SERVICE_TYPE_NAME")
    public String SERVICE_TYPE_NAME;
    @DatabaseField(columnName = "SERVICE_CODE")
    public String SERVICE_CODE;
    @DatabaseField(columnName = "SERVICE_NAME")
    public String SERVICE_NAME;
    @DatabaseField(columnName = "SERVICE_SEARCH")
    public String SERVICE_SEARCH;
    @DatabaseField(columnName = "ROOM_CODE")
    public String ROOM_CODE;
    @DatabaseField(columnName = "ROOM_NAME")
    public String ROOM_NAME;
    @DatabaseField(columnName = "ROOM_TYPE_CODE")
    public String ROOM_TYPE_CODE;
    @DatabaseField(columnName = "ROOM_TYPE_NAME")
    public String ROOM_TYPE_NAME;
    @DatabaseField(columnName = "DEPARTMENT_CODE")
    public String DEPARTMENT_CODE;
    @DatabaseField(columnName = "DEPARTMENT_NAME")
    public String DEPARTMENT_NAME;

    // INT
    @DatabaseField(columnName =FIELD_NAME_SERVICE_ROOM_ID)
    public long ROOM_ID;
    @DatabaseField(columnName ="SERVICE_ID")
    public long SERVICE_ID;
    @DatabaseField(columnName = FIELD_NAME_SERVICE_TYPE_ID)
    public long SERVICE_TYPE_ID;
    @DatabaseField(columnName = "DEPARTMENT_ID")
    public long DEPARTMENT_ID;
    @DatabaseField(columnName = "ROOM_TYPE_ID")
    public long ROOM_TYPE_ID;

}
