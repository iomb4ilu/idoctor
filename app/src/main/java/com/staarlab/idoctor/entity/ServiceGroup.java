package com.staarlab.idoctor.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by PhamHung on 1/3/17.
 * Nhom dich vu
 */
@DatabaseTable(tableName = "SERVICE_GROUP")
public class ServiceGroup implements Parcelable,Serializable {
    public static final String TABLE_FIELD_SERVICE_GROUP_CODE = "SERVICE_GROUP_CODE";
    public static final String TABLE_FIELD_SERVICE_GROUP_NAME = "SERVICE_GROUP_NAME";
    public static final String TABLE_FIELD_SEARCH = "TABLE_FIELD_SEARCH";
    @DatabaseField(columnName = "_id", id = true)
    public long ID;
    @DatabaseField(columnName = "CREATE_TIME")
    public String CREATE_TIME;
    @DatabaseField(columnName = "MODIFY_TIME")
    public String MODIFY_TIME;
    @DatabaseField(columnName = "IS_ACTIVE")
    public int IS_ACTIVE;
    @DatabaseField(columnName = "IS_DELETE")
    public int IS_DELETE;
    @DatabaseField(columnName = "GROUP_CODE")
    public String GROUP_CODE;
    @DatabaseField(columnName = TABLE_FIELD_SERVICE_GROUP_CODE)
    public String SERVICE_GROUP_CODE;
    @DatabaseField(columnName = TABLE_FIELD_SERVICE_GROUP_NAME)
    public String SERVICE_GROUP_NAME;

    @DatabaseField(columnName = TABLE_FIELD_SEARCH)
    public String SERVICE_GROUP_SEARCH;

    public ServiceGroup() {
    }

    protected ServiceGroup(Parcel in) {
        ID = in.readLong();
        CREATE_TIME = in.readString();
        MODIFY_TIME = in.readString();
        IS_ACTIVE = in.readInt();
        IS_DELETE = in.readInt();
        GROUP_CODE = in.readString();
        SERVICE_GROUP_CODE = in.readString();
        SERVICE_GROUP_NAME = in.readString();
        SERVICE_GROUP_SEARCH = in.readString();
    }

    public static final Creator<ServiceGroup> CREATOR = new Creator<ServiceGroup>() {
        @Override
        public ServiceGroup createFromParcel(Parcel in) {
            return new ServiceGroup(in);
        }

        @Override
        public ServiceGroup[] newArray(int size) {
            return new ServiceGroup[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(ID);
        parcel.writeString(CREATE_TIME);
        parcel.writeString(MODIFY_TIME);
        parcel.writeInt(IS_ACTIVE);
        parcel.writeInt(IS_DELETE);
        parcel.writeString(GROUP_CODE);
        parcel.writeString(SERVICE_GROUP_CODE);
        parcel.writeString(SERVICE_GROUP_NAME);
        parcel.writeString(SERVICE_GROUP_SEARCH);
    }
}
