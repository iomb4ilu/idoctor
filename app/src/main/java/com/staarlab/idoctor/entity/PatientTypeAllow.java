package com.staarlab.idoctor.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by PHAMHUNG on 3/16/2017.
 */

/**
 * {"ID":366,"CREATE_TIME":20170310141658,"MODIFY_TIME":20170310141658,"CREATOR":"duongnv","MODIFIER":"duongnv","APP_CREATOR":"MOS","APP_MODIFIER":"MOS","IS_ACTIVE":1,"IS_DELETE":0,"GROUP_CODE":null,
 * "PATIENT_TYPE_ID":205,"PATIENT_TYPE_ALLOW_ID":23,"HIS_PATIENT_TYPE":null,"HIS_PATIENT_TYPE1":null}
 */
@DatabaseTable(tableName = "PATIENT_TYPE_ALLOW")
public class PatientTypeAllow {
    public static final String FIELD_NAME_ID = "_id";
    public static final String FIELD_NAME_PATIENT_TYPE_ID = "PATIENT_TYPE_ID";
    public static final String FIELD_NAME_PATIENT_TYPE_ALLOW_ID = "PATIENT_TYPE_ALLOW_ID";

    @DatabaseField(columnName = FIELD_NAME_ID, id = true)
    public Long ID;
    @DatabaseField(columnName = FIELD_NAME_PATIENT_TYPE_ID)
    public long PATIENT_TYPE_ID;
    @DatabaseField(columnName = FIELD_NAME_PATIENT_TYPE_ALLOW_ID)
    public long PATIENT_TYPE_ALLOW_ID;
}
