package com.staarlab.idoctor.entity;

import java.io.Serializable;

/**
 * Created by PHAMHUNG on 1/8/2017.
 */

/**
 * "Id":927,
 * "MaterialTypeCode":"VTT034",
 * "MaterialTypeName":"Syringe có đầu xoáy",
 * "MaterialTypeHeinName":null,
 * "ServiceId":3910,
 * "ParentId":null,
 * "MediStockId":164,
 * "ServiceUnitId":null,
 * "IsLeaf":1,"IsActive":null,
 * "TotalAmount":10000.0,
 * "AvailableAmount":10000.0,
 * "ServiceUnitCode":"15",
 * "ServiceUnitName":"Cái",
 * "ServiceUnitSymbol":null,
 * "NationalCode":null,"NationalName":null,
 * "ManufacturerCode":"VTT034",
 * "ManufacturerName":null,
 * "NumOrder":null
 */
public class Material extends MedicineMaterial implements Serializable {

    public String MaterialTypeCode;
    public String MaterialTypeName;
    public String MaterialTypeHeinName;


    public Material() {
        super();
    }


}
