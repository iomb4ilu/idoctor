package com.staarlab.idoctor.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by PHAMHUNG on 1/8/2017.
 */

public class MedicineMaterial implements Serializable, Parcelable {

    public long Id;
    public long ServiceId;
    public long ParentId;
    public long MediStockId;
    public long IsLeaf;
    @SerializedName("TotalAmount")
    public Double TotalAmount;
    @SerializedName("AvailableAmount")
    public Double AvailableAmount;
    public String ServiceUnitCode;
    public String ServiceUnitName;
    public String ManufacturerCode;

    public MedicineMaterial() {
    }


    protected MedicineMaterial(Parcel in) {
        Id = in.readLong();
        ServiceId = in.readLong();
        ParentId = in.readLong();
        MediStockId = in.readLong();
        IsLeaf = in.readLong();
        TotalAmount = in.readDouble();
        AvailableAmount = in.readDouble();
        ServiceUnitCode = in.readString();
        ServiceUnitName = in.readString();
        ManufacturerCode = in.readString();
    }

    public static final Creator<MedicineMaterial> CREATOR = new Creator<MedicineMaterial>() {
        @Override
        public MedicineMaterial createFromParcel(Parcel in) {
            return new MedicineMaterial(in);
        }

        @Override
        public MedicineMaterial[] newArray(int size) {
            return new MedicineMaterial[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(Id);
        dest.writeLong(ServiceId);
        dest.writeLong(ParentId);
        dest.writeLong(MediStockId);
        dest.writeLong(IsLeaf);
        dest.writeDouble(TotalAmount);
        dest.writeDouble(AvailableAmount);
        dest.writeString(ServiceUnitCode);
        dest.writeString(ServiceUnitName);
        dest.writeString(ManufacturerCode);
    }
}
