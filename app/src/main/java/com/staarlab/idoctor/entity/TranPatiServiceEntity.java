package com.staarlab.idoctor.entity;

import com.google.gson.annotations.SerializedName;

public class TranPatiServiceEntity extends CommonServiceEntity {
    @SerializedName("TRAN_PATI_TYPE_NAME")
    private String tranPatiTypeName;

    @SerializedName("MEDI_ORG_CODE")
    private String mediOrgCode;

    @SerializedName("MEDI_ORG_NAME")
    private String mediOrgName;

    @SerializedName("CREATE_TIME")
    private String createTime;

    @SerializedName("CREATOR")
    private String creator;

    @SerializedName("MODIFIER")
    private String modifier;

    @SerializedName("MODIFY_TIME")
    private String modifyTime;

    @SerializedName("TRAN_PATI_FORM_NAME")
    private String tranPatiFormName;

    @SerializedName("TRAN_PATI_REASON_NAME")
    private String tranPatiReasonName;

    @SerializedName("CLINICAL_NOTE")
    private String clinicalNote;

    public String getTranPatiTypeName() {
        return tranPatiTypeName;
    }

    public void setTranPatiTypeName(String tranPatiTypeName) {
        this.tranPatiTypeName = tranPatiTypeName;
    }

    public String getMediOrgCode() {
        return mediOrgCode;
    }

    public void setMediOrgCode(String mediOrgCode) {
        this.mediOrgCode = mediOrgCode;
    }

    public String getMediOrgName() {
        return mediOrgName;
    }

    public void setMediOrgName(String mediOrgName) {
        this.mediOrgName = mediOrgName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getTranPatiFormName() {
        return tranPatiFormName;
    }

    public void setTranPatiFormName(String tranPatiFormName) {
        this.tranPatiFormName = tranPatiFormName;
    }

    public String getTranPatiReasonName() {
        return tranPatiReasonName;
    }

    public void setTranPatiReasonName(String tranPatiReasonName) {
        this.tranPatiReasonName = tranPatiReasonName;
    }

    public String getClinicalNote() {
        return clinicalNote;
    }

    public void setClinicalNote(String clinicalNote) {
        this.clinicalNote = clinicalNote;
    }
}
