package com.staarlab.idoctor.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by ducln on 17/11/2016.
 */

/**
 * {"Id":1542,
 * "MedicineTypeCode":"TT0017",
 * "MedicineTypeName":"Dolcontral 100mg/2ml",
 * "MedicineTypeHeinName":null,
 * "ServiceId":3584,
 * "ParentId":1525,
 * "MediStockId":144,
 * "ServiceUnitId":null,
 * "IsLeaf":1,"IsActive":null
 * "TotalAmount":2.0,
 * "AvailableAmount":2.0,
 * "ServiceUnitCode":"10",
 * "ServiceUnitName":"Ống",
 * "ServiceUnitSymbol":null,
 * "NationalCode":null,
 * "NationalName":null,
 * "ManufacturerCode":"TT0017",
 * "ManufacturerName":null,
 * "NumOrder":null}
 */
public class Medicine extends MedicineMaterial implements Serializable {

    public String MedicineTypeCode;
    public String MedicineTypeName;


}
