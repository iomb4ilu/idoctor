package com.staarlab.idoctor.entity;

/**
 * Created by PHAMHUNG on 3/9/2017.
 */

/**
 *         public List<long> EXP_MEST_IDs { get; set; }
 public List<long> EXP_MEST_TYPE_IDs { get; set; }
 public List<long> MATERIAL_IDs { get; set; }
 public long? MATERIAL_ID { get; set; }
 public long? EXP_MEST_ID { get; set; }
 public long? MEDI_STOCK_PERIOD_ID { get; set; }
 public bool? HAS_MEDI_STOCK_PERIOD { get; set; }
 public long? MATERIAL_TYPE_ID { get; set; }
 public long? AGGR_EXP_MEST_ID { get; set; }
 public long? EXP_MEST_STT_ID { get; set; }
 public long? MEDI_STOCK_ID { get; set; }
 public long? EXP_TIME_FROM { get; set; }
 public long? EXP_TIME_TO { get; set; }
 */
public class MaterialOldPrescription {
    public long EXP_MEST_ID;
    public String MATERIAL_TYPE_CODE;
    public String MATERIAL_TYPE_NAME;
    public Long MATERIAL_TYPE_ID;
    public long SERVICE_ID;
    public Double AMOUNT;
}
