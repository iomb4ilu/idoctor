package com.staarlab.idoctor.entity;

/**
 * Created by PHAMHUNG on 1/9/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Thông tin diện đối tượng chung
 */
public class PatientTypeAlter implements Serializable, Parcelable {
    public long ID;
    public long PATIENT_TYPE_ID;
    public long TREATMENT_TYPE_ID;
    public long TREATMENT_ID;
    public String PATIENT_TYPE_CODE;
    public String PATIENT_TYPE_NAME;
    public String TREATMENT_TYPE_CODE;
    public String TREATMENT_TYPE_NAME;
    public String HEIN_TREATMENT_TYPE_CODE;

    protected PatientTypeAlter(Parcel in) {
        ID = in.readLong();
        PATIENT_TYPE_ID = in.readLong();
        TREATMENT_TYPE_ID = in.readLong();
        TREATMENT_ID = in.readLong();
        PATIENT_TYPE_CODE = in.readString();
        PATIENT_TYPE_NAME = in.readString();
        TREATMENT_TYPE_CODE = in.readString();
        TREATMENT_TYPE_NAME = in.readString();
        HEIN_TREATMENT_TYPE_CODE = in.readString();
    }

    public static final Creator<PatientTypeAlter> CREATOR = new Creator<PatientTypeAlter>() {
        @Override
        public PatientTypeAlter createFromParcel(Parcel in) {
            return new PatientTypeAlter(in);
        }

        @Override
        public PatientTypeAlter[] newArray(int size) {
            return new PatientTypeAlter[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(ID);
        dest.writeLong(PATIENT_TYPE_ID);
        dest.writeLong(TREATMENT_TYPE_ID);
        dest.writeLong(TREATMENT_ID);
        dest.writeString(PATIENT_TYPE_CODE);
        dest.writeString(PATIENT_TYPE_NAME);
        dest.writeString(TREATMENT_TYPE_CODE);
        dest.writeString(TREATMENT_TYPE_NAME);
        dest.writeString(HEIN_TREATMENT_TYPE_CODE);
    }
}