package com.staarlab.idoctor.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by PHAMHUNG on 1/1/2017.
 * Lấy chính sách giá dịch vụ
 * "ID":7520,"CREATE_TIME":20170225163242,"MODIFY_TIME":20170225163751,"CREATOR":"tungnt","MODIFIER":"tungnt","APP_CREATOR":"MOS","APP_MODIFIER":"MOS","IS_ACTIVE":1,"IS_DELETE":0,"GROUP_CODE":null,"SERVICE_PATY_CODE":"0000006833","SERVICE_ID":964,"PATIENT_TYPE_ID":22,"PRICE":210000.0,"VAT_RATIO":0.0,"PRIORITY":null,"FROM_TIME":20170225000000,"TO_TIME":20170326234500,"TREATMENT_FROM_TIME":20170226000000,"TREATMENT_TO_TIME":20171231234500,"BRANCH_ID":null,"SERVICE_TYPE_ID":10,"SERVICE_TYPE_CODE":"SA","SERVICE_TYPE_NAME":"Siêu âm","SERVICE_CODE":"SA018","SERVICE_NAME":"Siêu âm Doppler động mạch thận","PATIENT_TYPE_CODE":"01","PATIENT_TYPE_NAME":"BHYT","BRANCH_CODE":null,"BRANCH_NAME":null
 */
@DatabaseTable(tableName = "SERVICE_PATY")
public class ServicePATY implements Parcelable {
    public static final String FIELD_NAME_SERVICE_PATY_SERVICE_ID = "SERVICE_ID";
    public static final String FIELD_NAME_SERVICE_PATY_PATIENT_TYPE_CODE = "PATIENT_TYPE_CODE";
    public static final String FIELD_NAME_SERVICE_PATY_PATIENT_TYPE_ID = "PATIENT_TYPE_ID";

    @DatabaseField(columnName = "_id", id = true)
    public long ID;
    @DatabaseField(columnName = "CREATE_TIME")
    public String CREATE_TIME;
    @DatabaseField(columnName = "MODIFY_TIME")
    public long MODIFY_TIME;
    @DatabaseField(columnName = "IS_ACTIVE")
    public int IS_ACTIVE;
    @DatabaseField(columnName = "IS_DELETE")
    public int IS_DELETE;
    @DatabaseField(columnName = "GROUP_CODE")
    public String GROUP_CODE;

    @DatabaseField(columnName = FIELD_NAME_SERVICE_PATY_SERVICE_ID)
    public long SERVICE_ID;
    @DatabaseField(columnName = FIELD_NAME_SERVICE_PATY_PATIENT_TYPE_ID)
    public Long PATIENT_TYPE_ID;
    @DatabaseField(columnName = "PRICE")
    public double PRICE;
    @DatabaseField(columnName = "SERVICE_TYPE_ID")
    public int SERVICE_TYPE_ID;
    @DatabaseField(columnName = "SERVICE_TYPE_CODE")
    public String SERVICE_TYPE_CODE;
    @DatabaseField(columnName = "SERVICE_TYPE_NAME")
    public String SERVICE_TYPE_NAME;

    @DatabaseField(columnName = "SERVICE_CODE")
    public String SERVICE_CODE;
    @DatabaseField(columnName = "SERVICE_NAME")
    public String SERVICE_NAME;
    @DatabaseField(columnName = FIELD_NAME_SERVICE_PATY_PATIENT_TYPE_CODE)
    public String PATIENT_TYPE_CODE;
    @DatabaseField(columnName = "PATIENT_TYPE_NAME")
    public String PATIENT_TYPE_NAME;
    @DatabaseField(columnName = "PRIORITY")
    public Long PRIORITY;
    @DatabaseField(columnName = "FROM_TIME")
    public Long FROM_TIME;
    @DatabaseField(columnName = "TO_TIME")
    public Long TO_TIME;
    @DatabaseField(columnName = "TREATMENT_FROM_TIME")
    public Long TREATMENT_FROM_TIME;
    @DatabaseField(columnName = "TREATMENT_TO_TIME")
    public Long TREATMENT_TO_TIME;


    public ServicePATY() {
    }

    protected ServicePATY(Parcel in) {
        ID = in.readLong();
        CREATE_TIME = in.readString();
        MODIFY_TIME = in.readLong();
        IS_ACTIVE = in.readInt();
        IS_DELETE = in.readInt();
        GROUP_CODE = in.readString();
        SERVICE_ID = in.readLong();
        PATIENT_TYPE_ID = in.readLong();
        PRICE = in.readDouble();
        SERVICE_TYPE_ID = in.readInt();
        SERVICE_TYPE_CODE = in.readString();
        SERVICE_TYPE_NAME = in.readString();
        SERVICE_CODE = in.readString();
        SERVICE_NAME = in.readString();
        PATIENT_TYPE_CODE = in.readString();
        PATIENT_TYPE_NAME = in.readString();
        PRIORITY = in.readLong();
        FROM_TIME = in.readLong();
        TO_TIME = in.readLong();
        TREATMENT_FROM_TIME = in.readLong();
        TREATMENT_TO_TIME = in.readLong();
    }

    public static final Creator<ServicePATY> CREATOR = new Creator<ServicePATY>() {
        @Override
        public ServicePATY createFromParcel(Parcel in) {
            return new ServicePATY(in);
        }

        @Override
        public ServicePATY[] newArray(int size) {
            return new ServicePATY[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(ID);
        parcel.writeString(CREATE_TIME);
        parcel.writeLong(MODIFY_TIME);
        parcel.writeInt(IS_ACTIVE);
        parcel.writeInt(IS_DELETE);
        parcel.writeString(GROUP_CODE);
        parcel.writeLong(SERVICE_ID);
        parcel.writeLong(PATIENT_TYPE_ID);
        parcel.writeDouble(PRICE);
        parcel.writeInt(SERVICE_TYPE_ID);
        parcel.writeString(SERVICE_TYPE_CODE);
        parcel.writeString(SERVICE_TYPE_NAME);
        parcel.writeString(SERVICE_CODE);
        parcel.writeString(SERVICE_NAME);
        parcel.writeString(PATIENT_TYPE_CODE);
        parcel.writeString(PATIENT_TYPE_NAME);
        parcel.writeLong(PRIORITY);
        parcel.writeLong(FROM_TIME);
        parcel.writeLong(TO_TIME);
        parcel.writeLong(TREATMENT_FROM_TIME);
        parcel.writeLong(TREATMENT_TO_TIME);
    }
}
