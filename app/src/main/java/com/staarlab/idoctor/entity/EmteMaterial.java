package com.staarlab.idoctor.entity;

/**
 * Created by PHAMHUNG on 3/7/2017.
 */

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * {"ID":342,"CREATE_TIME":20170224154951,"MODIFY_TIME":20170224154951,"CREATOR":"simnt","MODIFIER":"simnt","APP_CREATOR":"MOS","APP_MODIFIER":"MOS","IS_ACTIVE":1,"IS_DELETE":0,"GROUP_CODE":null,
 * "EXP_MEST_TEMPLATE_ID":1743,"MATERIAL_TYPE_ID":731,"AMOUNT":4.0,"EXP_MEST_TEMPLATE_CODE":"m2017","EXP_MEST_TEMPLATE_NAME":"m2017",
 * "MATERIAL_TYPE_CODE":"BH011","MATERIAL_TYPE_NAME":"Kim luồn tĩnh mạch ngoại vi","SERVICE_ID":2454,"SERVICE_UNIT_ID":44,"SERVICE_UNIT_NAME":"Cái","SERVICE_UNIT_CODE":"15"}
 */
@DatabaseTable(tableName = "EMTE_MATERIAL")
public class EmteMaterial implements Serializable {
    public static final String TABLE_NAME_MEST_TEMPLATE_ID = "EXP_MEST_TEMPLATE_ID";

    @DatabaseField(columnName = "_id", id = true)
    public long ID;
    @DatabaseField(columnName = "EXP_MEST_TEMPLATE_ID")
    public long EXP_MEST_TEMPLATE_ID;
    @DatabaseField(columnName = "AMOUNT")
    public double AMOUNT;
    @DatabaseField(columnName = "MATERIAL_TYPE_CODE")
    public String MATERIAL_TYPE_CODE;
    @DatabaseField(columnName = "MATERIAL_TYPE_NAME")
    public String MATERIAL_TYPE_NAME;
    @DatabaseField(columnName = "MATERIAL_TYPE_ID")
    public Long MATERIAL_TYPE_ID;
    @DatabaseField(columnName = "SERVICE_ID")
    public long SERVICE_ID;
}
