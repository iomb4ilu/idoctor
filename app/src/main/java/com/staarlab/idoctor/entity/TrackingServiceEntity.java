package com.staarlab.idoctor.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lucero on 3/12/17.
 */

public class TrackingServiceEntity extends CommonServiceEntity {
    @SerializedName("TRACKING_TIME")
    private String trackingTime;

    @SerializedName("CREATOR")
    private String creator;

    @SerializedName("CREATE_TIME")
    private String createTime;

    @SerializedName("MODIFIER")
    private String modifier;

    @SerializedName("MODIFY_TIME")
    private String modifyTime;

    public String getTrackingTime() {
        return trackingTime;
    }

    public void setTrackingTime(String trackingTime) {
        this.trackingTime = trackingTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }
}
