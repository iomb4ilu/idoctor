package com.staarlab.idoctor.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Author: Lucero
 * Created: 11/15/2016.
 */

public class Patient implements Serializable {
    @SerializedName("ID")
    private Long id;

    @SerializedName("GROUP_CODE")
    private String groupCode;

    @SerializedName("PATIENT_CODE")
    private String patientCode;

    @SerializedName("FIRST_NAME")
    private String firstName;

    @SerializedName("LAST_NAME")
    private String lastName;

    @SerializedName("DOB")
    private  String dob;

    @SerializedName("NATIONAL_NAME")
    private String nationalName;

    @SerializedName("ETHNIC_NAME")
    private String ethnicName;

    @SerializedName("RELIGION_NAME")
    private String religionName;

    @SerializedName("PROVINCE_NAME")
    private String provinceName;

    @SerializedName("DISTRICT_NAME")
    private String districtName;

    @SerializedName("COMMUNE_NAME")
    private String communeName;

    @SerializedName("VIR_ADDRESS")
    private String virAddress;

    @SerializedName("GENDER_ID")
    private Integer genderId;

    @SerializedName("GENDER_CODE")
    private String genderCode;

    @SerializedName("GENDER_NAME")
    private String genderName;

    @SerializedName("VIR_PATIENT_NAME")
    private String virPatientName;

    @SerializedName("PHONE")
    private String phone;

    @SerializedName("RELATIVE_TYPE")
    private String relativeType;

    @SerializedName("RELATIVE_NAME")
    private String relativeName;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getPatientCode() {
        return patientCode;
    }

    public void setPatientCode(String patientCode) {
        this.patientCode = patientCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getNationalName() {
        return nationalName;
    }

    public void setNationalName(String nationalName) {
        this.nationalName = nationalName;
    }

    public String getEthnicName() {
        return ethnicName;
    }

    public void setEthnicName(String ethnicName) {
        this.ethnicName = ethnicName;
    }

    public String getReligionName() {
        return religionName;
    }

    public void setReligionName(String religionName) {
        this.religionName = religionName;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getCommuneName() {
        return communeName;
    }

    public void setCommuneName(String communeName) {
        this.communeName = communeName;
    }

    public String getVirAddress() {
        return virAddress;
    }

    public void setVirAddress(String virAddress) {
        this.virAddress = virAddress;
    }

    public Integer getGenderId() {
        return genderId;
    }

    public void setGenderId(Integer genderId) {
        this.genderId = genderId;
    }

    public String getGenderCode() {
        return genderCode;
    }

    public void setGenderCode(String genderCode) {
        this.genderCode = genderCode;
    }

    public String getGenderName() {
        return genderName;
    }

    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }

    public String getVirPatientName() {
        return virPatientName;
    }

    public void setVirPatientName(String virPatientName) {
        this.virPatientName = virPatientName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRelativeType() {
        return relativeType;
    }

    public void setRelativeType(String relativeType) {
        this.relativeType = relativeType;
    }

    public String getRelativeName() {
        return relativeName;
    }

    public void setRelativeName(String relativeName) {
        this.relativeName = relativeName;
    }
}
