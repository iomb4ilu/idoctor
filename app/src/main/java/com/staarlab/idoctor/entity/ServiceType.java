package com.staarlab.idoctor.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by PHAMHUNG on 12/28/2016.
 */
@DatabaseTable(tableName = "SERVICE_TYPE")
public class ServiceType implements Parcelable {
    public static final String FIELD_NAME_SERVICE_TYPE_CODE = "SERVICE_TYPE_CODE";
    @DatabaseField(columnName = "_id", id = true)
    public long ID;
    @DatabaseField(columnName = "CREATE_TIME")
    public String CREATE_TIME;
    @DatabaseField(columnName = "MODIFY_TIME")
    public String MODIFY_TIME;
    @DatabaseField(columnName = "IS_ACTIVE")
    public int IS_ACTIVE;
    @DatabaseField(columnName = "IS_DELETE")
    public int IS_DELETE;
    @DatabaseField(columnName = "GROUP_CODE")
    public String GROUP_CODE;
    @DatabaseField(columnName = FIELD_NAME_SERVICE_TYPE_CODE)
    public String SERVICE_TYPE_CODE;
    @DatabaseField(columnName = "SERVICE_TYPE_NAME")
    public String SERVICE_TYPE_NAME;
    @DatabaseField(columnName = "SERVICE_TYPE_SEARCH")
    public String SERVICE_TYPE_SEARCH;

    public ServiceType() {
    }

    protected ServiceType(Parcel in) {
        ID = in.readLong();
        CREATE_TIME = in.readString();
        MODIFY_TIME = in.readString();
        IS_ACTIVE = in.readInt();
        IS_DELETE = in.readInt();
        GROUP_CODE = in.readString();
        SERVICE_TYPE_CODE = in.readString();
        SERVICE_TYPE_NAME = in.readString();
        SERVICE_TYPE_SEARCH = in.readString();
    }

    public static final Creator<ServiceType> CREATOR = new Creator<ServiceType>() {
        @Override
        public ServiceType createFromParcel(Parcel in) {
            return new ServiceType(in);
        }

        @Override
        public ServiceType[] newArray(int size) {
            return new ServiceType[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(ID);
        parcel.writeString(CREATE_TIME);
        parcel.writeString(MODIFY_TIME);
        parcel.writeInt(IS_ACTIVE);
        parcel.writeInt(IS_DELETE);
        parcel.writeString(GROUP_CODE);
        parcel.writeString(SERVICE_TYPE_CODE);
        parcel.writeString(SERVICE_TYPE_NAME);
        parcel.writeString(SERVICE_TYPE_SEARCH);
    }
}
