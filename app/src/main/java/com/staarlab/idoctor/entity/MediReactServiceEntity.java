package com.staarlab.idoctor.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created on 3/12/17.
 */

public class MediReactServiceEntity extends CommonServiceEntity {
    @SerializedName("MEDI_REACT_TYPE_CODE")
    private String mediReactTypeCode;

    @SerializedName("MEDI_REACT_TYPE_NAME")
    private String mediReactTypeName;

    @SerializedName("MEDICINE_TYPE_CODE")
    private String medicineTypeCode;

    @SerializedName("MEDICINE_TYPE_NAME")
    private String medicineTypeName;

    @SerializedName("MEDI_REACT_STT_NAME")
    private String mediReactSttName;

    @SerializedName("SERVICE_UNIT_NAME")
    private String serviceUnitName;

    @SerializedName("EXPIRED_DATE")
    private String expiredDate;

    public String getMediReactTypeCode() {
        return mediReactTypeCode == null ? "" : mediReactTypeCode;
    }

    public void setMediReactTypeCode(String mediReactTypeCode) {
        this.mediReactTypeCode = mediReactTypeCode;
    }

    public String getMediReactTypeName() {
        return mediReactTypeName == null ? "" : mediReactTypeName;
    }

    public void setMediReactTypeName(String mediReactTypeName) {
        this.mediReactTypeName = mediReactTypeName;
    }

    public String getMediReactSttName() {
        return mediReactSttName == null ? "" : mediReactSttName;
    }

    public void setMediReactSttName(String mediReactSttName) {
        this.mediReactSttName = mediReactSttName;
    }

    public String getMedicineTypeName() {
        return medicineTypeName;
    }

    public void setMedicineTypeName(String medicineTypeName) {
        this.medicineTypeName = medicineTypeName;
    }

    public String getServiceUnitName() {
        return serviceUnitName;
    }

    public void setServiceUnitName(String serviceUnitName) {
        this.serviceUnitName = serviceUnitName;
    }

    public String getMedicineTypeCode() {
        return medicineTypeCode;
    }

    public void setMedicineTypeCode(String medicineTypeCode) {
        this.medicineTypeCode = medicineTypeCode;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }
}
