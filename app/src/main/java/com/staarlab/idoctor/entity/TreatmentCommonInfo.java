package com.staarlab.idoctor.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by PhamHung on 1/6/17.
 */

public class TreatmentCommonInfo implements Serializable, Parcelable {
    @SerializedName("Patient")
    public Patient patient;
    @SerializedName("PatientTypeAlter")
    public PatientTypeAlter patientTypeAlter;
    @SerializedName("PatyAlterBhyt")
    public PatyAlterBhyt patyAlterBhyt;

    public long ID;
    @SerializedName("TREATMENT_CODE")
    public String TREATMENT_CODE;
    @SerializedName("END_DEPARTMENT_NAME")
    public String END_DEPARTMENT_NAME;
    @SerializedName("IN_TIME")
    public Long IN_TIME;
    @SerializedName("TREATMENT_END_TYPE_NAME")
    public String TREATMENT_END_TYPE_NAME;
    @SerializedName("END_ROOM_NAME")
    public String END_ROOM_NAME;
    @SerializedName("OUT_TIME")
    public Long OUT_TIME;
    @SerializedName("TREATMENT_RESULT_NAME")
    public String TREATMENT_RESULT_NAME;
    public long PATIENT_ID;
    public String PATIENT_CODE;
    public long ICD_ID;
    public String ICD_CODE;
    public String ICD_NAME;
    public String ICD_TEXT;
    public String ICD_MAIN_TEXT;

    protected TreatmentCommonInfo(Parcel in) {
        patientTypeAlter = in.readParcelable(PatientTypeAlter.class.getClassLoader());
        patyAlterBhyt = in.readParcelable(PatyAlterBhyt.class.getClassLoader());
        ID = in.readLong();
        TREATMENT_CODE = in.readString();
        END_DEPARTMENT_NAME = in.readString();
        IN_TIME = in.readLong();
        TREATMENT_END_TYPE_NAME = in.readString();
        END_ROOM_NAME = in.readString();
        OUT_TIME = in.readLong();
        TREATMENT_RESULT_NAME = in.readString();
        PATIENT_ID = in.readLong();
        PATIENT_CODE = in.readString();
        ICD_ID = in.readLong();
        ICD_CODE = in.readString();
        ICD_NAME = in.readString();
        ICD_TEXT = in.readString();
        ICD_MAIN_TEXT = in.readString();
    }

    public static final Creator<TreatmentCommonInfo> CREATOR = new Creator<TreatmentCommonInfo>() {
        @Override
        public TreatmentCommonInfo createFromParcel(Parcel in) {
            return new TreatmentCommonInfo(in);
        }

        @Override
        public TreatmentCommonInfo[] newArray(int size) {
            return new TreatmentCommonInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(patientTypeAlter, flags);
        dest.writeParcelable(patyAlterBhyt, flags);
        dest.writeLong(ID);
        dest.writeString(TREATMENT_CODE);
        dest.writeString(END_DEPARTMENT_NAME);
        dest.writeLong(IN_TIME);
        dest.writeString(TREATMENT_END_TYPE_NAME);
        dest.writeString(END_ROOM_NAME);
        dest.writeLong(OUT_TIME != null ? OUT_TIME : 0l);
        dest.writeString(TREATMENT_RESULT_NAME);
        dest.writeLong(PATIENT_ID);
        dest.writeString(PATIENT_CODE);
        dest.writeLong(ICD_ID);
        dest.writeString(ICD_CODE);
        dest.writeString(ICD_NAME);
        dest.writeString(ICD_TEXT);
        dest.writeString(ICD_MAIN_TEXT);
    }
}




