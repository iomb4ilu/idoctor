package com.staarlab.idoctor.entity;


import com.google.gson.annotations.SerializedName;

public class InfusionServiceEntity extends CommonServiceEntity {
    @SerializedName("INFUSION_CODE")
    private String infusionCode;

    @SerializedName("INFUSION_STT_NAME")
    private String infusionSttName;

    @SerializedName("START_TIME")
    private String startTime;

    @SerializedName("FINISH_TIME")
    private String finishTime;

    @SerializedName("CREATOR")
    private String creator;

    @SerializedName("CREATE_TIME")
    private String createTime;

    @SerializedName("MODIFIER")
    private String modifier;

    @SerializedName("MODIFY_TIME")
    private String modifyTime;

    public String getInfusionCode() {
        return infusionCode == null ? "" : infusionCode;
    }

    public void setInfusionCode(String infusionCode) {
        this.infusionCode = infusionCode;
    }

    public String getInfusionSttName() {
        return infusionSttName == null ? "" : infusionSttName;
    }

    public void setInfusionSttName(String infusionSttName) {
        this.infusionSttName = infusionSttName;
    }

    public String getStartTime() {
        return startTime == null ? "" : startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getFinishTime() {
        return finishTime == null ? "" : finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public String getCreator() {
        return creator == null ? "" : creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifier() {
        return modifier == null ? "" : modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getModifyTime() {
        return modifyTime == null ? "" : modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getCreateTime() {
        return createTime == null ? "" : createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
