package com.staarlab.idoctor.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by PHAMHUNG on 12/26/2016.
 */

/**
 * Danh mục đường dùng
 */
@DatabaseTable(tableName = "MEDICINE_USE_FORM")
public class MedicineUserForm implements Parcelable, Serializable {
    public static final String FIELD_NAME_USE_FORM_NAME = "MEDICINE_USE_FORM_NAME";
    public static final String FIELD_NAME_USE_FORM_CODE = "MEDICINE_USE_FORM_CODE";

    @DatabaseField(columnName = "_id", id = true)
    public Long ID;
    @DatabaseField(columnName = "CREATE_TIME")
    public String CREATE_TIME;
    @DatabaseField(columnName = "MODIFY_TIME")
    public String MODIFY_TIME;
    @DatabaseField(columnName = "IS_ACTIVE")
    public int IS_ACTIVE;
    @DatabaseField(columnName = "IS_DELETE")
    public int IS_DELETE;
    @DatabaseField(columnName = "GROUP_CODE")
    public String GROUP_CODE;
    @DatabaseField(columnName = FIELD_NAME_USE_FORM_CODE)
    public String MEDICINE_USE_FORM_CODE;
    @DatabaseField(columnName = FIELD_NAME_USE_FORM_NAME)
    public String MEDICINE_USE_FORM_NAME;


    public MedicineUserForm() {
    }

    protected MedicineUserForm(Parcel in) {
        ID = in.readLong();
        CREATE_TIME = in.readString();
        MODIFY_TIME = in.readString();
        IS_ACTIVE = in.readInt();
        IS_DELETE = in.readInt();
        GROUP_CODE = in.readString();
        MEDICINE_USE_FORM_CODE = in.readString();
        MEDICINE_USE_FORM_NAME = in.readString();

    }

    public static final Creator<MedicineUserForm> CREATOR = new Creator<MedicineUserForm>() {
        @Override
        public MedicineUserForm createFromParcel(Parcel in) {
            return new MedicineUserForm(in);
        }

        @Override
        public MedicineUserForm[] newArray(int size) {
            return new MedicineUserForm[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(ID != null ? ID : 1);
        parcel.writeString(CREATE_TIME);
        parcel.writeString(MODIFY_TIME);
        parcel.writeInt(IS_ACTIVE);
        parcel.writeInt(IS_DELETE);
        parcel.writeString(GROUP_CODE);
        parcel.writeString(MEDICINE_USE_FORM_CODE);
        parcel.writeString(MEDICINE_USE_FORM_NAME);

    }
}
