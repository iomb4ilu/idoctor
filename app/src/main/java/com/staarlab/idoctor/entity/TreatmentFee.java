package com.staarlab.idoctor.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;


public class TreatmentFee implements Parcelable,Serializable {
    @SerializedName("TOTAL_PRICE")
    private BigDecimal totalPrice;

    @SerializedName("TOTAL_HEIN_PRICE")
    private BigDecimal totalHeinPrice;

    @SerializedName("TOTAL_PATIENT_PRICE")
    private BigDecimal totalPatientPrice;

    @SerializedName("TOTAL_DISCOUNT")
    private BigDecimal totalDiscount;

    @SerializedName("TOTAL_BILL_AMOUNT")
    private BigDecimal totalBillAmount;

    @SerializedName("TOTAL_BILL_TRANSFER_AMOUNT")
    private BigDecimal totalBillTransferAmount;

    @SerializedName("TOTAL_BILL_EXEMPTION")
    private BigDecimal totalBillExemption;

    @SerializedName("TOTAL_DEPOSIT_AMOUNT")
    private BigDecimal totalDepositAmount;

    @SerializedName("TOTAL_REPAY_AMOUNT")
    private BigDecimal totalRepayAmount;

    @SerializedName("TOTAL_WITHDRAW_AMOUNT")
    private BigDecimal totalWithdrawAmount;

    protected TreatmentFee(Parcel in) {
        String number = in.readString();
        totalPrice = number == null ? null : new BigDecimal(number);
        number = in.readString();
        totalHeinPrice = number == null ? null : new BigDecimal(number);
        number = in.readString();
        totalPatientPrice = number == null ? null : new BigDecimal(number);
        number = in.readString();
        totalDiscount = number == null ? null : new BigDecimal(number);
        number = in.readString();
        totalBillAmount = number == null ? null : new BigDecimal(number);
        number = in.readString();
        totalBillTransferAmount = number == null ? null : new BigDecimal(number);
        number = in.readString();
        totalBillExemption = number == null ? null : new BigDecimal(number);
        number = in.readString();
        totalDepositAmount = number == null ? null : new BigDecimal(number);
        number = in.readString();
        totalRepayAmount = number == null ? null : new BigDecimal(number);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(totalPrice == null ? null : totalPrice.toString());
        dest.writeString(totalHeinPrice == null ? null : totalHeinPrice.toString());
        dest.writeString(totalPatientPrice == null ? null : totalPatientPrice.toString());
        dest.writeString(totalDiscount == null ? null : totalDiscount.toString());
        dest.writeString(totalBillAmount == null ? null : totalBillAmount.toString());
        dest.writeString(totalBillTransferAmount == null ? null : totalBillTransferAmount.toString());
        dest.writeString(totalBillExemption == null ? null : totalBillExemption.toString());
        dest.writeString(totalDepositAmount == null ? null : totalDepositAmount.toString());
        dest.writeString(totalRepayAmount == null ? null : totalRepayAmount.toString());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TreatmentFee> CREATOR = new Creator<TreatmentFee>() {
        @Override
        public TreatmentFee createFromParcel(Parcel in) {
            return new TreatmentFee(in);
        }

        @Override
        public TreatmentFee[] newArray(int size) {
            return new TreatmentFee[size];
        }
    };

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getTotalHeinPrice() {
        return totalHeinPrice;
    }

    public void setTotalHeinPrice(BigDecimal totalHeinPrice) {
        this.totalHeinPrice = totalHeinPrice;
    }

    public BigDecimal getTotalPatientPrice() {
        return totalPatientPrice;
    }

    public void setTotalPatientPrice(BigDecimal totalPatientPrice) {
        this.totalPatientPrice = totalPatientPrice;
    }

    public BigDecimal getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(BigDecimal totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public BigDecimal getTotalBillAmount() {
        return totalBillAmount;
    }

    public void setTotalBillAmount(BigDecimal totalBillAmount) {
        this.totalBillAmount = totalBillAmount;
    }

    public BigDecimal getTotalBillTransferAmount() {
        return totalBillTransferAmount;
    }

    public void setTotalBillTransferAmount(BigDecimal totalBillTransferAmount) {
        this.totalBillTransferAmount = totalBillTransferAmount;
    }

    public BigDecimal getTotalBillExemption() {
        return totalBillExemption;
    }

    public void setTotalBillExemption(BigDecimal totalBillExemption) {
        this.totalBillExemption = totalBillExemption;
    }

    public BigDecimal getTotalDepositAmount() {
        return totalDepositAmount;
    }

    public void setTotalDipositAmount(BigDecimal totalDepositAmount) {
        this.totalDepositAmount = totalDepositAmount;
    }

    public BigDecimal getTotalRepayAmount() {
        return totalRepayAmount;
    }

    public void setTotalRepayAmount(BigDecimal totalRepayAmount) {
        this.totalRepayAmount = totalRepayAmount;
    }

    public void setTotalDepositAmount(BigDecimal totalDepositAmount) {
        this.totalDepositAmount = totalDepositAmount;
    }

    public BigDecimal getTotalWithdrawAmount() {
        return totalWithdrawAmount;
    }

    public void setTotalWithdrawAmount(BigDecimal totalWithdrawAmount) {
        this.totalWithdrawAmount = totalWithdrawAmount;
    }
}
