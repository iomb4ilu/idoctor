package com.staarlab.idoctor.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;

public class CommonServiceDetailEntity implements Serializable {
    @SerializedName("SERVICE_TYPE_CODE")
    private String serviceTypeCode;

    @SerializedName("SERVICE_TYPE_NAME")
    private String serviceTypeName;

    @SerializedName("SERVICE_CODE")
    private String serviceCode;

    @SerializedName("SERVICE_NAME")
    private String serviceName;

    @SerializedName("SERVICE_UNIT_CODE")
    private String serviceUnitCode;

    @SerializedName("SERVICE_UNIT_NAME")
    private String serviceUnitName;

    @SerializedName("INTRUCTION_TIME")
    private String intructionTime;

    @SerializedName("EXECUTE_LOGINNAME")
    private String executeLoginName;

    @SerializedName("EXECUTE_USERNAME")
    private String executeUserName;

    @SerializedName("REQUEST_LOGINNAME")
    private String requestLoginName;

    @SerializedName("REQUEST_USERNAME")
    private String requestUserName;

    @SerializedName("FINISH_TIME")
    private String finishTime;

    @SerializedName("EXECUTE_ROOM_CODE")
    private String executeRoomCode;

    @SerializedName("EXECUTE_ROOM_NAME")
    private String executeRoomName;

    @SerializedName("EXECUTE_DEPARTMENT_CODE")
    private String executeDepartmentCode;

    @SerializedName("EXECUTE_DEPARTMENT_NAME")
    private String executeDepartmentName;

    @SerializedName("REQUEST_ROOM_CODE")
    private String requestRoomCode;

    @SerializedName("REQUEST_ROOM_NAME")
    private String requestRoomName;

    @SerializedName("REQUEST_DEPARTMENT_CODE")
    private String requestDepartmentCode;

    @SerializedName("REQUEST_DEPARTMENT_NAME")
    private String requestDepartmentName;

    @SerializedName("CONCLUDE")
    private String conclude;

    @SerializedName("NOTE")
    private String note;

    @SerializedName("DESCRIPTION")
    private String description;

    @SerializedName("PATIENT_TYPE_CODE")
    private String patientTypeCode;

    @SerializedName("PATIENT_TYPE_NAME")
    private String patientTypeName;

    @SerializedName("AMOUNT")
    private BigDecimal amount;

    public String getServiceTypeCode() {
        return serviceTypeCode;
    }

    public void setServiceTypeCode(String serviceTypeCode) {
        this.serviceTypeCode = serviceTypeCode;
    }

    public String getServiceTypeName() {
        return serviceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        this.serviceTypeName = serviceTypeName;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceUnitCode() {
        return serviceUnitCode;
    }

    public void setServiceUnitCode(String serviceUnitCode) {
        this.serviceUnitCode = serviceUnitCode;
    }

    public String getServiceUnitName() {
        return serviceUnitName;
    }

    public void setServiceUnitName(String serviceUnitName) {
        this.serviceUnitName = serviceUnitName;
    }

    public String getIntructionTime() {
        return intructionTime;
    }

    public void setIntructionTime(String intructionTime) {
        this.intructionTime = intructionTime;
    }

    public String getExecuteLoginName() {
        return executeLoginName;
    }

    public void setExecuteLoginName(String executeLoginName) {
        this.executeLoginName = executeLoginName;
    }

    public String getExecuteUserName() {
        return executeUserName;
    }

    public void setExecuteUserName(String executeUserName) {
        this.executeUserName = executeUserName;
    }

    public String getRequestLoginName() {
        return requestLoginName;
    }

    public void setRequestLoginName(String requestLoginName) {
        this.requestLoginName = requestLoginName;
    }

    public String getRequestUserName() {
        return requestUserName;
    }

    public void setRequestUserName(String requestUserName) {
        this.requestUserName = requestUserName;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public String getExecuteRoomCode() {
        return executeRoomCode;
    }

    public void setExecuteRoomCode(String executeRoomCode) {
        this.executeRoomCode = executeRoomCode;
    }

    public String getExecuteRoomName() {
        return executeRoomName;
    }

    public void setExecuteRoomName(String executeRoomName) {
        this.executeRoomName = executeRoomName;
    }

    public String getExecuteDepartmentCode() {
        return executeDepartmentCode;
    }

    public void setExecuteDepartmentCode(String executeDepartmentCode) {
        this.executeDepartmentCode = executeDepartmentCode;
    }

    public String getExecuteDepartmentName() {
        return executeDepartmentName;
    }

    public void setExecuteDepartmentName(String executeDepartmentName) {
        this.executeDepartmentName = executeDepartmentName;
    }

    public String getRequestRoomCode() {
        return requestRoomCode;
    }

    public void setRequestRoomCode(String requestRoomCode) {
        this.requestRoomCode = requestRoomCode;
    }

    public String getRequestRoomName() {
        return requestRoomName;
    }

    public void setRequestRoomName(String requestRoomName) {
        this.requestRoomName = requestRoomName;
    }

    public String getRequestDepartmentCode() {
        return requestDepartmentCode;
    }

    public void setRequestDepartmentCode(String requestDepartmentCode) {
        this.requestDepartmentCode = requestDepartmentCode;
    }

    public String getRequestDepartmentName() {
        return requestDepartmentName;
    }

    public void setRequestDepartmentName(String requestDepartmentName) {
        this.requestDepartmentName = requestDepartmentName;
    }

    public String getConclude() {
        return conclude;
    }

    public void setConclude(String conclude) {
        this.conclude = conclude;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPatientTypeCode() {
        return patientTypeCode;
    }

    public void setPatientTypeCode(String patientTypeCode) {
        this.patientTypeCode = patientTypeCode;
    }

    public String getPatientTypeName() {
        return patientTypeName;
    }

    public void setPatientTypeName(String patientTypeName) {
        this.patientTypeName = patientTypeName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
