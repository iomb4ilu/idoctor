package com.staarlab.idoctor.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Author: Lucero
 * Created: 12/21/16.
 */

public class BedRoom implements Serializable{
    @SerializedName("ID")
    private long id;

    @SerializedName("BED_ROOM_CODE")
    private String bedRoomCode;

    @SerializedName("BED_ROOM_NAME")
    private String bedRoomName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBedRoomCode() {
        return bedRoomCode;
    }

    public void setBedRoomCode(String bedRoomCode) {
        this.bedRoomCode = bedRoomCode;
    }

    public String getBedRoomName() {
        return bedRoomName;
    }

    public void setBedRoomName(String bedRoomName) {
        this.bedRoomName = bedRoomName;
    }
}
