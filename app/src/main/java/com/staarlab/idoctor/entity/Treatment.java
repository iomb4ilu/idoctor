package com.staarlab.idoctor.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Treatment implements Serializable, Parcelable {
    @SerializedName("UnpaidAmount")
    private double unpaidAmount;

    @SerializedName("IsUnpaidWarning")
    private boolean isUnpaidWarning;

    @SerializedName("ID")
    private long id;

    @SerializedName("IS_ACTIVE")
    private int isActive;

    @SerializedName("ADD_TIME")
    private String addTime;

    @SerializedName("TREATMENT_ID")
    private long treatmentId;

    @SerializedName("TREATMENT_CODE")
    private String treatmentCode;

    @SerializedName("PATIENT_ID")
    private long patientId;

    @SerializedName("BED_NAME")
    private String bedName;

    @SerializedName("PATIENT_CODE")
    private String patientCode;

    @SerializedName("FIRST_NAME")
    private String firstName;

    @SerializedName("LAST_NAME")
    private String lastName;

    @SerializedName("VIR_ADDRESS")
    private String virAddress;

    @SerializedName("VIR_PATIENT_NAME")
    private String virPatientName;

    @SerializedName("DOB")
    private String dob;

    @SerializedName("GENDER_CODE")
    private String genderCode;

    @SerializedName("GENDER_NAME")
    private String genderName;

    private TreatmentSummaryInfo commonInfo;


    protected Treatment(Parcel in) {
        unpaidAmount = in.readDouble();
        isUnpaidWarning = in.readByte() != 0;
        id = in.readLong();
        isActive = in.readInt();
        addTime = in.readString();
        treatmentId = in.readLong();
        treatmentCode = in.readString();
        patientId = in.readLong();
        bedName = in.readString();
        patientCode = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        virAddress = in.readString();
        virPatientName = in.readString();
        dob = in.readString();
        genderCode = in.readString();
        genderName = in.readString();
        commonInfo = in.readParcelable(TreatmentSummaryInfo.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(unpaidAmount);
        dest.writeByte((byte) (isUnpaidWarning ? 1 : 0));
        dest.writeLong(id);
        dest.writeInt(isActive);
        dest.writeString(addTime);
        dest.writeLong(treatmentId);
        dest.writeString(treatmentCode);
        dest.writeLong(patientId);
        dest.writeString(bedName);
        dest.writeString(patientCode);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(virAddress);
        dest.writeString(virPatientName);
        dest.writeString(dob);
        dest.writeString(genderCode);
        dest.writeString(genderName);
        dest.writeParcelable(commonInfo, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Treatment> CREATOR = new Creator<Treatment>() {
        @Override
        public Treatment createFromParcel(Parcel in) {
            return new Treatment(in);
        }

        @Override
        public Treatment[] newArray(int size) {
            return new Treatment[size];
        }
    };

    public Double getUnpaidAmount() {
        return unpaidAmount;
    }

    public void setUnpaidAmount(Double unpaidAmount) {
        this.unpaidAmount = unpaidAmount;
    }

    public Boolean getUnpaidWarning() {
        return isUnpaidWarning;
    }

    public void setUnpaidWarning(Boolean unpaidWarning) {
        isUnpaidWarning = unpaidWarning;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getActive() {
        return isActive;
    }

    public void setActive(Integer active) {
        isActive = active;
    }

    public Long getTreatmentId() {
        return treatmentId;
    }

    public void setTreatmentId(Long treatmentId) {
        this.treatmentId = treatmentId;
    }

    public String getTreatmentCode() {
        return treatmentCode;
    }

    public void setTreatmentCode(String treatmentCode) {
        this.treatmentCode = treatmentCode;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public String getBedName() {
        return bedName;
    }

    public void setBedName(String bedName) {
        this.bedName = bedName;
    }

    public String getPatientCode() {
        return patientCode;
    }

    public void setPatientCode(String patientCode) {
        this.patientCode = patientCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getVirAddress() {
        return virAddress;
    }

    public void setVirAddress(String virAddress) {
        this.virAddress = virAddress;
    }

    public String getVirPatientName() {
        return virPatientName;
    }

    public void setVirPatientName(String virPatientName) {
        this.virPatientName = virPatientName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGenderCode() {
        return genderCode;
    }

    public void setGenderCode(String genderCode) {
        this.genderCode = genderCode;
    }

    public String getGenderName() {
        return genderName;
    }

    public void setGenderName(String genderName) {
        this.genderName = genderName;
    }

    public TreatmentSummaryInfo getCommonInfo() {
        return commonInfo;
    }

    public void setCommonInfo(TreatmentSummaryInfo commonInfo) {
        this.commonInfo = commonInfo;
    }

    public String getAddTime() {
        return addTime;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }


}
