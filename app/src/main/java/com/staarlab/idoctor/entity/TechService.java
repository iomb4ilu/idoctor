package com.staarlab.idoctor.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by Lucero on 12/4/2016.
 * Dịch vụ kỹ thuật
 */
@DatabaseTable(tableName = "TECH_SERVICE")
public class TechService implements Parcelable,Serializable {

    public static final String FIELD_NAME_SERVICE_TYPE_ID = "SERVICE_TYPE_ID";
    public static final String FIELD_NAME_SERVICE_TYPE_CODE = "SERVICE_TYPE_CODE";
    public static final String FIELD_NAME_SERVICE_CONCRETE_ID = "CONCRETE_ID";
    public static final String FIELD_NAME_SERVICE_PARENT_ID = "PARENT_ID";
    public static final String FIELD_NAME_SERVICE_NUM_ORDER = "NUM_ORDER";


    @DatabaseField(columnName = "_id", id = true)
    public long ID;
    @DatabaseField(columnName = "CREATE_TIME")
    public String CREATE_TIME;
    @DatabaseField(columnName = "MODIFY_TIME")
    public String MODIFY_TIME;
    @DatabaseField(columnName = "IS_ACTIVE")
    public int IS_ACTIVE;
    @DatabaseField(columnName = "IS_DELETE")
    public int IS_DELETE;
    @DatabaseField(columnName = "GROUP_CODE")
    public String GROUP_CODE;
    @DatabaseField(columnName = FIELD_NAME_SERVICE_TYPE_CODE)
    public String SERVICE_TYPE_CODE;
    @DatabaseField(columnName = "SERVICE_TYPE_NAME")
    public String SERVICE_TYPE_NAME;
    @DatabaseField(columnName = "SERVICE_UNIT_CODE")
    public String SERVICE_UNIT_CODE;
    @DatabaseField(columnName = "SERVICE_UNIT_NAME")
    public String SERVICE_UNIT_NAME;
    @DatabaseField(columnName = "SERVICE_REPORT_CODE")
    public String SERVICE_REPORT_CODE;
    @DatabaseField(columnName = "SERVICE_REPORT_NAME")
    public String SERVICE_REPORT_NAME;
    @DatabaseField(columnName = "HEIN_SERVICE_BHYT_CODE")
    public String HEIN_SERVICE_BHYT_CODE;
    @DatabaseField(columnName = "HEIN_SERVICE_BHYT_NAME")
    public String HEIN_SERVICE_BHYT_NAME;
    @DatabaseField(columnName = "SERVICE_CODE")
    public String SERVICE_CODE;
    @DatabaseField(columnName = "SERVICE_NAME")
    public String SERVICE_NAME;
    @DatabaseField(columnName = "SERVICE_SEARCH")
    public String SERVICE_SEARCH;

    // INT
    @DatabaseField(columnName = FIELD_NAME_SERVICE_PARENT_ID)
    public int PARENT_ID;
    @DatabaseField(columnName = FIELD_NAME_SERVICE_CONCRETE_ID)
    public int CONCRETE_ID;
    @DatabaseField(columnName = "IS_LEAF")
    public int IS_LEAF;
    @DatabaseField(columnName = FIELD_NAME_SERVICE_NUM_ORDER)
    public int NUM_ORDER;
    @DatabaseField(columnName = FIELD_NAME_SERVICE_TYPE_ID)
    public long SERVICE_TYPE_ID;
    @DatabaseField(columnName = "SERVICE_UNIT_ID")
    public int SERVICE_UNIT_ID;
    @DatabaseField(columnName = "SERVICE_REPORT_ID")
    public int SERVICE_REPORT_ID;
    @DatabaseField(columnName = "HEIN_SERVICE_BHYT_ID")
    public int HEIN_SERVICE_BHYT_ID;

    //Constructor
    public TechService() {
    }

    protected TechService(Parcel in) {
        ID = in.readLong();
        CREATE_TIME = in.readString();
        MODIFY_TIME = in.readString();
        IS_ACTIVE = in.readInt();
        IS_DELETE = in.readInt();
        GROUP_CODE = in.readString();
        SERVICE_TYPE_CODE = in.readString();
        SERVICE_TYPE_NAME = in.readString();
        SERVICE_UNIT_CODE = in.readString();
        SERVICE_UNIT_NAME = in.readString();
        SERVICE_REPORT_CODE = in.readString();
        SERVICE_REPORT_NAME = in.readString();
        HEIN_SERVICE_BHYT_CODE = in.readString();
        HEIN_SERVICE_BHYT_NAME = in.readString();
        SERVICE_CODE = in.readString();
        SERVICE_NAME = in.readString();
        SERVICE_SEARCH = in.readString();
        PARENT_ID = in.readInt();
        CONCRETE_ID = in.readInt();
        IS_LEAF = in.readInt();
        NUM_ORDER = in.readInt();
        SERVICE_TYPE_ID = in.readLong();
        SERVICE_UNIT_ID = in.readInt();
        SERVICE_REPORT_ID = in.readInt();
        HEIN_SERVICE_BHYT_ID = in.readInt();
    }

    public static final Creator<TechService> CREATOR = new Creator<TechService>() {
        @Override
        public TechService createFromParcel(Parcel in) {
            return new TechService(in);
        }

        @Override
        public TechService[] newArray(int size) {
            return new TechService[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(ID);
        parcel.writeString(CREATE_TIME);
        parcel.writeString(MODIFY_TIME);
        parcel.writeInt(IS_ACTIVE);
        parcel.writeInt(IS_DELETE);
        parcel.writeString(GROUP_CODE);
        parcel.writeString(SERVICE_TYPE_CODE);
        parcel.writeString(SERVICE_TYPE_NAME);
        parcel.writeString(SERVICE_UNIT_CODE);
        parcel.writeString(SERVICE_UNIT_NAME);
        parcel.writeString(SERVICE_REPORT_CODE);
        parcel.writeString(SERVICE_REPORT_NAME);
        parcel.writeString(HEIN_SERVICE_BHYT_CODE);
        parcel.writeString(HEIN_SERVICE_BHYT_NAME);
        parcel.writeString(SERVICE_CODE);
        parcel.writeString(SERVICE_NAME);
        parcel.writeString(SERVICE_SEARCH);
        parcel.writeInt(PARENT_ID);
        parcel.writeInt(CONCRETE_ID);
        parcel.writeInt(IS_LEAF);
        parcel.writeInt(NUM_ORDER);
        parcel.writeLong(SERVICE_TYPE_ID);
        parcel.writeInt(SERVICE_UNIT_ID);
        parcel.writeInt(SERVICE_REPORT_ID);
        parcel.writeInt(HEIN_SERVICE_BHYT_ID);
    }
}
