package com.staarlab.idoctor.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Author: Lucero
 * Created: 12/29/16.
 */

public class Prescription implements Parcelable, Serializable {
    @SerializedName("ID")
    private Long id;

    @SerializedName("SERVICE_REQ_ID")
    private Long serviceReqId;

    @SerializedName("SERVICE_REQ_CODE")
    private String serviceReqCode;

    @SerializedName("SERVICE_REQ_STT_ID")
    private Long serviceReqSttId;

    @SerializedName("SERVICE_REQ_TYPE_ID")
    private Long serviceReqTypeId;

    @SerializedName("EXECUTE_ROOM_ID")
    private Long executeRoomId;

    @SerializedName("EXECUTE_DEPARTMENT_ID")
    private Long executeDepartmentId;

    @SerializedName("SERVICE_REQ_STT_CODE")
    private String serviceReqSttCode;

    @SerializedName("SERVICE_REQ_STT_NAME")
    private String serviceReqSttName;

    @SerializedName("SERVICE_REQ_TYPE_CODE")
    private String serviceReqTypeCode;

    @SerializedName("SERVICE_REQ_TYPE_NAME")
    private String serviceReqTypeName;

    @SerializedName("TOTAL_BILL_AMOUNT")
    private BigDecimal totalBillAmount;

    @SerializedName("TOTAL_DEPOSIT_AMOUNT")
    private BigDecimal totalDepositAmount;

    @SerializedName("TOTAL_REPAY_AMOUNT")
    private BigDecimal totalRepayAmount;

    @SerializedName("TOTAL_HEIN_PRICE")
    private BigDecimal totalHeinPrice;

    @SerializedName("TOTAL_PATIENT_PRICE")
    private BigDecimal totalPatientPrice;

    @SerializedName("TOTAL_PRICE")
    private BigDecimal totalPrice;

    @SerializedName("MEDICINE_USE_FORM_CODE")
    private String icdCode;

    @SerializedName("MEDICINE_USE_FORM_NAME")
    private String icdName;

    @SerializedName("EXP_MEST_TYPE_CODE")
    private String expMestTypeCode;

    @SerializedName("EXP_MEST_ID")
    private Long expMestId;

    @SerializedName("EXP_MEST_CODE")
    private String expMestCode;

    @SerializedName("EXP_MEST_TYPE_NAME")
    private String expMestTypeName;

    @SerializedName("EXP_MEST_STT_CODE")
    private String expMestSttCode;

    @SerializedName("EXP_MEST_STT_NAME")
    private String expMestSttName;

    @SerializedName("INTRUCTION_TIME")
    private String intructionTime;

    @SerializedName("REQUEST_DEPARTMENT_ID")
    private Long requestDepartmentId;

    @SerializedName("REQUEST_LOGINNAME")
    private String requestLoginName;

    @SerializedName("REQUEST_USERNAME")
    private String requestUserName;

    @SerializedName("REQUEST_ROOM_CODE")
    private String requestRoomCode;

    @SerializedName("REQUEST_ROOM_NAME")
    private String requestRoomName;

    @SerializedName("USE_TIME")
    private String useTime;

    @SerializedName("ICD_MAIN_TEXT")
    private String icdMainText;

    @SerializedName("EXECUTE_ROOM_NAME")
    private String executeRoomName;


    protected Prescription(Parcel in) {
        serviceReqCode = in.readString();
        serviceReqSttCode = in.readString();
        serviceReqSttName = in.readString();
        serviceReqTypeCode = in.readString();
        serviceReqTypeName = in.readString();
        icdCode = in.readString();
        icdName = in.readString();
        expMestTypeCode = in.readString();
        expMestCode = in.readString();
        expMestTypeName = in.readString();
        expMestSttCode = in.readString();
        expMestSttName = in.readString();
        intructionTime = in.readString();
        requestLoginName = in.readString();
        requestUserName = in.readString();
        requestRoomCode = in.readString();
        requestRoomName = in.readString();
        useTime = in.readString();
        icdMainText = in.readString();
        executeRoomName = in.readString();
    }

    public static final Creator<Prescription> CREATOR = new Creator<Prescription>() {
        @Override
        public Prescription createFromParcel(Parcel in) {
            return new Prescription(in);
        }

        @Override
        public Prescription[] newArray(int size) {
            return new Prescription[size];
        }
    };

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getServiceReqId() {
        return serviceReqId;
    }

    public void setServiceReqId(Long serviceReqId) {
        this.serviceReqId = serviceReqId;
    }

    public String getServiceReqCode() {
        return serviceReqCode;
    }

    public void setServiceReqCode(String serviceReqCode) {
        this.serviceReqCode = serviceReqCode;
    }

    public Long getServiceReqSttId() {
        return serviceReqSttId;
    }

    public void setServiceReqSttId(Long serviceReqSttId) {
        this.serviceReqSttId = serviceReqSttId;
    }

    public Long getServiceReqTypeId() {
        return serviceReqTypeId;
    }

    public void setServiceReqTypeId(Long serviceReqTypeId) {
        this.serviceReqTypeId = serviceReqTypeId;
    }

    public Long getExecuteRoomId() {
        return executeRoomId;
    }

    public void setExecuteRoomId(Long executeRoomId) {
        this.executeRoomId = executeRoomId;
    }

    public Long getExecuteDepartmentId() {
        return executeDepartmentId;
    }

    public void setExecuteDepartmentId(Long executeDepartmentId) {
        this.executeDepartmentId = executeDepartmentId;
    }

    public String getServiceReqSttCode() {
        return serviceReqSttCode;
    }

    public void setServiceReqSttCode(String serviceReqSttCode) {
        this.serviceReqSttCode = serviceReqSttCode;
    }

    public String getServiceReqSttName() {
        return serviceReqSttName;
    }

    public void setServiceReqSttName(String serviceReqSttName) {
        this.serviceReqSttName = serviceReqSttName;
    }

    public String getServiceReqTypeCode() {
        return serviceReqTypeCode;
    }

    public void setServiceReqTypeCode(String serviceReqTypeCode) {
        this.serviceReqTypeCode = serviceReqTypeCode;
    }

    public String getServiceReqTypeName() {
        return serviceReqTypeName;
    }

    public void setServiceReqTypeName(String serviceReqTypeName) {
        this.serviceReqTypeName = serviceReqTypeName;
    }

    public BigDecimal getTotalBillAmount() {
        return totalBillAmount;
    }

    public void setTotalBillAmount(BigDecimal totalBillAmount) {
        this.totalBillAmount = totalBillAmount;
    }

    public BigDecimal getTotalDepositAmount() {
        return totalDepositAmount;
    }

    public void setTotalDepositAmount(BigDecimal totalDepositAmount) {
        this.totalDepositAmount = totalDepositAmount;
    }

    public BigDecimal getTotalRepayAmount() {
        return totalRepayAmount;
    }

    public void setTotalRepayAmount(BigDecimal totalRepayAmount) {
        this.totalRepayAmount = totalRepayAmount;
    }

    public BigDecimal getTotalHeinPrice() {
        return totalHeinPrice;
    }

    public void setTotalHeinPrice(BigDecimal totalHeinPrice) {
        this.totalHeinPrice = totalHeinPrice;
    }

    public BigDecimal getTotalPatientPrice() {
        return totalPatientPrice;
    }

    public void setTotalPatientPrice(BigDecimal totalPatientPrice) {
        this.totalPatientPrice = totalPatientPrice;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getIcdCode() {
        return icdCode;
    }

    public void setIcdCode(String icdCode) {
        this.icdCode = icdCode;
    }

    public String getIcdName() {
        return icdName;
    }

    public void setIcdName(String icdName) {
        this.icdName = icdName;
    }

    public String getExpMestTypeCode() {
        return expMestTypeCode;
    }

    public void setExpMestTypeCode(String expMestTypeCode) {
        this.expMestTypeCode = expMestTypeCode;
    }

    public String getExpMestTypeName() {
        return expMestTypeName;
    }

    public void setExpMestTypeName(String expMestTypeName) {
        this.expMestTypeName = expMestTypeName;
    }

    public String getExpMestSttCode() {
        return expMestSttCode;
    }

    public void setExpMestSttCode(String expMestSttCode) {
        this.expMestSttCode = expMestSttCode;
    }

    public String getExpMestSttName() {
        return expMestSttName;
    }

    public void setExpMestSttName(String expMestSttName) {
        this.expMestSttName = expMestSttName;
    }

    public String getIntructionTime() {
        return intructionTime;
    }

    public void setIntructionTime(String intructionTime) {
        this.intructionTime = intructionTime;
    }

    public Long getRequestDepartmentId() {
        return requestDepartmentId;
    }

    public void setRequestDepartmentId(Long requestDepartmentId) {
        this.requestDepartmentId = requestDepartmentId;
    }

    public String getRequestLoginName() {
        return requestLoginName;
    }

    public void setRequestLoginName(String requestLoginName) {
        this.requestLoginName = requestLoginName;
    }

    public String getRequestUserName() {
        return requestUserName;
    }

    public void setRequestUserName(String requestUserName) {
        this.requestUserName = requestUserName;
    }

    public String getRequestRoomCode() {
        return requestRoomCode;
    }

    public void setRequestRoomCode(String requestRoomCode) {
        this.requestRoomCode = requestRoomCode;
    }

    public String getRequestRoomName() {
        return requestRoomName;
    }

    public void setRequestRoomName(String requestRoomName) {
        this.requestRoomName = requestRoomName;
    }

    public String getUseTime() {
        return useTime;
    }

    public void setUseTime(String useTime) {
        this.useTime = useTime;
    }

    public Long getExpMestId() {
        return expMestId;
    }

    public String getExpMestCode() {
        return expMestCode;
    }

    public String getIcdMainText() {
        return icdMainText;
    }

    public String getExecuteRoomName() {
        return executeRoomName;
    }

    public void setExecuteRoomName(String executeRoomName) {
        this.executeRoomName = executeRoomName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(serviceReqCode);
        dest.writeString(serviceReqSttCode);
        dest.writeString(serviceReqSttName);
        dest.writeString(serviceReqTypeCode);
        dest.writeString(serviceReqTypeName);
        dest.writeString(icdCode);
        dest.writeString(icdName);
        dest.writeString(expMestTypeCode);
        dest.writeString(expMestCode);
        dest.writeString(expMestTypeName);
        dest.writeString(expMestSttCode);
        dest.writeString(expMestSttName);
        dest.writeString(intructionTime);
        dest.writeString(requestLoginName);
        dest.writeString(requestUserName);
        dest.writeString(requestRoomCode);
        dest.writeString(requestRoomName);
        dest.writeString(useTime);
        dest.writeString(icdMainText);
        dest.writeString(executeRoomName);
    }
}
