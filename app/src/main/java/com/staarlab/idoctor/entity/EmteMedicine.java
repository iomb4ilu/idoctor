package com.staarlab.idoctor.entity;

/**
 * Created by PHAMHUNG on 3/7/2017.
 */

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * {"ID":1824,"CREATE_TIME":20170224154951,"MODIFY_TIME":20170224154951,"CREATOR":"simnt","MODIFIER":"simnt","APP_CREATOR":"MOS","APP_MODIFIER":"MOS","IS_ACTIVE":1,
 * "IS_DELETE":0,"GROUP_CODE":null,
 * "EXP_MEST_TEMPLATE_ID":1743,"MEDICINE_TYPE_ID":1235,"AMOUNT":8.0,"TUTORIAL":null,"EXP_MEST_TEMPLATE_CODE":"m2017","EXP_MEST_TEMPLATE_NAME":"m2017","MEDICINE_TYPE_CODE":"VP060","MEDICINE_TYPE_NAME":"Vitamin B comlex",
 * "SERVICE_ID":2535,"MEDICINE_USE_FORM_ID":null,"SERVICE_UNIT_ID":31,"SERVICE_UNIT_NAME":"Ống","SERVICE_UNIT_CODE":"10","MEDICINE_USE_FORM_CODE":null,"MEDICINE_USE_FORM_NAME":null}
 */
@DatabaseTable(tableName = "EMTE_MEDICINE")
public class EmteMedicine implements Serializable {
    public static final String TABLE_NAME_MEST_TEMPLATE_ID = "EXP_MEST_TEMPLATE_ID";

    @DatabaseField(columnName = "_id", id = true)
    public long ID;
    @DatabaseField(columnName = TABLE_NAME_MEST_TEMPLATE_ID)
    public long EXP_MEST_TEMPLATE_ID;
    @DatabaseField(columnName = "AMOUNT")
    public double AMOUNT;
    @DatabaseField(columnName = "TUTORIAL")
    public String TUTORIAL;
    @DatabaseField(columnName = "MEDICINE_TYPE_CODE")
    public String MEDICINE_TYPE_CODE;
    @DatabaseField(columnName = "MEDICINE_TYPE_NAME")
    public String MEDICINE_TYPE_NAME;
    @DatabaseField(columnName = "MEDICINE_TYPE_ID")
    public Long MEDICINE_TYPE_ID;
    @DatabaseField(columnName = "SERVICE_ID")
    public long SERVICE_ID;
    @DatabaseField(columnName = "MEDICINE_USE_FORM_ID")
    public Long MEDICINE_USE_FORM_ID;
    @DatabaseField(columnName = "MEDICINE_USE_FORM_CODE")
    public String MEDICINE_USE_FORM_CODE;
    @DatabaseField(columnName = "MEDICINE_USE_FORM_NAME")
    public String MEDICINE_USE_FORM_NAME;
    @DatabaseField(columnName = "SERVICE_UNIT_ID")
    public long SERVICE_UNIT_ID;
    @DatabaseField(columnName = "SERVICE_UNIT_NAME")
    public String SERVICE_UNIT_NAME;
}
