package com.staarlab.idoctor.entity;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

/**
 * Created on 3/12/17.
 */

public class DhstServiceEntity extends CommonServiceEntity {
    @SerializedName("CREATOR")
    private String creator;

    @SerializedName("MODIFIER")
    private String modifier;

    @SerializedName("DHST_CODE")
    private String dhstCode;

    @SerializedName("PULSE")
    private BigDecimal pulse;

    @SerializedName("TEMPERATURE")
    private BigDecimal temperature;

    @SerializedName("BLOOD_PRESSURE_MAX")
    private BigDecimal bloodPressureMax;

    @SerializedName("BLOOD_PRESSURE_MIN")
    private BigDecimal bloodPressureMin;

    @SerializedName("BREATH_RATE")
    private BigDecimal breathRate;

    @SerializedName("WEIGHT")
    private BigDecimal weight;

    @SerializedName("HEIGHT")
    private BigDecimal height;

    @SerializedName("VIR_BMI")
    private BigDecimal virBmi;

    @SerializedName("VIR_BODY_SURFACE_AREA")
    private BigDecimal virBodySurfaceArea;

    @SerializedName("MODIFY_TIME")
    private String modifyTime;

    @SerializedName("CREATE_TIME")
    private String createTime;

    public String getDhstCode() {
        return dhstCode;
    }

    public void setDhstCode(String dhstCode) {
        this.dhstCode = dhstCode;
    }

    public BigDecimal getPulse() {
        return pulse;
    }

    public void setPulse(BigDecimal pulse) {
        this.pulse = pulse;
    }

    public BigDecimal getTemperature() {
        return temperature;
    }

    public void setTemperature(BigDecimal temperature) {
        this.temperature = temperature;
    }

    public BigDecimal getBloodPressureMax() {
        return bloodPressureMax;
    }

    public void setBloodPressureMax(BigDecimal bloodPressureMax) {
        this.bloodPressureMax = bloodPressureMax;
    }

    public BigDecimal getBloodPressureMin() {
        return bloodPressureMin;
    }

    public void setBloodPressureMin(BigDecimal bloodPressureMin) {
        this.bloodPressureMin = bloodPressureMin;
    }

    public BigDecimal getBreathRate() {
        return breathRate;
    }

    public void setBreathRate(BigDecimal breathRate) {
        this.breathRate = breathRate;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public BigDecimal getHeight() {
        return height;
    }

    public void setHeight(BigDecimal height) {
        this.height = height;
    }

    public BigDecimal getVirBmi() {
        return virBmi;
    }

    public void setVirBmi(BigDecimal virBmi) {
        this.virBmi = virBmi;
    }

    public BigDecimal getVirBodySurfaceArea() {
        return virBodySurfaceArea;
    }

    public void setVirBodySurfaceArea(BigDecimal virBodySurfaceArea) {
        this.virBodySurfaceArea = virBodySurfaceArea;
    }

    public String getCreateTime() {
        return createTime == null ? "" : createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreator() {
        return creator == null ? "" : creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModifier() {
        return modifier == null ? "" : modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getModifyTime() {
        return modifyTime == null ? "" : modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }
}
