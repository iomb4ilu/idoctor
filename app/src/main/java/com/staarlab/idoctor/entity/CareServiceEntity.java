package com.staarlab.idoctor.entity;


import com.google.gson.annotations.SerializedName;

public class CareServiceEntity extends CommonServiceEntity {
    @SerializedName("CREATE_TIME")
    private String createTime;

    @SerializedName("CARE_CODE")
    private String careCode;

    @SerializedName("MUCOCUTANEOUS")
    private String mucocutaneous;

    @SerializedName("URINE")
    private String urine;

    @SerializedName("DEJECTA")
    private String dejecta;

    @SerializedName("NUTRITION")
    private String nutrition;

    @SerializedName("SANITARY")
    private String sanitary;

    @SerializedName("TUTORIAL")
    private String tutorial;

    @SerializedName("EDUCATION")
    private String education;

    @SerializedName("HAS_ADD_MEDICINE")
    private Integer hasAddMedicine;

    @SerializedName("HAS_MEDICINE")
    private Integer hasMedicine;

    @SerializedName("HAS_TEST")
    private Integer hasTest;

    public String getCareCode() {
        return careCode;
    }

    public void setCareCode(String careCode) {
        this.careCode = careCode;
    }

    public String getMucocutaneous() {
        return mucocutaneous == null ? "" : mucocutaneous;
    }

    public void setMucocutaneous(String mucocutaneous) {
        this.mucocutaneous = mucocutaneous;
    }

    public String getUrine() {
        return urine == null ? "" : urine;
    }

    public void setUrine(String urine) {
        this.urine = urine;
    }

    public String getDejecta() {
        return dejecta == null ? "" : dejecta;
    }

    public void setDejecta(String dejecta) {
        this.dejecta = dejecta;
    }

    public String getNutrition() {
        return nutrition == null ? "" : nutrition;
    }

    public void setNutrition(String nutrition) {
        this.nutrition = nutrition;
    }

    public String getSanitary() {
        return sanitary == null ? "" : sanitary;
    }

    public void setSanitary(String sanitary) {
        this.sanitary = sanitary;
    }

    public String getTutorial() {
        return tutorial == null ? "" : tutorial;
    }

    public void setTutorial(String tutorial) {
        this.tutorial = tutorial;
    }

    public String getEducation() {
        return education == null ? "" : education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public Integer getHasAddMedicine() {
        return hasAddMedicine;
    }

    public void setHasAddMedicine(Integer hasAddMedicine) {
        this.hasAddMedicine = hasAddMedicine;
    }

    public Integer getHasMedicine() {
        return hasMedicine;
    }

    public void setHasMedicine(Integer hasMedicine) {
        this.hasMedicine = hasMedicine;
    }

    public Integer getHasTest() {
        return hasTest;
    }

    public void setHasTest(Integer hasTest) {
        this.hasTest = hasTest;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
}
