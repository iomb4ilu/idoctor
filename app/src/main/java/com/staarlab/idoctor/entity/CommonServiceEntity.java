package com.staarlab.idoctor.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CommonServiceEntity implements Serializable {
    @SerializedName("SERVICE_REQ_ID")
    private Long serviceReqId;

    @SerializedName("SERVICE_REQ_CODE")
    private String serviceReqCode;

    @SerializedName("INTRUCTION_TIME")
    private String intructionTime;

    @SerializedName("EXECUTE_ROOM_NAME")
    private String executeRoomName;

    @SerializedName("EXECUTE_LOGINNAME")
    private String executeLoginname;

    @SerializedName("EXECUTE_USERNAME")
    private String executeUsername;

    @SerializedName("REQUEST_ROOM_NAME")
    private String requestRoomName;

    @SerializedName("REQUEST_LOGINNAME")
    private String requestLoginname;

    @SerializedName("REQUEST_USERNAME")
    private String requestUsername;

    @SerializedName("EXECUTE_TIME")
    private String executeTime;

    public String getServiceReqCode() {
        return serviceReqCode;
    }

    public void setServiceReqCode(String serviceReqCode) {
        this.serviceReqCode = serviceReqCode;
    }

    public String getIntructionTime() {
        return intructionTime;
    }

    public void setIntructionTime(String intructionTime) {
        this.intructionTime = intructionTime;
    }

    public String getExecuteRoomName() {
        return executeRoomName;
    }

    public void setExecuteRoomName(String executeRoomName) {
        this.executeRoomName = executeRoomName;
    }

    public String getExecuteLoginname() {
        return executeLoginname == null ? "" : executeLoginname;
    }

    public void setExecuteLoginname(String executeLoginname) {
        this.executeLoginname = executeLoginname;
    }

    public String getExecuteUsername() {
        return executeUsername == null ? "" : executeUsername;
    }

    public void setExecuteUsername(String executeUsername) {
        this.executeUsername = executeUsername;
    }

    public String getRequestRoomName() {
        return requestRoomName;
    }

    public void setRequestRoomName(String requestRoomName) {
        this.requestRoomName = requestRoomName;
    }

    public String getRequestUsername() {
        return requestUsername == null ? "" : requestUsername;
    }

    public void setRequestUsername(String requestUsername) {
        this.requestUsername = requestUsername;
    }

    public Long getServiceReqId() {
        return serviceReqId;
    }

    public void setServiceReqId(Long serviceReqId) {
        this.serviceReqId = serviceReqId;
    }

    public String getRequestLoginname() {
        return requestLoginname == null ? "" : requestLoginname;
    }

    public void setRequestLoginname(String requestLoginname) {
        this.requestLoginname = requestLoginname;
    }

    public String getExecuteTime() {
        return executeTime == null ? "" : executeTime;
    }

    public void setExecuteTime(String executeTime) {
        this.executeTime = executeTime;
    }

}
