package com.staarlab.idoctor.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by PhamHung on 1/3/17.
 * Cau hinh nhom dich vu - dich vu
 */
@DatabaseTable(tableName = "SERVICE_SERG")
public class ServiceSegr implements Parcelable{
    public static final String FIELD_NAME_SERVICE_ID = "SERVICE_ID";
    public static final String FIELD_NAME_SERVICE_GROUP_ID = "SERVICE_GROUP_ID";

    @DatabaseField(columnName = "_id", id = true)
    public long ID;
    @DatabaseField(columnName = "CREATE_TIME")
    public String CREATE_TIME;
    @DatabaseField(columnName = "MODIFY_TIME")
    public String MODIFY_TIME;
    @DatabaseField(columnName = "IS_ACTIVE")
    public int IS_ACTIVE;
    @DatabaseField(columnName = "IS_DELETE")
    public int IS_DELETE;
    @DatabaseField(columnName = "GROUP_CODE")
    public String GROUP_CODE;
    @DatabaseField(columnName = FIELD_NAME_SERVICE_GROUP_ID)
    public long SERVICE_GROUP_ID;
    @DatabaseField(columnName = FIELD_NAME_SERVICE_ID)
    public long SERVICE_ID;
    @DatabaseField(columnName = "AMOUNT")
    public long AMOUNT;
    @DatabaseField(columnName = "IS_EXPEND")
    public long IS_EXPEND;
    @DatabaseField(columnName = "HIS_SERVICE_GROUP")
    public long HIS_SERVICE_GROUP;
    @DatabaseField(columnName = "HIS_SERVICE")
    public long HIS_SERVICE;

    public ServiceSegr(){}

    protected ServiceSegr(Parcel in) {
        ID = in.readLong();
        CREATE_TIME = in.readString();
        MODIFY_TIME = in.readString();
        IS_ACTIVE = in.readInt();
        IS_DELETE = in.readInt();
        GROUP_CODE = in.readString();
        SERVICE_GROUP_ID = in.readLong();
        SERVICE_ID = in.readLong();
        AMOUNT = in.readLong();
        IS_EXPEND = in.readLong();
        HIS_SERVICE_GROUP = in.readLong();
        HIS_SERVICE = in.readLong();
    }

    public static final Creator<ServiceSegr> CREATOR = new Creator<ServiceSegr>() {
        @Override
        public ServiceSegr createFromParcel(Parcel in) {
            return new ServiceSegr(in);
        }

        @Override
        public ServiceSegr[] newArray(int size) {
            return new ServiceSegr[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(ID);
        parcel.writeString(CREATE_TIME);
        parcel.writeString(MODIFY_TIME);
        parcel.writeInt(IS_ACTIVE);
        parcel.writeInt(IS_DELETE);
        parcel.writeString(GROUP_CODE);
        parcel.writeLong(SERVICE_GROUP_ID);
        parcel.writeLong(SERVICE_ID);
        parcel.writeLong(AMOUNT);
        parcel.writeLong(IS_EXPEND);
        parcel.writeLong(HIS_SERVICE_GROUP);
        parcel.writeLong(HIS_SERVICE);
    }
}
