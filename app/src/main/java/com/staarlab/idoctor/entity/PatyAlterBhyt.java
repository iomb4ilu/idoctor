package com.staarlab.idoctor.entity;

/**
 * Created by PHAMHUNG on 1/9/2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Thông tin đặc thù của đối tượng BHYT
 */
public class PatyAlterBhyt implements Serializable, Parcelable {
    public long ID;
    public String HEIN_MEDI_ORG_CODE;
    public String HEIN_MEDI_ORG_NAME;
    public String HEIN_CARD_NUMBER;
    public String ADDRESS;

    public long HEIN_CARD_FROM_TIME;
    public long HEIN_CARD_TO_TIME;
    public long PATIENT_TYPE_ID;
    public long TREATMENT_TYPE_ID;
    public String PATIENT_CODE;
    public String VIR_PATIENT_NAME;
    public String GENDER_CODE;
    public String GENDER_NAME;

    protected PatyAlterBhyt(Parcel in) {
        ID = in.readLong();
        HEIN_MEDI_ORG_CODE = in.readString();
        HEIN_MEDI_ORG_NAME = in.readString();
        HEIN_CARD_NUMBER = in.readString();
        ADDRESS = in.readString();
        HEIN_CARD_FROM_TIME = in.readLong();
        HEIN_CARD_TO_TIME = in.readLong();
        PATIENT_TYPE_ID = in.readLong();
        TREATMENT_TYPE_ID = in.readLong();
        PATIENT_CODE = in.readString();
        VIR_PATIENT_NAME = in.readString();
        GENDER_CODE = in.readString();
        GENDER_NAME = in.readString();
    }

    public static final Creator<PatyAlterBhyt> CREATOR = new Creator<PatyAlterBhyt>() {
        @Override
        public PatyAlterBhyt createFromParcel(Parcel in) {
            return new PatyAlterBhyt(in);
        }

        @Override
        public PatyAlterBhyt[] newArray(int size) {
            return new PatyAlterBhyt[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(ID);
        dest.writeString(HEIN_MEDI_ORG_CODE);
        dest.writeString(HEIN_MEDI_ORG_NAME);
        dest.writeString(HEIN_CARD_NUMBER);
        dest.writeString(ADDRESS);
        dest.writeLong(HEIN_CARD_FROM_TIME);
        dest.writeLong(HEIN_CARD_TO_TIME);
        dest.writeLong(PATIENT_TYPE_ID);
        dest.writeLong(TREATMENT_TYPE_ID);
        dest.writeString(PATIENT_CODE);
        dest.writeString(VIR_PATIENT_NAME);
        dest.writeString(GENDER_CODE);
        dest.writeString(GENDER_NAME);
    }

}