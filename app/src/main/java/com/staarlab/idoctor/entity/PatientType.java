package com.staarlab.idoctor.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by PHAMHUNG on 1/1/2017.
 */
@DatabaseTable(tableName = "PATIENT_TYPE")
public class PatientType implements Parcelable {
    public static final String FIELD_NAME_ID = "_id";
    public static final String FIELD_NAME_PATIENT_TYPE_NAME = "PATIENT_TYPE_NAME";

    @DatabaseField(columnName = FIELD_NAME_ID, id = true)
    public Long ID;
    @DatabaseField(columnName = "CREATE_TIME")
    public String CREATE_TIME;
    @DatabaseField(columnName = "MODIFY_TIME")
    public String MODIFY_TIME;
    @DatabaseField(columnName = "IS_ACTIVE")
    public int IS_ACTIVE;
    @DatabaseField(columnName = "IS_DELETE")
    public int IS_DELETE;
    @DatabaseField(columnName = "GROUP_CODE")
    public String GROUP_CODE;
    @DatabaseField(columnName = "PATIENT_TYPE_CODE")
    public String PATIENT_TYPE_CODE;
    @DatabaseField(columnName = FIELD_NAME_PATIENT_TYPE_NAME)
    public String PATIENT_TYPE_NAME;

    public PatientType(){}

    protected PatientType(Parcel in) {
        ID = in.readLong();
        CREATE_TIME = in.readString();
        MODIFY_TIME = in.readString();
        IS_ACTIVE = in.readInt();
        IS_DELETE = in.readInt();
        GROUP_CODE = in.readString();
        PATIENT_TYPE_CODE = in.readString();
        PATIENT_TYPE_NAME = in.readString();
    }

    public static final Creator<PatientType> CREATOR = new Creator<PatientType>() {
        @Override
        public PatientType createFromParcel(Parcel in) {
            return new PatientType(in);
        }

        @Override
        public PatientType[] newArray(int size) {
            return new PatientType[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(ID);
        parcel.writeString(CREATE_TIME);
        parcel.writeString(MODIFY_TIME);
        parcel.writeInt(IS_ACTIVE);
        parcel.writeInt(IS_DELETE);
        parcel.writeString(GROUP_CODE);
        parcel.writeString(PATIENT_TYPE_CODE);
        parcel.writeString(PATIENT_TYPE_NAME);
    }
}
