package com.staarlab.idoctor.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Author: Lucero
 * Created: 12/22/16.
 */

public class TreatmentSummaryInfo implements Serializable, Parcelable {
    @SerializedName("ID")
    private long id;

    @SerializedName("TREATMENT_CODE")
    private String treatmentCode;

    @SerializedName("PATIENT_ID")
    private long patientId;

    @SerializedName("PATIENT_CODE")
    private String patientCode;

    @SerializedName("COUNT_EXAM")
    private float countExam;

    @SerializedName("COUNT_TEST")
    private float countTest;

    @SerializedName("COUNT_DIIM")
    private float countDiim;

    @SerializedName("COUNT_MISU")
    private float countMisu;

    @SerializedName("COUNT_FUEX")
    private float countFuex;

    @SerializedName("COUNT_PRES")
    private float countPres;

    @SerializedName("COUNT_BED")
    private float countBed;

    @SerializedName("COUNT_ENDO")
    private float countEndo;

    @SerializedName("COUNT_SUIM")
    private float countSuim;

    @SerializedName("COUNT_SURG")
    private float countSurg;

    @SerializedName("COUNT_OTHER")
    private float countOther;

    @SerializedName("COUNT_REHA")
    private float countReha;

    @SerializedName("COUNT_CARE")
    private float countCare;

    @SerializedName("COUNT_DHST")
    private float countDhst;

    @SerializedName("COUNT_INFUSION")
    private float countInfusion;

    @SerializedName("COUNT_TRACKING")
    private float countTracking;

    @SerializedName("COUNT_MEDI_REACT")
    private float countMediReact;

    @SerializedName("COUNT_DEBATE")
    private float countDebate;

    @SerializedName("COUNT_TRAN_PATI")
    private float countTranPati;

    @SerializedName("COUNT_DEATH")
    private float countDeath;

    @SerializedName("TOTAL_BILL_AMOUNT")
    private double totalBillAmount;

    @SerializedName("TOTAL_BILL_TRANSFER_AMOUNT")
    private double totalBillTransferAmount;

    @SerializedName("TOTAL_BILL_EXEMPTION")
    private double totalBillExemption;

    @SerializedName("TOTAL_DEPOSIT_AMOUNT")
    private double totalDepositAmount;

    @SerializedName("TOTAL_REPAY_AMOUNT")
    private double totalRepayAmount;

    @SerializedName("TOTAL_PRICE")
    private double totalPrice;

    @SerializedName("TOTAL_HEIN_PRICE")
    private double totalHeinPrice;

    @SerializedName("TOTAL_PATIENT_PRICE")
    private double totalPatientPrice;

    @SerializedName("TOTAL_DISCOUNT")
    private double totalDiscount;


    protected TreatmentSummaryInfo(Parcel in) {
        id = in.readLong();
        treatmentCode = in.readString();
        patientId = in.readLong();
        patientCode = in.readString();
        countExam = in.readFloat();
        countTest = in.readFloat();
        countDiim = in.readFloat();
        countMisu = in.readFloat();
        countFuex = in.readFloat();
        countPres = in.readFloat();
        countBed = in.readFloat();
        countEndo = in.readFloat();
        countSuim = in.readFloat();
        countSurg = in.readFloat();
        countOther = in.readFloat();
        countReha = in.readFloat();
        countCare = in.readFloat();
        countDhst = in.readFloat();
        countInfusion = in.readFloat();
        countTracking = in.readFloat();
        countMediReact = in.readFloat();
        countDebate = in.readFloat();
        countTranPati = in.readFloat();
        countDeath = in.readFloat();
        totalBillAmount = in.readDouble();
        totalBillTransferAmount = in.readDouble();
        totalBillExemption = in.readDouble();
        totalDepositAmount = in.readDouble();
        totalRepayAmount = in.readDouble();
        totalPrice = in.readDouble();
        totalHeinPrice = in.readDouble();
        totalPatientPrice = in.readDouble();
        totalDiscount = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(treatmentCode);
        dest.writeLong(patientId);
        dest.writeString(patientCode);
        dest.writeFloat(countExam);
        dest.writeFloat(countTest);
        dest.writeFloat(countDiim);
        dest.writeFloat(countMisu);
        dest.writeFloat(countFuex);
        dest.writeFloat(countPres);
        dest.writeFloat(countBed);
        dest.writeFloat(countEndo);
        dest.writeFloat(countSuim);
        dest.writeFloat(countSurg);
        dest.writeFloat(countOther);
        dest.writeFloat(countReha);
        dest.writeFloat(countCare);
        dest.writeFloat(countDhst);
        dest.writeFloat(countInfusion);
        dest.writeFloat(countTracking);
        dest.writeFloat(countMediReact);
        dest.writeFloat(countDebate);
        dest.writeFloat(countTranPati);
        dest.writeFloat(countDeath);
        dest.writeDouble(totalBillAmount);
        dest.writeDouble(totalBillTransferAmount);
        dest.writeDouble(totalBillExemption);
        dest.writeDouble(totalDepositAmount);
        dest.writeDouble(totalRepayAmount);
        dest.writeDouble(totalPrice);
        dest.writeDouble(totalHeinPrice);
        dest.writeDouble(totalPatientPrice);
        dest.writeDouble(totalDiscount);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TreatmentSummaryInfo> CREATOR = new Creator<TreatmentSummaryInfo>() {
        @Override
        public TreatmentSummaryInfo createFromParcel(Parcel in) {
            return new TreatmentSummaryInfo(in);
        }

        @Override
        public TreatmentSummaryInfo[] newArray(int size) {
            return new TreatmentSummaryInfo[size];
        }
    };

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTreatmentCode() {
        return treatmentCode;
    }

    public void setTreatmentCode(String treatmentCode) {
        this.treatmentCode = treatmentCode;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public String getPatientCode() {
        return patientCode;
    }

    public void setPatientCode(String patientCode) {
        this.patientCode = patientCode;
    }

    public Float getCountExam() {
        return countExam;
    }

    public void setCountExam(Float countExam) {
        this.countExam = countExam;
    }

    public Float getCountTest() {
        return countTest;
    }

    public void setCountTest(Float countTest) {
        this.countTest = countTest;
    }

    public Float getCountDiim() {
        return countDiim;
    }

    public void setCountDiim(Float countDiim) {
        this.countDiim = countDiim;
    }

    public Float getCountMisu() {
        return countMisu;
    }

    public void setCountMisu(Float countMisu) {
        this.countMisu = countMisu;
    }

    public Float getCountFuex() {
        return countFuex;
    }

    public void setCountFuex(Float countFuex) {
        this.countFuex = countFuex;
    }

    public Float getCountPres() {
        return countPres;
    }

    public void setCountPres(Float countPres) {
        this.countPres = countPres;
    }

    public Float getCountBed() {
        return countBed;
    }

    public void setCountBed(Float countBed) {
        this.countBed = countBed;
    }

    public Float getCountEndo() {
        return countEndo;
    }

    public void setCountEndo(Float countEndo) {
        this.countEndo = countEndo;
    }

    public Float getCountSuim() {
        return countSuim;
    }

    public void setCountSuim(Float countSuim) {
        this.countSuim = countSuim;
    }

    public Float getCountSurg() {
        return countSurg;
    }

    public void setCountSurg(Float countSurg) {
        this.countSurg = countSurg;
    }

    public Float getCountOther() {
        return countOther;
    }

    public void setCountOther(Float countOther) {
        this.countOther = countOther;
    }

    public Float getCountReha() {
        return countReha;
    }

    public void setCountReha(Float countReha) {
        this.countReha = countReha;
    }

    public Float getCountCare() {
        return countCare;
    }

    public void setCountCare(Float countCare) {
        this.countCare = countCare;
    }

    public Float getCountDhst() {
        return countDhst;
    }

    public void setCountDhst(Float countDhst) {
        this.countDhst = countDhst;
    }

    public Float getCountInfusion() {
        return countInfusion;
    }

    public void setCountInfusion(Float countInfusion) {
        this.countInfusion = countInfusion;
    }

    public Float getCountTracking() {
        return countTracking;
    }

    public void setCountTracking(Float countTracking) {
        this.countTracking = countTracking;
    }

    public Float getCountMediReact() {
        return countMediReact;
    }

    public void setCountMediReact(Float countMediReact) {
        this.countMediReact = countMediReact;
    }

    public Float getCountDebate() {
        return countDebate;
    }

    public void setCountDebate(Float countDebate) {
        this.countDebate = countDebate;
    }

    public Float getCountTranPati() {
        return countTranPati;
    }

    public void setCountTranPati(Float countTranPati) {
        this.countTranPati = countTranPati;
    }

    public Float getCountDeath() {
        return countDeath;
    }

    public void setCountDeath(Float countDeath) {
        this.countDeath = countDeath;
    }

    public double getTotalBillAmount() {
        return totalBillAmount;
    }

    public void setTotalBillAmount(double totalBillAmount) {
        this.totalBillAmount = totalBillAmount;
    }

    public double getTotalBillTransferAmount() {
        return totalBillTransferAmount;
    }

    public void setTotalBillTransferAmount(double totalBillTransferAmount) {
        this.totalBillTransferAmount = totalBillTransferAmount;
    }

    public double getTotalBillExemption() {
        return totalBillExemption;
    }

    public void setTotalBillExemption(double totalBillExemption) {
        this.totalBillExemption = totalBillExemption;
    }

    public double getTotalDepositAmount() {
        return totalDepositAmount;
    }

    public void setTotalDepositAmount(double totalDepositAmount) {
        this.totalDepositAmount = totalDepositAmount;
    }

    public double getTotalRepayAmount() {
        return totalRepayAmount;
    }

    public void setTotalRepayAmount(double totalRepayAmount) {
        this.totalRepayAmount = totalRepayAmount;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getTotalHeinPrice() {
        return totalHeinPrice;
    }

    public void setTotalHeinPrice(double totalHeinPrice) {
        this.totalHeinPrice = totalHeinPrice;
    }

    public double getTotalPatientPrice() {
        return totalPatientPrice;
    }

    public void setTotalPatientPrice(double totalPatientPrice) {
        this.totalPatientPrice = totalPatientPrice;
    }

    public double getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(double totalDiscount) {
        this.totalDiscount = totalDiscount;
    }


}
