package com.staarlab.idoctor.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by PHAMHUNG on 12/26/2016.
 */

/**
 * Chuẩn đoán chính
 */
@DatabaseTable(tableName = "ICD")
public class ICD implements Parcelable, Serializable {
    public static final String FIELD_NAME_ICD_NAME = "ICD_NAME";
    public static final String FIELD_NAME_ICD_CODE = "ICD_CODE";
    public static final String FIELD_NAME_ICD_SEARCH = "ICD_SEARCH";

    @DatabaseField(columnName = "_id", id = true)
    public long ID;
    @DatabaseField(columnName = "CREATE_TIME")
    public String CREATE_TIME;
    @DatabaseField(columnName = "MODIFY_TIME")
    public String MODIFY_TIME;
    @DatabaseField(columnName = "IS_ACTIVE")
    public int IS_ACTIVE;
    @DatabaseField(columnName = "IS_DELETE")
    public int IS_DELETE;
    @DatabaseField(columnName = "GROUP_CODE")
    public String GROUP_CODE;
    @DatabaseField(columnName = FIELD_NAME_ICD_CODE)
    public String ICD_CODE;
    @DatabaseField(columnName = FIELD_NAME_ICD_NAME)
    public String ICD_NAME;
    @DatabaseField(columnName = FIELD_NAME_ICD_SEARCH, columnDefinition = "TEXT COLLATE NOCASE")
    public String ICD_SEARCH;
    @DatabaseField(columnName = "ICD_NAME_EN")
    public String ICD_NAME_EN;
    @DatabaseField(columnName = "ICD_GROUP_ID")
    public String ICD_GROUP_ID;

    // Business field
    public String ICD_MAIN_NAME;
    public String ICD_TEXT;

    public ICD() {
    }

    protected ICD(Parcel in) {
        ID = in.readLong();
        CREATE_TIME = in.readString();
        MODIFY_TIME = in.readString();
        IS_ACTIVE = in.readInt();
        IS_DELETE = in.readInt();
        GROUP_CODE = in.readString();
        ICD_CODE = in.readString();
        ICD_NAME = in.readString();
        ICD_SEARCH = in.readString();
        ICD_NAME_EN = in.readString();
        ICD_GROUP_ID = in.readString();
        ICD_TEXT = in.readString();
    }

    public static final Creator<ICD> CREATOR = new Creator<ICD>() {
        @Override
        public ICD createFromParcel(Parcel in) {
            return new ICD(in);
        }

        @Override
        public ICD[] newArray(int size) {
            return new ICD[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(ID);
        parcel.writeString(CREATE_TIME);
        parcel.writeString(MODIFY_TIME);
        parcel.writeInt(IS_ACTIVE);
        parcel.writeInt(IS_DELETE);
        parcel.writeString(GROUP_CODE);
        parcel.writeString(ICD_CODE);
        parcel.writeString(ICD_NAME);
        parcel.writeString(ICD_SEARCH);
        parcel.writeString(ICD_NAME_EN);
        parcel.writeString(ICD_GROUP_ID);
        parcel.writeString(ICD_TEXT);
    }
}
