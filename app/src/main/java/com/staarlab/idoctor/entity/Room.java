package com.staarlab.idoctor.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Room implements Serializable {
    public static final String ROOM_TYPE_GI = "GI";

    @SerializedName("ROOM_ID")
    private Long roomId;

    @SerializedName("ROOM_CODE")
    private String roomCode;

    @SerializedName("ROOM_NAME")
    private String roomName;

    @SerializedName("ROOM_TYPE_CODE")
    private String roomTypeCode;

    public Room() {

    }

    public Room(String roomName) {
        this.roomName = roomName;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }

    public String getRoomCode() {
        return roomCode;
    }

    public void setRoomCode(String roomCode) {
        this.roomCode = roomCode;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomTypeCode() {
        return roomTypeCode;
    }

    public void setRoomTypeCode(String roomTypeCode) {
        this.roomTypeCode = roomTypeCode;
    }
}
