package com.staarlab.idoctor;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

import com.staarlab.idoctor.helper.share.MBSSharePreference;
import com.staarlab.idoctor.ui.activity.LoginActivity;
import com.staarlab.idoctor.util.AppConst;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by PHAMHUNG on 12/17/2016.
 */

public class MBSApplication extends Application {
    private static MBSApplication instance;

    public static MBSApplication get() {
        return instance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        MBSApplication.instance = this;

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
    }

    public void logout(Context context) {
        //Delete authentication
        MBSSharePreference.get().remove(AppConst.PREF_KEY_RENEW_CODE);
        MBSSharePreference.get().remove(AppConst.PREF_KEY_TOKEN_CODE);
        MBSSharePreference.get().remove(AppConst.PREF_KEY_USER_NAME);

        Intent intent = new Intent(context, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }


}
