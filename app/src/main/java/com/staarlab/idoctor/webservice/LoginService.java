package com.staarlab.idoctor.webservice;

import com.staarlab.idoctor.model.LoginModel;
import com.staarlab.idoctor.model.ResponseObjectModel;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by minhtu on 12/16/16.
 */

public interface LoginService {
    @GET("/api/Token/Login/")
    void login(Callback<ResponseObjectModel<LoginModel>> loginModel);

    @GET("/api/Token/GetAuthenticated/")
    void getAuthenticated(Callback<ResponseObjectModel<LoginModel>> loginModel);

    @GET("/api/Token/Renew/")
    void renew(Callback<ResponseObjectModel<LoginModel>> loginModel);

    @POST("/api/Token/ChangePassword/")
    void changePassword(@Body String body, Callback<ResponseObjectModel<Boolean>> loginModel);
}
