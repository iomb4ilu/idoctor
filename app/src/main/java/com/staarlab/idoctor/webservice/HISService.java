package com.staarlab.idoctor.webservice;

import com.staarlab.idoctor.entity.BedRoom;
import com.staarlab.idoctor.entity.CareServiceEntity;
import com.staarlab.idoctor.entity.CommonServiceDetailEntity;
import com.staarlab.idoctor.entity.CommonServiceEntity;
import com.staarlab.idoctor.entity.DebateServiceEntity;
import com.staarlab.idoctor.entity.DhstServiceEntity;
import com.staarlab.idoctor.entity.EmteMaterial;
import com.staarlab.idoctor.entity.EmteMedicine;
import com.staarlab.idoctor.entity.ExpMestTemplate;
import com.staarlab.idoctor.entity.ICD;
import com.staarlab.idoctor.entity.InfusionServiceEntity;
import com.staarlab.idoctor.entity.Material;
import com.staarlab.idoctor.entity.MaterialOldPrescription;
import com.staarlab.idoctor.entity.MediReactServiceEntity;
import com.staarlab.idoctor.entity.MedicalStock;
import com.staarlab.idoctor.entity.Medicine;
import com.staarlab.idoctor.entity.MedicineOldPrescription;
import com.staarlab.idoctor.entity.MedicineUserForm;
import com.staarlab.idoctor.entity.Patient;
import com.staarlab.idoctor.entity.PatientTypeAllow;
import com.staarlab.idoctor.entity.Prescription;
import com.staarlab.idoctor.entity.PatientType;
import com.staarlab.idoctor.entity.Room;
import com.staarlab.idoctor.entity.SereServ;
import com.staarlab.idoctor.entity.ServiceGroup;
import com.staarlab.idoctor.entity.ServicePATY;
import com.staarlab.idoctor.entity.ServiceRoom;
import com.staarlab.idoctor.entity.ServiceSegr;
import com.staarlab.idoctor.entity.ServiceType;
import com.staarlab.idoctor.entity.TechService;
import com.staarlab.idoctor.entity.TrackingServiceEntity;
import com.staarlab.idoctor.entity.TranPatiServiceEntity;
import com.staarlab.idoctor.entity.Treatment;
import com.staarlab.idoctor.entity.TreatmentCommonInfo;
import com.staarlab.idoctor.entity.TreatmentSummaryInfo;
import com.staarlab.idoctor.entity.TreatmentFee;
import com.staarlab.idoctor.model.BodyModel;
import com.staarlab.idoctor.model.PostResponModel;
import com.staarlab.idoctor.model.ResponseArrayModel;
import com.staarlab.idoctor.model.ResponseObjectModel;


import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Author: Lucero
 * Created: 12/5/2016.
 */

public interface HISService {
    @GET("/api/HisUserRoom/GetView")
    void getRoomByDoctor(@Query("param") String param, Callback<ResponseArrayModel<Room>> cb);

    @GET("/api/HisBedRoom/Get")
    void getBedRoomByRoomId(@Query("param") String param, Callback<ResponseArrayModel<BedRoom>> cb);

    @GET("/api/HisTreatmentBedRoom/GetViewSdoCurrentInByBedRoomId")
    void getTreatmentByBedRoom(@Query("param") String param, Callback<ResponseArrayModel<Treatment>> cb);

    @GET("/api/HisPatient/GetView")
    void getPatientInfo(@Query("param") String param, Callback<ResponseArrayModel<Patient>> cb);

    @GET("/api/HisTreatment/GetSummaryView")
    void getTreatmentSummaryInfo(@Query("param") String param, Callback<ResponseArrayModel<TreatmentSummaryInfo>> cb);

    @GET("/api/HisIcd/Get")
    void getMainICD(@Query("param") String param, Callback<ResponseArrayModel<ICD>> cb);

    @GET("/api/HisPrescription/GetView")
    void getHisPrescription(@Query("param") String param, Callback<ResponseArrayModel<Prescription>> cb);

    @GET("/api/HisService/GetView")
    void getTechService(@Query("param") String param, Callback<ResponseArrayModel<TechService>> cb);

    @GET("/api/HisServiceType/Get")
    void getTechServiceType(@Query("param") String param, Callback<ResponseArrayModel<ServiceType>> cb);

    @GET("/api/HisPatientType/Get")
    void getPatientType(@Query("param") String param, Callback<ResponseArrayModel<PatientType>> cb);

    @GET("/api/HisServicePaty/GetView")
    void getServicePATY(@Query("param") String param, Callback<ResponseArrayModel<ServicePATY>> cb);

    @GET("/api/HisExamServiceReq/GetView")
    void getHisExam(@Query("param") String param, Callback<ResponseArrayModel<CommonServiceEntity>> cb);

    @GET("/api/HisServiceGroup/Get")
    void getServiceGroup(@Query("param") String param, Callback<ResponseArrayModel<ServiceGroup>> cb);

    @GET("/api/HisDeathServiceReq/GetView")
    void getHisDeath(@Query("param") String param, Callback<ResponseArrayModel<CommonServiceEntity>> cb);

    @GET("/api/HisServSegr/Get")
    void getServiceSegr(@Query("param") String param, Callback<ResponseArrayModel<ServiceSegr>> cb);

    @GET("/api/HisTranPati/GetView")
    void getHisTranPati(@Query("param") String param, Callback<ResponseArrayModel<TranPatiServiceEntity>> cb);

    @POST("/api/HisServiceReq/CreateCombo")
    void postSpecifyService(@Body BodyModel object, Callback<PostResponModel> cb);

    @GET("/api/HisDebate/Get")
    void getHisDebate(@Query("param") String param, Callback<ResponseArrayModel<DebateServiceEntity>> cb);

    @GET("/api/HisMediReact/GetView")
    void getHisMediReact(@Query("param") String param, Callback<ResponseArrayModel<MediReactServiceEntity>> cb);

    @GET("/api/HisTracking/GetView")
    void getHisTracking(@Query("param") String param, Callback<ResponseArrayModel<TrackingServiceEntity>> cb);

    @GET("/api/HisInfusion/GetView")
    void getHisInfusion(@Query("param") String param, Callback<ResponseArrayModel<InfusionServiceEntity>> cb);

    @GET("/api/HisDhst/Get")
    void getHisDhst(@Query("param") String param, Callback<ResponseArrayModel<DhstServiceEntity>> cb);

    @GET("/api/HisCare/Get")
    void getHisCare(@Query("param") String param, Callback<ResponseArrayModel<CareServiceEntity>> cb);

    @GET("/api/HisRehaServiceReq/GetView")
    void getHisReha(@Query("param") String param, Callback<ResponseArrayModel<CommonServiceEntity>> cb);

    @GET("/api/HisOtherServiceReq/GetView")
    void getHisOther(@Query("param") String param, Callback<ResponseArrayModel<CommonServiceEntity>> cb);

    @GET("/api/HisSurgServiceReq/GetView")
    void getHisSurg(@Query("param") String param, Callback<ResponseArrayModel<CommonServiceEntity>> cb);

    @GET("/api/HisSuimServiceReq/GetView")
    void getHisSuim(@Query("param") String param, Callback<ResponseArrayModel<CommonServiceEntity>> cb);

    @GET("/api/HisEndoServiceReq/GetView")
    void getHisEndo(@Query("param") String param, Callback<ResponseArrayModel<CommonServiceEntity>> cb);

    @GET("/api/HisBedServiceReq/GetView")
    void getHisBed(@Query("param") String param, Callback<ResponseArrayModel<CommonServiceEntity>> cb);

    @GET("/api/HisFuexServiceReq/GetView")
    void getHisFuex(@Query("param") String param, Callback<ResponseArrayModel<CommonServiceEntity>> cb);

    @GET("/api/HisMisuServiceReq/GetView")
    void getHisMisu(@Query("param") String param, Callback<ResponseArrayModel<CommonServiceEntity>> cb);

    @GET("/api/HisDiimServiceReq/GetView")
    void getHisDiim(@Query("param") String param, Callback<ResponseArrayModel<CommonServiceEntity>> cb);

    @GET("/api/HisTestServiceReq/GetView")
    void getHisTest(@Query("param") String param, Callback<ResponseArrayModel<CommonServiceEntity>> cb);

    @GET("/api/HisTreatment/GetFeeView")
    void getHisTreatmentFee(@Query("param") String param, Callback<ResponseArrayModel<TreatmentFee>> cb);

    @GET("/api/HisSereServ/GetView")
    void getHisSereServ(@Query("param") String param, Callback<ResponseArrayModel<CommonServiceDetailEntity>> cb);

    @GET("/api/HisTreatment/GetCommonInfo")
    void getHisTreatmentCommonInfo(@Query("param") String param, Callback<ResponseObjectModel<TreatmentCommonInfo>> cb);

    @GET("/api/HisMediStock/Get")
    void getMedicalStock(@Query("param") String param, Callback<ResponseArrayModel<MedicalStock>> cb);

    @GET("/api/HisMedicineUseForm/Get")
    void getMedicineUseForm(@Query("param") String param, Callback<ResponseArrayModel<MedicineUserForm>> cb);

    @GET("/api/HisMedicineType/GetInStockMedicineType")
    void getMedicine(@Query("param") String param, Callback<ResponseArrayModel<Medicine>> cb);

    @GET("/api/HisMaterialType/GetInStockMaterialType")
    void getMaterial(@Query("param") String param, Callback<ResponseArrayModel<Material>> cb);

    @POST("/api/HisPrescription/Create")
    void postMedicineMaterial(@Body BodyModel object, Callback<PostResponModel> cb);

    @GET("/api/HisServiceRoom/GetView")
    void getServiceRoom(@Query("param") String param, Callback<ResponseArrayModel<ServiceRoom>> cb);

    @POST("/api/Token/UpdateWorkPlace")
    void postWorkingSessionInfo(@Body BodyModel object, Callback<PostResponModel> cb);

    @GET("/api/HisMestRoom/GetView")
    void getStockByRoomId(@Query("param") String param, Callback<ResponseArrayModel<MedicalStock>> cb);

    @GET("/api/HisSereServ/GetView")
    void getListOfPlacedService(@Query("param") String param, Callback<ResponseArrayModel<SereServ>> cb);

    @GET("/api/HisExpMestMedicine/GetView")
    void getMedicineFromOldPrescription(@Query("param") String param, Callback<ResponseArrayModel<MedicineOldPrescription>> cb);

    @GET("/api/HisExpMestMaterial/GetView")
    void getMaterialFromOldPrescription(@Query("param") String param, Callback<ResponseArrayModel<MaterialOldPrescription>> cb);

    @GET("/api/HisExpMestTemplate/Get")
    void getExpMestTemplate(@Query("param") String param, Callback<ResponseArrayModel<ExpMestTemplate>> cb);

    @GET("/api/HisEmteMedicineType/GetView")
    void getEmteMedicine(@Query("param") String param, Callback<ResponseArrayModel<EmteMedicine>> cb);

    @GET("/api/HisEmteMaterialType/GetView")
    void getEmteMaterial(@Query("param") String param, Callback<ResponseArrayModel<EmteMaterial>> cb);

    @GET("/api/HisPatientTypeAllow/Get")
    void getPatientTypeAllow(@Query("param") String param, Callback<ResponseArrayModel<PatientTypeAllow>> cb);


}
