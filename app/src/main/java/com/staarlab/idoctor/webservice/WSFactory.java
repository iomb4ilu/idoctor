package com.staarlab.idoctor.webservice;


import android.util.Base64;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.staarlab.idoctor.BuildConfig;
import com.staarlab.idoctor.helper.share.MBSSharePreference;
import com.staarlab.idoctor.util.AppConst;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.android.AndroidLog;
import retrofit.converter.GsonConverter;

/**
 * Author: Lucero
 * Created: 12/5/2016.
 */

public class WSFactory {
    private static final String APPLICATION_CODE = "MBS";
    public static final String WEBSERVICE_AUTH_URL = "http://acs.12c.vn";
    public static final String WEBSERVICE_BUSINESS_URL = "http://mos.12c.vn";

    public static HISService createHisService(final String tokenCode) {
        RestAdapter.Builder builder = new RestAdapter.Builder().setEndpoint(MBSSharePreference.get().getServerAPI())
                .setConverter(new GsonConverter(new GsonBuilder().create()));
        builder.setRequestInterceptor(new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader(AppConst.API_TOKEN_CODE, tokenCode);
                request.addHeader(AppConst.API_APP_CODE, APPLICATION_CODE);
                request.addHeader(AppConst.API_ACCEPT, AppConst.API_ACCEPT_VALUE);
                Log.d("XXXX", "TokenCode:" + tokenCode);
                Log.d("XXXX", "ApplicationCode:" + APPLICATION_CODE);
                Log.d("XXXX", "Accept:application/json");
            }
        });
        builder.setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.BASIC : RestAdapter.LogLevel.NONE).setLog(new AndroidLog("RETROFIT"));
        RestAdapter restAdapter = builder.build();
        return restAdapter.create(HISService.class);
    }

    public static LoginService createLoginService(String userName, String password) {
        RestAdapter.Builder builder = new RestAdapter.Builder().setEndpoint(MBSSharePreference.get().getServerAuthentication())
                .setConverter(new GsonConverter(new GsonBuilder().create()));
        String credentials = APPLICATION_CODE + ":" + userName + ":" + password;
        final String basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        builder.setRequestInterceptor(new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                request.addHeader("Authorization", basic);
            }
        });
        RestAdapter restAdapter = builder.build();
        return restAdapter.create(LoginService.class);
    }

    public static LoginService createLoginService(final String tokenCode) {
        RestAdapter.Builder builder = new RestAdapter.Builder().setEndpoint(MBSSharePreference.get().getServerAuthentication())
                .setConverter(new GsonConverter(new GsonBuilder().create()));
        if (tokenCode != null) {
            builder.setRequestInterceptor(new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade request) {
                    request.addHeader(AppConst.API_TOKEN_CODE, tokenCode);
                    request.addHeader(AppConst.API_APP_CODE, APPLICATION_CODE);
                }
            });
        }
        RestAdapter adapter = builder.build();
        return adapter.create(LoginService.class);
    }

    public static LoginService createLoginServiceRenewCode(final String renewCode) {
        RestAdapter.Builder builder = new RestAdapter.Builder().setEndpoint(MBSSharePreference.get().getServerAuthentication())
                .setConverter(new GsonConverter(new GsonBuilder().create()));
        if (renewCode != null) {
            builder.setRequestInterceptor(new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade request) {
                    request.addHeader("RenewCode", renewCode);
                    request.addHeader(AppConst.API_APP_CODE, APPLICATION_CODE);
                }
            });
        }

        RestAdapter adapter = builder.build();
        return adapter.create(LoginService.class);
    }

    public static LoginService createLoginServiceChangePassword(final String tokenCode, final String oldPass, final String newPass) {
        RestAdapter.Builder builder = new RestAdapter.Builder().setEndpoint(MBSSharePreference.get().getServerAuthentication())
                .setConverter(new GsonConverter(new GsonBuilder().create()));
        if (tokenCode != null && oldPass != null && newPass != null) {
            String credentials = oldPass + ":" + newPass;
            final String password =
                    Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
            builder.setRequestInterceptor(new RequestInterceptor() {
                @Override
                public void intercept(RequestFacade request) {
                    request.addHeader(AppConst.API_TOKEN_CODE, tokenCode);
                    request.addHeader(AppConst.API_APP_CODE, APPLICATION_CODE);
                    request.addHeader("Password", password);
                }
            });
        }

        RestAdapter adapter = builder.build();
        return adapter.create(LoginService.class);
    }

}
