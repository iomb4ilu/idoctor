package com.staarlab.idoctor.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.Room;

import java.util.ArrayList;
import java.util.List;

public class RoomAdapter extends BaseAdapter {
    static class ViewHolder {
        TextView roomName;
        View roomCanvas;
    }

    private Context context;
    private List<Room> roomList;

    public RoomAdapter(Context context, List<Room> roomList) {
        this.context = context;
        this.roomList = roomList;
    }

    @Override
    public Room getItem(int position) {
        return roomList.get(position);
    }

    @Override
    public int getCount() {
        return roomList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Room room = getItem(position);
        ViewHolder vh;
        if (convertView == null) {
            vh = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.adapter_room_list_item, parent, false);
            vh.roomName = (TextView) convertView.findViewById(R.id.roomName);
            vh.roomCanvas = convertView.findViewById(R.id.roomCanvas);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        vh.roomName.setText(room.getRoomName());
        return convertView;
    }

    public void refreshData(List<Room> rooms){
        if(roomList == null) roomList = new ArrayList<>();
        roomList.clear();
        roomList.addAll(rooms);
        notifyDataSetChanged();
    }
}
