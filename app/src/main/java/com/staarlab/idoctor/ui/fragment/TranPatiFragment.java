package com.staarlab.idoctor.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.CommonServiceEntity;
import com.staarlab.idoctor.entity.TranPatiServiceEntity;
import com.staarlab.idoctor.model.ResponseArrayModel;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class TranPatiFragment extends CommonServiceFragment {
    private TextView txtTranPati1;
    private TextView txtTranPati2;
    private TextView txtTranPati3;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        txtTranPati1 = (TextView) v.findViewById(R.id.lblTranPati1);
        txtTranPati2 = (TextView) v.findViewById(R.id.lblTranPati2);
        txtTranPati3 = (TextView) v.findViewById(R.id.lblTranPati3);
        return v;
    }

    @Override
    protected void loadData() {
        Long treatmentId = getArguments().getLong(ARG_TREATMENT_ID, -1L);
        String param = "{'ApiData':{'TREATMENT_ID':'" + treatmentId + "'}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getHisTranPati(param, new Callback<ResponseArrayModel<TranPatiServiceEntity>>() {
            @Override
            public void success(ResponseArrayModel<TranPatiServiceEntity> responseArrayModel, Response response) {
                mAdapter.setDataList(responseArrayModel.getDataList());
                onFinishedDataLoad();
            }

            @Override
            public void failure(RetrofitError error) {
                onFinishedDataLoad();
            }
        });
    }

    @Override
    protected void onLstDataItemClick(int position) {
        mLayoutRight.setVisibility(View.VISIBLE);
        TranPatiServiceEntity entity = (TranPatiServiceEntity) mAdapter.getItem(position);
        txtTranPati1.setText(entity.getClinicalNote());
        txtTranPati2.setText(entity.getTranPatiReasonName());
        txtTranPati3.setText(entity.getTranPatiFormName());
    }
}
