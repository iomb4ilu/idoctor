package com.staarlab.idoctor.ui.fragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.helper.db.MBSDatabaseHelper;
import com.staarlab.idoctor.ui.activity.BaseActivity;


/**
 * @author PHAMHUNG
 * @since 8/13/2015
 */
public class BaseFragment extends Fragment {
    protected static Context context;
    protected String titleRes;
    protected Integer logoRes;
    private Float paddingLeft;

    public BaseFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getActivity();
    }

    public MBSDatabaseHelper getDatabaseHelper(){
        if (getActivity() instanceof BaseActivity){
            return ((BaseActivity) getActivity()).getHelper();
        }
        return null;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public boolean onBackPressed() {
        return false;
    }

    public boolean hasTitle() {
        return getTitle() != null;
    }

    public boolean hasLogo() {
        return getLogoResource() != null;
    }

    public String getTitle() {
        return titleRes;
    }

    protected void setTitleResource(Integer titleRes) {
        this.titleRes = getActivity().getResources().getString(titleRes);
    }

    public void setTitle(String title) {
        this.titleRes = title;
    }

    public Integer getLogoResource() {
        return logoRes;
    }

    public void setLogoResource(Integer logoRes) {
        this.logoRes = logoRes;
    }

    public void replaceCurrentFragment(Fragment fragment) {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).replaceCurrentFragment(fragment);
        }
    }

}
