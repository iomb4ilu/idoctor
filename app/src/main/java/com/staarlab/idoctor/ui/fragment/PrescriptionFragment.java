package com.staarlab.idoctor.ui.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.CommonServiceDetailEntity;
import com.staarlab.idoctor.entity.Prescription;
import com.staarlab.idoctor.helper.layout.GeneralSwipeRefreshLayout;
import com.staarlab.idoctor.model.ResponseArrayModel;
import com.staarlab.idoctor.ui.adapter.PrescriptionListAdapter;
import com.staarlab.idoctor.ui.adapter.ServiceDetailAdapter;
import com.staarlab.idoctor.util.AppConst;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.WSFactory;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.content.Context.MODE_PRIVATE;

public class PrescriptionFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final String ARG_TREATMENT_ID = "ARG_TREATMENT_ID";
    private ListView mLstData;
    private PrescriptionListAdapter mAdapter;
    protected ListView mLstDetail;
    protected View mLayoutLeft;
    protected View mLayoutRight;
    protected GeneralSwipeRefreshLayout mSwipeRefresh;
    protected GeneralSwipeRefreshLayout mSwipeRefreshSub;
    protected ServiceDetailAdapter mDetailAdapter;
    private HISService hisService;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(AppConst.APP_PREF_NAME, MODE_PRIVATE);
        hisService = WSFactory.createHisService(sharedPreferences.getString(AppConst.PREF_KEY_TOKEN_CODE, ""));

        View v = inflater.inflate(R.layout.fragment_treatment_prescription, container, false);
        mLstData = (ListView) v.findViewById(R.id.lstData);
        mLstDetail = (ListView) v.findViewById(R.id.lstDetail);
        mLayoutRight = v.findViewById(R.id.layoutRight);
        mLayoutLeft = v.findViewById(R.id.layoutLeft);
        mSwipeRefresh = (GeneralSwipeRefreshLayout) mLayoutLeft.findViewById(R.id.swipe_refresh);
        mSwipeRefreshSub = (GeneralSwipeRefreshLayout) mLayoutRight.findViewById(R.id.swipe_refresh_sub);
        mAdapter = new PrescriptionListAdapter(getContext());
        mDetailAdapter = new ServiceDetailAdapter(getContext(), AppConst.TAB_CODE_MEDI_REACT);
        mLstData.setAdapter(mAdapter);
        mLstDetail.setAdapter(mDetailAdapter);

        handleEvent();
        mSwipeRefresh.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefresh.setRefreshing(true);
                loadData();
            }
        });

        return v;
    }

    private void loadData() {
        Long treatmentId = getArguments().getLong(ARG_TREATMENT_ID, -1L);
        String param = "{'ApiData':{'TREATMENT_ID':'" + treatmentId + "'}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getHisPrescription(param, new Callback<ResponseArrayModel<Prescription>>() {
            @Override
            public void success(ResponseArrayModel<Prescription> responseArrayModel, Response response) {
                mAdapter.setDataList(responseArrayModel.getDataList());
                onFinishedDataLoad();
            }

            @Override
            public void failure(RetrofitError error) {
                onFinishedDataLoad() ;
            }
        });
    }

    private void handleEvent() {
        mLstData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mLayoutRight.setVisibility(View.VISIBLE);
                mSwipeRefreshSub.setRefreshing(true);
                loadDetail(position);
            }
        });
        mLayoutLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLayoutRight.setVisibility(View.GONE);
            }
        });
        mSwipeRefresh.setOnRefreshListener(this);
        mSwipeRefreshSub.setOnRefreshListener(this);
        mSwipeRefresh.setOnChildScrollUpListener(new GeneralSwipeRefreshLayout.OnChildScrollUpListener() {
            @Override
            public boolean canChildScrollUp() {
                return mLstData.getFirstVisiblePosition() != 0;
            }
        });
        mSwipeRefreshSub.setOnChildScrollUpListener(new GeneralSwipeRefreshLayout.OnChildScrollUpListener() {
            @Override
            public boolean canChildScrollUp() {
                return mLstData.getFirstVisiblePosition() != 0;
            }
        });
    }

    private void loadDetail(int rowNum) {
        Long id = mAdapter.getItem(rowNum).getServiceReqId();
        String param = "{'ApiData':{'SERVICE_REQ_ID':'" + id + "'}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getHisSereServ(param, new Callback<ResponseArrayModel<CommonServiceDetailEntity>>() {
            @Override
            public void success(ResponseArrayModel<CommonServiceDetailEntity> responModel, Response response) {
                mDetailAdapter.refreshData(responModel.getDataList());
                onFinishedSubDataLoad();
            }

            @Override
            public void failure(RetrofitError error) {
                onFinishedSubDataLoad();
            }
        });
    }

    @Override
    public void onRefresh() {
        mSwipeRefresh.setRefreshing(false);
        mSwipeRefreshSub.setRefreshing(false);
    }

    protected void onFinishedDataLoad() {
        mSwipeRefresh.setRefreshing(false);
    }

    protected void onFinishedSubDataLoad() {
        mSwipeRefreshSub.setRefreshing(false);
    }
}
