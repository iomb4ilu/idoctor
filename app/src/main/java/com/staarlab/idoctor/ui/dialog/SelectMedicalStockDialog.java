package com.staarlab.idoctor.ui.dialog;


import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.MedicalStock;
import com.staarlab.idoctor.helper.layout.DividerItemDecoration;
import com.staarlab.idoctor.helper.layout.HeaderRecyclerDividerItemDecorator;
import com.staarlab.idoctor.helper.share.MBSSharePreference;
import com.staarlab.idoctor.model.ResponseArrayModel;
import com.staarlab.idoctor.ui.activity.BaseActivity;
import com.staarlab.idoctor.util.StringUtils;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.WSFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.view.View.GONE;

/**
 * Created by PhamHung on 12/27/16.
 * Get Medial Stock by Room_Id
 */

public class SelectMedicalStockDialog implements View.OnClickListener {
    private static final String TAG = SelectMedicalStockDialog.class.getName();
    protected Context context;
    private Dialog dialog;

    private TextView txtTitle;
    private RecyclerView rvInventory;
    private ImageButton imgSearch;
    private ImageButton imgClose;
    private EditText edtQuery;
    private View viewSearch;

    private LayoutInflater layoutInflater;
    private RecyclerView.LayoutManager layoutManager;
    private List<MedicalStock> items = new ArrayList<>();
    private InventoryAdapter adapter;

    private String query;
    private onMedicalStockSelected listener;
    private HISService hisService;
    private Long roomId;

    public SelectMedicalStockDialog(Context ctx, long roomId) {
        this.context = ctx;
        this.roomId = roomId;
        this.layoutManager = new LinearLayoutManager(context);
        layoutInflater = LayoutInflater.from(context);
        this.dialog = new Dialog(this.context);
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.dialog.setContentView(R.layout.dialog_select);
        this.dialog.setCancelable(false);
        this.hisService = WSFactory.createHisService(MBSSharePreference.get().getTokenCode());
        initViews();
    }

    private void initViews() {
        query = "";
        viewSearch = dialog.findViewById(R.id.viewSearch);
        imgSearch = (ImageButton) dialog.findViewById(R.id.imgSearch);
        imgClose = (ImageButton) dialog.findViewById(R.id.btnClose);
        edtQuery = (EditText) dialog.findViewById(R.id.edtQuery);
        txtTitle = (TextView) dialog.findViewById(R.id.dialog_title);
        txtTitle.setText(R.string.select_medical_stock);
        viewSearch.setVisibility(GONE);
        edtQuery.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtQuery.getWindowToken(), 0);
                if (!query.equalsIgnoreCase(edtQuery.getText().toString())) {
                    query = StringUtils.getEngStringFromUnicodeString(edtQuery.getText().toString().toLowerCase().trim());
                    doQueryDatabase(query);
                }
                return true;
            }
        });
        rvInventory = (RecyclerView) dialog.findViewById(R.id.recycler_view);
        rvInventory.setHasFixedSize(true);
        rvInventory.setLayoutManager(layoutManager);
        rvInventory.addItemDecoration(
                new HeaderRecyclerDividerItemDecorator(
                        context, DividerItemDecoration.VERTICAL_LIST, false, false,
                        R.drawable.divider_0));

        adapter = new InventoryAdapter();
        rvInventory.setAdapter(adapter);

        imgClose.setOnClickListener(this);
        imgSearch.setOnClickListener(this);
        if (roomId != null) {
            getStockByRoomId(roomId);
        } else {
            new LoadMedicalStock().execute(query);
        }
    }

    public void setMedicalStockSelectListener(onMedicalStockSelected l) {
        this.listener = l;
    }

    public void show() {
        dialog.show();
    }

    public void dismiss() {
        dialog.dismiss();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnClose:
                dismiss();
                break;
            case R.id.imgSearch:
                InputMethodManager imm = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtQuery.getWindowToken(), 0);
                if (!query.equalsIgnoreCase(edtQuery.getText().toString())) {
                    query = StringUtils.getEngStringFromUnicodeString(edtQuery.getText().toString().toLowerCase().trim());
                    doQueryDatabase(query);
                }
                break;
        }
    }

    private void doQueryDatabase(String query) {
        new LoadMedicalStock().execute(query);
    }

    private View.OnClickListener onSelectItem = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getTag() instanceof MedicalStock) {
                MedicalStock item = (MedicalStock) v.getTag();
                listener.onSelected(item);
            }
            dismiss();
        }
    };

    // Get Stock by Id via network
    private void getStockByRoomId(long roomId) {
        String param = "{'ApiData': {'room_id':" + roomId + "}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getStockByRoomId(param, new Callback<ResponseArrayModel<MedicalStock>>() {
            @Override
            public void success(ResponseArrayModel<MedicalStock> medicalStock, Response response) {
                items = medicalStock.getDataList();
                adapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                new LoadMedicalStock().execute();
            }
        });
    }

    // LOAD DATA LOCAL
    class LoadMedicalStock extends AsyncTask<String, Void, List<MedicalStock>> {

        @Override
        protected List<MedicalStock> doInBackground(String... params) {
            Dao<MedicalStock, Integer> medicalStockDao = ((BaseActivity) context).getHelper().getMedicalStockDao();
            QueryBuilder<MedicalStock, Integer> qb = medicalStockDao.queryBuilder();
//            qb.orderBy(MedicalStock.FIELD_NAME_STOCK_CODE, true);
            qb.orderByRaw(MedicalStock.FIELD_NAME_STOCK_NAME + " COLLATE NOCASE");
            try {
                PreparedQuery<MedicalStock> preparedQuery = qb.prepare();
                return medicalStockDao.query(preparedQuery);

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new ArrayList<>();
        }

        @Override
        protected void onPostExecute(List<MedicalStock> lst) {
            super.onPostExecute(lst);
            items = lst;
            adapter.notifyDataSetChanged();
        }
    }

    private class InventoryAdapter extends RecyclerView.Adapter {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = layoutInflater.inflate(R.layout.adapter_select_icd, parent, false);
            return new MedicalStockViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            MedicalStockViewHolder vh = (MedicalStockViewHolder) holder;
            MedicalStock item = items.get(position);
            vh.tvName.setText(item.MEDI_STOCK_NAME);
            vh.tvCode.setText(item.MEDI_STOCK_CODE);
            vh.itemView.setTag(item);
            vh.itemView.setOnClickListener(onSelectItem);
        }

        @Override
        public int getItemCount() {
            if (items != null) {
                return items.size();
            }
            return 0;
        }
    }

    private class MedicalStockViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        TextView tvCode;

        public MedicalStockViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvCode = (TextView) itemView.findViewById(R.id.tvCode);

        }
    }

    public interface onMedicalStockSelected {
        void onSelected(MedicalStock item);
    }

}
