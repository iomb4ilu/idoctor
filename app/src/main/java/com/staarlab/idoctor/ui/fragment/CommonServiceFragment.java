package com.staarlab.idoctor.ui.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.CommonServiceDetailEntity;
import com.staarlab.idoctor.entity.CommonServiceEntity;
import com.staarlab.idoctor.helper.layout.GeneralSwipeRefreshLayout;
import com.staarlab.idoctor.model.ResponseArrayModel;
import com.staarlab.idoctor.ui.adapter.CommonServiceAdapter;
import com.staarlab.idoctor.ui.adapter.ServiceDetailAdapter;
import com.staarlab.idoctor.util.AppConst;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.WSFactory;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.content.Context.MODE_PRIVATE;

public abstract class CommonServiceFragment<T extends CommonServiceEntity> extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final String ARG_TREATMENT_ID = "ARG_TREATMENT_ID";
    public static final String ARG_TAB_CODE = "ARG_TAB_CODE";
    protected View mLayoutLeft;
    protected ListView mLstData;
    protected ListView mLstDetail;
    protected View mLayoutRight;
    protected GeneralSwipeRefreshLayout mSwipeRefresh;
    protected GeneralSwipeRefreshLayout mSwipeRefreshSub;
    protected CommonServiceAdapter<T> mAdapter;
    protected ServiceDetailAdapter mDetailAdapter;
    protected HISService hisService;
    protected String tabCode;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        tabCode = getArguments().getString(ARG_TAB_CODE, "");
        SharedPreferences sharedPreferences = getContext().getSharedPreferences(AppConst.APP_PREF_NAME, MODE_PRIVATE);
        hisService = WSFactory.createHisService(sharedPreferences.getString(AppConst.PREF_KEY_TOKEN_CODE, ""));
        View v;
        switch (tabCode) {
            case AppConst.TAB_CODE_MEDI_REACT:
                v = inflater.inflate(R.layout.fragment_medireact_service, container, false);
                break;
            case AppConst.TAB_CODE_DHST:
                v = inflater.inflate(R.layout.fragment_dhst_service, container, false);
                break;
            case AppConst.TAB_CODE_CARE:
                v = inflater.inflate(R.layout.fragment_care_service, container, false);
                break;
            case AppConst.TAB_CODE_INFUSION:
                v = inflater.inflate(R.layout.fragment_infusion_service, container, false);
                break;
            case AppConst.TAB_CODE_TRAN_PATI:
                v = inflater.inflate(R.layout.fragment_tranpati_service, container, false);
                break;
            default:
                v = inflater.inflate(R.layout.fragment_treatment_common, container, false);
                break;
        }
        mLayoutLeft = v.findViewById(R.id.layoutLeft);
        mLayoutRight = v.findViewById(R.id.layoutRight);
        mSwipeRefresh = (GeneralSwipeRefreshLayout) mLayoutLeft.findViewById(R.id.swipe_refresh);
        mLstData = (ListView) mSwipeRefresh.findViewById(R.id.lstData);
        if (!tabCode.equals(AppConst.TAB_CODE_TRAN_PATI)) {
            mSwipeRefreshSub = (GeneralSwipeRefreshLayout) mLayoutRight.findViewById(R.id.swipe_refresh_sub);
            mLstDetail = (ListView) mSwipeRefreshSub.findViewById(R.id.lstDetail);
        }

        switch (tabCode) {
            case AppConst.TAB_CODE_MEDI_REACT:
                loadHeaderMediReact(v);
                break;
            case AppConst.TAB_CODE_TRACKING:
                loadHeaderTracking(v);
                break;
            case AppConst.TAB_CODE_DHST:
                loadHeaderDhst(v);
                break;
            case AppConst.TAB_CODE_DEBATE:
                loadHeaderDebate(v);
                break;
            case AppConst.TAB_CODE_TRAN_PATI:
                loadHeaderTranPati(v);
                break;
        }

        mAdapter = new CommonServiceAdapter<>(getContext(), tabCode);
        mDetailAdapter = new ServiceDetailAdapter(getContext(), tabCode);
        mLstData.setAdapter(mAdapter);
        if (!tabCode.equals(AppConst.TAB_CODE_TRAN_PATI)) {
            mLstDetail.setAdapter(mDetailAdapter);
        }
        handleEvent();
        mSwipeRefresh.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefresh.setRefreshing(true);
                loadData();
            }
        });
        return v;
    }

    protected abstract void loadData();

    private void handleEvent() {
        mLstData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                onLstDataItemClick(position);
            }
        });
        mLayoutLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideRightPanel();
            }
        });
        mSwipeRefresh.setOnRefreshListener(this);
        if (!tabCode.equals(AppConst.TAB_CODE_TRAN_PATI)) {
            mSwipeRefreshSub.setOnRefreshListener(this);
        }
        mSwipeRefresh.setOnChildScrollUpListener(new GeneralSwipeRefreshLayout.OnChildScrollUpListener() {
            @Override
            public boolean canChildScrollUp() {
                return mLstData.getFirstVisiblePosition() != 0;
            }
        });
        if (!tabCode.equals(AppConst.TAB_CODE_TRAN_PATI)) {
            mSwipeRefreshSub.setOnChildScrollUpListener(new GeneralSwipeRefreshLayout.OnChildScrollUpListener() {
                @Override
                public boolean canChildScrollUp() {
                    return mLstData.getFirstVisiblePosition() != 0;
                }
            });
        }
    }

    protected void onLstDataItemClick(final int position) {
        mLayoutRight.setVisibility(View.VISIBLE);
        mSwipeRefreshSub.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshSub.setRefreshing(true);
                loadDetail(position);
            }
        });
    }

    public void hideRightPanel() {
        mLayoutRight.setVisibility(View.GONE);
    }

    private void loadDetail(int rowNum) {
        Long id = mAdapter.getItem(rowNum).getServiceReqId();
        String param = "{'ApiData':{'SERVICE_REQ_ID':'" + id + "'}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getHisSereServ(param, new Callback<ResponseArrayModel<CommonServiceDetailEntity>>() {
            @Override
            public void success(ResponseArrayModel<CommonServiceDetailEntity> responModel, Response response) {
                mDetailAdapter.refreshData(responModel.getDataList());
                onFinishedSubDataLoad();
            }

            @Override
            public void failure(RetrofitError error) {
                onFinishedSubDataLoad();
            }
        });
    }

    @Override
    public void onRefresh() {
        loadData();
    }

    private void loadHeaderDhst(View v) {
        TextView txtC1 = (TextView) v.findViewById(R.id.txtC1);
        TextView txtC2 = (TextView) v.findViewById(R.id.txtC2);
        TextView txtC3 = (TextView) v.findViewById(R.id.txtC3);
        TextView txtC4 = (TextView) v.findViewById(R.id.txtC4);
        TextView txtC5 = (TextView) v.findViewById(R.id.txtC5);
        TextView txtC6 = (TextView) v.findViewById(R.id.txtC6);
        TextView txtC7 = (TextView) v.findViewById(R.id.txtC7);
        TextView txtC8 = (TextView) v.findViewById(R.id.txtC8);
        TextView txtC9 = (TextView) v.findViewById(R.id.txtC9);
        TextView txtC10 = (TextView) v.findViewById(R.id.txtC10);
        TextView txtC11 = (TextView) v.findViewById(R.id.txtC11);
        TextView txtC12 = (TextView) v.findViewById(R.id.txtC12);
        TextView txtC13 = (TextView) v.findViewById(R.id.txtC13);
        TextView txtC14 = (TextView) v.findViewById(R.id.txtC14);
        TextView txtC15 = (TextView) v.findViewById(R.id.txtC15);
        txtC1.setText(R.string.tab_dhst_c1);
        txtC2.setText(R.string.tab_dhst_c2);
        txtC3.setText(R.string.tab_dhst_c3);
        txtC4.setText(R.string.tab_dhst_c4);
        txtC5.setText(R.string.tab_dhst_c5);
        txtC6.setText(R.string.tab_dhst_c6);
        txtC7.setText(R.string.tab_dhst_c7);
        txtC8.setText(R.string.tab_dhst_c8);
        txtC9.setText(R.string.tab_dhst_c9);
        txtC10.setText(R.string.tab_dhst_c10);
        txtC11.setText(R.string.tab_dhst_c11);
        txtC12.setText(R.string.tab_dhst_c12);
        txtC13.setText(R.string.tab_dhst_c13);
        txtC14.setText(R.string.tab_dhst_c14);
        txtC15.setText(R.string.tab_dhst_c15);
    }

    private void loadHeaderMediReact(View v) {
        TextView txtC1 = (TextView) v.findViewById(R.id.txtC1);
        TextView txtC2 = (TextView) v.findViewById(R.id.txtC2);
        TextView txtC3 = (TextView) v.findViewById(R.id.txtC3);
        TextView txtC4 = (TextView) v.findViewById(R.id.txtC4);
        TextView txtC5 = (TextView) v.findViewById(R.id.txtC5);
        TextView txtC6 = (TextView) v.findViewById(R.id.txtC6);
        TextView txtC7 = (TextView) v.findViewById(R.id.txtC7);
        TextView txtC8 = (TextView) v.findViewById(R.id.txtC8);
        txtC1.setText(R.string.tab_medi_react_c1);
        txtC2.setText(R.string.tab_medi_react_c2);
        txtC3.setText(R.string.tab_medi_react_c3);
        txtC4.setText(R.string.tab_medi_react_c4);
        txtC5.setText(R.string.tab_medi_react_c5);
        txtC6.setText(R.string.tab_medi_react_c6);
        txtC7.setVisibility(View.GONE);
        txtC8.setVisibility(View.GONE);

        // Detail
        TextView txtDetailC1 = (TextView) v.findViewById(R.id.txtDetailC1);
        TextView txtDetailC2 = (TextView) v.findViewById(R.id.txtDetailC2);
        TextView txtDetailC3 = (TextView) v.findViewById(R.id.txtDetailC3);
        TextView txtDetailC4 = (TextView) v.findViewById(R.id.txtDetailC4);
        TextView txtDetailC5 = (TextView) v.findViewById(R.id.txtDetailC5);
        txtDetailC1.setText(R.string.header_detail_medreact_c1);
        txtDetailC2.setText(R.string.header_detail_medreact_c2);
        txtDetailC3.setText(R.string.header_detail_medreact_c3);
        txtDetailC4.setVisibility(View.GONE);
        txtDetailC5.setText(R.string.header_detail_medreact_c4);
    }

    private void loadHeaderTracking(View v) {
        TextView txtC1 = (TextView) v.findViewById(R.id.txtC1);
        TextView txtC2 = (TextView) v.findViewById(R.id.txtC2);
        TextView txtC3 = (TextView) v.findViewById(R.id.txtC3);
        TextView txtC4 = (TextView) v.findViewById(R.id.txtC4);
        TextView txtC5 = (TextView) v.findViewById(R.id.txtC5);
        TextView txtC6 = (TextView) v.findViewById(R.id.txtC6);
        txtC1.setText(R.string.tab_tracking_c1);
        txtC2.setText(R.string.tab_tracking_c2);
        txtC3.setText(R.string.tab_tracking_c3);
        txtC4.setText(R.string.tab_tracking_c4);
        txtC5.setText(R.string.tab_tracking_c5);
        txtC6.setText("");
    }

    private void loadHeaderDebate(View v) {
        TextView txtC1 = (TextView) v.findViewById(R.id.txtC1);
        TextView txtC2 = (TextView) v.findViewById(R.id.txtC2);
        TextView txtC3 = (TextView) v.findViewById(R.id.txtC3);
        TextView txtC4 = (TextView) v.findViewById(R.id.txtC4);
        TextView txtC5 = (TextView) v.findViewById(R.id.txtC5);
        TextView txtC6 = (TextView) v.findViewById(R.id.txtC6);
        txtC1.setText(R.string.tab_debate_c1);
        txtC2.setText(R.string.tab_debate_c2);
        txtC3.setText(R.string.tab_debate_c3);
        txtC4.setText(R.string.tab_debate_c4);
        txtC5.setText(R.string.tab_debate_c5);
        txtC6.setText("");
    }

    private void loadHeaderTranPati(View v) {
        TextView txtC1 = (TextView) v.findViewById(R.id.txtC1);
        TextView txtC2 = (TextView) v.findViewById(R.id.txtC2);
        TextView txtC3 = (TextView) v.findViewById(R.id.txtC3);
        TextView txtC4 = (TextView) v.findViewById(R.id.txtC4);
        TextView txtC5 = (TextView) v.findViewById(R.id.txtC5);
        TextView txtC6 = (TextView) v.findViewById(R.id.txtC6);
        TextView txtC7 = (TextView) v.findViewById(R.id.txtC7);
        txtC7.setVisibility(View.VISIBLE);
        txtC1.setText(R.string.tab_tranpati_c1);
        txtC2.setText(R.string.tab_tranpati_c2);
        txtC3.setText(R.string.tab_tranpati_c3);
        txtC4.setText(R.string.tab_tranpati_c4);
        txtC5.setText(R.string.tab_tranpati_c5);
        txtC6.setText(R.string.tab_tranpati_c6);
        txtC7.setText(R.string.tab_tranpati_c7);
    }

    protected void onFinishedDataLoad() {
        mSwipeRefresh.setRefreshing(false);
    }

    protected void onFinishedSubDataLoad() {
        mSwipeRefreshSub.setRefreshing(false);
    }
}

