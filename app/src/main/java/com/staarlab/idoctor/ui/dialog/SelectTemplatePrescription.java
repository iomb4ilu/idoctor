package com.staarlab.idoctor.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.ExpMestTemplate;
import com.staarlab.idoctor.entity.ICD;
import com.staarlab.idoctor.helper.layout.DividerItemDecoration;
import com.staarlab.idoctor.helper.layout.HeaderRecyclerDividerItemDecorator;
import com.staarlab.idoctor.ui.activity.BaseActivity;
import com.staarlab.idoctor.util.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gun on 3/8/17.
 */

public class SelectTemplatePrescription implements View.OnClickListener {
    protected Context context;
    private Dialog dialog;

    private TextView txtTitle;
    private RecyclerView rvICD;
    private ImageButton imgSearch;
    private ImageButton imgClose;
    private EditText edtQuery;

    private LayoutInflater layoutInflater;
    private RecyclerView.LayoutManager layoutManager;
    private List<ExpMestTemplate> items = new ArrayList<>();
    private TemplatePrescriptionAdapter adapter;

    private String query;
    private OnTemplatePrescriptionSelect listener;

    public SelectTemplatePrescription(Context ctx) {
        this.context = ctx;
        this.layoutManager = new LinearLayoutManager(context);
        layoutInflater = LayoutInflater.from(context);
        this.dialog = new Dialog(this.context);
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.dialog.setContentView(R.layout.dialog_select);
        this.dialog.setCancelable(false);

        initViews();
    }

    private void initViews() {
        query = "";
        imgSearch = (ImageButton) dialog.findViewById(R.id.imgSearch);
        imgClose = (ImageButton) dialog.findViewById(R.id.btnClose);
        edtQuery = (EditText) dialog.findViewById(R.id.edtQuery);
        txtTitle = (TextView) dialog.findViewById(R.id.dialog_title);
        txtTitle.setText(R.string.select_template_prescription);
        edtQuery.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtQuery.getWindowToken(), 0);
                if (!query.equalsIgnoreCase(edtQuery.getText().toString())) {
                    query = StringUtils.getEngStringFromUnicodeString(edtQuery.getText().toString().toLowerCase().trim());
                    doQueryDatabase(query);
                }
                return true;
            }
        });
        rvICD = (RecyclerView) dialog.findViewById(R.id.recycler_view);
        rvICD.setHasFixedSize(true);
        rvICD.setLayoutManager(layoutManager);
        rvICD.addItemDecoration(
                new HeaderRecyclerDividerItemDecorator(
                        context, DividerItemDecoration.VERTICAL_LIST, false, false,
                        R.drawable.divider_0));

        adapter = new TemplatePrescriptionAdapter();
        rvICD.setAdapter(adapter);

        imgClose.setOnClickListener(this);
        imgSearch.setOnClickListener(this);
        new LoadExpMestTemplate().execute(query);

    }
    public void setTemplatePrescriptionSelectListener(OnTemplatePrescriptionSelect l) {
        this.listener = l;
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnClose:
                dismiss();
                break;
            case R.id.imgSearch:
                InputMethodManager imm = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtQuery.getWindowToken(), 0);
                if (!query.equalsIgnoreCase(edtQuery.getText().toString())) {
                    query = StringUtils.getEngStringFromUnicodeString(edtQuery.getText().toString().toLowerCase().trim());
                    doQueryDatabase(query);
                }
                break;
        }
    }

    private void doQueryDatabase(String query) {
        new LoadExpMestTemplate().execute(query);
    }

    // LOAD DATA LOCAL
    class LoadExpMestTemplate extends AsyncTask<String, Void, List<ExpMestTemplate>> {

        @Override
        protected List<ExpMestTemplate> doInBackground(String... params) {
            String q = params[0];
            Dao<ExpMestTemplate, Integer> mestTemplateDao = ((BaseActivity) context).getHelper().getMestTemplateDao();
            QueryBuilder<ExpMestTemplate, Integer> qb = mestTemplateDao.queryBuilder();
            qb.orderByRaw(ExpMestTemplate.FIELD_NAME_EXP_TEMPLATE_NAME + " COLLATE NOCASE");
//            qb.orderBy(ExpMestTemplate.FIELD_NAME_EXP_TEMPLATE_NAME, true);
            if (!((BaseActivity) context).getHelper().isOpen()) {
                Toast.makeText(context, "Database is closed", Toast.LENGTH_SHORT).show();
                return new ArrayList<>();
            }
            try {
                if (q.isEmpty()) {
                    PreparedQuery<ExpMestTemplate> preparedQuery = qb.prepare();
                    return mestTemplateDao.query(preparedQuery);
                } else {
                    qb.where().like(ExpMestTemplate.FIELD_NAME_EXP_TEMPLATE_SEARCH, "%" + q + "%");
                    PreparedQuery<ExpMestTemplate> preparedQuery = qb.prepare();
                    return mestTemplateDao.query(preparedQuery);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new ArrayList<>();
        }

        @Override
        protected void onPostExecute(List<ExpMestTemplate> icdLst) {
            super.onPostExecute(icdLst);
            items = icdLst;
            adapter.notifyDataSetChanged();
        }
    }

    public void show() {
        dialog.show();
    }

    public void dismiss() {
        dialog.dismiss();
    }

    private View.OnClickListener onSelectItem = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getTag() instanceof ExpMestTemplate) {
                ExpMestTemplate item = (ExpMestTemplate) v.getTag();
                listener.onSelected(item);
            }
            dismiss();
        }
    };

    class TemplatePrescriptionAdapter extends RecyclerView.Adapter<TemplatePrescriptionViewHolder> {

        @Override
        public TemplatePrescriptionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = layoutInflater.inflate(R.layout.adapter_select_icd, parent, false);
            return new TemplatePrescriptionViewHolder(view);
        }

        @Override
        public void onBindViewHolder(TemplatePrescriptionViewHolder holder, int position) {

            ExpMestTemplate item = items.get(position);
            holder.tvName.setText(item.EXP_MEST_TEMPLATE_NAME);
            holder.tvCode.setText(item.DESCRIPTION);
            holder.itemView.setTag(item);
            holder.itemView.setOnClickListener(onSelectItem);
        }

        @Override
        public int getItemCount() {
            if (items != null) {
                return items.size();
            }
            return 0;
        }
    }

    class TemplatePrescriptionViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvCode;
        public TemplatePrescriptionViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvCode = (TextView) itemView.findViewById(R.id.tvCode);
        }
    }

    public interface OnTemplatePrescriptionSelect {
        void onSelected(ExpMestTemplate item);
    }
}
