package com.staarlab.idoctor.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.helper.share.MBSSharePreference;

/**
 * Created by PHAMHUNG on 2/19/2017.
 */

public class SelectServerDialog implements View.OnClickListener {
    protected Context context;
    private Dialog dialog;

    private EditText edtServerAuthentication;
    private EditText edtServerAPI;
    private Button btnSave;
    private Button btnCancel;
    private TextView tvError;

    public SelectServerDialog(Context cx) {
        this.context = cx;
        this.dialog = new Dialog(this.context);
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.dialog.setContentView(R.layout.dialog_select_server);
        this.dialog.setCancelable(false);

        initViews();
    }

    private void initViews() {
        edtServerAuthentication = (EditText) dialog.findViewById(R.id.edtServerAuthentication);
        edtServerAPI = (EditText) dialog.findViewById(R.id.edtServerApi);
        btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        btnSave = (Button) dialog.findViewById(R.id.btnSave);
        tvError = (TextView) dialog.findViewById(R.id.tvError);

        edtServerAuthentication.setText(MBSSharePreference.get().getServerAuthentication());
        edtServerAPI.setText(MBSSharePreference.get().getServerAPI());
        btnSave.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    public void show() {
        dialog.show();
    }

    public void dismiss() {
        dialog.dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSave:
                String svAuthen = edtServerAuthentication.getText().toString();
                String svApi = edtServerAPI.getText().toString();
                checkServerLink(svAuthen, svApi);
                MBSSharePreference.get().putServerAuthentication(svAuthen);
                MBSSharePreference.get().putServerAPI(svApi);
                dismiss();
                break;
            case R.id.btnCancel:
                dismiss();
                break;
        }
    }

    private void checkServerLink(String svAuthen, String svApi) {
        if (svAuthen.isEmpty() || svApi.isEmpty()) {
            tvError.setVisibility(View.VISIBLE);
        } else {
            tvError.setVisibility(View.INVISIBLE);
        }
    }
}
