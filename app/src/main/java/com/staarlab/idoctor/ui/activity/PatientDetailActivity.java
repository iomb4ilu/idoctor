package com.staarlab.idoctor.ui.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.Patient;
import com.staarlab.idoctor.entity.TreatmentSummaryInfo;
import com.staarlab.idoctor.model.ResponseArrayModel;
import com.staarlab.idoctor.ui.adapter.PatientTabsPagerAdapter;
import com.staarlab.idoctor.ui.fragment.CommonServiceFragment;
import com.staarlab.idoctor.util.AppConst;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.WSFactory;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PatientDetailActivity extends BaseActivity {
    public static final String P_PATIENT_ID = "P_PATIENT_ID";
    public static final String P_TREATMENT_COMMON_INFO = "P_TREATMENT_COMMON_INFO";
    public static final String P_TAB_CODE = "P_TAB_CODE";

    private PatientTabsPagerAdapter mPatientTabsPagerAdapter;
    private ViewPager mViewPager;
    private ActionBar mActionBar;
    private TabLayout mTabLayout;

    private SharedPreferences sharedPreferences;
    private Patient mPatient;
    private HISService hisService;
    private TreatmentSummaryInfo treatmentSummaryInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);

        long patientId = getIntent().getLongExtra(P_PATIENT_ID, 0L);
        treatmentSummaryInfo = getIntent().getParcelableExtra(P_TREATMENT_COMMON_INFO);
        String tabCode = getIntent().getStringExtra(P_TAB_CODE);

        sharedPreferences = getSharedPreferences(AppConst.APP_PREF_NAME, MODE_PRIVATE);
        hisService = WSFactory.createHisService(sharedPreferences.getString(AppConst.PREF_KEY_TOKEN_CODE, ""));

        mViewPager = (ViewPager) findViewById(R.id.container);
        mPatientTabsPagerAdapter = new PatientTabsPagerAdapter(getSupportFragmentManager(), this);
        mViewPager.setAdapter(mPatientTabsPagerAdapter);
        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mTabLayout.setupWithViewPager(mViewPager);
        showPatientInfo(patientId, treatmentSummaryInfo, tabCode);
    }

    private void showPatientInfo(final long patientId, final TreatmentSummaryInfo info, final String tabCode) {
        if (patientId <= 0) return;

        String param = "{'ApiData':{'ID':'" + patientId + "'}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getPatientInfo(param, new Callback<ResponseArrayModel<Patient>>() {
            @Override
            public void success(ResponseArrayModel<Patient> patientResponseArrayModel, Response response) {
                if (patientResponseArrayModel.getDataList() == null || patientResponseArrayModel.getDataList().size() <= 0) {
                    return;
                }
                mPatient = patientResponseArrayModel.getDataList().get(0);
                if (mPatient != null) {
                    mActionBar.setTitle(mPatient.getVirPatientName() + " (" + mPatient.getPatientCode() + ")");
                    mPatientTabsPagerAdapter.setPatient(mPatient);
                    mPatientTabsPagerAdapter.setTreatmentCommonInfo(info);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            int index = mPatientTabsPagerAdapter.getTabIndexByCode(tabCode);
                            if (index >= 0) {
                                mTabLayout.getTabAt(index).select();
                            }
                        }
                    }, 100);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("XXXX", "Fail: " + error.getMessage());
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Resources rs = getResources();
        List<String> tabTitles = new ArrayList<>();
        tabTitles.add(rs.getString(R.string.tab_title_general));
        if (treatmentSummaryInfo != null) {
            for (int i = 0; i < AppConst.INFO_TABS.length; i++) {
                String code = AppConst.INFO_TABS[i];
                try {
                    Method getMethod = TreatmentSummaryInfo.class.getMethod("get" + code);
                    Float count = (Float) getMethod.invoke(treatmentSummaryInfo);
                    if (count != null && count > 0) {
                        tabTitles.add(rs.getString(AppConst.INFO_TAB_TITLE_IDS[i]));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        for (int i = 0; i < tabTitles.size(); i++) {
            String title = tabTitles.get(i);
            Intent menuIntent = new Intent();
            menuIntent.putExtra("tabIndex", i);
            menu.add(title).setIntent(menuIntent);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        final int tabIndex = item.getIntent().getIntExtra("tabIndex", 0);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mTabLayout.getTabAt(tabIndex).select();
            }
        }, 100);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        Fragment f = mPatientTabsPagerAdapter.getItem(mTabLayout.getSelectedTabPosition());
//        if (f != null && f instanceof CommonServiceFragment) {
//            if (((CommonServiceFragment) f).isShowingRightPanel()) {
//                ((CommonServiceFragment) f).hideRightPanel();
//                return;
//            }
//            super.onBackPressed();
//        }
        super.onBackPressed();
    }

    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_patient_detail, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }
}
