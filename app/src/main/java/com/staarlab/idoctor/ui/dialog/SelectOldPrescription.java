package com.staarlab.idoctor.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.Prescription;
import com.staarlab.idoctor.helper.layout.DividerItemDecoration;
import com.staarlab.idoctor.helper.layout.HeaderRecyclerDividerItemDecorator;
import com.staarlab.idoctor.helper.share.MBSSharePreference;
import com.staarlab.idoctor.model.ResponseArrayModel;
import com.staarlab.idoctor.util.CommonUtil;
import com.staarlab.idoctor.util.StringUtils;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.WSFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by PHAMHUNG on 3/4/2017.
 */

public class SelectOldPrescription implements View.OnClickListener {
    private static final String TAG = SelectMedicalStockDialog.class.getName();
    protected Context context;
    private long treatmentId;

    private Dialog dialog;
    private LayoutInflater layoutInflater;
    private RecyclerView.LayoutManager layoutManager;
    private View viewSearch;
    private ImageButton imgClose;
    private TextView txtTitle;
    RecyclerView rvOldPresciprion;
    private OldPrescriptionAdapter adapter;
    private List<Prescription> items = new ArrayList<>();

    private HISService hisService;
    private onOldPrescriptionSelected listener;

    public SelectOldPrescription(Context ctx, long treatmentId) {
        this.context = ctx;
        this.treatmentId = treatmentId;

        this.layoutManager = new LinearLayoutManager(context);
        layoutInflater = LayoutInflater.from(context);
        this.dialog = new Dialog(this.context);
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.dialog.setContentView(R.layout.dialog_select);
        this.dialog.setCancelable(false);
        this.hisService = WSFactory.createHisService(MBSSharePreference.get().getTokenCode());
        initViews();
    }

    public void setListener(onOldPrescriptionSelected listener){
        this.listener = listener;
    }

    private void initViews() {
        viewSearch = dialog.findViewById(R.id.viewSearch);
        viewSearch.setVisibility(View.GONE);
        txtTitle = (TextView) dialog.findViewById(R.id.dialog_title);
        txtTitle.setText(R.string.select_old_prescription);
        imgClose = (ImageButton) dialog.findViewById(R.id.btnClose);
        imgClose.setOnClickListener(this);
        rvOldPresciprion = (RecyclerView) dialog.findViewById(R.id.recycler_view);
        rvOldPresciprion.setHasFixedSize(true);
        rvOldPresciprion.setLayoutManager(layoutManager);
        rvOldPresciprion.addItemDecoration(
                new HeaderRecyclerDividerItemDecorator(
                        context, DividerItemDecoration.VERTICAL_LIST, false, false,
                        R.drawable.divider_0));

        adapter = new OldPrescriptionAdapter();
        rvOldPresciprion.setAdapter(adapter);
        getListOfOldPrescription();
    }

    private void getListOfOldPrescription() {
        String param = "{'ApiData':{'TREATMENT_ID':'" + treatmentId + "'}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getHisPrescription(param, new Callback<ResponseArrayModel<Prescription>>() {
            @Override
            public void success(ResponseArrayModel<Prescription> responseArrayModel, Response response) {
                if (responseArrayModel.isSuccess()) {
                    items.clear();
                    items.addAll(responseArrayModel.getDataList());
                    Collections.sort(items, new Comparator<Prescription>() {
                        @Override
                        public int compare(Prescription lhs, Prescription rhs) {
                            if (lhs == null || lhs.getIntructionTime() == null) return 1;
                            else if (rhs == null || rhs.getIntructionTime() == null) return -1;
                            return lhs.getIntructionTime().compareToIgnoreCase(rhs.getIntructionTime()) * (-1);
                        }
                    });
                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(context, R.string.select_load_fail, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(context, R.string.select_load_fail, Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void show() {
        dialog.show();
    }

    public void dismiss() {
        dialog.dismiss();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnClose:
                dismiss();
                break;
        }
    }

    private View.OnClickListener onSelectItem = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getTag() instanceof Prescription) {
                Prescription item = (Prescription) v.getTag();
                listener.onSelected(item);
            }
            dismiss();
        }
    };


    private class OldPrescriptionAdapter extends RecyclerView.Adapter {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = layoutInflater.inflate(R.layout.adapter_select_icd, parent, false);
            return new OldPrescriptionViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            OldPrescriptionViewHolder vh = (OldPrescriptionViewHolder) holder;
            Prescription item = items.get(position);
            vh.tvName.setText(StringUtils.joinWithDashes(context, item.getExpMestCode(), CommonUtil.hisString2DateTimeAsString(item.getIntructionTime())));
            vh.tvCode.setText(item.getIcdMainText());
            vh.itemView.setTag(item);
            vh.itemView.setOnClickListener(onSelectItem);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }

    private class OldPrescriptionViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        TextView tvCode;

        public OldPrescriptionViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvCode = (TextView) itemView.findViewById(R.id.tvCode);

        }
    }


    public interface onOldPrescriptionSelected {
        void onSelected(Prescription prescription);
    }
}