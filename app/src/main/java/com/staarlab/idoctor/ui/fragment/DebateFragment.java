package com.staarlab.idoctor.ui.fragment;

import android.util.Base64;
import android.util.Log;

import com.staarlab.idoctor.entity.CommonServiceEntity;
import com.staarlab.idoctor.entity.DebateServiceEntity;
import com.staarlab.idoctor.model.ResponseArrayModel;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DebateFragment extends CommonServiceFragment {
    @Override
    protected void loadData() {
        Long treatmentId = getArguments().getLong(ARG_TREATMENT_ID, -1L);
        String param = "{'ApiData':{'TREATMENT_ID':'" + treatmentId + "'}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getHisDebate(param, new Callback<ResponseArrayModel<DebateServiceEntity>>() {
            @Override
            public void success(ResponseArrayModel<DebateServiceEntity> responseArrayModel, Response response) {
                mAdapter.setDataList(responseArrayModel.getDataList());
                onFinishedDataLoad();
            }

            @Override
            public void failure(RetrofitError error) {
                onFinishedDataLoad();
            }
        });
    }

    @Override
    protected void onLstDataItemClick(int position) {

    }
}
