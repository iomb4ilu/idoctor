package com.staarlab.idoctor.ui.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.Patient;
import com.staarlab.idoctor.entity.TreatmentCommonInfo;
import com.staarlab.idoctor.entity.TreatmentFee;
import com.staarlab.idoctor.model.ResponseArrayModel;
import com.staarlab.idoctor.model.ResponseObjectModel;
import com.staarlab.idoctor.util.AppConst;
import com.staarlab.idoctor.util.CommonUtil;
import com.staarlab.idoctor.util.NumberFormatUtils;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.WSFactory;

import java.math.BigDecimal;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Author: Lucero
 * Created: 12/28/16.
 */

public class PatientInfoFragment extends Fragment {
    public static final String ARG_PATIENT = "ARG_PATIENT";
    public static final String ARG_TREATMENT_ID = "ARG_TREATMENT_ID";
    private HISService hisService;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_general_info_tab, container, false);
        Patient patient = (Patient) getArguments().getSerializable(ARG_PATIENT);
        Long treatmentId = getArguments().getLong(ARG_TREATMENT_ID);
        SharedPreferences sharedPref = getContext().getSharedPreferences(AppConst.APP_PREF_NAME, Context.MODE_PRIVATE);
        hisService = WSFactory.createHisService(sharedPref.getString(AppConst.PREF_KEY_TOKEN_CODE, ""));
        if (patient != null) {
            TextView txtPatientCode = (TextView) v.findViewById(R.id.txtPatientCode);
            TextView txtPatientNational = (TextView) v.findViewById(R.id.txtPatientNational);
            TextView txtPatientPhone = (TextView) v.findViewById(R.id.txtPatientPhone);
            TextView txtPatientFamily = (TextView) v.findViewById(R.id.txtPatientFamily);
            TextView txtPatientDob = (TextView) v.findViewById(R.id.txtPatientDob);

            TextView txtPatientName = (TextView) v.findViewById(R.id.txtPatientName);
            TextView txtPatientFolk = (TextView) v.findViewById(R.id.txtPatientFolk);
            TextView txtPatientAddress = (TextView) v.findViewById(R.id.txtPatientAddress);
            TextView txtPatientRelationship = (TextView) v.findViewById(R.id.txtPatientRelationship);
            TextView txtPatientSex = (TextView) v.findViewById(R.id.txtPatientSex);

            txtPatientCode.setText(patient.getPatientCode());
            txtPatientNational.setText(patient.getNationalName());
            txtPatientPhone.setText(patient.getPhone());
            txtPatientFamily.setText(patient.getRelativeName());
            txtPatientDob.setText(CommonUtil.hisString2DateAsString(patient.getDob()));

            txtPatientName.setText(patient.getVirPatientName());
            txtPatientFolk.setText(patient.getEthnicName());
            txtPatientAddress.setText(patient.getVirAddress());
            txtPatientRelationship.setText(patient.getRelativeType());
            txtPatientSex.setText(patient.getGenderName());

            loadTreatmentInfo(v, treatmentId);
            loadFeeInfo(v, treatmentId);
        }
        return v;
    }

    private void loadTreatmentInfo(final View v, Long treatmentId) {
        String param = "{'ApiData':" + treatmentId + "}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getHisTreatmentCommonInfo(param, new Callback<ResponseObjectModel<TreatmentCommonInfo>>() {
            @Override
            public void success(ResponseObjectModel<TreatmentCommonInfo> responseModel, Response response) {
                if (responseModel.getDataResult() == null) {
                    return;
                }
                TreatmentCommonInfo info = responseModel.getDataResult();
                TextView txtTreatmentCode = (TextView) v.findViewById(R.id.txtTreatmentCode);
                TextView txtDeptName = (TextView) v.findViewById(R.id.txtDeptName);
                TextView txtInTime = (TextView) v.findViewById(R.id.txtInTime);
                TextView txtStatus = (TextView) v.findViewById(R.id.txtStatus);
                TextView txtRoom = (TextView) v.findViewById(R.id.txtRoom);
                TextView txtOutTime = (TextView) v.findViewById(R.id.txtOutTime);
                TextView txtResult = (TextView) v.findViewById(R.id.txtResult);

                txtTreatmentCode.setText(info.TREATMENT_CODE);
                txtDeptName.setText(info.END_DEPARTMENT_NAME);
                txtInTime.setText(CommonUtil.hisString2DateTimeAsString(String.valueOf(info.IN_TIME)));
                txtStatus.setText(info.TREATMENT_END_TYPE_NAME);
                txtRoom.setText(info.END_ROOM_NAME);
                txtOutTime.setText(CommonUtil.hisString2DateTimeAsString(String.valueOf(info.OUT_TIME)));
                txtResult.setText(info.TREATMENT_END_TYPE_NAME);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void loadFeeInfo(final View v, Long treatmentId) {
        String param = "{'ApiData':{'ID':'" + treatmentId + "'}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getHisTreatmentFee(param, new Callback<ResponseArrayModel<TreatmentFee>>() {
            @Override
            public void success(ResponseArrayModel<TreatmentFee> responseArrayModel, Response response) {
                TreatmentFee fee = responseArrayModel.getDataList().get(0);
                TextView txtTotalPrice = (TextView) v.findViewById(R.id.txtTotalPrice);
                TextView txtTotalHeinPrice = (TextView) v.findViewById(R.id.txtTotalHeinPrice);
                TextView txtTotalPatientPrice = (TextView) v.findViewById(R.id.txtTotalPatientPrice);
                TextView txtTotalDiscount = (TextView) v.findViewById(R.id.txtTotalDiscount);
                TextView txtDaThu = (TextView) v.findViewById(R.id.txtDaThu);
                TextView txtCanThuThem = (TextView) v.findViewById(R.id.txtCanThuThem);
                TextView txtTotalDeposit = (TextView) v.findViewById(R.id.txtTotalDeposit);
                TextView txtTotalBillAmount = (TextView) v.findViewById(R.id.txtTotalBillAmount);
                TextView txtTotalBillTransfer = (TextView) v.findViewById(R.id.txtTotalBillTransfer);
                TextView txtTotalRepay = (TextView) v.findViewById(R.id.txtTotalRepay);


                txtTotalPrice.setText(NumberFormatUtils.formatMoney(fee.getTotalPrice()));
                txtTotalHeinPrice.setText(NumberFormatUtils.formatMoney(fee.getTotalHeinPrice()));
                txtTotalPatientPrice.setText(NumberFormatUtils.formatMoney(fee.getTotalPatientPrice()));
                txtTotalDiscount.setText(NumberFormatUtils.formatMoney(fee.getTotalDiscount()));
                txtTotalDeposit.setText(NumberFormatUtils.formatMoney(fee.getTotalDepositAmount()));
                BigDecimal received = fee.getTotalDepositAmount().add(fee.getTotalBillAmount()).subtract(fee.getTotalBillTransferAmount())
                        .subtract(fee.getTotalRepayAmount()).subtract(fee.getTotalBillExemption());
                BigDecimal receivable = fee.getTotalPatientPrice().subtract(received).subtract(fee.getTotalBillExemption());
                txtDaThu.setText(NumberFormatUtils.formatMoney(received));
                txtCanThuThem.setText(NumberFormatUtils.formatMoney(receivable));
                txtTotalBillAmount.setText(NumberFormatUtils.formatMoney(fee.getTotalBillAmount()));
                txtTotalBillTransfer.setText(NumberFormatUtils.formatMoney(fee.getTotalBillTransferAmount()));
                txtTotalRepay.setText(NumberFormatUtils.formatMoney(fee.getTotalRepayAmount()));
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
