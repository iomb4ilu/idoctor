package com.staarlab.idoctor.ui.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.staarlab.idoctor.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ducln on 18/11/2016.
 */

public class ChooseMedicineFragment extends DialogFragment implements View.OnClickListener {
    private Context context;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        context = getActivity();
        View viewDialog = getActivity().getLayoutInflater().inflate(R.layout.fragment_choose_medicine, null);

        Spinner spinner = (Spinner) viewDialog.findViewById(R.id.spinner);
        List<String> list = new ArrayList<String>();
        list.add("Viên");
        list.add("Hộp");
        list.add("Ống");
        list.add("Lọ");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        Spinner spinner2 = (Spinner) viewDialog.findViewById(R.id.spinnerUseWay);
        List<String> list2 = new ArrayList<String>();
        list2.add("Uống");
        list2.add("Tiếm");
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, list2);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(dataAdapter2);

        Button ibSave = (Button) viewDialog.findViewById(R.id.ibSave);
        ibSave.setOnClickListener(this);
        Button  ibCancel = (Button) viewDialog.findViewById(R.id.ibCancel);
        ibCancel.setOnClickListener(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(viewDialog);
        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ibCancel:
                dismiss();
                break;
            case R.id.ibSave:
                dismiss();
                break;
            default:
                break;
        }
    }
}
