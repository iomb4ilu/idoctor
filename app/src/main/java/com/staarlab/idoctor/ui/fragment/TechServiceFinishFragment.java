package com.staarlab.idoctor.ui.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.ICD;
import com.staarlab.idoctor.entity.ServiceGroup;
import com.staarlab.idoctor.entity.Treatment;
import com.staarlab.idoctor.entity.TreatmentFee;
import com.staarlab.idoctor.helper.layout.DividerItemDecoration;
import com.staarlab.idoctor.helper.layout.HeaderRecyclerDividerItemDecorator;
import com.staarlab.idoctor.helper.share.MBSSharePreference;
import com.staarlab.idoctor.model.BodyModel;
import com.staarlab.idoctor.model.PostResponModel;
import com.staarlab.idoctor.ui.activity.BaseActivity;
import com.staarlab.idoctor.util.HardCodeUtils;
import com.staarlab.idoctor.util.NumberFormatUtils;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.WSFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class TechServiceFinishFragment extends BaseFragment {
    private static final String TAG = TechServiceFinishFragment.class.getName();
    Treatment treatment;
    private TreatmentFee fee;
    private ICD icd;
    private TechServiceSpecifyFragment.TechServiceHolder[] lstSelectedTechService;
    private boolean isPriority;
    private long applyTime;

    private TextView tvPatientName;
    private TextView tvTreatmentCode;
    private TextView tvMustPay;
    private TextView tvMustPayBN;
    Toolbar toolbar;

    private RecyclerView rvSelectedService;
    SelectedFinishAdapter adapter;
    ProgressDialog mProgressDialog;

    private HISService hisService;

    public TechServiceFinishFragment() {
        // Required empty public constructor
    }


    public static TechServiceFinishFragment newInstance(Treatment tm, TreatmentFee fee, ICD icd, TechServiceSpecifyFragment.TechServiceHolder[] selectedService, boolean isPriority, long applyTime) {
        TechServiceFinishFragment fragment = new TechServiceFinishFragment();
        Bundle args = new Bundle();
        args.putParcelable("tm", tm);
        args.putParcelable("icd", icd);
        args.putParcelable("fee", fee);
        args.putParcelableArray("lss", selectedService);
        args.putBoolean("b", isPriority);
        args.putLong("ap", applyTime);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            treatment = getArguments().getParcelable("tm");
            icd = getArguments().getParcelable("icd");
            fee = getArguments().getParcelable("fee");
            Parcelable[] tempParcelable = getArguments().getParcelableArray("lss");
            lstSelectedTechService = new TechServiceSpecifyFragment.TechServiceHolder[tempParcelable.length];
            for (int i = 0; i < tempParcelable.length; i++) {
                lstSelectedTechService[i] = (TechServiceSpecifyFragment.TechServiceHolder) tempParcelable[i];
            }
            isPriority = getArguments().getBoolean("b");
            applyTime = getArguments().getLong("ap");
        }

        adapter = new SelectedFinishAdapter();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tech_service_finish, container, false);
        tvPatientName = (TextView) view.findViewById(R.id.tvPatientName);
        tvTreatmentCode = (TextView) view.findViewById(R.id.tvTreatmentCode);
        tvMustPay = (TextView) view.findViewById(R.id.tvMustPay);
        tvMustPayBN = (TextView) view.findViewById(R.id.tvMustPayBN);

        tvPatientName.setText(treatment.getVirPatientName());
        tvTreatmentCode.setText(treatment.getTreatmentCode());
        tvMustPay.setText(fee == null || fee.getTotalPrice() == null ? "0" : NumberFormatUtils.formatMoney(fee.getTotalPrice()));
        tvMustPayBN.setText(fee == null || fee.getTotalPatientPrice() == null ? "0" : NumberFormatUtils.formatMoney(fee.getTotalPatientPrice()));

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        if (toolbar != null) {
            ((BaseActivity) getActivity()).setSupportActionBar(toolbar);
        }
        (getActivity()).setTitle(R.string.finish_service_title);

        rvSelectedService = (RecyclerView) view.findViewById(R.id.rvSelectedService);
        rvSelectedService.setHasFixedSize(true);
        rvSelectedService.setLayoutManager(new LinearLayoutManager(context));
        rvSelectedService.addItemDecoration(
                new HeaderRecyclerDividerItemDecorator(
                        context, DividerItemDecoration.VERTICAL_LIST, false, false,
                        R.drawable.divider_104));
        rvSelectedService.setAdapter(adapter);

        hisService = WSFactory.createHisService(MBSSharePreference.get().getTokenCode());
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(context);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setMessage(getString(R.string.sending_data));
            }
            mProgressDialog.show();
            postData();
        }
        return super.onOptionsItemSelected(item);
    }

    private void postData() {
        HisServiceReqSDO data = new HisServiceReqSDO();
        data.IcdId = icd.ID;
        data.IcdText = icd.ICD_TEXT;
        data.IcdMainText = icd.ICD_MAIN_NAME;
        data.TreatmentId = treatment.getTreatmentId();
        data.Priority = isPriority ? Long.valueOf(1) : Long.valueOf(0);
        data.InstructionTime = applyTime;
        List<ServiceReqDetailSDO> lstData = new ArrayList<>();
        for (TechServiceSpecifyFragment.TechServiceHolder service : lstSelectedTechService) {
            ServiceReqDetailSDO sdo = new ServiceReqDetailSDO();
            sdo.PatientTypeId = service.isWithBHXH ? HardCodeUtils.PATIENT_TYPE_ID_BHXH : service.patientType.ID;
            sdo.ServiceId = service.service.id;
            sdo.RoomId = null;
            sdo.EmbedPatientTypeId = service.isWithBHXH ? service.patientType.ID : null;
            sdo.Amount = BigDecimal.valueOf(service.number);
            sdo.IsExpend = service.isExpend ? 1 : 0;
            lstData.add(sdo);
        }
        data.ServiceReqDetails = lstData;
        BodyModel<HisServiceReqSDO> bodyModel = new BodyModel<>(data);
        hisService.postSpecifyService(bodyModel, new Callback<PostResponModel>() {

            @Override
            public void success(PostResponModel postResponModel, Response response) {
                if (postResponModel.isSuccess()) {
                    showDialogConfirmSuccess();
                } else {
                    String error = postResponModel.getParam().getMessages() != null && postResponModel.getParam().getMessages().size() > 0 ? postResponModel.getParam().getMessages().get(0) : getString(R.string.finish_service_post_fail);
                    Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                }
                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(context, R.string.finish_service_post_fail, Toast.LENGTH_SHORT).show();
                if (mProgressDialog != null) {
                    mProgressDialog.dismiss();
                }
            }
        });
    }

    private void showDialogConfirmSuccess() {
        Dialog.Builder builder = null;
        builder = new SimpleDialog.Builder(R.style.Material_App_Dialog_Simple_Light) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                super.onPositiveActionClicked(fragment);
                getActivity().finish();
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
            }
        };

        ((SimpleDialog.Builder) builder)
                .message(getString(R.string.finish_service_post_success))
                .title(getString(R.string.notify))
                .positiveAction(getString(R.string.confirm));
        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.setCancelable(false);
        fragment.show(getFragmentManager(), null);
    }

    public class HisServiceReqSDO {
        public List<ServiceReqDetailSDO> ServiceReqDetails;
        public Long Id = null;
        public Long ParentId = null;  //HIS_SERVICE_REQ
        public Long ExecuteGroupId = null;
        public long IcdId;
        public String IcdText;
        public String IcdMainText;
        public long TreatmentId;
        public long InstructionTime;
        public Long Priority = null;
        public Integer IsNotRequireFee = null;

        public HisServiceReqSDO() {
        }
    }

    class ServiceReqDetailSDO {
        public long PatientTypeId;
        public long ServiceId;
        public Long ParentId = null;
        public Long RoomId = null;
        public Long EmbedPatientTypeId = null;
        public BigDecimal Amount;
        public Integer IsExpend = null;
        public Integer IsOutKtcFee = null;

        public ServiceReqDetailSDO() {
        }
    }

    class SelectedFinishAdapter extends RecyclerView.Adapter<SelectedFinishViewHolder> {

        @Override
        public SelectedFinishViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.adapter_specify_service_finish_list_item, parent, false);
            return new SelectedFinishViewHolder(view);
        }

        @Override
        public void onBindViewHolder(SelectedFinishViewHolder holder, int position) {
            TechServiceSpecifyFragment.TechServiceHolder item = lstSelectedTechService[position];
            holder.tvServiceCode.setText(item.service.serviceCode);
            holder.tvServiceName.setText(item.service.serviceName);
            holder.tvPatientType.setText(item.patientType.PATIENT_TYPE_NAME);
            holder.tvNumber.setText(String.valueOf(item.number));
            holder.tvExpend.setText(item.isExpend ? getString(R.string.yes) : getString(R.string.no));
            holder.tvWithBHYT.setText(item.isWithBHXH ? getString(R.string.yes) : getString(R.string.no));
            holder.tvPriceUnit.setText(item.isExpend ? "0" : item.priceUnit == null ? "0" : NumberFormatUtils.formatMoney(item.priceUnit));
        }

        @Override
        public int getItemCount() {
            return lstSelectedTechService != null ? lstSelectedTechService.length : 0;
        }
    }

    class SelectedFinishViewHolder extends RecyclerView.ViewHolder {

        TextView tvServiceCode;
        TextView tvServiceName;
        TextView tvPatientType;
        TextView tvNumber;
        TextView tvExpend;
        TextView tvWithBHYT;
        TextView tvPriceUnit;

        public SelectedFinishViewHolder(View itemView) {
            super(itemView);
            tvServiceCode = (TextView) itemView.findViewById(R.id.tvServiceInfo);
            tvServiceName = (TextView) itemView.findViewById(R.id.tvServiceName);
            tvPatientType = (TextView) itemView.findViewById(R.id.tvPatientType);
            tvNumber = (TextView) itemView.findViewById(R.id.tvNumber);
            tvExpend = (TextView) itemView.findViewById(R.id.tvExpend);
            tvWithBHYT = (TextView) itemView.findViewById(R.id.tvWithBHYT);
            tvPriceUnit = (TextView) itemView.findViewById(R.id.tvPriceUnit);
        }
    }
}


