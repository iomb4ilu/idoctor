package com.staarlab.idoctor.ui.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.EmteMaterial;
import com.staarlab.idoctor.entity.EmteMedicine;
import com.staarlab.idoctor.entity.ICD;
import com.staarlab.idoctor.entity.Material;
import com.staarlab.idoctor.entity.MaterialOldPrescription;
import com.staarlab.idoctor.entity.MedicalStock;
import com.staarlab.idoctor.entity.Medicine;
import com.staarlab.idoctor.entity.MedicineMaterial;
import com.staarlab.idoctor.entity.MedicineOldPrescription;
import com.staarlab.idoctor.entity.MedicineUserForm;
import com.staarlab.idoctor.entity.PatientType;
import com.staarlab.idoctor.entity.SereServ;
import com.staarlab.idoctor.entity.ServicePATY;
import com.staarlab.idoctor.entity.TreatmentCommonInfo;
import com.staarlab.idoctor.helper.layout.SlidingTabLayout;
import com.staarlab.idoctor.helper.layout.ViewEmptyStateLayout;
import com.staarlab.idoctor.helper.share.MBSSharePreference;
import com.staarlab.idoctor.model.ResponseArrayModel;
import com.staarlab.idoctor.ui.activity.BaseActivity;
import com.staarlab.idoctor.ui.dialog.SelectMedicineMaterialDialog;
import com.staarlab.idoctor.util.DateTimeUtils;
import com.staarlab.idoctor.util.NumberFormatUtils;
import com.staarlab.idoctor.util.StringUtils;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.WSFactory;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MedicineMaterialSpecifyFragment extends BaseFragment implements MedicineMaterialListFragment.OnSelectMedicineMaterial, MedicineMaterialListFragment.IUpdateInventory {
    private static final int FINISH_LOAD_INVENTORY = 1;
    private final int FINISH_LOAD_OLD_OR_TEMPLATE_PRESCRIPTION = 2;

    private TreatmentCommonInfo treatmentCommonInfo;
    private ICD icd;
    private long roomId;
    private MedicalStock medicalStock;
    private Long numberRemedy;
    private String advice;
    private long instructionTime, useTime;
    private long oldMestId;
    private long templateId;
    private HISService hisService;

    private LeftSideViewHolder leftSideViewHolder;
    private RightSideViewHolder rightSideViewHolder;

    private LinearLayoutManager layoutManager;
    List<MedicineMaterialHolder> lstSelectedMedicineMaterial = new ArrayList<>();

    private Map<Long, MedicineMaterialHolder> mapSelectedMedicineMaterial = new LinkedHashMap<>();
    private List<Long> lstPlacedServiceToday = new ArrayList<>();
    private Map<Long, MedicineMaterial> mapInventory = new ConcurrentHashMap<>();

    //Old Prescription
    HashMap<Long, MedicineOldPrescription> mapMedicineOldPrescription = new HashMap<>();
    HashMap<Long, MaterialOldPrescription> mapMaterialOldPrescription = new HashMap<>();

    //Template Prescription
    HashMap<Long, EmteMedicine> mapEmteMedicineTemplatePrescription = new HashMap<>();
    HashMap<Long, EmteMaterial> mapEmteMaterialTemplatePrescription = new HashMap<>();

    // Map ServicePATY
    Map<Long, ServicePATY> mapServicePatyForOldPrescription = new ConcurrentHashMap<>();
    Map<Long, ServicePATY> mapServicePatyForTemplatePrescription = new ConcurrentHashMap<>();
    private boolean isFirstInit = true;

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == FINISH_LOAD_INVENTORY) {
                // Placed from old prescription if any
                if (oldMestId != 0l) {
                    mapServicePatyForOldPrescription.clear();
                    getMedicineMaterialFromOldPrescription(oldMestId);
                }
                // Placed from template presciption if any
                if (templateId != 0l) {
                    mapServicePatyForTemplatePrescription.clear();
                    getMedicineMaterialFromTemplate(templateId);
                }
            }
            return false;
        }
    });

    public static MedicineMaterialSpecifyFragment newInstance(TreatmentCommonInfo treatment, long roomId,
                                                              ICD icd, MedicalStock stock, long remedies, String advice,
                                                              long instructionT, long useT, long mestId, long templateId) {
        MedicineMaterialSpecifyFragment fragment = new MedicineMaterialSpecifyFragment();
        Bundle args = new Bundle();
        args.putParcelable("TREATMENT", treatment);
        args.putParcelable("ICD", icd);
        args.putParcelable("stockId", stock);
        args.putLong("p", remedies);
        args.putLong("l", roomId);
        args.putLong("li", instructionT);
        args.putLong("lu", useT);
        args.putLong("old", mestId);
        args.putLong("template", templateId);
        args.putString("str", advice);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            treatmentCommonInfo = getArguments().getParcelable("TREATMENT");
            icd = getArguments().getParcelable("ICD");
            roomId = getArguments().getLong("l");
            medicalStock = getArguments().getParcelable("stockId");
            numberRemedy = getArguments().getLong("p");
            instructionTime = getArguments().getLong("li");
            useTime = getArguments().getLong("lu");
            oldMestId = getArguments().getLong("old");
            templateId = getArguments().getLong("template");
            advice = getArguments().getString("str");
        }
        hisService = WSFactory.createHisService(MBSSharePreference.get().getTokenCode());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_medicine_material_specify, container, false);
        // Get placed service today
        String param = "{'ApiData':{'TREATMENT_ID':" + treatmentCommonInfo.patientTypeAlter.TREATMENT_ID + ",'INTRUCTION_TIME_FROM':" + DateTimeUtils.getBeginingTimeOfToday() + ",'INTRUCTION_TIME_TO':" + DateTimeUtils.getEndTimeOfToday() + "}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getListOfPlacedService(param, new Callback<ResponseArrayModel<SereServ>>() {
            @Override
            public void success(ResponseArrayModel<SereServ> sereServResponseArrayModel, Response response) {
                lstPlacedServiceToday.clear();
                for (SereServ ss : sereServResponseArrayModel.getDataList()) {
                    lstPlacedServiceToday.add(ss.SERVICE_ID);
                }
                findViews(view);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(context, R.string.specify_service_get_placed_service_fail, Toast.LENGTH_SHORT).show();
                findViews(view);
            }

        });
        return view;
    }

    boolean isGetMedicineFromOldFinish = false;
    boolean isGetMaterialFromOldFinish = false;
    boolean isGetMedicineFromTemplateFinish = false;
    boolean isGetMaterialFromTemplateFinish = false;

    private void getMedicineMaterialFromOldPrescription(long oldMestId) {
        String param = "{'ApiData':{'EXP_MEST_ID':" + oldMestId + "}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getMedicineFromOldPrescription(param, new Callback<ResponseArrayModel<MedicineOldPrescription>>() {
            @Override
            public void success(ResponseArrayModel<MedicineOldPrescription> medicineMaterialOldPrescriptionResponseArrayModel, Response response) {
                if (medicineMaterialOldPrescriptionResponseArrayModel.isSuccess()) {
                    mapMedicineOldPrescription.clear();
                    for (MedicineOldPrescription old : medicineMaterialOldPrescriptionResponseArrayModel.getDataList()) {
                        mapMedicineOldPrescription.put(old.SERVICE_ID, old);
                    }
                }
                if (mapMedicineOldPrescription.size() > 0) {
                    new CalculatePriceForOldPrescription().execute(true);
                } else {
                    isGetMedicineFromOldFinish = true;
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

        hisService.getMaterialFromOldPrescription(param, new Callback<ResponseArrayModel<MaterialOldPrescription>>() {
            @Override
            public void success(ResponseArrayModel<MaterialOldPrescription> medicineMaterialOldPrescriptionResponseArrayModel, Response response) {
                if (medicineMaterialOldPrescriptionResponseArrayModel.isSuccess()) {
                    mapMaterialOldPrescription.clear();
                    for (MaterialOldPrescription old : medicineMaterialOldPrescriptionResponseArrayModel.getDataList()) {
                        mapMaterialOldPrescription.put(old.SERVICE_ID, old);
                    }
                }
                if (mapMaterialOldPrescription.size() > 0) {
                    new CalculatePriceForOldPrescription().execute(false);
                } else {
                    isGetMaterialFromOldFinish = true;
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    private void getMedicineMaterialFromTemplate(long templateId) {
        new GetMedicineFromTemplate().execute(templateId);
        new GetMaterialFromTemplate().execute(templateId);

    }

    private void findViews(View view) {
        layoutManager = new LinearLayoutManager(context);
        leftSideViewHolder = new LeftSideViewHolder(view);
        rightSideViewHolder = new RightSideViewHolder(view);
        rebindData();
    }

    @Override
    public void onSelectedMedicineMaterial(double number, MedicineMaterial medicineMaterial, PatientType patientType, MedicineUserForm userForm, boolean isExpend, boolean isWithBHYT, BigDecimal priceUnit, String tut) {
        MedicineMaterialHolder holder = new MedicineMaterialHolder();
        holder.number = number;
        holder.userForm = userForm;
        holder.medicineMaterial = medicineMaterial;
        holder.patientType = patientType;
        holder.isExpend = isExpend;
        holder.isWithBHXH = isWithBHYT;
        holder.priceUnit = priceUnit;
        holder.tutorial = tut;
        if (mapSelectedMedicineMaterial.containsKey(medicineMaterial.ServiceId)){
            holder.number = holder.number + mapSelectedMedicineMaterial.get(medicineMaterial.ServiceId).number;
        }
        mapSelectedMedicineMaterial.put(medicineMaterial.ServiceId, holder);
        rebindData();
    }

    private void rebindData() {
        lstSelectedMedicineMaterial.clear();
        rightSideViewHolder.total = BigDecimal.ZERO;
        if (mapSelectedMedicineMaterial.size() > 0) {
            for (MedicineMaterialHolder m : mapSelectedMedicineMaterial.values()) {
                lstSelectedMedicineMaterial.add(m);
                if (!m.isExpend) {
                    rightSideViewHolder.total = rightSideViewHolder.total.add(m.priceUnit.multiply(BigDecimal.valueOf(m.number)));
                }
            }
            rightSideViewHolder.viewState.updateViewState(rightSideViewHolder.viewStateSelectedService = ViewEmptyStateLayout.VIEW_STATE_NORMAL);
        } else {
            rightSideViewHolder.viewState.updateViewState(rightSideViewHolder.viewStateSelectedService = ViewEmptyStateLayout.VIEW_STATE_EMPTY);
        }
        rightSideViewHolder.tvTotal.setText(NumberFormatUtils.formatMoney(rightSideViewHolder.total));
        rightSideViewHolder.adapter.notifyDataSetChanged();
    }

    boolean isUpdateInventoryFromMedicine = false;
    boolean isUpdateInventoryFromMaterial = false;

    @Override
    public void updateInventory(List<MedicineMaterial> data, int fromUpdate) {
        if (fromUpdate == MedicineMaterialListFragment.UPDATE_FROM_MATERIAL) {
            isUpdateInventoryFromMaterial = true;
        } else if (fromUpdate == MedicineMaterialListFragment.UPDATE_FROM_MEDICINE) {
            isUpdateInventoryFromMedicine = true;
        }
        if (data != null && data.size() > 0) {
            for (MedicineMaterial mm : data) {
                mapInventory.put(mm.ServiceId, mm);
            }
        }
        if (isFirstInit && isUpdateInventoryFromMaterial && isUpdateInventoryFromMedicine) {
            isFirstInit = false;
            Message mss = mHandler.obtainMessage(FINISH_LOAD_INVENTORY);
            mHandler.sendMessage(mss);
        }
    }

    public class LeftSideViewHolder {
        Toolbar toolbarTitle;
        ViewPager mViewPager;
        SlidingTabLayout mSlidingTabLayout;

        String filterString = "";

        public LeftSideViewHolder(View view) {
            toolbarTitle = (Toolbar) view.findViewById(R.id.toolbarLeftAbove);
            toolbarTitle.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
            toolbarTitle.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().onBackPressed();
                }
            });
            ((BaseActivity) getActivity()).setSupportActionBar(toolbarTitle);
            setTitleResource(R.string.specify_medicine_material_title);
            mViewPager = (ViewPager) view.findViewById(R.id.view_pager);
            mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);

            mViewPager.setAdapter(new MyDummyPagerAdapter(getChildFragmentManager()));
            mSlidingTabLayout.setDistributeEvenly(false);
            mSlidingTabLayout.setViewPager(mViewPager, (BaseActivity) getActivity());
        }


        class MyDummyPagerAdapter extends FragmentPagerAdapter {
            String[] tabNames;

            public MyDummyPagerAdapter(FragmentManager fm) {
                super(fm);
                tabNames = getResources().getStringArray(R.array.specify_medicine_material_tabs_title);
            }


            @Override
            public Fragment getItem(int position) {
                long[] array = new long[lstPlacedServiceToday.size()];
                for (int i = 0; i < lstPlacedServiceToday.size(); i++) {
                    array[i] = lstPlacedServiceToday.get(i);
                }
                switch (position) {
                    case 0: {
                        MedicineMaterialListFragment fragment = MedicineMaterialListFragment.newInstance(true, medicalStock.MEDI_STOCK_ID == null ? medicalStock.ID : medicalStock.MEDI_STOCK_ID, treatmentCommonInfo, array, instructionTime);
                        fragment.setUpdater(MedicineMaterialSpecifyFragment.this);
                        fragment.setListener(MedicineMaterialSpecifyFragment.this);
                        return fragment;
                    }

                    case 1: {
                        MedicineMaterialListFragment fragment = MedicineMaterialListFragment.newInstance(false, medicalStock.MEDI_STOCK_ID == null ? medicalStock.ID : medicalStock.MEDI_STOCK_ID, treatmentCommonInfo, array, instructionTime);
                        fragment.setUpdater(MedicineMaterialSpecifyFragment.this);
                        fragment.setListener(MedicineMaterialSpecifyFragment.this);
                        return fragment;
                    }

                    default:
                        return new BaseFragment();
                }
            }

            @Override
            public int getCount() {
                return tabNames.length;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return tabNames[position];
            }
        }
    }

    public class RightSideViewHolder implements
            Toolbar.OnMenuItemClickListener, SelectMedicineMaterialDialog.OnNumberEnteredListener {
        Toolbar toolbar;
        ViewEmptyStateLayout viewState;
        RecyclerView rvSelectedMedicineMaterial;
        TextView tvTotal;
        TextView tvPatientInfo;

        SelectedMedicineMaterialAdapter adapter;

        BigDecimal total = BigDecimal.ZERO;
        private int viewStateSelectedService = ViewEmptyStateLayout.VIEW_STATE_EMPTY;

        public RightSideViewHolder(View view) {
            adapter = new SelectedMedicineMaterialAdapter();
            tvTotal = (TextView) view.findViewById(R.id.tvTotal);
            viewState = (ViewEmptyStateLayout) view.findViewById(R.id.view_state);
            tvPatientInfo = (TextView) view.findViewById(R.id.tvPatientInfo);
            toolbar = (Toolbar) view.findViewById(R.id.toolbarRightAbove);
            toolbar.inflateMenu(R.menu.menu_next);
            toolbar.setOnMenuItemClickListener(this);
            viewState.updateViewState(viewStateSelectedService);
            tvPatientInfo.setText(StringUtils.joinWithDashes(context, treatmentCommonInfo.patient.getVirPatientName(), treatmentCommonInfo.TREATMENT_CODE));
            tvTotal.setText(NumberFormatUtils.formatMoney(total));

            rvSelectedMedicineMaterial = (RecyclerView) view.findViewById(R.id.recyclerViewSelectedProduct);
            rvSelectedMedicineMaterial.setHasFixedSize(true);
            rvSelectedMedicineMaterial.setLayoutManager(layoutManager);
            rvSelectedMedicineMaterial.setAdapter(adapter);

        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            if (lstSelectedMedicineMaterial == null || lstSelectedMedicineMaterial.size() <= 0) {
                Toast.makeText(context, R.string.specify_medicine_material_selected_list_is_empty, Toast.LENGTH_SHORT).show();
            } else {
                MedicineMaterialHolder[] lstSelectedService = new MedicineMaterialHolder[lstSelectedMedicineMaterial != null ? lstSelectedMedicineMaterial.size() : 0];
                if (lstSelectedMedicineMaterial != null && lstSelectedMedicineMaterial.size() > 0) {
                    for (int i = 0; i < lstSelectedMedicineMaterial.size(); i++) {
                        lstSelectedService[i] = lstSelectedMedicineMaterial.get(i);
                    }
                }
                replaceCurrentFragment(MedicineMaterialFinishFragment.newInstance(treatmentCommonInfo, medicalStock, roomId, icd, lstSelectedService, numberRemedy, advice, instructionTime, useTime));
            }
            return true;
        }

        private View.OnClickListener onRemoveClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MedicineMaterialHolder item = (MedicineMaterialHolder) v.getTag();
                String name = "";
                if (item.medicineMaterial instanceof Medicine) {
                    name = ((Medicine) item.medicineMaterial).MedicineTypeName;
                } else if (item.medicineMaterial instanceof Material) {
                    name = ((Material) item.medicineMaterial).MaterialTypeName;
                }
                Dialog.Builder builder = null;
                builder = new SimpleDialog.Builder(R.style.Material_App_Dialog_Simple_Light) {
                    @Override
                    public void onPositiveActionClicked(DialogFragment fragment) {
                        super.onPositiveActionClicked(fragment);
                        mapSelectedMedicineMaterial.remove(item.medicineMaterial.ServiceId);
                        rebindData();
                    }

                    @Override
                    public void onNegativeActionClicked(DialogFragment fragment) {
                        super.onNegativeActionClicked(fragment);
                    }
                };

                ((SimpleDialog.Builder) builder)
                        .message(name)
                        .title(getString(item.medicineMaterial instanceof Medicine ? R.string.specify_medicine_material_warning_delete_medicine : R.string.specify_medicine_material_warning_delete_material))
                        .positiveAction(getString(R.string.yes))
                        .negativeAction(getString(R.string.no));

                DialogFragment fragment = DialogFragment.newInstance(builder);
                fragment.setCancelable(false);
                fragment.show(getFragmentManager(), null);

            }
        };
        private View.OnClickListener onClickQuantity = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MedicineMaterialHolder item = (MedicineMaterialHolder) v.getTag();
                if (mapInventory.containsKey(item.medicineMaterial.ServiceId)) {
                    item.medicineMaterial.AvailableAmount = mapInventory.get(item.medicineMaterial.ServiceId).AvailableAmount;
                    item.medicineMaterial.TotalAmount = mapInventory.get(item.medicineMaterial.ServiceId).TotalAmount;
                } else {
                    item.medicineMaterial.AvailableAmount = item.medicineMaterial.AvailableAmount != null ? item.medicineMaterial.AvailableAmount : 0d;
                    item.medicineMaterial.TotalAmount = item.medicineMaterial.TotalAmount != null ? item.medicineMaterial.TotalAmount : 0d;
                }
                SelectMedicineMaterialDialog dialog = new SelectMedicineMaterialDialog(context, item, lstPlacedServiceToday, treatmentCommonInfo.IN_TIME, instructionTime);
                dialog.setOnEnterNumberListener(MedicineMaterialSpecifyFragment.RightSideViewHolder.this);
                dialog.show();
            }
        };

        @Override
        public void onNumberEntered(double number, MedicineMaterial medicineMaterial, PatientType patientType, MedicineUserForm useForm, boolean isExpend, boolean isWithBHYT, BigDecimal priceUnit, String tutorial) {
            MedicineMaterialHolder holder = new MedicineMaterialHolder();
            holder.number = number;
            holder.medicineMaterial = medicineMaterial;
            holder.patientType = patientType;
            holder.userForm = useForm;
            holder.isExpend = isExpend;
            holder.isWithBHXH = isWithBHYT;
            holder.priceUnit = priceUnit;
            holder.tutorial = tutorial;

            mapSelectedMedicineMaterial.put(medicineMaterial.ServiceId, holder);
            rebindData();
        }

        class SelectedMedicineMaterialAdapter extends RecyclerView.Adapter<SelectedMedicineMaterialHolder> {

            @Override
            public SelectedMedicineMaterialHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(context).inflate(R.layout.adapter_right_medicine_material, parent, false);
                return new SelectedMedicineMaterialHolder(view);
            }

            @Override
            public void onBindViewHolder(SelectedMedicineMaterialHolder holder, int position) {
                MedicineMaterialHolder item = lstSelectedMedicineMaterial.get(position);
                if (item.medicineMaterial instanceof Medicine) {
                    holder.tvMedicineMaterialName.setText(((Medicine) item.medicineMaterial).MedicineTypeName);
                    holder.tvMedicineMaterialCode.setText(((Medicine) item.medicineMaterial).MedicineTypeCode);
                } else if (item.medicineMaterial instanceof Material) {
                    holder.tvMedicineMaterialName.setText(((Material) item.medicineMaterial).MaterialTypeName);
                    holder.tvMedicineMaterialCode.setText(((Material) item.medicineMaterial).MaterialTypeCode);
                }
                if (mapInventory.containsKey(item.medicineMaterial.ServiceId)) {
                    MedicineMaterial mm = mapInventory.get(item.medicineMaterial.ServiceId);
                    if (item.number > mm.AvailableAmount) {
                        holder.tvMedicineMaterialCaution.setVisibility(View.VISIBLE);
                        holder.tvMedicineMaterialCaution.setText(R.string.specify_medicine_material_caution_exceed_number);
                    } else {
                        holder.tvMedicineMaterialCaution.setVisibility(View.GONE);
                    }
                }

                holder.tvPriceUnit.setText(item.isExpend ? "0" : item.priceUnit.compareTo(BigDecimal.ZERO) == 0 ? getString(R.string.specify_service_selected_zero_price) : NumberFormatUtils.formatMoney(item.priceUnit));
                long temp = (long) item.number;
                holder.tvQuantity.setText(item.number == temp ? String.format("%d", temp) : String.valueOf(item.number));

                if (lstPlacedServiceToday.contains(item.medicineMaterial.ServiceId)) {
                    holder.tvQuantity.setBackgroundResource(R.drawable.bg_specify_service_quantity_warning);
                } else {
                    holder.tvQuantity.setBackgroundResource(R.drawable.bg_specify_service_quantity);
                }
                holder.tvQuantity.setOnClickListener(onClickQuantity);
                holder.itemView.setOnClickListener(onClickQuantity);
                holder.ibRemove.setOnClickListener(onRemoveClickListener);
                holder.ibRemove.setTag(item);
                holder.tvQuantity.setTag(item);
                holder.itemView.setTag(item);
            }

            @Override
            public int getItemCount() {
                return lstSelectedMedicineMaterial == null ? 0 : lstSelectedMedicineMaterial.size();
            }
        }

        class SelectedMedicineMaterialHolder extends RecyclerView.ViewHolder {
            private TextView tvMedicineMaterialName;
            private TextView tvMedicineMaterialCode;
            private TextView tvQuantity;
            private TextView tvPriceUnit;
            private ImageButton ibRemove;
            private TextView tvMedicineMaterialCaution;

            public SelectedMedicineMaterialHolder(View itemView) {
                super(itemView);
                tvMedicineMaterialName = (TextView) itemView.findViewById(R.id.tvMedicineMaterialName);
                tvMedicineMaterialCode = (TextView) itemView.findViewById(R.id.tvMedicineMaterialCode);
                tvQuantity = (TextView) itemView.findViewById(R.id.tvQuantity);
                ibRemove = (ImageButton) itemView.findViewById(R.id.ibRemove);
                tvPriceUnit = (TextView) itemView.findViewById(R.id.tvServicePriceUnit);
                tvMedicineMaterialCaution = (TextView) itemView.findViewById(R.id.tvMedicineMaterialCaution);
            }
        }
    }

    /**
     * Calculate price for service placed from old prescription
     */
    public class CalculatePriceForOldPrescription extends AsyncTask<Boolean, Void, Void> {
        long fromTime, toTime, treatmentFromTime, treatmentToTime;
        long currentFromTime, currentToTime, currentTreatmentFromTime, currentTreatmentToTime;
        boolean isMedicine;

        @Override
        protected Void doInBackground(Boolean... params) {
            isMedicine = params[0];
            long applyTime = instructionTime;
            long inTime = treatmentCommonInfo.IN_TIME;
            Set<Long> setServiceIds = isMedicine ? mapMedicineOldPrescription.keySet() : mapMaterialOldPrescription.keySet();
            Dao<ServicePATY, Integer> servicePATYDao = getDatabaseHelper().getServicePATYDao();
            QueryBuilder<ServicePATY, Integer> qb = servicePATYDao.queryBuilder();
            try {
                qb.where().eq(ServicePATY.FIELD_NAME_SERVICE_PATY_PATIENT_TYPE_ID, treatmentCommonInfo.patientTypeAlter.PATIENT_TYPE_ID).and()
                        .in(ServicePATY.FIELD_NAME_SERVICE_PATY_SERVICE_ID, setServiceIds);
                PreparedQuery preparedQuery = qb.prepare();
                List<ServicePATY> results = servicePATYDao.query(preparedQuery);
                /**
                 * chỗ lấy ra chính sách giá dựa vào những input sau:
                 - dịch vụ chỉ định là j
                 - patient_type_id lấy theo default bệnh nhân, giữ nguyên số lượng đặt và dịch vụ kể cả trường hợp không có giá
                 - thời gian chỉ định (instruction_time) nằm trong khoảng from_time, to_time (của bảng service_paty) hoặc thời gian vào điều trị (in_time trong bảng his_treatment)
                 nằm trong khoảng treatment_from_time và treatment_to_time (trong bảng his_service_paty)
                 nếu lấy theo những điều kiện đó mà ra 2 bản ghi thì ưu tiên service_paty có priority lớn nhất
                 so sánh priority mà vẫn có hơn 1 bản ghi thì ưu tiên modify_time lớn hơn
                 */
                for (ServicePATY sp : results) {
                    if (!mapServicePatyForOldPrescription.containsKey(sp.SERVICE_ID)) {
                        mapServicePatyForOldPrescription.put(sp.SERVICE_ID, sp);
                    } else {
                        // So sanh
                        ServicePATY current = mapServicePatyForOldPrescription.get(sp.SERVICE_ID);
                        currentFromTime = current.FROM_TIME != null ? current.FROM_TIME : Long.MIN_VALUE;
                        currentToTime = current.TO_TIME != null ? current.TO_TIME : Long.MAX_VALUE;
                        currentTreatmentFromTime = current.TREATMENT_FROM_TIME != null ? current.TREATMENT_FROM_TIME : Long.MIN_VALUE;
                        currentTreatmentToTime = current.TREATMENT_TO_TIME != null ? current.TREATMENT_TO_TIME : Long.MAX_VALUE;

                        fromTime = sp.FROM_TIME != null ? sp.FROM_TIME : Long.MIN_VALUE;
                        toTime = sp.TO_TIME != null ? sp.TO_TIME : Long.MAX_VALUE;
                        treatmentFromTime = sp.TREATMENT_FROM_TIME != null ? sp.TREATMENT_FROM_TIME : Long.MIN_VALUE;
                        treatmentToTime = sp.TREATMENT_TO_TIME != null ? sp.TREATMENT_TO_TIME : Long.MAX_VALUE;

                        if ((fromTime < applyTime && applyTime < toTime) || treatmentFromTime < inTime && inTime < treatmentToTime) {
                            if ((currentFromTime < applyTime && applyTime < currentToTime) || (currentTreatmentFromTime < inTime && inTime < currentTreatmentToTime)) {
                                current.PRIORITY = current.PRIORITY != null ? current.PRIORITY : 0;
                                sp.PRIORITY = sp.PRIORITY != null ? sp.PRIORITY : 0;
                                if (sp.PRIORITY > current.PRIORITY) {
                                    mapServicePatyForOldPrescription.put(sp.SERVICE_ID, sp);
                                } else if (sp.PRIORITY.equals(current.PRIORITY)) {
                                    if (sp.MODIFY_TIME > current.MODIFY_TIME) {
                                        mapServicePatyForOldPrescription.put(sp.SERVICE_ID, sp);
                                    }
                                }
                            } else {
                                mapServicePatyForOldPrescription.put(sp.SERVICE_ID, sp);
                            }
                        }
                    }
                }
                for (long serviceId : setServiceIds) {
                    MedicineMaterialHolder item = new MedicineMaterialHolder();
                    if (isMedicine) {
                        MedicineOldPrescription old = mapMedicineOldPrescription.get(serviceId);
                        Medicine medicine = new Medicine();
                        medicine.MedicineTypeName = old.MEDICINE_TYPE_NAME;
                        medicine.MedicineTypeCode = old.MEDICINE_TYPE_CODE;
                        medicine.Id = old.MEDICINE_TYPE_ID;
                        item.medicineMaterial = medicine;
                        item.medicineMaterial.ServiceId = old.SERVICE_ID;
                        item.userForm = new MedicineUserForm();
                        item.userForm.ID = old.MEDICINE_USE_FORM_ID;
                        item.tutorial = old.TUTORIAL;
                        item.number = old.AMOUNT;
                    } else {
                        MaterialOldPrescription old = mapMaterialOldPrescription.get(serviceId);
                        Material material = new Material();
                        material.MaterialTypeName = old.MATERIAL_TYPE_NAME;
                        material.MaterialTypeCode = old.MATERIAL_TYPE_CODE;
                        material.Id = old.MATERIAL_TYPE_ID;
                        item.medicineMaterial = material;
                        item.medicineMaterial.ServiceId = old.SERVICE_ID;
                        item.number = old.AMOUNT;
                    }
                    item.patientType = new PatientType();
                    item.patientType.ID = treatmentCommonInfo.patientTypeAlter.PATIENT_TYPE_ID;
                    item.patientType.PATIENT_TYPE_NAME = treatmentCommonInfo.patientTypeAlter.PATIENT_TYPE_NAME;

                    if (mapServicePatyForOldPrescription.containsKey(serviceId)) {
                        item.priceUnit = BigDecimal.valueOf(mapServicePatyForOldPrescription.get(serviceId).PRICE);
                    } else {
                        item.priceUnit = BigDecimal.ZERO;
                    }
                    if (mapInventory.containsKey(serviceId)) {
                        item.medicineMaterial.AvailableAmount = mapInventory.get(serviceId).AvailableAmount;
                        item.medicineMaterial.TotalAmount = mapInventory.get(serviceId).TotalAmount;
                    } else {
                        item.medicineMaterial.AvailableAmount = item.medicineMaterial.AvailableAmount != null ? item.medicineMaterial.AvailableAmount : 0d;
                        item.medicineMaterial.TotalAmount = item.medicineMaterial.TotalAmount != null ? item.medicineMaterial.TotalAmount : 0d;
                    }
                    mapSelectedMedicineMaterial.put(serviceId, item);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (isMedicine) {
                isGetMedicineFromOldFinish = true;
            } else {
                isGetMaterialFromOldFinish = true;
            }
            if (isGetMedicineFromOldFinish && isGetMaterialFromOldFinish) {
                isGetMedicineFromOldFinish = false;
                isGetMaterialFromOldFinish = false;
                checkIfServiceOutOfStockOrNotInStock();
            }
        }
    }

    private void checkIfServiceOutOfStockOrNotInStock() {
        final HashMap<Long, MedicineMaterialHolder> mapOutOfStock = new HashMap<>();
        for (MedicineMaterialHolder holder : mapSelectedMedicineMaterial.values()) {
            if (mapInventory.containsKey(holder.medicineMaterial.ServiceId)) {
                MedicineMaterial inventory = mapInventory.get(holder.medicineMaterial.ServiceId);
                if (holder.number > inventory.AvailableAmount) {
                    mapOutOfStock.put(holder.medicineMaterial.ServiceId, holder);
                }
            } else {
                mapOutOfStock.put(holder.medicineMaterial.ServiceId, holder);
            }
        }

        if (mapOutOfStock.size() > 0) {
            StringBuilder builderStr = new StringBuilder();
            builderStr.append("\n");
            for (MedicineMaterialHolder holder : mapOutOfStock.values()) {
                builderStr.append("(");
                if (holder.medicineMaterial instanceof Medicine) {
                    builderStr.append(((Medicine) holder.medicineMaterial).MedicineTypeName);
                } else if (holder.medicineMaterial instanceof Material) {
                    builderStr.append(((Material) holder.medicineMaterial).MaterialTypeName);
                }
                builderStr.append(" : ");
                long temp = (long) holder.number;
                builderStr.append(temp == holder.number ? String.format("%d", temp) : String.valueOf(holder.number));
                builderStr.append(") ");
            }
            Dialog.Builder builder = null;
            builder = new SimpleDialog.Builder(R.style.Material_App_Dialog_Simple_Light) {
                @Override
                public void onPositiveActionClicked(DialogFragment fragment) {
                    super.onPositiveActionClicked(fragment);
                    for (MedicineMaterialHolder holder : mapOutOfStock.values()) {
                        if (!mapInventory.containsKey(holder.medicineMaterial.ServiceId)) {
                            mapSelectedMedicineMaterial.remove(holder.medicineMaterial.ServiceId);
                        } else {
                            MedicineMaterialHolder item = mapSelectedMedicineMaterial.get(holder.medicineMaterial.ServiceId);
                            item.number = mapInventory.get(holder.medicineMaterial.ServiceId).AvailableAmount;
                        }
                    }
                    rebindData();
                }

                @Override
                public void onNegativeActionClicked(DialogFragment fragment) {
                    super.onNegativeActionClicked(fragment);
                    getActivity().onBackPressed();
                }
            };

            ((SimpleDialog.Builder) builder)
                    .message(getString(R.string.specify_medicine_material_warning_out_of_stock) + builderStr.toString())
                    .title(getString(R.string.notify))
                    .positiveAction(getString(R.string.yes))
                    .negativeAction(getString(R.string.no));

            DialogFragment fragment = DialogFragment.newInstance(builder);
            fragment.setCancelable(false);
            fragment.show(getFragmentManager(), null);
        } else {
            rebindData();
        }
    }

    /**
     * Retrieve service and price from template prescription
     */
    private class GetMedicineFromTemplate extends AsyncTask<Long, Void, Void> {
        long fromTime, toTime, treatmentFromTime, treatmentToTime;
        long currentFromTime, currentToTime, currentTreatmentFromTime, currentTreatmentToTime;

        @Override
        protected Void doInBackground(Long... id) {
            long templateId = id[0];
            Dao<EmteMedicine, Integer> emteMedicines = ((BaseActivity) context).getHelper().getEmteMedicine();
            QueryBuilder<EmteMedicine, Integer> queryBuilder = emteMedicines.queryBuilder();
            try {
                queryBuilder.where().eq(EmteMedicine.TABLE_NAME_MEST_TEMPLATE_ID, templateId);
                PreparedQuery<EmteMedicine> preparedQuery = queryBuilder.prepare();
                List<EmteMedicine> lstEmteMedicines = emteMedicines.query(preparedQuery);
                if (lstEmteMedicines == null || lstEmteMedicines.size() == 0) return null;
                for (EmteMedicine emte : lstEmteMedicines) {
                    mapEmteMedicineTemplatePrescription.put(emte.SERVICE_ID, emte);
                }
                // Calculate price
                long applyTime = instructionTime;
                long inTime = treatmentCommonInfo.IN_TIME;
                Set<Long> setServiceIds = mapEmteMedicineTemplatePrescription.keySet();
                Dao<ServicePATY, Integer> servicePATYDao = getDatabaseHelper().getServicePATYDao();
                QueryBuilder<ServicePATY, Integer> qb = servicePATYDao.queryBuilder();
                qb.where().eq(ServicePATY.FIELD_NAME_SERVICE_PATY_PATIENT_TYPE_ID, treatmentCommonInfo.patientTypeAlter.PATIENT_TYPE_ID).and()
                        .in(ServicePATY.FIELD_NAME_SERVICE_PATY_SERVICE_ID, setServiceIds);
                PreparedQuery pq = qb.prepare();
                List<ServicePATY> results = servicePATYDao.query(pq);
                for (ServicePATY sp : results) {
                    if (!mapServicePatyForTemplatePrescription.containsKey(sp.SERVICE_ID)) {
                        mapServicePatyForTemplatePrescription.put(sp.SERVICE_ID, sp);
                    } else {
                        // So sanh
                        ServicePATY current = mapServicePatyForTemplatePrescription.get(sp.SERVICE_ID);
                        currentFromTime = current.FROM_TIME != null ? current.FROM_TIME : Long.MIN_VALUE;
                        currentToTime = current.TO_TIME != null ? current.TO_TIME : Long.MAX_VALUE;
                        currentTreatmentFromTime = current.TREATMENT_FROM_TIME != null ? current.TREATMENT_FROM_TIME : Long.MIN_VALUE;
                        currentTreatmentToTime = current.TREATMENT_TO_TIME != null ? current.TREATMENT_TO_TIME : Long.MAX_VALUE;

                        fromTime = sp.FROM_TIME != null ? sp.FROM_TIME : Long.MIN_VALUE;
                        toTime = sp.TO_TIME != null ? sp.TO_TIME : Long.MAX_VALUE;
                        treatmentFromTime = sp.TREATMENT_FROM_TIME != null ? sp.TREATMENT_FROM_TIME : Long.MIN_VALUE;
                        treatmentToTime = sp.TREATMENT_TO_TIME != null ? sp.TREATMENT_TO_TIME : Long.MAX_VALUE;

                        if ((fromTime < applyTime && applyTime < toTime) || treatmentFromTime < inTime && inTime < treatmentToTime) {
                            if ((currentFromTime < applyTime && applyTime < currentToTime) || (currentTreatmentFromTime < inTime && inTime < currentTreatmentToTime)) {
                                current.PRIORITY = current.PRIORITY != null ? current.PRIORITY : 0;
                                sp.PRIORITY = sp.PRIORITY != null ? sp.PRIORITY : 0;
                                if (sp.PRIORITY > current.PRIORITY) {
                                    mapServicePatyForTemplatePrescription.put(sp.SERVICE_ID, sp);
                                } else if (sp.PRIORITY.equals(current.PRIORITY)) {
                                    if (sp.MODIFY_TIME > current.MODIFY_TIME) {
                                        mapServicePatyForTemplatePrescription.put(sp.SERVICE_ID, sp);
                                    }
                                }
                            } else {
                                mapServicePatyForTemplatePrescription.put(sp.SERVICE_ID, sp);
                            }
                        }
                    }
                }

                for (EmteMedicine em : lstEmteMedicines) {
                    MedicineMaterialHolder holder = new MedicineMaterialHolder();
                    Medicine mm = new Medicine();

                    mm.ServiceId = em.SERVICE_ID;
                    mm.ServiceUnitName = em.SERVICE_UNIT_NAME;
                    mm.MedicineTypeName = em.MEDICINE_TYPE_NAME;
                    mm.MedicineTypeCode = em.MEDICINE_TYPE_CODE;
                    mm.Id = em.MEDICINE_TYPE_ID;
                    if (mapInventory.containsKey(em.SERVICE_ID)) {
                        mm.AvailableAmount = mapInventory.get(em.SERVICE_ID).AvailableAmount;
                        mm.TotalAmount = mapInventory.get(em.SERVICE_ID).AvailableAmount;
                    }
                    holder.number = em.AMOUNT;
                    MedicineUserForm userForm = new MedicineUserForm();
                    userForm.ID = em.MEDICINE_USE_FORM_ID;
                    holder.tutorial = em.TUTORIAL;
                    PatientType pt = new PatientType();
                    pt.ID = treatmentCommonInfo.patientTypeAlter.PATIENT_TYPE_ID;
                    pt.PATIENT_TYPE_NAME = treatmentCommonInfo.patientTypeAlter.PATIENT_TYPE_NAME;
                    if (mapServicePatyForTemplatePrescription.containsKey(em.SERVICE_ID)) {
                        holder.priceUnit = BigDecimal.valueOf(mapServicePatyForTemplatePrescription.get(em.SERVICE_ID).PRICE);
                    } else {
                        holder.priceUnit = BigDecimal.ZERO;
                    }
                    holder.medicineMaterial = mm;
                    holder.userForm = userForm;
                    holder.patientType = pt;
                    mapSelectedMedicineMaterial.put(em.SERVICE_ID, holder);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            isGetMedicineFromTemplateFinish = true;
            if (isGetMaterialFromTemplateFinish && isGetMedicineFromTemplateFinish) {
                isGetMaterialFromTemplateFinish = false;
                isGetMedicineFromOldFinish = false;
                checkIfServiceOutOfStockOrNotInStock();
            }
        }
    }


    private class GetMaterialFromTemplate extends AsyncTask<Long, Void, Void> {
        long fromTime, toTime, treatmentFromTime, treatmentToTime;
        long currentFromTime, currentToTime, currentTreatmentFromTime, currentTreatmentToTime;

        @Override
        protected Void doInBackground(Long... id) {
            long templateId = id[0];
            Dao<EmteMaterial, Integer> emteMaterials = ((BaseActivity) context).getHelper().getEmteMaterial();
            QueryBuilder<EmteMaterial, Integer> queryBuilder = emteMaterials.queryBuilder();
            try {
                queryBuilder.where().eq(EmteMaterial.TABLE_NAME_MEST_TEMPLATE_ID, templateId);
                PreparedQuery<EmteMaterial> preparedQuery = queryBuilder.prepare();
                List<EmteMaterial> lstEmteMaterial = emteMaterials.query(preparedQuery);
                if (lstEmteMaterial == null || lstEmteMaterial.size() == 0) return null;
                for (EmteMaterial em : lstEmteMaterial) {
                    mapEmteMaterialTemplatePrescription.put(em.SERVICE_ID, em);
                }
                // Calculate price
                long applyTime = instructionTime;
                long inTime = treatmentCommonInfo.IN_TIME;
                Set<Long> setServiceIds = mapEmteMaterialTemplatePrescription.keySet();
                Dao<ServicePATY, Integer> servicePATYDao = getDatabaseHelper().getServicePATYDao();
                QueryBuilder<ServicePATY, Integer> qb = servicePATYDao.queryBuilder();
                qb.where().eq(ServicePATY.FIELD_NAME_SERVICE_PATY_PATIENT_TYPE_ID, treatmentCommonInfo.patientTypeAlter.PATIENT_TYPE_ID).and()
                        .in(ServicePATY.FIELD_NAME_SERVICE_PATY_SERVICE_ID, setServiceIds);
                PreparedQuery pq = qb.prepare();
                List<ServicePATY> results = servicePATYDao.query(pq);
                for (ServicePATY sp : results) {
                    if (!mapServicePatyForTemplatePrescription.containsKey(sp.SERVICE_ID)) {
                        mapServicePatyForTemplatePrescription.put(sp.SERVICE_ID, sp);
                    } else {
                        // So sanh
                        ServicePATY current = mapServicePatyForTemplatePrescription.get(sp.SERVICE_ID);
                        currentFromTime = current.FROM_TIME != null ? current.FROM_TIME : Long.MIN_VALUE;
                        currentToTime = current.TO_TIME != null ? current.TO_TIME : Long.MAX_VALUE;
                        currentTreatmentFromTime = current.TREATMENT_FROM_TIME != null ? current.TREATMENT_FROM_TIME : Long.MIN_VALUE;
                        currentTreatmentToTime = current.TREATMENT_TO_TIME != null ? current.TREATMENT_TO_TIME : Long.MAX_VALUE;

                        fromTime = sp.FROM_TIME != null ? sp.FROM_TIME : Long.MIN_VALUE;
                        toTime = sp.TO_TIME != null ? sp.TO_TIME : Long.MAX_VALUE;
                        treatmentFromTime = sp.TREATMENT_FROM_TIME != null ? sp.TREATMENT_FROM_TIME : Long.MIN_VALUE;
                        treatmentToTime = sp.TREATMENT_TO_TIME != null ? sp.TREATMENT_TO_TIME : Long.MAX_VALUE;

                        if ((fromTime < applyTime && applyTime < toTime) || treatmentFromTime < inTime && inTime < treatmentToTime) {
                            if ((currentFromTime < applyTime && applyTime < currentToTime) || (currentTreatmentFromTime < inTime && inTime < currentTreatmentToTime)) {
                                current.PRIORITY = current.PRIORITY != null ? current.PRIORITY : 0;
                                sp.PRIORITY = sp.PRIORITY != null ? sp.PRIORITY : 0;
                                if (sp.PRIORITY > current.PRIORITY) {
                                    mapServicePatyForTemplatePrescription.put(sp.SERVICE_ID, sp);
                                } else if (sp.PRIORITY.equals(current.PRIORITY)) {
                                    if (sp.MODIFY_TIME > current.MODIFY_TIME) {
                                        mapServicePatyForTemplatePrescription.put(sp.SERVICE_ID, sp);
                                    }
                                }
                            } else {
                                mapServicePatyForTemplatePrescription.put(sp.SERVICE_ID, sp);
                            }
                        }
                    }
                }

                for (EmteMaterial em : lstEmteMaterial) {
                    MedicineMaterialHolder holder = new MedicineMaterialHolder();
                    Material mt = new Material();
                    mt.ServiceId = em.SERVICE_ID;
                    mt.MaterialTypeName = em.MATERIAL_TYPE_NAME;
                    mt.MaterialTypeCode = em.MATERIAL_TYPE_CODE;
                    mt.Id = em.MATERIAL_TYPE_ID;
                    if (mapInventory.containsKey(em.SERVICE_ID)) {
                        mt.AvailableAmount = mapInventory.get(em.SERVICE_ID).AvailableAmount;
                        mt.TotalAmount = mapInventory.get(em.SERVICE_ID).AvailableAmount;
                    }
                    PatientType pt = new PatientType();
                    pt.ID = treatmentCommonInfo.patientTypeAlter.PATIENT_TYPE_ID;
                    pt.PATIENT_TYPE_NAME = treatmentCommonInfo.patientTypeAlter.PATIENT_TYPE_NAME;
                    holder.medicineMaterial = mt;
                    holder.patientType = pt;
                    holder.number = em.AMOUNT;
                    if (mapServicePatyForTemplatePrescription.containsKey(em.SERVICE_ID)) {
                        holder.priceUnit = BigDecimal.valueOf(mapServicePatyForTemplatePrescription.get(em.SERVICE_ID).PRICE);
                    } else {
                        holder.priceUnit = BigDecimal.ZERO;
                    }
                    mapSelectedMedicineMaterial.put(em.SERVICE_ID, holder);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            isGetMaterialFromTemplateFinish = true;
            if (isGetMaterialFromTemplateFinish && isGetMedicineFromTemplateFinish) {
                isGetMaterialFromTemplateFinish = false;
                isGetMedicineFromOldFinish = false;
                checkIfServiceOutOfStockOrNotInStock();
            }
        }
    }

    public static class MedicineMaterialHolder implements Serializable, Parcelable {
        public MedicineMaterial medicineMaterial;
        public double number;
        public PatientType patientType;
        public MedicineUserForm userForm;
        public BigDecimal priceUnit;
        public boolean isExpend;
        public boolean isWithBHXH;
        public String tutorial;

        public MedicineMaterialHolder() {
        }

        protected MedicineMaterialHolder(Parcel in) {
            medicineMaterial = in.readParcelable(MedicineMaterial.class.getClassLoader());
            number = in.readDouble();
            patientType = in.readParcelable(PatientType.class.getClassLoader());
            userForm = in.readParcelable(MedicineUserForm.class.getClassLoader());
            isExpend = in.readByte() != 0;
            isWithBHXH = in.readByte() != 0;
            tutorial = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(medicineMaterial, flags);
            dest.writeDouble(number);
            dest.writeParcelable(patientType, flags);
            dest.writeParcelable(userForm, flags);
            dest.writeByte((byte) (isExpend ? 1 : 0));
            dest.writeByte((byte) (isWithBHXH ? 1 : 0));
            dest.writeString(tutorial);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<MedicineMaterialHolder> CREATOR = new Creator<MedicineMaterialHolder>() {
            @Override
            public MedicineMaterialHolder createFromParcel(Parcel in) {
                return new MedicineMaterialHolder(in);
            }

            @Override
            public MedicineMaterialHolder[] newArray(int size) {
                return new MedicineMaterialHolder[size];
            }
        };
    }
}
