package com.staarlab.idoctor.ui.fragment;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.staarlab.idoctor.R;

import com.staarlab.idoctor.helper.share.MBSSharePreference;
import com.staarlab.idoctor.ui.service.SyncDataService;

public class SettingGeneralFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = SettingGeneralFragment.class.getName();

    View viewSync;
    View viewOther;
    static boolean isSyncing = false;
    static ImageView imgSyncRotate;
    static String param;
    static TextView txtSyncStatus;
    EditText edtIntervalTimeUpdate;

    public SettingGeneralFragment() {
        // Required empty public constructor
    }


    public static SettingGeneralFragment newInstance() {
        SettingGeneralFragment fragment = new SettingGeneralFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SyncDataService.ACTION_UPDATE_SUCCESS);
        intentFilter.addAction(SyncDataService.ACTION_UPDATE_FAIL);
        LocalBroadcastManager.getInstance(context).registerReceiver(receiver, intentFilter);
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(SyncDataService.ACTION_UPDATE_SUCCESS)) {
                isSyncing = false;
                updateUI();
            } else if (intent.getAction().equalsIgnoreCase(SyncDataService.ACTION_UPDATE_FAIL)) {
                Toast.makeText(context, R.string.sync_data_fail, Toast.LENGTH_SHORT).show();
                isSyncing = false;
                updateUI();
            }
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        LocalBroadcastManager.getInstance(context).unregisterReceiver(receiver);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting_general, container, false);
        viewSync = view.findViewById(R.id.viewSync);
        imgSyncRotate = (ImageView) view.findViewById(R.id.imgSyncRotate);
        imgSyncRotate.setVisibility(View.INVISIBLE);
        txtSyncStatus = (TextView) view.findViewById(R.id.txtSyncStatus);
        edtIntervalTimeUpdate = (EditText) view.findViewById(R.id.edtIntervalTimeUpdate);
        edtIntervalTimeUpdate.setText(String.valueOf(MBSSharePreference.get().getMMIntervalUpdate()));
        edtIntervalTimeUpdate.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    if (!v.getText().toString().isEmpty()) {
                        int interval = Integer.parseInt(v.getText().toString());
                        if (interval < 10) {
                            edtIntervalTimeUpdate.setText(String.valueOf(MBSSharePreference.get().getMMIntervalUpdate()));
                            Toast.makeText(context, R.string.settings_interval_time_must_be_greater_than, Toast.LENGTH_SHORT).show();
                        } else {
                            MBSSharePreference.get().putMMIntervalUpdate(interval);
                        }
                    }

                    return true;
                }
                return false;
            }
        });
        viewSync.setOnClickListener(this);
        viewOther = view.findViewById(R.id.viewOther);
        viewOther.setOnClickListener(this);
        updateUI();
        return view;
    }

    private static void updateUI() {

        txtSyncStatus.setText(MBSSharePreference.get().getLastUpdate().isEmpty() ?
                context.getString(R.string.settings_notify_unsync)
                : String.format(context.getString(R.string.settings_last_sync), MBSSharePreference.get().getLastUpdate()));
        if (isSyncing) {
            imgSyncRotate.setVisibility(View.VISIBLE);
            imgSyncRotate.startAnimation(AnimationUtils.loadAnimation(context, R.anim.image_sync_rotate));
        } else {
            imgSyncRotate.clearAnimation();
            imgSyncRotate.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.viewSync && !isSyncing) {
            isSyncing = true;
            doSyncData();
        } else if (v.getId() == R.id.viewOther) {

        }
    }

    @Override
    public boolean onBackPressed() {
        getActivity().finish();
        return true;
    }

    private void doSyncData() {
        imgSyncRotate.setVisibility(View.VISIBLE);
        imgSyncRotate.startAnimation(AnimationUtils.loadAnimation(context, R.anim.image_sync_rotate));
        Intent intent = new Intent(context, SyncDataService.class);
        context.startService(intent);
    }

}
