package com.staarlab.idoctor.ui.fragment;

import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.staarlab.idoctor.entity.CommonServiceDetailEntity;
import com.staarlab.idoctor.entity.CommonServiceEntity;
import com.staarlab.idoctor.entity.MediReactServiceEntity;
import com.staarlab.idoctor.model.ResponseArrayModel;
import com.staarlab.idoctor.util.CommonUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MediReactFragment extends CommonServiceFragment {
    @Override
    protected void loadData() {
        Long treatmentId = getArguments().getLong(ARG_TREATMENT_ID, -1L);
        String param = "{'ApiData':{'TREATMENT_ID':'" + treatmentId + "'}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getHisMediReact(param, new Callback<ResponseArrayModel<MediReactServiceEntity>>() {
            @Override
            public void success(ResponseArrayModel<MediReactServiceEntity> responseArrayModel, Response response) {
                mAdapter.setDataList(responseArrayModel.getDataList());
                onFinishedDataLoad();
            }

            @Override
            public void failure(RetrofitError error) {
                onFinishedDataLoad();
            }
        });
    }

    @Override
    protected void onLstDataItemClick(int position) {
        MediReactServiceEntity entity = (MediReactServiceEntity) mAdapter.getItem(position);
        List<CommonServiceDetailEntity> detailEntities = new ArrayList<>();
        CommonServiceDetailEntity detailEntity = new CommonServiceDetailEntity();
        detailEntity.setServiceCode(entity.getMedicineTypeCode());
        detailEntity.setServiceName(entity.getMedicineTypeName());
        detailEntity.setServiceUnitName(CommonUtil.hisString2DateAsString(entity.getExpiredDate()));
        detailEntity.setPatientTypeName(entity.getServiceUnitName());
        detailEntities.add(detailEntity);
        mLayoutRight.setVisibility(View.VISIBLE);
        mDetailAdapter.refreshData(detailEntities);
    }
}
