package com.staarlab.idoctor.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.CareServiceEntity;
import com.staarlab.idoctor.entity.CommonServiceEntity;
import com.staarlab.idoctor.entity.DebateServiceEntity;
import com.staarlab.idoctor.entity.DhstServiceEntity;
import com.staarlab.idoctor.entity.InfusionServiceEntity;
import com.staarlab.idoctor.entity.MediReactServiceEntity;
import com.staarlab.idoctor.entity.TrackingServiceEntity;
import com.staarlab.idoctor.entity.TranPatiServiceEntity;
import com.staarlab.idoctor.util.AppConst;
import com.staarlab.idoctor.util.CommonUtil;

import java.util.ArrayList;
import java.util.List;


public class CommonServiceAdapter<T extends CommonServiceEntity> extends BaseAdapter {
    static class ViewHolder {
        TextView txtC1;
        TextView txtC2;
        TextView txtC3;
        TextView txtC4;
        TextView txtC5;
        TextView txtC6;
        TextView txtC7;
        TextView txtC8;
        TextView txtC9;
        TextView txtC10;
        TextView txtC11;
        TextView txtC12;
        TextView txtC13;
        TextView txtC14;
        TextView txtC15;
    }

    private List<T> dataList;
    private Context mContext;
    private String mTabCode;

    public CommonServiceAdapter(Context context, String tabCode) {
        mContext = context;
        mTabCode = tabCode;
    }

    @Override
    public int getCount() {
        return dataList == null ? 0 : dataList.size();
    }

    @Override
    public T getItem(int i) {
        return dataList.get(i);
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        T p = getItem(i);
        ViewHolder vh;
        if (convertView == null) {
            vh = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.adapter_common_list_item, viewGroup, false);
            vh.txtC1 = (TextView) convertView.findViewById(R.id.txtC1);
            vh.txtC2 = (TextView) convertView.findViewById(R.id.txtC2);
            vh.txtC3 = (TextView) convertView.findViewById(R.id.txtC3);
            vh.txtC4 = (TextView) convertView.findViewById(R.id.txtC4);
            vh.txtC5 = (TextView) convertView.findViewById(R.id.txtC5);
            vh.txtC6 = (TextView) convertView.findViewById(R.id.txtC6);
            vh.txtC7 = (TextView) convertView.findViewById(R.id.txtC7);
            vh.txtC8 = (TextView) convertView.findViewById(R.id.txtC8);
            vh.txtC9 = (TextView) convertView.findViewById(R.id.txtC9);
            vh.txtC10 = (TextView) convertView.findViewById(R.id.txtC10);
            vh.txtC11 = (TextView) convertView.findViewById(R.id.txtC11);
            vh.txtC12 = (TextView) convertView.findViewById(R.id.txtC12);
            vh.txtC13 = (TextView) convertView.findViewById(R.id.txtC13);
            vh.txtC14 = (TextView) convertView.findViewById(R.id.txtC14);
            vh.txtC15 = (TextView) convertView.findViewById(R.id.txtC15);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        switch (mTabCode) {
            case AppConst.TAB_CODE_MEDI_REACT:
                mapMediReactInfo(vh, (MediReactServiceEntity) p);
                break;
            case AppConst.TAB_CODE_TRACKING:
                mapTrackingInfo(vh, (TrackingServiceEntity) p);
                break;
            case AppConst.TAB_CODE_DHST:
                mapDhstInfo(vh, (DhstServiceEntity) p);
                break;
            case AppConst.TAB_CODE_CARE:
                mapCareInfo(vh, (CareServiceEntity) p);
                break;
            case AppConst.TAB_CODE_INFUSION:
                mapInfusionInfo(vh, (InfusionServiceEntity) p);
                break;
            case AppConst.TAB_CODE_DEBATE:
                mapDebateInfo(vh, (DebateServiceEntity) p);
                break;
            case AppConst.TAB_CODE_TRAN_PATI:
                mapTranPatiInfo(vh, (TranPatiServiceEntity) p);
                break;
            default:
                mapGeneralInfo(vh, p);
                break;
        }
        return convertView;
    }

    private void mapDhstInfo(ViewHolder vh, DhstServiceEntity p) {
        vh.txtC1.setText(p.getDhstCode());
        vh.txtC2.setText(p.getExecuteUsername());
        vh.txtC3.setText(p.getPulse() == null ? "" : String.valueOf(p.getPulse()));
        vh.txtC4.setText(p.getTemperature() == null ? "" : String.valueOf(p.getTemperature()));
        vh.txtC5.setText(String.valueOf(p.getBloodPressureMin()) + "/" + String.valueOf(p.getBloodPressureMax()));
        vh.txtC6.setText(String.valueOf(p.getBreathRate()));

        vh.txtC7.setVisibility(View.VISIBLE);
        vh.txtC8.setVisibility(View.VISIBLE);
        vh.txtC9.setVisibility(View.VISIBLE);
        vh.txtC10.setVisibility(View.VISIBLE);
        vh.txtC11.setVisibility(View.VISIBLE);
        vh.txtC12.setVisibility(View.VISIBLE);
        vh.txtC13.setVisibility(View.VISIBLE);
        vh.txtC14.setVisibility(View.VISIBLE);
        vh.txtC15.setVisibility(View.VISIBLE);

        vh.txtC7.setText(p.getWeight() == null ? "" : String.valueOf(p.getWeight()));
        vh.txtC8.setText(p.getHeight() == null ? "" : String.valueOf(p.getHeight()));
        vh.txtC9.setText(p.getVirBmi() == null ? "" : String.valueOf(p.getVirBmi()));
        vh.txtC10.setText(p.getVirBodySurfaceArea() == null ? "" : String.valueOf(p.getVirBodySurfaceArea()));
        vh.txtC11.setText(String.valueOf(p.getExecuteUsername()));
        vh.txtC12.setText(CommonUtil.hisString2DateTimeAsString(p.getCreateTime()));
        vh.txtC13.setText(p.getCreator());
        vh.txtC14.setText(CommonUtil.hisString2DateTimeAsString(p.getModifyTime()));
        vh.txtC15.setText(p.getModifier());
    }

    private void mapTrackingInfo(ViewHolder vh, TrackingServiceEntity p) {
        vh.txtC1.setText(p.getCreator());
        vh.txtC2.setText(CommonUtil.hisString2DateTimeAsString(p.getTrackingTime()));
        vh.txtC3.setText(CommonUtil.hisString2DateTimeAsString(p.getCreateTime()));
        vh.txtC4.setText(p.getModifier());
        vh.txtC5.setText(CommonUtil.hisString2DateTimeAsString(p.getModifyTime()));
        vh.txtC6.setText("");
    }

    private void mapMediReactInfo(ViewHolder vh, MediReactServiceEntity p) {
        vh.txtC1.setText(p.getMediReactTypeCode());
        vh.txtC2.setText(p.getMediReactTypeName());
        vh.txtC3.setText(p.getMediReactSttName());
        vh.txtC4.setText(p.getRequestLoginname() + " - " + p.getRequestUsername());
        vh.txtC5.setText(p.getExecuteUsername());
        vh.txtC6.setText(CommonUtil.hisString2DateTimeAsString(p.getExecuteTime()));
        vh.txtC7.setVisibility(View.GONE);
        vh.txtC8.setVisibility(View.GONE);
    }

    private void mapGeneralInfo(ViewHolder vh, T p) {
        vh.txtC1.setText(p.getServiceReqCode());
        vh.txtC2.setText(CommonUtil.hisString2DateTimeAsString(p.getIntructionTime()));
        vh.txtC3.setText(p.getExecuteRoomName());
        vh.txtC4.setText(p.getExecuteLoginname() + " - " + p.getExecuteUsername());
        vh.txtC5.setText(p.getRequestRoomName());
        vh.txtC6.setText(p.getRequestUsername());
    }

    private void mapCareInfo(ViewHolder vh, CareServiceEntity p) {
        vh.txtC7.setVisibility(View.VISIBLE);
        vh.txtC8.setVisibility(View.VISIBLE);
        vh.txtC9.setVisibility(View.VISIBLE);
        vh.txtC10.setVisibility(View.VISIBLE);
        vh.txtC11.setVisibility(View.VISIBLE);
        vh.txtC12.setVisibility(View.VISIBLE);
        vh.txtC13.setVisibility(View.VISIBLE);
        vh.txtC14.setVisibility(View.VISIBLE);

        vh.txtC1.setText(p.getCareCode());
        vh.txtC2.setText(CommonUtil.hisString2DateTimeAsString(p.getCreateTime()));
        vh.txtC3.setText(p.getMucocutaneous());
        vh.txtC4.setText(p.getUrine());
        vh.txtC5.setText(p.getDejecta());
        vh.txtC6.setText(p.getNutrition());
        vh.txtC7.setText(p.getSanitary());
        vh.txtC8.setText(p.getTutorial());
        vh.txtC9.setText(p.getEducation());
        vh.txtC10.setText(p.getHasTest() != null && p.getHasTest() == 1 ? "X" : "");
        vh.txtC11.setText(p.getHasAddMedicine() != null && p.getHasAddMedicine() == 1 ? "X" : "");
        vh.txtC12.setText(p.getHasMedicine() != null && p.getHasMedicine() == 1 ? "X" : "");
        vh.txtC13.setText(p.getExecuteUsername());
        vh.txtC14.setText(CommonUtil.hisString2DateTimeAsString(p.getExecuteTime()));
    }

    private void mapInfusionInfo(ViewHolder vh, InfusionServiceEntity p) {
        vh.txtC7.setVisibility(View.VISIBLE);
        vh.txtC8.setVisibility(View.VISIBLE);
        vh.txtC9.setVisibility(View.VISIBLE);
        vh.txtC10.setVisibility(View.VISIBLE);
        vh.txtC1.setText(p.getInfusionCode());
        vh.txtC2.setText(p.getInfusionSttName());
        vh.txtC3.setText(p.getRequestLoginname());
        vh.txtC4.setText(CommonUtil.hisString2DateTimeAsString(p.getStartTime()));
        vh.txtC5.setText(CommonUtil.hisString2DateTimeAsString(p.getFinishTime()));
        vh.txtC6.setText(p.getExecuteLoginname());
        vh.txtC7.setText(p.getCreator());
        vh.txtC8.setText(CommonUtil.hisString2DateTimeAsString(p.getCreateTime()));
        vh.txtC9.setText(p.getModifier());
        vh.txtC10.setText(CommonUtil.hisString2DateTimeAsString(p.getModifyTime()));
    }

    private void mapDebateInfo(ViewHolder vh, DebateServiceEntity p) {
        vh.txtC7.setVisibility(View.GONE);
        vh.txtC1.setText(CommonUtil.hisString2DateTimeAsString(p.getDebateTime()));
        vh.txtC2.setText(p.getRequestContent());
        vh.txtC3.setText(p.getCreator());
        vh.txtC4.setText(CommonUtil.hisString2DateTimeAsString(p.getModifyTime()));
        vh.txtC5.setText(p.getModifier());
    }

    private void mapTranPatiInfo(ViewHolder vh, TranPatiServiceEntity p) {
//        vh.txtC6.setVisibility(View.VISIBLE);
        vh.txtC7.setVisibility(View.VISIBLE);

        vh.txtC1.setText(p.getTranPatiTypeName());
        vh.txtC2.setText(p.getMediOrgCode());
        vh.txtC3.setText(p.getMediOrgName());
        vh.txtC4.setText(CommonUtil.hisString2DateTimeAsString(p.getCreateTime()));
        vh.txtC5.setText(p.getCreator());
        vh.txtC6.setText(CommonUtil.hisString2DateTimeAsString(p.getModifyTime()));
        vh.txtC7.setText(p.getModifier());
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void setDataList(List<T> dataList) {
        if (this.dataList == null) {
            this.dataList = new ArrayList<>();
        }
        this.dataList.clear();
        this.dataList.addAll(dataList);
        notifyDataSetChanged();
    }
}
