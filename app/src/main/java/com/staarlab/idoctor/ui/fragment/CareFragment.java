package com.staarlab.idoctor.ui.fragment;

import android.util.Base64;
import android.util.Log;

import com.staarlab.idoctor.entity.CareServiceEntity;
import com.staarlab.idoctor.entity.CommonServiceEntity;
import com.staarlab.idoctor.model.ResponseArrayModel;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class CareFragment extends CommonServiceFragment {
    @Override
    protected void loadData() {
        Long treatmentId = getArguments().getLong(ARG_TREATMENT_ID, -1L);
        String param = "{'ApiData':{'TREATMENT_ID':'" + treatmentId + "'}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getHisCare(param, new Callback<ResponseArrayModel<CareServiceEntity>>() {
            @Override
            public void success(ResponseArrayModel<CareServiceEntity> responseArrayModel, Response response) {
                mAdapter.setDataList(responseArrayModel.getDataList());
                onFinishedDataLoad();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("XXXXX", "Fail: " + error.getMessage());
                onFinishedDataLoad();
            }
        });
    }

    @Override
    protected void onLstDataItemClick(int position) {

    }
}
