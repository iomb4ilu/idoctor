package com.staarlab.idoctor.ui.fragment;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.ICD;
import com.staarlab.idoctor.entity.PatientType;
import com.staarlab.idoctor.entity.ServiceGroup;
import com.staarlab.idoctor.entity.Treatment;
import com.staarlab.idoctor.entity.TreatmentCommonInfo;
import com.staarlab.idoctor.entity.TreatmentFee;
import com.staarlab.idoctor.helper.share.MBSSharePreference;
import com.staarlab.idoctor.model.ResponseArrayModel;
import com.staarlab.idoctor.model.ResponseObjectModel;
import com.staarlab.idoctor.ui.dialog.SelectICDDialog;
import com.staarlab.idoctor.ui.dialog.SelectServiceGroupDialog;
import com.staarlab.idoctor.util.DateTimeUtils;
import com.staarlab.idoctor.util.NumberFormatUtils;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.WSFactory;

import java.util.Calendar;
import java.util.Date;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class TechServiceDiagnosticFragment extends BaseFragment implements Toolbar.OnMenuItemClickListener, View.OnClickListener, SelectICDDialog.OnICDSelectListener, SelectServiceGroupDialog.OnServiceGroupSelectListener {
    private HISService hisService;
    Treatment treatment;
    private ICD icd;
    private ServiceGroup serviceGroup;
    private long roomId;
    private TreatmentCommonInfo treatmentCommonInfo;
    private TreatmentFee fee;

    private TextView tvPatientName;
    private TextView tvTreatmentCode;
    private TextView tvMustPay;
    private TextView tvMustPayBN;
    private EditText edtMainExaminationCode;
    private EditText edtMainExaminationNote;
    private EditText edtMinorExaminationNote;
    private EditText edtServiceGroupCode;
    private EditText edtServiceGroupNote;
    private EditText edtDate;
    private EditText edtTime;
    private CheckBox chkPriority;
    private Button btnClearServiceGroup;
    private Button btnClearICD;

    private Date applyDate;

    public TechServiceDiagnosticFragment() {
        // Required empty public constructor
    }

    public static TechServiceDiagnosticFragment newInstance(Treatment treatment, long roomId) {
        TechServiceDiagnosticFragment fragment = new TechServiceDiagnosticFragment();
        Bundle args = new Bundle();
        args.putParcelable("TREATMENT", treatment);
        args.putLong("roomId", roomId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            treatment = getArguments().getParcelable("TREATMENT");
            roomId = getArguments().getLong("roomId");
        }
        hisService = WSFactory.createHisService(MBSSharePreference.get().getTokenCode());
        getPatientCommonInfo();
        loadFeeInfo();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tech_diagnostic, container, false);

        edtMainExaminationCode = (EditText) view.findViewById(R.id.edtMainExaminationCode);
        edtMainExaminationNote = (EditText) view.findViewById(R.id.edtMainExaminationNote);
        edtServiceGroupCode = (EditText) view.findViewById(R.id.edtServiceGroupCode);
        edtServiceGroupNote = (EditText) view.findViewById(R.id.edtServiceGroupNote);
        edtMinorExaminationNote = (EditText) view.findViewById(R.id.edtMinorExaminationNote);
        chkPriority = (CheckBox) view.findViewById(R.id.chkPriority);
        edtDate = (EditText) view.findViewById(R.id.edtDate);
        edtTime = (EditText) view.findViewById(R.id.edtTime);
        tvPatientName = (TextView) view.findViewById(R.id.tvPatientName);
        tvTreatmentCode = (TextView) view.findViewById(R.id.tvTreatmentCode);
        tvMustPay = (TextView) view.findViewById(R.id.tvMustPay);
        tvMustPayBN = (TextView) view.findViewById(R.id.tvMustPayBN);
        btnClearServiceGroup = (Button) view.findViewById(R.id.btnClearServiceGroup);
        btnClearICD = (Button) view.findViewById(R.id.btnClearICD);
        tvPatientName.setText(treatment.getVirPatientName());
        tvTreatmentCode.setText(treatment.getTreatmentCode());
        tvMustPay.setText(fee == null || fee.getTotalPrice() == null ? "0" : NumberFormatUtils.formatMoney(fee.getTotalPrice()));
        tvMustPayBN.setText(fee == null || fee.getTotalPatientPrice() == null ? "0" : NumberFormatUtils.formatMoney(fee.getTotalPatientPrice()));
        btnClearServiceGroup.setVisibility(View.GONE);
        btnClearICD.setVisibility(View.GONE);

        edtMainExaminationCode.setOnClickListener(this);
        edtMainExaminationNote.setOnClickListener(this);
        edtServiceGroupCode.setOnClickListener(this);
        edtServiceGroupNote.setOnClickListener(this);
        btnClearServiceGroup.setOnClickListener(this);
        btnClearICD.setOnClickListener(this);

        edtDate.setText(DateTimeUtils.formatDate(DateTimeUtils.currentDate()));
        edtTime.setText(DateTimeUtils.formatTime(DateTimeUtils.currentDate()));
        edtDate.setOnClickListener(this);
        edtTime.setOnClickListener(this);
        applyDate = DateTimeUtils.currentDate();

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.inflateMenu(R.menu.menu_next);
        toolbar.setOnMenuItemClickListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        if (serviceGroup != null) {
            edtServiceGroupCode.setText(serviceGroup.SERVICE_GROUP_CODE);
            edtServiceGroupNote.setText(serviceGroup.SERVICE_GROUP_NAME);
            btnClearServiceGroup.setVisibility(View.VISIBLE);
        }
        if (icd != null) {
            edtMainExaminationCode.setText(icd.ICD_TEXT);
            edtMainExaminationNote.setText(icd.ICD_MAIN_NAME != null ? icd.ICD_MAIN_NAME : icd.ICD_NAME);
            edtMainExaminationNote.setPadding(0, 0, context.getResources().getDimensionPixelSize(R.dimen.dimen_large), 0);
            edtMainExaminationNote.setFocusableInTouchMode(true);
            edtMainExaminationNote.setFocusable(true);
            edtMinorExaminationNote.setText(icd.ICD_TEXT != null ? icd.ICD_TEXT : "");
            btnClearICD.setVisibility(View.VISIBLE);
        }

        return view;
    }

    private void bindView() {
        edtMainExaminationCode.setText(treatmentCommonInfo.ICD_CODE != null ? treatmentCommonInfo.ICD_CODE : "");
        edtMainExaminationNote.setText(treatmentCommonInfo.ICD_MAIN_TEXT == null ? treatmentCommonInfo.ICD_NAME != null ? treatmentCommonInfo.ICD_NAME : "" : treatmentCommonInfo.ICD_MAIN_TEXT);
        edtMinorExaminationNote.setText(treatmentCommonInfo.ICD_TEXT != null ? treatmentCommonInfo.ICD_TEXT : "");
        if (treatmentCommonInfo.ICD_CODE != null) {
            icd = new ICD();
            icd.ID = treatmentCommonInfo.ICD_ID;
            icd.ICD_NAME = treatmentCommonInfo.ICD_NAME;
            icd.ICD_CODE = treatmentCommonInfo.ICD_CODE;
            icd.ICD_TEXT = treatmentCommonInfo.ICD_TEXT != null ? treatmentCommonInfo.ICD_TEXT : "";
            edtMainExaminationNote.setFocusableInTouchMode(true);
            edtMainExaminationNote.setFocusable(true);
            edtMainExaminationNote.setPadding(0, 0, context.getResources().getDimensionPixelSize(R.dimen.dimen_large), 0);
            btnClearICD.setVisibility(View.VISIBLE);
        }
    }

    private ProgressDialog mProgressDialog;

    private void getPatientCommonInfo() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.diagnostic_get_patient_info));
        }
        mProgressDialog.show();

        String param = "{'ApiData':" + treatment.getTreatmentId() + "}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getHisTreatmentCommonInfo(param, new Callback<ResponseObjectModel<TreatmentCommonInfo>>() {
            @Override
            public void success(ResponseObjectModel<TreatmentCommonInfo> responseObjectModel, Response response) {
                if (mProgressDialog != null) mProgressDialog.dismiss();
                treatmentCommonInfo = responseObjectModel.getDataResult();
                bindView();
            }

            @Override
            public void failure(RetrofitError error) {
                if (mProgressDialog != null) mProgressDialog.dismiss();
                Toast.makeText(context, R.string.get_patient_info_fail, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadFeeInfo() {
        String param = "{'ApiData':{'ID':'" + treatment.getTreatmentId() + "'}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getHisTreatmentFee(param, new Callback<ResponseArrayModel<TreatmentFee>>() {
            @Override
            public void success(ResponseArrayModel<TreatmentFee> responseArrayModel, Response response) {
                if (responseArrayModel.isSuccess()) {
                    fee = responseArrayModel.getDataList().get(0);
                    tvMustPay.setText(fee == null || fee.getTotalPrice() == null ? "0" : NumberFormatUtils.formatMoney(fee.getTotalPrice()));
                    tvMustPayBN.setText(fee == null || fee.getTotalPatientPrice() == null ? "0" : NumberFormatUtils.formatMoney(fee.getTotalPatientPrice()));
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    @Override
    public boolean onBackPressed() {

        return true;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (icd == null) {
            edtMainExaminationCode.setError("");
            edtMainExaminationNote.setError("");
            Toast.makeText(context, R.string.diagnostic_material_not_select_icd, Toast.LENGTH_SHORT).show();
            return true;
        }
        icd.ICD_MAIN_NAME = edtMainExaminationNote.getText().toString();
        icd.ICD_TEXT = edtMinorExaminationNote.getText().toString();
        long applyTime = DateTimeUtils.formatLongTime(applyDate);
        PatientType patientType = new PatientType();
        patientType.ID = treatmentCommonInfo.patientTypeAlter.PATIENT_TYPE_ID;
        patientType.PATIENT_TYPE_NAME = treatmentCommonInfo.patientTypeAlter.PATIENT_TYPE_NAME;
        replaceCurrentFragment(TechServiceSpecifyFragment.newInstance(treatment, roomId, fee, icd, serviceGroup, chkPriority.isChecked(), applyTime, patientType, treatmentCommonInfo.IN_TIME));
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edtMainExaminationCode: {
                SelectICDDialog dialog = new SelectICDDialog(context);
                dialog.setICDSelectListener(this);
                dialog.show();
            }
            break;
            case R.id.edtMainExaminationNote: {
                if (!edtMainExaminationNote.isFocusable()) {
                    SelectICDDialog dialog = new SelectICDDialog(context);
                    dialog.setICDSelectListener(this);
                    dialog.show();
                }
            }
            break;
            case R.id.edtServiceGroupNote:
            case R.id.edtServiceGroupCode: {
                SelectServiceGroupDialog dialog = new SelectServiceGroupDialog(context);
                dialog.setServiceGroupSelectListener(this);
                dialog.show();
            }
            break;
            case R.id.edtDate: {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(applyDate);
                DatePickerDialog dialog = new DatePickerDialog(context, dateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
                dialog.setCancelable(false);
                dialog.show();
            }
            break;

            case R.id.edtTime: {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(applyDate);
                TimePickerDialog dialog = new TimePickerDialog(context, timeSetListener, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
                dialog.setCancelable(false);
                dialog.show();
            }
            break;
            case R.id.btnClearServiceGroup: {
                serviceGroup = null;
                edtServiceGroupCode.setText("");
                edtServiceGroupNote.setText("");
                edtServiceGroupNote.setPadding(0, 0, 0, 0);
                btnClearServiceGroup.setVisibility(View.GONE);
            }
            break;
            case R.id.btnClearICD: {
                icd = null;
                edtMainExaminationCode.setText("");
                edtMainExaminationNote.setText("");
                edtMainExaminationNote.setFocusable(false);
                edtMainExaminationNote.setFocusableInTouchMode(false);
                edtMainExaminationNote.setPadding(0, 0, 0, 0);
                btnClearICD.setVisibility(View.GONE);
            }
            break;
        }

    }

    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(applyDate);
            calendar.set(year, monthOfYear, dayOfMonth);
            applyDate = calendar.getTime();
            String date = DateTimeUtils.formatDate(applyDate);
            edtDate.setText(date);
        }
    };

    private TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(applyDate);
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);
            applyDate = calendar.getTime();
            String time = DateTimeUtils.formatTime(applyDate);
            edtTime.setText(time);
        }
    };


    @Override
    public void onSelected(ICD item) {
        edtMainExaminationCode.setText(item.ICD_CODE);
        edtMainExaminationNote.setText(item.ICD_NAME);
        edtMainExaminationNote.setError(null);
        edtMainExaminationCode.setError(null);
        edtMainExaminationNote.setFocusable(true);
        edtMainExaminationNote.setFocusableInTouchMode(true);
        edtMainExaminationNote.setPadding(0, 0, context.getResources().getDimensionPixelSize(R.dimen.dimen_large), 0);
        btnClearICD.setVisibility(View.VISIBLE);
        icd = item;
    }

    @Override
    public void onServiceGroupSelected(ServiceGroup item) {
        edtServiceGroupCode.setText(item.SERVICE_GROUP_CODE);
        edtServiceGroupNote.setText(item.SERVICE_GROUP_NAME);
        edtServiceGroupNote.setPadding(0, 0, context.getResources().getDimensionPixelSize(R.dimen.dimen_large), 0);
        serviceGroup = item;
        btnClearServiceGroup.setVisibility(View.VISIBLE);
    }
}
