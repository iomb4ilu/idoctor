package com.staarlab.idoctor.ui.fragment;


import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.ICD;
import com.staarlab.idoctor.entity.PatientType;
import com.staarlab.idoctor.entity.SereServ;
import com.staarlab.idoctor.entity.ServiceGroup;
import com.staarlab.idoctor.entity.ServicePATY;
import com.staarlab.idoctor.entity.ServiceSegr;
import com.staarlab.idoctor.entity.ServiceType;
import com.staarlab.idoctor.entity.TechService;
import com.staarlab.idoctor.entity.Treatment;
import com.staarlab.idoctor.entity.TreatmentFee;
import com.staarlab.idoctor.helper.layout.BreadcrumbToolbar;
import com.staarlab.idoctor.helper.layout.DividerItemDecoration;
import com.staarlab.idoctor.helper.layout.GeneralSwipeRefreshLayout;
import com.staarlab.idoctor.helper.layout.HeaderRecyclerDividerItemDecorator;
import com.staarlab.idoctor.helper.layout.ViewEmptyStateLayout;
import com.staarlab.idoctor.helper.share.MBSSharePreference;
import com.staarlab.idoctor.model.ResponseArrayModel;
import com.staarlab.idoctor.ui.activity.BaseActivity;
import com.staarlab.idoctor.ui.dialog.SelectServiceDialog;
import com.staarlab.idoctor.util.DateTimeUtils;
import com.staarlab.idoctor.util.NumberFormatUtils;
import com.staarlab.idoctor.util.StringUtils;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.WSFactory;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class TechServiceSpecifyFragment extends BaseFragment {
    private Treatment treatment;
    private TreatmentFee fee;
    private ICD icd;
    private ServiceGroup serviceGroup;
    private boolean isPriority;
    private long roomId;
    private long applyTime;
    private PatientType defaultPatientType;
    private Long inTime;

    private LeftSideViewHolder leftSideViewHolder;
    private RightSideViewHolder rightSideViewHolder;

    private LinearLayoutManager serviceLayoutManager;
    private LinearLayoutManager serviceSelectedLayoutManager;
    List<ServiceCompound> lstTechService = new ArrayList<>();
    List<TechServiceHolder> lstSelectedTechService = new ArrayList<>();

    private Map<Long, TechServiceHolder> mapSelectedService = new LinkedHashMap<>();
    private HISService hisService;
    private List<Long> lstPlacedServiceToday = new ArrayList<>();

    public TechServiceSpecifyFragment() {
        // Required empty public constructor
    }


    public static TechServiceSpecifyFragment newInstance(Treatment treatment, long roomId, TreatmentFee fee, ICD icd, ServiceGroup serviceG, boolean isPriority, long appT, PatientType patientType, long inTime) {
        TechServiceSpecifyFragment fragment = new TechServiceSpecifyFragment();
        Bundle args = new Bundle();
        args.putParcelable("TREATMENT", treatment);
        args.putParcelable("ICD", icd);
        args.putParcelable("fee", fee);
        args.putParcelable("SERVICE_GROUP", serviceG);
        args.putBoolean("p", isPriority);
        args.putLong("l", roomId);
        args.putLong("ap", appT);
        args.putParcelable("patientType", patientType);
        args.putLong("inTime", inTime);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            treatment = getArguments().getParcelable("TREATMENT");
            icd = getArguments().getParcelable("ICD");
            fee = getArguments().getParcelable("fee");
            serviceGroup = getArguments().getParcelable("SERVICE_GROUP");
            isPriority = getArguments().getBoolean("p");
            roomId = getArguments().getLong("l");
            applyTime = getArguments().getLong("ap");
            defaultPatientType = getArguments().getParcelable("patientType");
            inTime = getArguments().getLong("inTime");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tech_specify, container, false);
        findViews(view);
        // Get placed service today
        hisService = WSFactory.createHisService(MBSSharePreference.get().getTokenCode());
        String param = "{'ApiData':{'TREATMENT_ID':" + treatment.getTreatmentId() + ",'INTRUCTION_TIME_FROM':" + DateTimeUtils.getBeginingTimeOfToday() + ",'INTRUCTION_TIME_TO':" + DateTimeUtils.getEndTimeOfToday() + "}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getListOfPlacedService(param, new Callback<ResponseArrayModel<SereServ>>() {
            @Override
            public void success(ResponseArrayModel<SereServ> sereServResponseArrayModel, Response response) {
                lstPlacedServiceToday.clear();
                for (SereServ ss : sereServResponseArrayModel.getDataList()) {
                    lstPlacedServiceToday.add(ss.SERVICE_ID);
                }
                if (serviceGroup != null) {
                    new GetServiceFromServiceGroup().execute(serviceGroup.ID, defaultPatientType.ID, applyTime, inTime);
                }

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(context, R.string.specify_service_get_placed_service_fail, Toast.LENGTH_SHORT).show();
                if (serviceGroup != null) {
                    new GetServiceFromServiceGroup().execute(serviceGroup.ID, defaultPatientType.ID, applyTime, inTime);
                }
            }

        });

        return view;
    }

    private void findViews(View view) {
        serviceLayoutManager = new LinearLayoutManager(context);
        serviceSelectedLayoutManager = new LinearLayoutManager(context);
        leftSideViewHolder = new LeftSideViewHolder(view);
        rightSideViewHolder = new RightSideViewHolder(view);
        rebindData();
    }


    private void rebindData() {
        lstSelectedTechService.clear();
        rightSideViewHolder.total = BigDecimal.ZERO;
        if (mapSelectedService.size() > 0) {
            for (TechServiceHolder tech : mapSelectedService.values()) {
                lstSelectedTechService.add(tech);
                if (!tech.isExpend) {
                    rightSideViewHolder.total = rightSideViewHolder.total.add(tech.priceUnit.multiply(BigDecimal.valueOf(tech.number)));
                }
            }
            rightSideViewHolder.viewState.updateViewState(rightSideViewHolder.viewStateSelectedService = ViewEmptyStateLayout.VIEW_STATE_NORMAL);
        } else {
            rightSideViewHolder.viewState.updateViewState(rightSideViewHolder.viewStateSelectedService = ViewEmptyStateLayout.VIEW_STATE_EMPTY);
        }
        rightSideViewHolder.tvTotal.setText(NumberFormatUtils.formatMoney(rightSideViewHolder.total));
        rightSideViewHolder.adapter.notifyDataSetChanged();
    }


    public class LeftSideViewHolder implements SwipeRefreshLayout.OnRefreshListener,
            SearchView.OnQueryTextListener, BreadcrumbToolbar.BreadcrumbToolbarListener, SelectServiceDialog.OnNumberEnteredListener {
        LinearLayout layout;
        BreadcrumbToolbar toolbar;
        Toolbar toolbarTitle;
        SearchView searchView;
        RecyclerView rvService;
        GeneralSwipeRefreshLayout swipeRefreshLayout;
        ServiceAdapter adapter;
        Stack<ServiceCompound> breadcrumbStack = new Stack<>();
        private String filterString = "";

        boolean loading;

        public LeftSideViewHolder(View view) {
            adapter = new ServiceAdapter();
            layout = (LinearLayout) view.findViewById(R.id.layoutLeft);
            rvService = (RecyclerView) view.findViewById(R.id.recyclerViewListService);
            swipeRefreshLayout = (GeneralSwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
            toolbarTitle = (Toolbar) view.findViewById(R.id.toolbarLeftAbove);
            toolbar = (BreadcrumbToolbar) view.findViewById(R.id.toolbarLeftBelow);
            toolbar.setBreadcrumbToolbarListener(LeftSideViewHolder.this);
            toolbar.setTitle("");
            rvService.setLayoutManager(serviceLayoutManager);
            rvService.addItemDecoration(
                    new HeaderRecyclerDividerItemDecorator(
                            context, DividerItemDecoration.VERTICAL_LIST, false, false,
                            R.drawable.divider_80));
            rvService.setAdapter(adapter);

            // Init first time breadcrumb
            ServiceCompound compound = new ServiceCompound();
            compound.isServiceType = true;
            compound.serviceCode = getString(R.string.specify_service_type);
            breadcrumbStack.add(compound);

            toolbarTitle.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
            toolbarTitle.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().onBackPressed();
                }
            });
            toolbarTitle.inflateMenu(R.menu.menu_search);
            searchView = (SearchView) MenuItemCompat.getActionView(toolbarTitle.getMenu().findItem(R.id.action_search));
            searchView.setOnQueryTextListener(this);

            swipeRefreshLayout.setOnChildScrollUpListener(new GeneralSwipeRefreshLayout.OnChildScrollUpListener() {
                @Override
                public boolean canChildScrollUp() {
                    ((BaseActivity) getActivity()).closeSoftKey();
                    return serviceLayoutManager.findFirstVisibleItemPosition() > 0 ||
                            rvService.getChildAt(0) == null ||
                            rvService.getChildAt(0).getTop() < 0;
                }
            });
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(loading);
                    swipeRefreshLayout.setOnRefreshListener(LeftSideViewHolder.this);
                    checkIfHaveData();
                }
            });

        }

        private void checkIfHaveData() {
            ServiceCompound serviceCompound = breadcrumbStack.peek();
            if (serviceCompound.isServiceType) {
                new LoadServiceType().execute();
            } else {
                new LoadService(serviceCompound).execute();
            }
            swipeRefreshLayout.setRefreshing(true);
        }

        @Override
        public void onRefresh() {
            checkIfHaveData();
        }

        @Override
        public boolean onQueryTextSubmit(String query) {
            searchView.clearFocus();
            return true;
        }

        @Override
        public boolean onQueryTextChange(String s) {
            filterString = s;
            adapter.setFilter(filterString);
            return true;
        }

        @Override
        public void onBreadcrumbToolbarItemPop(int position) {
            for (int i = breadcrumbStack.size(); i > position; i--) {
                breadcrumbStack.pop();
            }
            ServiceCompound compound = breadcrumbStack.peek();
            if (toolbar != null) {
                toolbar.onBreadcrumbAction(breadcrumbStack.size());
            }
            if (compound.isServiceType) {
                new LoadServiceType().execute();
            } else {
                new LoadService(compound).execute();
            }
        }

        @Override
        public void onBreadcrumbToolbarEmpty() {
            toolbar.setTitle("");
        }

        @Override
        public String getBreadcrumbItemName() {
            ServiceCompound compound = breadcrumbStack.peek();
            return compound.serviceCode;
        }

        protected View.OnClickListener OnSelectService = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServiceCompound serviceCompound = (ServiceCompound) v.getTag();
                if (serviceCompound.isLeaf) {
                    // POP UP
                    SelectServiceDialog dialog = new SelectServiceDialog(context, serviceCompound, defaultPatientType.ID, lstPlacedServiceToday, inTime, applyTime);
                    dialog.setOnEnterNumberListener(LeftSideViewHolder.this);
                    dialog.show();
                } else {
                    new LoadService(serviceCompound).execute();
                    serviceCompound.isServiceType = false;
                    breadcrumbStack.add(serviceCompound);
                    if (toolbar != null) {
                        toolbar.onBreadcrumbAction(breadcrumbStack.size());
                    }
                }
                searchView.setQuery("", false);
                filterString = "";
            }
        };

        @Override
        public void onNumberEntered(int number, ServiceCompound serviceCompound, PatientType patientType, boolean isExpend, boolean isWithBHYT, BigDecimal priceUnit) {
            // Add Selected Item
            TechServiceHolder tech = new TechServiceHolder();
            tech.number = number;
            tech.service = serviceCompound;
            tech.patientType = patientType;
            tech.isExpend = isExpend;
            tech.isWithBHXH = isWithBHYT;
            tech.priceUnit = priceUnit;
            if(mapSelectedService.containsKey(serviceCompound.id)){
                tech.number = tech.number + mapSelectedService.get(serviceCompound.id).number;
            }
            mapSelectedService.put(serviceCompound.id, tech);
            rebindData();
        }

        class ServiceAdapter extends RecyclerView.Adapter<ServiceListItem> {
            List<ServiceCompound> viewTechService = new ArrayList<>();

            void updateData() {
                if (!StringUtils.isNullOrEmpty(filterString)) setFilter(filterString);
                else setFilter(null);
            }

            @Override
            public ServiceListItem onCreateViewHolder(ViewGroup parent, int viewType) {
                View v = LayoutInflater.from(context).inflate(R.layout.adapter_left_tech_service, parent, false);
                return new ServiceListItem(v);
            }

            @Override
            public void onBindViewHolder(ServiceListItem holder, int position) {
                ServiceListItem item = holder;
                ServiceCompound content = viewTechService.get(position);
                item.txtServiceName.setText(content.serviceName);
                item.txtServiceCode.setText(content.serviceCode);
                item.imgHasChildren.setVisibility(content.isLeaf ? View.INVISIBLE : View.VISIBLE);
                item.itemView.setTag(content);
                item.itemView.setOnClickListener(OnSelectService);
            }

            @Override
            public int getItemCount() {
                return viewTechService.size();
            }

            public void setFilter(String queryText) {
                viewTechService.clear();
                if (queryText == null || TextUtils.isEmpty(queryText)) {
                    viewTechService.addAll(lstTechService);
                } else {
                    queryText = StringUtils.getEngStringFromUnicodeString(queryText).toLowerCase();
                    for (ServiceCompound sc : lstTechService) {
                        String name = sc.serviceName != null ? StringUtils.getEngStringFromUnicodeString(sc.serviceName).toLowerCase() : "";
                        String code = sc.serviceCode != null ? StringUtils.getEngStringFromUnicodeString(sc.serviceCode).toLowerCase() : "";
                        if (code.contains(queryText) || name.contains(queryText)) {
                            viewTechService.add(sc);
                        }
                    }
                }
                notifyDataSetChanged();
            }
        }

        class ServiceListItem extends RecyclerView.ViewHolder {
            TextView txtServiceCode;
            TextView txtServiceName;
            ImageView imgHasChildren;

            public ServiceListItem(View itemView) {
                super(itemView);
                txtServiceCode = (TextView) itemView.findViewById(R.id.txtServiceCode);
                txtServiceName = (TextView) itemView.findViewById(R.id.txtServiceName);
                imgHasChildren = (ImageView) itemView.findViewById(R.id.imgHasChildren);
            }
        }

        // LOAD DATA LOCAL SQLITE
        class LoadServiceType extends AsyncTask<Void, Void, List<ServiceCompound>> {

            @Override
            protected List<ServiceCompound> doInBackground(Void... params) {
                List<ServiceCompound> lstServiceTypeCompound = new ArrayList<>();
                Dao<ServiceType, Integer> serviceTypeDao = getDatabaseHelper().getServiceTypeDao();
                List<ServiceType> lstServiceType;
                QueryBuilder<ServiceType, Integer> qb = serviceTypeDao.queryBuilder();
                try {
                    qb.where().notIn(ServiceType.FIELD_NAME_SERVICE_TYPE_CODE, new String[]{"TH", "VT"});
                    PreparedQuery<ServiceType> preparedQuery = qb.prepare();
                    lstServiceType = serviceTypeDao.query(preparedQuery);
                    for (ServiceType s : lstServiceType) {
                        ServiceCompound sc = new ServiceCompound();
                        sc.id = s.ID;
                        sc.serviceCode = s.SERVICE_TYPE_CODE;
                        sc.serviceTypeCode = s.SERVICE_TYPE_CODE;
                        sc.serviceName = s.SERVICE_TYPE_NAME;
                        sc.serviceSearch = s.SERVICE_TYPE_SEARCH;
                        sc.isServiceType = true;
                        lstServiceTypeCompound.add(sc);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return lstServiceTypeCompound;
            }

            @Override
            protected void onPostExecute(List<ServiceCompound> serviceCompounds) {
                super.onPostExecute(serviceCompounds);
                lstTechService = serviceCompounds;
                adapter.updateData();
                swipeRefreshLayout.setRefreshing(false);
                if (toolbar != null) {
                    toolbar.onBreadcrumbAction(breadcrumbStack.size());
                }
            }
        }

        class LoadService extends AsyncTask<Void, Void, List<ServiceCompound>> {
            private ServiceCompound service;

            public LoadService(ServiceCompound s) {
                this.service = s;
            }

            @Override
            protected List<ServiceCompound> doInBackground(Void... params) {
                List<ServiceCompound> lstService = new ArrayList<>();
                String code = service.serviceTypeCode;
                Dao<TechService, Integer> serviceDao = getDatabaseHelper().getTechServiceDao();
                QueryBuilder<TechService, Integer> qb = serviceDao.queryBuilder();
                try {
                    if (service.isServiceType) {
                        qb.where().eq(TechService.FIELD_NAME_SERVICE_TYPE_CODE, code).and().eq(TechService.FIELD_NAME_SERVICE_PARENT_ID, 0);
                        qb.orderBy(TechService.FIELD_NAME_SERVICE_NUM_ORDER, false);
                    } else {
                        qb.where().eq(TechService.FIELD_NAME_SERVICE_TYPE_CODE, code).and().eq(TechService.FIELD_NAME_SERVICE_PARENT_ID, service.concreteID);
                        qb.orderBy(TechService.FIELD_NAME_SERVICE_NUM_ORDER, false);
                    }
                    PreparedQuery<TechService> preparedQuery = qb.prepare();
                    List<TechService> lstTechService = serviceDao.query(preparedQuery);
                    for (TechService s : lstTechService) {
                        ServiceCompound sc = new ServiceCompound();
                        sc.id = s.ID;
                        sc.isServiceType = false;
                        sc.serviceCode = s.SERVICE_CODE;
                        sc.serviceTypeCode = s.SERVICE_TYPE_CODE;
                        sc.serviceName = s.SERVICE_NAME;
                        sc.isLeaf = s.IS_LEAF == 1;
                        sc.concreteID = s.CONCRETE_ID;
                        sc.parentId = s.PARENT_ID;
                        lstService.add(sc);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return lstService;
            }

            @Override
            protected void onPostExecute(List<ServiceCompound> serviceCompounds) {
                super.onPostExecute(serviceCompounds);
                lstTechService = serviceCompounds;
                adapter.updateData();
                leftSideViewHolder.swipeRefreshLayout.setRefreshing(false);
            }
        }
    }


    public class RightSideViewHolder implements
            Toolbar.OnMenuItemClickListener, SelectServiceDialog.OnNumberEnteredListener {
        LinearLayout layout;
        Toolbar toolbar;
        ViewEmptyStateLayout viewState;
        RecyclerView rvSelectedService;
        TextView tvTotal;
        TextView tvPatientInfo;

        SelectedServiceAdapter adapter;

        BigDecimal total = BigDecimal.ZERO;
        private int viewStateSelectedService = ViewEmptyStateLayout.VIEW_STATE_EMPTY;


        public RightSideViewHolder(View view) {
            adapter = new SelectedServiceAdapter();
            layout = (LinearLayout) view.findViewById(R.id.layoutRight);
            tvTotal = (TextView) view.findViewById(R.id.tvTotal);
            viewState = (ViewEmptyStateLayout) view.findViewById(R.id.view_state);
            tvPatientInfo = (TextView) view.findViewById(R.id.tvPatientInfo);
            toolbar = (Toolbar) view.findViewById(R.id.toolbarRightAbove);
            toolbar.inflateMenu(R.menu.menu_next);
            toolbar.setOnMenuItemClickListener(this);
            viewState.updateViewState(viewStateSelectedService);
            tvPatientInfo.setText(treatment.getVirPatientName() + " - " + treatment.getTreatmentCode());
            tvTotal.setText(NumberFormatUtils.formatMoney(total));

            rvSelectedService = (RecyclerView) view.findViewById(R.id.recyclerViewSelectedProduct);
            rvSelectedService.setHasFixedSize(true);
            rvSelectedService.setLayoutManager(serviceSelectedLayoutManager);
            rvSelectedService.setAdapter(adapter);

        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            if (lstSelectedTechService == null || lstSelectedTechService.size() <= 0) {
                Toast.makeText(context, R.string.specify_service_selected_list_is_empty, Toast.LENGTH_SHORT).show();
            } else {
                TechServiceHolder[] lstSelectedService = new TechServiceHolder[lstSelectedTechService != null ? lstSelectedTechService.size() : 0];
                if (lstSelectedTechService != null && lstSelectedTechService.size() > 0) {
                    for (int i = 0; i < lstSelectedTechService.size(); i++) {
                        lstSelectedService[i] = lstSelectedTechService.get(i);
                    }
                }
                replaceCurrentFragment(TechServiceFinishFragment.newInstance(treatment, fee, icd, lstSelectedService, isPriority, applyTime));
            }
            return true;
        }

        private View.OnClickListener onRemoveClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TechServiceHolder item = (TechServiceHolder) v.getTag();

                Dialog.Builder builder = null;
                builder = new SimpleDialog.Builder(R.style.Material_App_Dialog_Simple_Light) {
                    @Override
                    public void onPositiveActionClicked(DialogFragment fragment) {
                        super.onPositiveActionClicked(fragment);
                        mapSelectedService.remove(item.service.id);
                        rebindData();
                    }

                    @Override
                    public void onNegativeActionClicked(DialogFragment fragment) {
                        super.onNegativeActionClicked(fragment);
                    }
                };

                ((SimpleDialog.Builder) builder)
                        .message(item.service.serviceName)
                        .title(getString(R.string.specify_service_warning_delete))
                        .positiveAction(getString(R.string.yes))
                        .negativeAction(getString(R.string.no));

                DialogFragment fragment = DialogFragment.newInstance(builder);
                fragment.setCancelable(false);
                fragment.show(getFragmentManager(), null);

            }
        };
        private View.OnClickListener onClickQuantity = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TechServiceHolder item = (TechServiceHolder) v.getTag();
                SelectServiceDialog dialog = new SelectServiceDialog(context, item, lstPlacedServiceToday, inTime, applyTime);
                dialog.setOnEnterNumberListener(RightSideViewHolder.this);
                dialog.show();
            }
        };

        @Override
        public void onNumberEntered(int number, ServiceCompound serviceCompound, PatientType patientType, boolean isExpend, boolean isWithBHYT, BigDecimal priceUnit) {
            // Add Selected Item
            TechServiceHolder tech = new TechServiceHolder();
            tech.number = number;
            tech.service = serviceCompound;
            tech.patientType = patientType;
            tech.isExpend = isExpend;
            tech.isWithBHXH = isWithBHYT;
            tech.priceUnit = priceUnit;
            mapSelectedService.put(serviceCompound.id, tech);
            rebindData();
        }

        class SelectedServiceAdapter extends RecyclerView.Adapter<SelectedServiceViewHolder> {

            @Override
            public SelectedServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(context).inflate(R.layout.adapter_right_tech_service, parent, false);
                return new SelectedServiceViewHolder(view);
            }

            @Override
            public void onBindViewHolder(SelectedServiceViewHolder holder, int position) {
                TechServiceHolder item = lstSelectedTechService.get(position);
                holder.itemView.setOnClickListener(onClickQuantity);
                holder.tvServiceName.setText(item.service.serviceName);
                holder.tvServiceInfo.setText(item.service.serviceCode);
                holder.tvServiceInfo.setText(StringUtils.joinWithDashes(context, item.service.serviceCode,
                        item.isExpend ? "0" : item.priceUnit.compareTo(BigDecimal.ZERO) == 0 ? getString(R.string.specify_service_selected_zero_price) : NumberFormatUtils.formatMoney(item.priceUnit)));
                holder.tvQuantity.setText(String.valueOf(item.number));
                holder.tvQuantity.setOnClickListener(onClickQuantity);
                holder.ibRemove.setOnClickListener(onRemoveClickListener);
                holder.ibRemove.setTag(item);
                holder.tvQuantity.setTag(item);
                holder.itemView.setTag(item);

                if (lstPlacedServiceToday.contains(item.service.id)) {
                    holder.tvQuantity.setBackgroundResource(R.drawable.bg_specify_service_quantity_warning);
                } else {
                    holder.tvQuantity.setBackgroundResource(R.drawable.bg_specify_service_quantity);
                }
            }

            @Override
            public int getItemCount() {
                return lstSelectedTechService == null ? 0 : lstSelectedTechService.size();
            }
        }
    }

    public class ServiceCompound {
        public long id;
        public String serviceName;
        public String serviceCode;
        public String serviceSearch;
        public String serviceTypeCode;
        public boolean isLeaf;
        public int parentId;
        public int concreteID;
        public boolean isServiceType;
    }

    public static class TechServiceHolder implements Serializable, Parcelable {
        public ServiceCompound service;
        public int number;
        public PatientType patientType;
        public BigDecimal priceUnit;
        public boolean isExpend;
        public boolean isWithBHXH;

        protected TechServiceHolder() {
        }

        protected TechServiceHolder(Parcel in) {
            number = in.readInt();
            patientType = in.readParcelable(PatientType.class.getClassLoader());
            isExpend = in.readByte() != 0;
            isWithBHXH = in.readByte() != 0;
        }

        public static final Creator<TechServiceHolder> CREATOR = new Creator<TechServiceHolder>() {
            @Override
            public TechServiceHolder createFromParcel(Parcel in) {
                return new TechServiceHolder(in);
            }

            @Override
            public TechServiceHolder[] newArray(int size) {
                return new TechServiceHolder[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(number);
            dest.writeParcelable(patientType, flags);
            dest.writeByte((byte) (isExpend ? 1 : 0));
            dest.writeByte((byte) (isWithBHXH ? 1 : 0));
        }
    }

    class SelectedServiceViewHolder extends RecyclerView.ViewHolder {
        private TextView tvServiceName;
        private TextView tvServiceInfo;
        private TextView tvQuantity;
        private ImageButton ibRemove;

        public SelectedServiceViewHolder(View itemView) {
            super(itemView);
            tvServiceName = (TextView) itemView.findViewById(R.id.tvServiceName);
            tvServiceInfo = (TextView) itemView.findViewById(R.id.tvServiceInfo);
            tvQuantity = (TextView) itemView.findViewById(R.id.tvQuantity);
            ibRemove = (ImageButton) itemView.findViewById(R.id.ibRemove);
        }
    }

    // Take a quick tech service order with using service group
    HashMap<Long, ServicePATY> mapServicePaty = new HashMap<>();

    /**
     * select *
     * from SERVICE_PATY
     * where SERVICE_ID in (select SERVICE_ID from SERVICE_SERG where SERVICE_GROUP_ID =22)
     * and PATIENT_TYPE_ID =22
     */
    class GetServiceFromServiceGroup extends AsyncTask<Long, Void, List<TechServiceHolder>> {
        long fromTime, toTime, treatmentFromTime, treatmentToTime;
        long currentFromTime, currentToTime, currentTreatmentFromTime, currentTreatmentToTime;

        @Override
        protected List<TechServiceHolder> doInBackground(Long... params) {
            long serviceGroupId = params[0];
            long defaultPatientTypeId = params[1];
            long applyTime = params[2];
            long inTime = params[3];
            List<TechServiceHolder> lst = new ArrayList<>();

            Dao<ServicePATY, Integer> servicePATYDao = getDatabaseHelper().getServicePATYDao();
            QueryBuilder<ServicePATY, Integer> qb = servicePATYDao.queryBuilder();

            Dao<ServiceSegr, Integer> serviceSegrs = getDatabaseHelper().getServiceSegrDao();
            QueryBuilder<ServiceSegr, Integer> qbSegrs = serviceSegrs.queryBuilder();
            try {
                qbSegrs.selectColumns(ServiceSegr.FIELD_NAME_SERVICE_ID);
                qbSegrs.where().eq(ServiceSegr.FIELD_NAME_SERVICE_GROUP_ID, serviceGroupId);

                qb.where().eq(ServicePATY.FIELD_NAME_SERVICE_PATY_PATIENT_TYPE_ID, defaultPatientTypeId).and()
                        .in(ServicePATY.FIELD_NAME_SERVICE_PATY_SERVICE_ID, qbSegrs);

                PreparedQuery preparedQuery = qb.prepare();
                List<ServicePATY> results = servicePATYDao.query(preparedQuery);
                mapServicePaty.clear();
                /**
                 * chỗ lấy ra chính sách giá dựa vào những input sau:
                 - dịch vụ chỉ định là j
                 - patient_type_id là gì
                 - thời gian chỉ định (instruction_time) nằm trong khoảng from_time, to_time (của bảng service_paty) hoặc thời gian vào điều trị (in_time trong bảng his_treatment)
                 nằm trong khoảng treatment_from_time và treatment_to_time (trong bảng his_service_paty)
                 nếu lấy theo những điều kiện đó mà ra 2 bản ghi thì ưu tiên service_paty có priority lớn nhất
                 so sánh priority mà vẫn có hơn 1 bản ghi thì ưu tiên modify_time lớn hơn
                 */
                for (ServicePATY sp : results) {
                    if (!mapServicePaty.containsKey(sp.SERVICE_ID)) {
                        mapServicePaty.put(sp.SERVICE_ID, sp);
                    } else {
                        // So sanh
                        ServicePATY current = mapServicePaty.get(sp.SERVICE_ID);
                        currentFromTime = current.FROM_TIME != null ? current.FROM_TIME : Long.MIN_VALUE;
                        currentToTime = current.TO_TIME != null ? current.TO_TIME : Long.MAX_VALUE;
                        currentTreatmentFromTime = current.TREATMENT_FROM_TIME != null ? current.TREATMENT_FROM_TIME : Long.MIN_VALUE;
                        currentTreatmentToTime = current.TREATMENT_TO_TIME != null ? current.TREATMENT_TO_TIME : Long.MAX_VALUE;

                        fromTime = sp.FROM_TIME != null ? sp.FROM_TIME : Long.MIN_VALUE;
                        toTime = sp.TO_TIME != null ? sp.TO_TIME : Long.MAX_VALUE;
                        treatmentFromTime = sp.TREATMENT_FROM_TIME != null ? sp.TREATMENT_FROM_TIME : Long.MIN_VALUE;
                        treatmentToTime = sp.TREATMENT_TO_TIME != null ? sp.TREATMENT_TO_TIME : Long.MAX_VALUE;

                        if ((fromTime < applyTime && applyTime < toTime) || treatmentFromTime < inTime && inTime < treatmentToTime) {
                            if ((currentFromTime < applyTime && applyTime < currentToTime) || (currentTreatmentFromTime < inTime && inTime < currentTreatmentToTime)) {
                                current.PRIORITY = current.PRIORITY != null ? current.PRIORITY : 0;
                                sp.PRIORITY = sp.PRIORITY != null ? sp.PRIORITY : 0;
                                if (sp.PRIORITY > current.PRIORITY) {
                                    mapServicePaty.put(sp.SERVICE_ID, sp);
                                } else if (sp.PRIORITY.equals(current.PRIORITY)) {
                                    if (sp.MODIFY_TIME > current.MODIFY_TIME) {
                                        mapServicePaty.put(sp.SERVICE_ID, sp);
                                    }
                                }
                            } else {
                                mapServicePaty.put(sp.SERVICE_ID, sp);
                            }
                        }
                    }
                }
                // Get details
                qbSegrs = serviceSegrs.queryBuilder();
                qbSegrs.where().eq(ServiceSegr.FIELD_NAME_SERVICE_GROUP_ID, serviceGroupId);
                List<ServiceSegr> details = serviceSegrs.query(qbSegrs.prepare());
                HashMap<Long, ServiceSegr> mapDetails = new HashMap<>();
                for (ServiceSegr ss : details) {
                    mapDetails.put(ss.SERVICE_ID, ss);
                }
                // Mapping

                for (Map.Entry<Long, ServicePATY> entry : mapServicePaty.entrySet()) {
                    ServiceSegr ss = mapDetails.get(entry.getKey());
                    ServicePATY sp = mapServicePaty.get(entry.getKey());

                    TechServiceHolder item = new TechServiceHolder();

                    item.priceUnit = new BigDecimal(entry.getValue().PRICE);
                    item.number = (int) ss.AMOUNT;
                    item.isExpend = ss.IS_EXPEND != 0;
                    ServiceCompound sc = new ServiceCompound();
                    sc.serviceName = sp.SERVICE_NAME;
                    sc.isServiceType = false;
                    sc.serviceCode = sp.SERVICE_CODE;
                    sc.id = sp.SERVICE_ID;
                    item.service = sc;

                    item.patientType = defaultPatientType;
                    lst.add(item);

                }
                return lst;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<TechServiceHolder> lstTechServiceHolder) {
            super.onPostExecute(lstTechServiceHolder);
            for (TechServiceHolder tech : lstTechServiceHolder) {
                mapSelectedService.put(tech.service.id, tech);
            }
            rebindData();
        }
    }

}

