package com.staarlab.idoctor.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.Treatment;
import com.staarlab.idoctor.entity.TreatmentSummaryInfo;
import com.staarlab.idoctor.model.ResponseArrayModel;
import com.staarlab.idoctor.ui.activity.MedicineMaterialsActivity;
import com.staarlab.idoctor.ui.activity.PatientDetailActivity;
import com.staarlab.idoctor.ui.activity.TechServiceActivity;
import com.staarlab.idoctor.util.AppConst;
import com.staarlab.idoctor.util.CommonUtil;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.WSFactory;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Author: Lucero
 * Created: 11/15/2016.
 */

public class PatientRvAdapter extends RecyclerView.Adapter<PatientRvAdapter.ViewHolder> {
    class ViewHolder extends RecyclerView.ViewHolder {
        final View layoutRow;
        final TextView tvPatientName;
        final TextView tvPatientCode;
        final TextView tvTreatmentCode;
        final TextView tvPatientBirthday;
        final TextView tvSpecifyService;
        final TextView tvSpecifyMM;
        final View viewDetail;
        final ImageView imgGender;

        ViewHolder(View v) {
            super(v);
            layoutRow = v.findViewById(R.id.layoutRow);
            tvPatientName = (TextView) v.findViewById(R.id.tvPatientName);
            tvPatientCode = (TextView) v.findViewById(R.id.tvPatientCode);
            tvTreatmentCode = (TextView) v.findViewById(R.id.tvTreatmentCode);
            tvPatientBirthday = (TextView) v.findViewById(R.id.tvPatientBirthday);
            tvSpecifyService = (TextView) v.findViewById(R.id.tvSpecifyService);
            tvSpecifyMM = (TextView) v.findViewById(R.id.tvSpecifyMM);
            viewDetail = v.findViewById(R.id.viewDetail);
            imgGender = (ImageView) v.findViewById(R.id.imgGender);
        }
    }

    private Context mContext;
    private long roomId;
    private List<Treatment> mPatients;
    private HISService hisService;

    public PatientRvAdapter(Context context, long id) {
        super();
        this.roomId = id;
        mContext = context;
        hisService = WSFactory.createHisService(context.getSharedPreferences(AppConst.APP_PREF_NAME, Context.MODE_PRIVATE)
                .getString(AppConst.PREF_KEY_TOKEN_CODE, ""));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.adapter_patient_list_item, parent, false);
        return new ViewHolder(v);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Treatment treatment = (Treatment) v.getTag();
            if (v.getId() == R.id.tvSpecifyService) {
                showTechService(treatment, roomId);
            } else if (v.getId() == R.id.tvSpecifyMM) {
                showMaterialAndMedicine(treatment, roomId);
            } else if (v.getId() == R.id.viewDetail) {
                showViewInfoPopupMenu(v, treatment);
            } else {
                showPatientDetail(treatment, AppConst.TAB_CODE_GENERAL);
            }
        }
    };

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Treatment patient = mPatients.get(position);
        holder.tvPatientName.setText(patient.getVirPatientName());
        holder.tvPatientCode.setText(patient.getPatientCode());
        holder.tvTreatmentCode.setText(patient.getTreatmentCode());
        holder.tvPatientBirthday.setText(CommonUtil.hisString2DateAsString(patient.getDob()));
        holder.imgGender.setBackgroundResource(patient.getGenderCode().equalsIgnoreCase("02") ? R.drawable.ic_male : R.drawable.ic_female);

        holder.tvSpecifyService.setOnClickListener(onClickListener);
        holder.tvSpecifyMM.setOnClickListener(onClickListener);
        holder.viewDetail.setOnClickListener(onClickListener);
        holder.layoutRow.setOnClickListener(onClickListener);

        holder.tvSpecifyService.setTag(patient);
        holder.tvSpecifyMM.setTag(patient);
        holder.viewDetail.setTag(patient);
        holder.layoutRow.setTag(patient);
    }

    private void showPatientDetail(Treatment treatment, String tabCode) {
        if (treatment.getCommonInfo() != null) {
            Intent intent = new Intent(mContext, PatientDetailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putLong(PatientDetailActivity.P_PATIENT_ID, treatment.getPatientId());
            bundle.putParcelable(PatientDetailActivity.P_TREATMENT_COMMON_INFO, treatment.getCommonInfo());
            bundle.putString(PatientDetailActivity.P_TAB_CODE, tabCode);
            intent.putExtras(bundle);
            mContext.startActivity(intent);
        } else {
            loadTreatmentSummaryInfo(treatment, 1, null);
        }
    }

    @Override
    public int getItemCount() {
        return mPatients == null ? 0 : mPatients.size();
    }

    public void refresh(List<Treatment> patients, boolean notifyDataChanged) {
        mPatients = patients;
        if (notifyDataChanged) notifyDataSetChanged();
    }

    private void showViewInfoPopupMenu(final View view, final Treatment treatment) {
        if (treatment.getCommonInfo() != null) {
            showPopupMenu(treatment, treatment.getCommonInfo(), mContext, view);
        } else {
            loadTreatmentSummaryInfo(treatment, 0, view);
        }
    }

    private void loadTreatmentSummaryInfo(final Treatment treatment, final int postAction, final View view) {
        String param = "{'ApiData':{'ID':" + treatment.getTreatmentId() + "}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getTreatmentSummaryInfo(param, new Callback<ResponseArrayModel<TreatmentSummaryInfo>>() {
            @Override
            public void success(ResponseArrayModel<TreatmentSummaryInfo> responseArrayModel, Response response) {
                if (responseArrayModel.getDataList() == null || responseArrayModel.getDataList().size() <= 0) {
                    return;
                }
                TreatmentSummaryInfo info = responseArrayModel.getDataList().get(0);
                treatment.setCommonInfo(info);
                if (postAction == 0) {
                    showPopupMenu(treatment, info, mContext, view);
                } else if (postAction == 1) {
                    Intent intent = new Intent(mContext, PatientDetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putLong(PatientDetailActivity.P_PATIENT_ID, treatment.getPatientId());
                    bundle.putParcelable(PatientDetailActivity.P_TREATMENT_COMMON_INFO, treatment.getCommonInfo());
                    bundle.putString(PatientDetailActivity.P_TAB_CODE, "");
                    intent.putExtras(bundle);
                    mContext.startActivity(intent);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("XXXX", "Fail:" + error.getMessage());
            }
        });
    }

    private void showPopupMenu(final Treatment treatment, final TreatmentSummaryInfo treatmentSummaryInfo,
                               Context context, View view) {
        Resources rs = mContext.getResources();
        final List<String> menuCodes = new ArrayList<>();
        List<String> menuTitles = new ArrayList<>();
        menuCodes.add(AppConst.TAB_CODE_GENERAL);
        menuTitles.add(rs.getString(R.string.tab_title_general));
        for (int i = 0; i < AppConst.INFO_TABS.length; i++) {
            String code = AppConst.INFO_TABS[i];
            try {
                Method getMethod = TreatmentSummaryInfo.class.getMethod("get" + code);
                Float count = (Float) getMethod.invoke(treatmentSummaryInfo);
                if (count != null && count > 0) {
                    menuCodes.add(code);
                    menuTitles.add(rs.getString(AppConst.INFO_TAB_TITLE_IDS[i]));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        PopupMenu popupMenu = new PopupMenu(context, view);
        for (int i = 0; i < menuTitles.size(); i++) {
            Intent menuIntent = new Intent();
            menuIntent.putExtra("tabCode", menuCodes.get(i));
            popupMenu.getMenu().add(menuTitles.get(i)).setIntent(menuIntent);
        }
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                String tabCode = item.getIntent().getStringExtra("tabCode");
                showPatientDetail(treatment, tabCode);
                return false;
            }
        });
        popupMenu.show();
    }

    private void showActionPopupMenu(Button btnAction, final Treatment treatment) {
        PopupMenu popupMenu = new PopupMenu(mContext, btnAction);
        popupMenu.inflate(R.menu.menu_patient_detail);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.specify_technique:
                        showTechService(treatment, roomId);
                        break;
                    case R.id.specify_drug_and_material:
                        showMaterialAndMedicine(treatment, roomId);
                        break;
                }
                return false;
            }
        });
        popupMenu.show();
    }

    private void showTechService(Treatment treatment, long roomId) {
        Intent intent = new Intent(mContext, TechServiceActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("TREATMENT", treatment);
        bundle.putLong("roomId", roomId);
        intent.putExtras(bundle);
        mContext.startActivity(intent);
    }

    private void showMaterialAndMedicine(Treatment treatment, long roomId) {
        Intent intent = new Intent(mContext, MedicineMaterialsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("TREATMENT", treatment);
        bundle.putLong("roomId", roomId);
        intent.putExtras(bundle);
        mContext.startActivity(intent);
    }
}
