package com.staarlab.idoctor.ui.service;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.staarlab.idoctor.entity.EmteMaterial;
import com.staarlab.idoctor.entity.EmteMedicine;
import com.staarlab.idoctor.entity.ExpMestTemplate;
import com.staarlab.idoctor.entity.ICD;
import com.staarlab.idoctor.entity.MedicalStock;
import com.staarlab.idoctor.entity.MedicineUserForm;
import com.staarlab.idoctor.entity.PatientType;
import com.staarlab.idoctor.entity.PatientTypeAllow;
import com.staarlab.idoctor.entity.ServiceGroup;
import com.staarlab.idoctor.entity.ServicePATY;
import com.staarlab.idoctor.entity.ServiceRoom;
import com.staarlab.idoctor.entity.ServiceSegr;
import com.staarlab.idoctor.entity.ServiceType;
import com.staarlab.idoctor.entity.TechService;
import com.staarlab.idoctor.helper.db.MBSDatabaseHelper;
import com.staarlab.idoctor.helper.share.MBSSharePreference;
import com.staarlab.idoctor.model.ResponseArrayModel;
import com.staarlab.idoctor.util.DateTimeUtils;
import com.staarlab.idoctor.util.StringUtils;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.WSFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Callable;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by PHAMHUNG on 3/5/2017.
 */

public class SyncDataService extends Service {
    private static String TAG = "SyncDataService";

    public final static String ACTION_UPDATE_SUCCESS = "ACTION_UPDATE_SUCCESS";
    public final static String ACTION_UPDATE_FAIL = "ACTION_UPDATE_FAIL";

    private static final int WHAT_ICD = 0;
    private static final int WHAT_TECH_SERVICE = 1;
    private static final int WHAT_TECH_SERVICE_TYPE = 2;
    private static final int WHAT_PATIENT_TYPE = 3;
    private static final int WHAT_SERVICE_PATY = 4;
    private static final int WHAT_SERVICE_GROUP = 5;
    private static final int WHAT_SERVICE_SEGR = 6;
    private static final int WHAT_MEDICAL_STOCK = 7;
    private static final int WHAT_MEDICINE_USE_FORM = 8;
    private static final int WHAT_SERVICE_ROOM = 9;
    private static final int WHAT_MEST_TEMPLATE = 10;
    private static final int WHAT_EMTE_MEDICINE = 11;
    private static final int WHAT_EMTE_MATERIAL = 12;
    private static final int WHAT_PATIENT_TYPE_ALLOW = 13;

    List<ICD> lstICD = new ArrayList<>();
    List<TechService> lstTechService = new ArrayList<>();
    List<ServiceType> lstTechServiceType = new ArrayList<>();
    List<PatientType> lstPatientType = new ArrayList<>();
    List<ServicePATY> lstServicePATY = new ArrayList<>();
    List<ServiceGroup> lstServiceGroup = new ArrayList<>();
    List<ServiceSegr> lstServiceSegr = new ArrayList<>();
    List<MedicalStock> lstMedicalStock = new ArrayList<>();
    List<MedicineUserForm> lstMedicineUserForms = new ArrayList<>();
    List<ServiceRoom> lstServiceRoom = new ArrayList<>();
    List<ExpMestTemplate> lstExpMestTemplates = new ArrayList<>();
    List<EmteMedicine> lstEmteMedicines = new ArrayList<>();
    List<EmteMaterial> lstEmteMaterials = new ArrayList<>();
    List<PatientTypeAllow> lstPatientTypeAllows = new ArrayList<>();

    private static HISService hisService;

    @Override
    public void onCreate() {
        super.onCreate();
        hisService = WSFactory.createHisService(MBSSharePreference.get().getTokenCode());
        isSyncingFail = false;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        syncData();
        return super.onStartCommand(intent, flags, startId);
    }

    private void syncData() {
        syncICD();
    }

    private void sendBroadcastIfSyncSuccess() {
        Intent i = new Intent();
        i.setAction(ACTION_UPDATE_SUCCESS);
        LocalBroadcastManager.getInstance(SyncDataService.this).sendBroadcast(i);
        stopSelf();
    }

    private void sendBroadcastIfSyncFail() {
        Intent i = new Intent();
        i.setAction(ACTION_UPDATE_FAIL);
        LocalBroadcastManager.getInstance(SyncDataService.this).sendBroadcast(i);
        stopSelf();
    }

    // SYNC DATABASE
    // Sync Task
    // ICD -> TechService -> TechServiceTYpe -> PatientType ->ServicePATY ->ServiceGroup ->ServiceSegr -> Medical Stock -> MedicineUse Form
    //
    String param = "";
    boolean isSyncingFail = false;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            new WriteDataTask(msg.what).execute();
        }
    };

    private void syncICD() {
        param = "{'ApiData':{'IS_ACTIVE':1}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getMainICD(param, new Callback<ResponseArrayModel<ICD>>() {
            @Override
            public void success(ResponseArrayModel<ICD> data, Response response) {
                Log.i(TAG, "Load ICD success");
                MBSDatabaseHelper.getInstance().clearTableICD();
                lstICD = data.getDataList();
                Message msg = mHandler.obtainMessage(WHAT_ICD);
                mHandler.dispatchMessage(msg);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.i(TAG, "Load ICD fail");
                isSyncingFail = true;
                sendBroadcastIfSyncFail();
            }
        });
    }

    private void syncTechService() {
        param = "{'ApiData':{'IS_ACTIVE':1}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getTechService(param, new Callback<ResponseArrayModel<TechService>>() {
            @Override
            public void success(ResponseArrayModel<TechService> data, Response response) {
                Log.i(TAG, "Load Tech Service success");
                MBSDatabaseHelper.getInstance().clearTableTechService();
                lstTechService = data.getDataList();
                Message msg = mHandler.obtainMessage(WHAT_TECH_SERVICE);
                mHandler.dispatchMessage(msg);

            }

            @Override
            public void failure(RetrofitError error) {
                isSyncingFail = true;
                sendBroadcastIfSyncFail();
            }
        });
    }

    private void syncTechServiceType() {
        param = "{'ApiData':{'IS_ACTIVE':1}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getTechServiceType(param, new Callback<ResponseArrayModel<ServiceType>>() {
            @Override
            public void success(ResponseArrayModel<ServiceType> data, Response response) {
                Log.i(TAG, "Load Tech Service success");
                MBSDatabaseHelper.getInstance().clearTableServiceType();
                lstTechServiceType = data.getDataList();
                Message msg = mHandler.obtainMessage(WHAT_TECH_SERVICE_TYPE);
                mHandler.dispatchMessage(msg);

            }

            @Override
            public void failure(RetrofitError error) {
                isSyncingFail = true;
                sendBroadcastIfSyncFail();
            }
        });
    }

    private void syncPatientType() {
        param = "{'ApiData':{'IS_ACTIVE':1}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getPatientType(param, new Callback<ResponseArrayModel<PatientType>>() {
            @Override
            public void success(ResponseArrayModel<PatientType> data, Response response) {
                Log.i(TAG, "Load Patient type success");
                MBSDatabaseHelper.getInstance().clearTablePatientType();
                lstPatientType = data.getDataList();
                Message msg = mHandler.obtainMessage(WHAT_PATIENT_TYPE);
                mHandler.dispatchMessage(msg);

            }

            @Override
            public void failure(RetrofitError error) {
                isSyncingFail = true;
                sendBroadcastIfSyncFail();
            }
        });
    }

    private void syncServicePATY() {
        param = "{'ApiData':{'IS_ACTIVE':1}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getServicePATY(param, new Callback<ResponseArrayModel<ServicePATY>>() {
            @Override
            public void success(ResponseArrayModel<ServicePATY> data, Response response) {
                Log.i(TAG, "Load service PATY success");
                MBSDatabaseHelper.getInstance().clearTableServicePATY();
                lstServicePATY = data.getDataList();
                Message msg = mHandler.obtainMessage(WHAT_SERVICE_PATY);
                mHandler.dispatchMessage(msg);

            }

            @Override
            public void failure(RetrofitError error) {
                isSyncingFail = true;
                sendBroadcastIfSyncFail();
            }
        });
    }

    private void syncServiceGroup() {
        param = "{'ApiData':{'IS_ACTIVE':1}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getServiceGroup(param, new Callback<ResponseArrayModel<ServiceGroup>>() {
            @Override
            public void success(ResponseArrayModel<ServiceGroup> data, Response response) {
                Log.i(TAG, "Load Service Group success");
                MBSDatabaseHelper.getInstance().clearTableServiceGroup();
                lstServiceGroup = data.getDataList();
                Message msg = mHandler.obtainMessage(WHAT_SERVICE_GROUP);
                mHandler.dispatchMessage(msg);

            }

            @Override
            public void failure(RetrofitError error) {
                isSyncingFail = true;
                sendBroadcastIfSyncFail();
            }
        });
    }

    private void syncServiceSegr() {
        param = "{'ApiData':{'IS_ACTIVE':1}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getServiceSegr(param, new Callback<ResponseArrayModel<ServiceSegr>>() {
            @Override
            public void success(ResponseArrayModel<ServiceSegr> data, Response response) {
                Log.i(TAG, "Load Service Serg success");
                MBSDatabaseHelper.getInstance().clearTableServiceSegr();
                lstServiceSegr = data.getDataList();
                Message msg = mHandler.obtainMessage(WHAT_SERVICE_SEGR);
                mHandler.dispatchMessage(msg);

            }

            @Override
            public void failure(RetrofitError error) {
                isSyncingFail = true;
                sendBroadcastIfSyncFail();
            }
        });
    }

    private void syncMedicalStock() {
        param = "{'ApiData':{'IS_ACTIVE':1}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getMedicalStock(param, new Callback<ResponseArrayModel<MedicalStock>>() {
            @Override
            public void success(ResponseArrayModel<MedicalStock> data, Response response) {
                Log.i(TAG, "Load Medical Stock success");
                MBSDatabaseHelper.getInstance().clearTableMedicalStock();
                lstMedicalStock = data.getDataList();
                Message msg = mHandler.obtainMessage(WHAT_MEDICAL_STOCK);
                mHandler.dispatchMessage(msg);

            }

            @Override
            public void failure(RetrofitError error) {
                isSyncingFail = true;
                sendBroadcastIfSyncFail();
            }
        });
    }

    private void syncMedicineUseForm() {
        param = "{'ApiData':{'IS_ACTIVE':1}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getMedicineUseForm(param, new Callback<ResponseArrayModel<MedicineUserForm>>() {
            @Override
            public void success(ResponseArrayModel<MedicineUserForm> data, Response response) {
                Log.i(TAG, "Load Medicine Use form success");
                MBSDatabaseHelper.getInstance().clearTableMedicineUseForm();
                lstMedicineUserForms = data.getDataList();
                Message msg = mHandler.obtainMessage(WHAT_MEDICINE_USE_FORM);
                mHandler.dispatchMessage(msg);

            }

            @Override
            public void failure(RetrofitError error) {
                isSyncingFail = true;
                sendBroadcastIfSyncFail();
            }
        });
    }

    private void syncServiceRoom() {
        param = "{'ApiData':{'IS_ACTIVE':1}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getServiceRoom(param, new Callback<ResponseArrayModel<ServiceRoom>>() {
            @Override
            public void success(ResponseArrayModel<ServiceRoom> data, Response response) {
                Log.i(TAG, "Load Service Room success");
                MBSDatabaseHelper.getInstance().clearTableServiceRoom();
                lstServiceRoom = data.getDataList();
                Message msg = mHandler.obtainMessage(WHAT_SERVICE_ROOM);
                mHandler.dispatchMessage(msg);
            }

            @Override
            public void failure(RetrofitError error) {
                isSyncingFail = true;
                sendBroadcastIfSyncFail();
            }
        });
    }

    private void syncExpTemplate() {
        param = "{'ApiData':{'IS_ACTIVE':1}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getExpMestTemplate(param, new Callback<ResponseArrayModel<ExpMestTemplate>>() {
            @Override
            public void success(ResponseArrayModel<ExpMestTemplate> data, Response response) {
                Log.i(TAG, "Load Exp Template success");
                MBSDatabaseHelper.getInstance().clearTableExpTemplate();
                lstExpMestTemplates = data.getDataList();
                Message msg = mHandler.obtainMessage(WHAT_MEST_TEMPLATE);
                mHandler.dispatchMessage(msg);
            }

            @Override
            public void failure(RetrofitError error) {
                isSyncingFail = true;
                sendBroadcastIfSyncFail();
            }
        });
    }

    private void syncEmteMedicine() {
        param = "{'ApiData':{'IS_ACTIVE':1}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getEmteMedicine(param, new Callback<ResponseArrayModel<EmteMedicine>>() {
            @Override
            public void success(ResponseArrayModel<EmteMedicine> data, Response response) {
                Log.i(TAG, "Load Exp Template success");
                MBSDatabaseHelper.getInstance().clearTableEmteMedicine();
                lstEmteMedicines = data.getDataList();
                Message msg = mHandler.obtainMessage(WHAT_EMTE_MEDICINE);
                mHandler.dispatchMessage(msg);
            }

            @Override
            public void failure(RetrofitError error) {
                isSyncingFail = true;
                sendBroadcastIfSyncFail();
            }
        });
    }

    private void syncEmteMaterial() {
        param = "{'ApiData':{'IS_ACTIVE':1}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getEmteMaterial(param, new Callback<ResponseArrayModel<EmteMaterial>>() {
            @Override
            public void success(ResponseArrayModel<EmteMaterial> data, Response response) {
                Log.i(TAG, "Load Exp Template success");
                MBSDatabaseHelper.getInstance().clearTableEmteMaterial();
                lstEmteMaterials = data.getDataList();
                Message msg = mHandler.obtainMessage(WHAT_EMTE_MATERIAL);
                mHandler.dispatchMessage(msg);
            }

            @Override
            public void failure(RetrofitError error) {
                isSyncingFail = true;
                sendBroadcastIfSyncFail();
            }
        });
    }

    private void syncPatientTypeAllow() {
        param = "{'ApiData':{'IS_ACTIVE':1}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getPatientTypeAllow(param, new Callback<ResponseArrayModel<PatientTypeAllow>>() {
            @Override
            public void success(ResponseArrayModel<PatientTypeAllow> data, Response response) {
                Log.i(TAG, "Load Patient Type Allow");
                MBSDatabaseHelper.getInstance().clearTablePatientTypeAllow();
                lstPatientTypeAllows = data.getDataList();
                Message msg = mHandler.obtainMessage(WHAT_PATIENT_TYPE_ALLOW);
                mHandler.dispatchMessage(msg);
            }

            @Override
            public void failure(RetrofitError error) {
                isSyncingFail = true;
                sendBroadcastIfSyncFail();
            }
        });
    }

//-------------- WRITE TO SQLITE

    class WriteDataTask extends AsyncTask<Void, Void, Void> {
        private int sync;

        public WriteDataTask(int sync) {
            this.sync = sync;
        }

        @Override
        protected Void doInBackground(Void... params) {
            switch (sync) {
                case WHAT_ICD:
                    if (lstICD != null && lstICD.size() > 0) {
                        try {
                            Log.i(TAG, "Parse ICD " + lstICD.size());
                            final Dao<ICD, Integer> icdDao = MBSDatabaseHelper.getInstance().getIcdDao();
                            icdDao.callBatchTasks(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    for (ICD icd : lstICD) {
                                        icd.ICD_SEARCH = StringUtils.getEngStringFromUnicodeString(icd.ICD_CODE + " " + icd.ICD_NAME + " " + icd.ICD_NAME_EN);
                                        icdDao.create(icd);
                                    }
                                    return null;
                                }
                            });

                        } catch (Exception ex) {
                            Log.e(TAG, ex.getMessage());
                            isSyncingFail = true;
                        }
                    }
                    break;
                case WHAT_TECH_SERVICE:
                    if (lstTechService != null && lstTechService.size() > 0) {
                        try {
                            Log.i(TAG, "Parse Tech Service " + lstTechService.size());
                            final Dao<TechService, Integer> techServiceDao = MBSDatabaseHelper.getInstance().getTechServiceDao();
                            techServiceDao.callBatchTasks(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    for (TechService service : lstTechService) {
                                        service.SERVICE_SEARCH = StringUtils.getEngStringFromUnicodeString(service.SERVICE_CODE + " " + service.SERVICE_NAME);
                                        techServiceDao.create(service);
                                    }
                                    return null;
                                }
                            });

                        } catch (Exception ex) {
                            Log.e(TAG, ex.getMessage());
                            isSyncingFail = true;
                        }
                    }
                    break;
                case WHAT_TECH_SERVICE_TYPE:
                    if (lstTechServiceType != null && lstTechServiceType.size() > 0) {
                        try {
                            Log.i(TAG, "Parse Tech Service Type " + lstTechServiceType.size());
                            final Dao<ServiceType, Integer> techServiceTypeDao = MBSDatabaseHelper.getInstance().getServiceTypeDao();
                            techServiceTypeDao.callBatchTasks(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    for (ServiceType type : lstTechServiceType) {
                                        type.SERVICE_TYPE_SEARCH = StringUtils.getEngStringFromUnicodeString(type.SERVICE_TYPE_CODE + " " + type.SERVICE_TYPE_NAME);
                                        techServiceTypeDao.create(type);
                                    }
                                    return null;
                                }
                            });

                        } catch (Exception ex) {
                            Log.e(TAG, ex.getMessage());
                            isSyncingFail = true;
                        }
                    }
                    break;
                case WHAT_PATIENT_TYPE:
                    if (lstPatientType != null && lstPatientType.size() > 0) {
                        try {
                            Log.i(TAG, "Parse Patient Type " + lstPatientType.size());
                            final Dao<PatientType, Integer> patientTypeDao = MBSDatabaseHelper.getInstance().getPatientTypeDao();
                            patientTypeDao.callBatchTasks(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    for (PatientType type : lstPatientType) {
                                        patientTypeDao.create(type);
                                    }
                                    return null;
                                }
                            });

                        } catch (Exception ex) {
                            Log.e(TAG, ex.getMessage());
                            isSyncingFail = true;
                        }
                    }
                    break;
                case WHAT_SERVICE_PATY:
                    if (lstServicePATY != null && lstServicePATY.size() > 0) {
                        try {
                            Log.i(TAG, "Parse Service PATY " + lstServicePATY.size());
                            final Dao<ServicePATY, Integer> servicePATHDao = MBSDatabaseHelper.getInstance().getServicePATYDao();
                            servicePATHDao.callBatchTasks(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    for (ServicePATY type : lstServicePATY) {
                                        servicePATHDao.create(type);
                                    }
                                    return null;
                                }
                            });

                        } catch (Exception ex) {
                            Log.e(TAG, ex.getMessage());
                            isSyncingFail = true;
                        }
                    }
                    break;
                case WHAT_SERVICE_GROUP:
                    if (lstServiceGroup != null && lstServiceGroup.size() > 0) {
                        try {
                            Log.i(TAG, "Parse Service Group " + lstServiceGroup.size());
                            final Dao<ServiceGroup, Integer> serviceGroupDao = MBSDatabaseHelper.getInstance().getServiceGroupDao();
                            serviceGroupDao.callBatchTasks(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    for (ServiceGroup type : lstServiceGroup) {
                                        type.SERVICE_GROUP_SEARCH = StringUtils.getEngStringFromUnicodeString(type.SERVICE_GROUP_CODE + " " + type.SERVICE_GROUP_NAME);
                                        serviceGroupDao.create(type);
                                    }
                                    return null;
                                }
                            });

                        } catch (Exception ex) {
                            Log.e(TAG, ex.getMessage());
                            isSyncingFail = true;
                        }
                    }
                    break;
                case WHAT_SERVICE_SEGR:
                    if (lstServiceSegr != null && lstServiceSegr.size() > 0) {
                        try {
                            Log.i(TAG, "Parse Service SEGR " + lstServiceSegr.size());
                            final Dao<ServiceSegr, Integer> serviceSegrDao = MBSDatabaseHelper.getInstance().getServiceSegrDao();
                            serviceSegrDao.callBatchTasks(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    for (ServiceSegr type : lstServiceSegr) {
                                        serviceSegrDao.create(type);
                                    }
                                    return null;
                                }
                            });

                        } catch (Exception ex) {
                            Log.e(TAG, ex.getMessage());
                            isSyncingFail = true;
                        }
                    }
                    break;
                case WHAT_MEDICAL_STOCK:
                    if (lstMedicalStock != null && lstMedicalStock.size() > 0) {
                        try {
                            Log.i(TAG, "Parse Service SEGR " + lstMedicalStock.size());
                            final Dao<MedicalStock, Integer> medicalStockDao = MBSDatabaseHelper.getInstance().getMedicalStockDao();
                            medicalStockDao.callBatchTasks(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    for (MedicalStock type : lstMedicalStock) {
                                        medicalStockDao.create(type);
                                    }
                                    return null;
                                }
                            });

                        } catch (Exception ex) {
                            Log.e(TAG, ex.getMessage());
                            isSyncingFail = true;
                        }
                    }
                    break;
                case WHAT_MEDICINE_USE_FORM:
                    if (lstMedicineUserForms != null && lstMedicineUserForms.size() > 0) {
                        try {
                            Log.i(TAG, "Parse Service SEGR " + lstMedicineUserForms.size());
                            final Dao<MedicineUserForm, Integer> medicineUserFormDao = MBSDatabaseHelper.getInstance().getMedicineUserFormDao();
                            medicineUserFormDao.callBatchTasks(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    for (MedicineUserForm type : lstMedicineUserForms) {
                                        medicineUserFormDao.create(type);
                                    }
                                    return null;
                                }
                            });

                        } catch (Exception ex) {
                            Log.e(TAG, ex.getMessage());
                            isSyncingFail = true;
                        }
                    }
                    break;
                case WHAT_SERVICE_ROOM:
                    if (lstServiceRoom != null && lstServiceRoom.size() > 0) {
                        try {
                            Log.i(TAG, "Parse Service Room " + lstServiceRoom.size());
                            final Dao<ServiceRoom, Integer> serviceRoomDao = MBSDatabaseHelper.getInstance().getServiceRoomDao();
                            serviceRoomDao.callBatchTasks(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    for (ServiceRoom type : lstServiceRoom) {
                                        serviceRoomDao.create(type);
                                    }
                                    return null;
                                }
                            });

                        } catch (Exception ex) {
                            Log.e(TAG, ex.getMessage());
                            isSyncingFail = true;
                        }
                    }
                    break;
                case WHAT_MEST_TEMPLATE:
                    if (lstExpMestTemplates != null && lstExpMestTemplates.size() > 0) {
                        try {
                            Log.i(TAG, "Parse ExpMest Template " + lstExpMestTemplates.size());
                            final Dao<ExpMestTemplate, Integer> expMestTemplates = MBSDatabaseHelper.getInstance().getMestTemplateDao();
                            expMestTemplates.callBatchTasks(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    for (ExpMestTemplate type : lstExpMestTemplates) {
                                        type.EXP_MEST_TEMPLATE_SEARCH = StringUtils.getEngStringFromUnicodeString(type.EXP_MEST_TEMPLATE_NAME + " " + type.FIELD_NAME_EXP_TEMPLATE_DESCRIPTION);
                                        expMestTemplates.create(type);
                                    }
                                    return null;
                                }
                            });

                        } catch (Exception ex) {
                            Log.e(TAG, ex.getMessage());
                            isSyncingFail = true;
                        }
                    }
                    break;

                case WHAT_EMTE_MEDICINE:
                    if (lstEmteMedicines != null && lstEmteMedicines.size() > 0) {
                        try {
                            Log.i(TAG, "Parse Emte Medicine " + lstEmteMedicines.size());
                            final Dao<EmteMedicine, Integer> emteMedicines = MBSDatabaseHelper.getInstance().getEmteMedicine();
                            emteMedicines.callBatchTasks(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    for (EmteMedicine type : lstEmteMedicines) {
                                        emteMedicines.create(type);
                                    }
                                    return null;
                                }
                            });

                        } catch (Exception ex) {
                            Log.e(TAG, ex.getMessage());
                            isSyncingFail = true;
                        }
                    }
                    break;

                case WHAT_EMTE_MATERIAL:
                    if (lstEmteMaterials != null && lstEmteMaterials.size() > 0) {
                        try {
                            Log.i(TAG, "Parse Emte Material " + lstEmteMaterials.size());
                            final Dao<EmteMaterial, Integer> emteMaterials = MBSDatabaseHelper.getInstance().getEmteMaterial();
                            emteMaterials.callBatchTasks(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    for (EmteMaterial type : lstEmteMaterials) {
                                        emteMaterials.create(type);
                                    }
                                    return null;
                                }
                            });

                        } catch (Exception ex) {
                            Log.e(TAG, ex.getMessage());
                            isSyncingFail = true;
                        }
                    }
                    break;
                case WHAT_PATIENT_TYPE_ALLOW:
                    if (lstPatientTypeAllows != null && lstPatientTypeAllows.size() > 0) {
                        try {
                            Log.i(TAG, "Parse Patient Type Allow " + lstPatientTypeAllows.size());
                            final Dao<PatientTypeAllow, Integer> patientTypeAllows = MBSDatabaseHelper.getInstance().getPatientTypeAllow();
                            patientTypeAllows.callBatchTasks(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    for (PatientTypeAllow type : lstPatientTypeAllows) {
                                        patientTypeAllows.create(type);
                                    }
                                    return null;
                                }
                            });

                        } catch (Exception ex) {
                            Log.e(TAG, ex.getMessage());
                            isSyncingFail = true;
                        }
                    }
                    break;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            switch (sync) {
                case WHAT_ICD:
                    Log.i(TAG, "Write ICD success");
                    lstICD = null;
                    syncTechService();
                    break;
                case WHAT_TECH_SERVICE:
                    Log.i(TAG, "Write Tech Service success");
                    lstTechService = null;
                    syncTechServiceType();
                    break;
                case WHAT_TECH_SERVICE_TYPE:
                    Log.i(TAG, "Write Service Type success");
                    lstTechServiceType = null;
                    syncPatientType();
                    break;
                case WHAT_PATIENT_TYPE:
                    Log.i(TAG, "Write Patient Type success");
                    lstPatientType = null;
                    syncServicePATY();
                    break;
                case WHAT_SERVICE_PATY:
                    Log.i(TAG, "Write Service PATY success");
                    lstServicePATY = null;
                    syncServiceGroup();
                    break;
                case WHAT_SERVICE_GROUP:
                    Log.i(TAG, "Write Service Group success");
                    lstServiceGroup = null;
                    syncServiceSegr();
                    break;
                case WHAT_SERVICE_SEGR:
                    Log.i(TAG, "Write Service Segr success");
                    lstServiceSegr = null;
                    syncMedicalStock();
                    break;
                case WHAT_MEDICAL_STOCK:
                    Log.i(TAG, "Write Medical Stock success");
                    lstMedicalStock = null;
                    syncMedicineUseForm();
                    break;
                case WHAT_MEDICINE_USE_FORM:
                    Log.i(TAG, "Write Medicine Use Form success");
                    lstMedicineUserForms = null;
                    syncServiceRoom();
                    break;
                case WHAT_SERVICE_ROOM:
                    Log.i(TAG, "Write Service Room success");
                    lstServiceRoom = null;
                    syncExpTemplate();
                    break;
                case WHAT_MEST_TEMPLATE:
                    Log.i(TAG, "Write Exp Mest Template success");
                    lstExpMestTemplates = null;
                    syncEmteMedicine();
                    break;
                case WHAT_EMTE_MEDICINE:
                    Log.i(TAG, "Write Emte Medicine success");
                    lstEmteMedicines = null;
                    syncEmteMaterial();
                    break;
                case WHAT_EMTE_MATERIAL:
                    Log.i(TAG, "Write Emte Material success");
                    lstEmteMaterials = null;
                    syncPatientTypeAllow();
                    break;

                case WHAT_PATIENT_TYPE_ALLOW:
                    Log.i(TAG, "Write Patient Type Allow success");
                    lstPatientTypeAllows = null;
                    MBSSharePreference.get().putLastUpdate(DateTimeUtils.formatDateAndTime(Calendar.getInstance().getTime()));
                    sendBroadcastIfSyncSuccess();
                    break;

            }
        }
    }
}
