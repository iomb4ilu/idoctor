package com.staarlab.idoctor.ui.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.Material;
import com.staarlab.idoctor.entity.Medicine;
import com.staarlab.idoctor.entity.MedicineMaterial;
import com.staarlab.idoctor.entity.MedicineUserForm;
import com.staarlab.idoctor.entity.PatientType;
import com.staarlab.idoctor.entity.TreatmentCommonInfo;
import com.staarlab.idoctor.helper.layout.DividerItemDecoration;
import com.staarlab.idoctor.helper.layout.GeneralSwipeRefreshLayout;
import com.staarlab.idoctor.helper.layout.HeaderRecyclerDividerItemDecorator;
import com.staarlab.idoctor.helper.layout.ViewEmptyStateLayout;
import com.staarlab.idoctor.helper.share.MBSSharePreference;
import com.staarlab.idoctor.model.ResponseArrayModel;
import com.staarlab.idoctor.ui.dialog.SelectMedicineMaterialDialog;
import com.staarlab.idoctor.util.NumberFormatUtils;
import com.staarlab.idoctor.util.StringUtils;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.WSFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MedicineMaterialListFragment extends BaseFragment implements GeneralSwipeRefreshLayout.OnRefreshListener, SelectMedicineMaterialDialog.OnNumberEnteredListener, SearchView.OnQueryTextListener {
    public static int UPDATE_FROM_MEDICINE = 1;
    public static int UPDATE_FROM_MATERIAL = 2;

    private TextView tvInfoTitle;
    private GeneralSwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvData;
    private SearchView searchView;
    ViewEmptyStateLayout viewState;

    private boolean isMedicineTab;
    private long medicalStockId;
    private TreatmentCommonInfo treatmentCommonInfo;
    long[] arrPlacedServiceToday;
    private long instructionTime;
    boolean loading;

    List<MedicineMaterial> data = new ArrayList<>();
    MedicineMaterialAdapter adapter;
    private int viewStateSelectedService = ViewEmptyStateLayout.VIEW_STATE_EMPTY;

    private static HISService hisService;
    OnSelectMedicineMaterial listener;
    private static Handler mHandler;
    boolean flagFirstInit = true;
    private String filterString = "";
    IUpdateInventory updater;


    public MedicineMaterialListFragment() {
        // Required empty public constructor
    }

    public static MedicineMaterialListFragment newInstance(boolean isMedicine, long medicalStockId, TreatmentCommonInfo info, long[] lstPlacedService, long instructionTime) {
        MedicineMaterialListFragment fragment = new MedicineMaterialListFragment();
        Bundle args = new Bundle();
        args.putBoolean("isMedicine", isMedicine);
        args.putLong("medicalStockId", medicalStockId);
        args.putParcelable("info", info);
        args.putLongArray("lst", lstPlacedService);
        args.putLong("instructionTime", instructionTime);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isMedicineTab = getArguments().getBoolean("isMedicine");
            medicalStockId = getArguments().getLong("medicalStockId");
            treatmentCommonInfo = getArguments().getParcelable("info");
            arrPlacedServiceToday = getArguments().getLongArray("lst");
            instructionTime = getArguments().getLong("instructionTime");
        }
        mHandler = new Handler();
        hisService = WSFactory.createHisService(MBSSharePreference.get().getTokenCode());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_medicine_material_list, container, false);
        adapter = new MedicineMaterialAdapter();

        viewState = (ViewEmptyStateLayout) view.findViewById(R.id.view_state);
        tvInfoTitle = (TextView) view.findViewById(R.id.tvInfoTitle);
        swipeRefreshLayout = (GeneralSwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        rvData = (RecyclerView) view.findViewById(R.id.recyclerView);
        rvData.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvData.addItemDecoration(
                new HeaderRecyclerDividerItemDecorator(
                        context, DividerItemDecoration.VERTICAL_LIST, false, false,
                        R.drawable.divider_80));
        rvData.setAdapter(adapter);
        viewState.updateViewState(viewStateSelectedService);

        tvInfoTitle.setText(isMedicineTab ? R.string.specify_medicine_material_medicine : R.string.specify_medicine_material_material);
        swipeRefreshLayout.setOnChildScrollUpListener(new GeneralSwipeRefreshLayout.OnChildScrollUpListener() {
            @Override
            public boolean canChildScrollUp() {
                return rvData.getChildAt(0) == null ||
                        rvData.getChildAt(0).getTop() < 0;
            }
        });
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(loading);
                swipeRefreshLayout.setOnRefreshListener(MedicineMaterialListFragment.this);
                checkIfHaveData();
            }
        });
        flagFirstInit = true;
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_search, menu);
        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        searchView.setOnQueryTextListener(this);

    }

    private void checkIfHaveData() {
        swipeRefreshLayout.setRefreshing(true);
        runnable.run();
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            try {
                String param = "{'ApiData':{'MEDI_STOCK_ID':" + medicalStockId + "}}";
                param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
                if (isMedicineTab) {
                    hisService.getMedicine(param, new Callback<ResponseArrayModel<Medicine>>() {
                        @Override
                        public void success(ResponseArrayModel<Medicine> medicineResponseArrayModel, Response response) {
                            data.clear();
                            if (medicineResponseArrayModel != null && medicineResponseArrayModel.getDataList() != null && medicineResponseArrayModel.getDataList().size() > 0) {
                                for (Medicine me : medicineResponseArrayModel.getDataList()) {
                                    data.add(me);
                                }
                            }
                            updateListOfMedicineMaterial(UPDATE_FROM_MEDICINE);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            error.printStackTrace();
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    });
                } else {
                    hisService.getMaterial(param, new Callback<ResponseArrayModel<Material>>() {
                        @Override
                        public void success(ResponseArrayModel<Material> materialResponseArrayModel, Response response) {
                            data.clear();
                            if (materialResponseArrayModel != null && materialResponseArrayModel.getDataList() != null && materialResponseArrayModel.getDataList().size() > 0) {
                                for (Material me : materialResponseArrayModel.getDataList()) {
                                    data.add(me);
                                }
                            }
                            updateListOfMedicineMaterial(UPDATE_FROM_MATERIAL);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            error.printStackTrace();
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    });
                }
            } finally {
                mHandler.postDelayed(runnable, MBSSharePreference.get().getMMIntervalUpdate() * 1000);
            }
        }
    };

    private void updateListOfMedicineMaterial(int fromUpdate) {
        if (data == null || data.size() == 0) {
            viewState.updateViewState(viewStateSelectedService = ViewEmptyStateLayout.VIEW_STATE_EMPTY);
        } else {
            viewState.updateViewState(viewStateSelectedService = ViewEmptyStateLayout.VIEW_STATE_NORMAL);
            Collections.sort(data, new Comparator<MedicineMaterial>() {
                @Override
                public int compare(MedicineMaterial lhs, MedicineMaterial rhs) {
                    if (lhs instanceof Medicine && rhs instanceof Medicine) {
                        return ((Medicine) lhs).MedicineTypeName.compareTo(((Medicine) rhs).MedicineTypeName);
                    } else if (lhs instanceof Material && rhs instanceof Material) {
                        return ((Material) lhs).MaterialTypeName.compareTo(((Material) rhs).MaterialTypeName);
                    }
                    return 0;
                }
            });
        }
        adapter.updateData();
        updater.updateInventory(data, fromUpdate);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!flagFirstInit)
            mHandler.postDelayed(runnable, MBSSharePreference.get().getMMIntervalUpdate() * 1000);
    }

    @Override
    public void onStop() {
        super.onStop();
        flagFirstInit = false;
        mHandler.removeCallbacksAndMessages(null);
    }


    @Override
    public void onRefresh() {
        checkIfHaveData();
    }

    View.OnClickListener onClickItem = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            MedicineMaterial item = (MedicineMaterial) v.getTag();
            List<Long> temp = new ArrayList<>();
            for (long l : arrPlacedServiceToday)
                temp.add(l);

            SelectMedicineMaterialDialog dialog = new SelectMedicineMaterialDialog(context, item, treatmentCommonInfo, temp, instructionTime);
            dialog.setOnEnterNumberListener(MedicineMaterialListFragment.this);
            dialog.show();

            searchView.setQuery("", false);
            filterString = "";
        }
    };

    public void setListener(OnSelectMedicineMaterial listener) {
        this.listener = listener;
    }

    public void setUpdater(IUpdateInventory updater) {
        this.updater = updater;
    }

    @Override
    public void onNumberEntered(double number, MedicineMaterial medicineMaterial, PatientType patientType, MedicineUserForm useForm, boolean isExpend, boolean isWithBHYT, BigDecimal priceUnit, String tut) {
        this.listener.onSelectedMedicineMaterial(number, medicineMaterial, patientType, useForm, isExpend, isWithBHYT, priceUnit, tut);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        searchView.clearFocus();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        filterString = s;
        adapter.setFilter(filterString);
        return true;
    }

    interface OnSelectMedicineMaterial {
        void onSelectedMedicineMaterial(double number, MedicineMaterial medicineMaterial, PatientType patientType, MedicineUserForm useForm, boolean isExpend, boolean isWithBHYT, BigDecimal priceUnit, String tut);
    }

    interface IUpdateInventory {
        void updateInventory(List<MedicineMaterial> data, int fromUpdate);
    }

    class MedicineMaterialAdapter extends RecyclerView.Adapter<MedicineMaterialViewHolder> {
        List<MedicineMaterial> viewData = new ArrayList<>();

        void updateData() {
            if (!StringUtils.isNullOrEmpty(filterString)) setFilter(filterString);
            else setFilter(null);
        }

        @Override
        public MedicineMaterialViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.adapter_left_medicine_material_specify, parent, false);
            return new MedicineMaterialViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MedicineMaterialViewHolder holder, int position) {
            MedicineMaterial item = viewData.get(position);
            if (item instanceof Material) {
                holder.tvMedicineMaterialInfo.setText(((Material) item).MaterialTypeName);
            } else if (item instanceof Medicine) {
                holder.tvMedicineMaterialInfo.setText(((Medicine) item).MedicineTypeName);
            }
            holder.tvAvailableStock.setText(NumberFormatUtils.formatNumber(BigDecimal.valueOf(item.AvailableAmount)));
            holder.tvTotalStock.setText(NumberFormatUtils.formatNumber(BigDecimal.valueOf(item.TotalAmount)));
            holder.itemView.setTag(item);
            holder.itemView.setOnClickListener(onClickItem);
        }

        @Override
        public int getItemCount() {
            return viewData != null ? viewData.size() : 0;
        }

        public void setFilter(String queryText) {
            viewData.clear();
            if (queryText == null || TextUtils.isEmpty(queryText)) {
                viewData.addAll(data);
            } else {
                queryText = StringUtils.getEngStringFromUnicodeString(queryText).toLowerCase();
                for (MedicineMaterial mm : data) {
                    String name = "", code = "";
                    if (mm instanceof Material) {
                        name = ((Material) mm).MaterialTypeName != null ? StringUtils.getEngStringFromUnicodeString(((Material) mm).MaterialTypeName).toLowerCase() : "";
                        code = ((Material) mm).MaterialTypeCode != null ? StringUtils.getEngStringFromUnicodeString(((Material) mm).MaterialTypeCode).toLowerCase() : "";
                    } else if (mm instanceof Medicine) {
                        name = ((Medicine) mm).MedicineTypeName != null ? StringUtils.getEngStringFromUnicodeString(((Medicine) mm).MedicineTypeName).toLowerCase() : "";
                        code = ((Medicine) mm).MedicineTypeCode != null ? StringUtils.getEngStringFromUnicodeString(((Medicine) mm).MedicineTypeCode).toLowerCase() : "";

                    }
                    if (code.contains(queryText) || name.contains(queryText)) {
                        viewData.add(mm);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    class MedicineMaterialViewHolder extends RecyclerView.ViewHolder {
        TextView tvMedicineMaterialInfo;
        TextView tvAvailableStock;
        TextView tvTotalStock;

        public MedicineMaterialViewHolder(View itemView) {
            super(itemView);
            tvMedicineMaterialInfo = (TextView) itemView.findViewById(R.id.tvMedicineMaterialInfo);
            tvAvailableStock = (TextView) itemView.findViewById(R.id.tvAvailableStock);
            tvTotalStock = (TextView) itemView.findViewById(R.id.tvTotalStock);

        }
    }
}
