package com.staarlab.idoctor.ui.activity;

import android.os.Bundle;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.Treatment;
import com.staarlab.idoctor.ui.fragment.MedicineMaterialDiagnosticFragment;

/**
 * Created by ducln on 16/11/2016.
 */

public class MedicineMaterialsActivity extends BaseActivity {

    private Treatment treatment;
    private long roomId;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_materials);
        treatment = getIntent().getParcelableExtra("TREATMENT");
        roomId = getIntent().getLongExtra("roomId", 0l);
        replaceCurrentFragment(MedicineMaterialDiagnosticFragment.newInstance(treatment, roomId));

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
