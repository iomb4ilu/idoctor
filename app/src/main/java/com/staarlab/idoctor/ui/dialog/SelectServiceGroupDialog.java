package com.staarlab.idoctor.ui.dialog;


import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.ServiceGroup;
import com.staarlab.idoctor.helper.layout.DividerItemDecoration;
import com.staarlab.idoctor.helper.layout.HeaderRecyclerDividerItemDecorator;
import com.staarlab.idoctor.ui.activity.BaseActivity;
import com.staarlab.idoctor.util.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PhamHung on 12/27/16.
 */

public class SelectServiceGroupDialog implements View.OnClickListener {
    private static final String TAG = SelectServiceGroupDialog.class.getName();
    protected Context context;
    private Dialog dialog;

    private TextView txtTitle;
    private RecyclerView rvServiceGroup;
    private ImageButton imgSearch;
    private ImageButton imgClose;
    private EditText edtQuery;

    private LayoutInflater layoutInflater;
    private RecyclerView.LayoutManager layoutManager;
    private List<ServiceGroup> items = new ArrayList<>();
    private ServiceGroupAdapter adapter;

    private String query;
    private OnServiceGroupSelectListener listener;

    public SelectServiceGroupDialog(Context ctx) {
        this.context = ctx;
        this.layoutManager = new LinearLayoutManager(context);
        layoutInflater = LayoutInflater.from(context);
        this.dialog = new Dialog(this.context);
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.dialog.setContentView(R.layout.dialog_select);
        this.dialog.setCancelable(false);

        initViews();
    }

    private void initViews() {
        query = "";
        imgSearch = (ImageButton) dialog.findViewById(R.id.imgSearch);
        imgClose = (ImageButton) dialog.findViewById(R.id.btnClose);
        edtQuery = (EditText) dialog.findViewById(R.id.edtQuery);
        txtTitle = (TextView) dialog.findViewById(R.id.dialog_title);
        txtTitle.setText(R.string.select_service_group);
        rvServiceGroup = (RecyclerView) dialog.findViewById(R.id.recycler_view);
        rvServiceGroup.setHasFixedSize(true);
        rvServiceGroup.setLayoutManager(layoutManager);
        rvServiceGroup.addItemDecoration(
                new HeaderRecyclerDividerItemDecorator(
                        context, DividerItemDecoration.VERTICAL_LIST, false, false,
                        R.drawable.divider_0));

        adapter = new ServiceGroupAdapter();
        rvServiceGroup.setAdapter(adapter);

        imgClose.setOnClickListener(this);
        imgSearch.setOnClickListener(this);
        edtQuery.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edtQuery.getWindowToken(), 0);
                    if (!query.equalsIgnoreCase(edtQuery.getText().toString())) {
                        query = StringUtils.getEngStringFromUnicodeString(edtQuery.getText().toString().toLowerCase().trim());
                        doQueryDatabase(query);
                    }
                    return true;
                }
                return false;
            }
        });

        new GetServiceGroupData().execute("");

    }

    public void setServiceGroupSelectListener(OnServiceGroupSelectListener l) {
        this.listener = l;
    }

    public void show() {
        dialog.show();
    }

    public void dismiss() {
        dialog.dismiss();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnClose:
                dismiss();
                break;
            case R.id.imgSearch:
                InputMethodManager imm = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtQuery.getWindowToken(), 0);
                if (!query.equalsIgnoreCase(edtQuery.getText().toString())) {
                    query = StringUtils.getEngStringFromUnicodeString(edtQuery.getText().toString().toLowerCase().trim());
                    doQueryDatabase(query);
                }
                break;
        }
    }

    private void doQueryDatabase(String query) {
        if (context instanceof BaseActivity) {
            try {
                new GetServiceGroupData().execute(query);
            } catch (Exception ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
    }

    private View.OnClickListener onSelectServiceGroupItem = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getTag() instanceof ServiceGroup) {
                ServiceGroup item = (ServiceGroup) v.getTag();
                listener.onServiceGroupSelected(item);
            }
            dismiss();
        }
    };


    private class ServiceGroupAdapter extends RecyclerView.Adapter {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = layoutInflater.inflate(R.layout.adapter_select_icd, parent, false);
            return new ServiceGroupViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ServiceGroupViewHolder vh = (ServiceGroupViewHolder) holder;
            ServiceGroup item = items.get(position);
            vh.tvName.setText(item.SERVICE_GROUP_NAME);
            vh.tvCode.setText(item.SERVICE_GROUP_CODE);
            vh.itemView.setTag(item);
            vh.itemView.setOnClickListener(onSelectServiceGroupItem);
        }

        @Override
        public int getItemCount() {
            if (items != null) {
                return items.size();
            }
            return 0;
        }
    }

    private class ServiceGroupViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        TextView tvCode;

        public ServiceGroupViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvCode = (TextView) itemView.findViewById(R.id.tvCode);

        }
    }

    public interface OnServiceGroupSelectListener {
        void onServiceGroupSelected(ServiceGroup item);
    }

    class GetServiceGroupData extends AsyncTask<String, Void, List<ServiceGroup>> {

        @Override
        protected List<ServiceGroup> doInBackground(String... strings) {
            String q = strings[0];
            try {
                Dao<ServiceGroup, Integer> serviceGroupDao = ((BaseActivity) context).getHelper().getServiceGroupDao();
                if (q.isEmpty()) {
                    return serviceGroupDao.queryForAll();
                } else {
                    QueryBuilder<ServiceGroup, Integer> qb = serviceGroupDao.queryBuilder();
                    qb.where().like(ServiceGroup.TABLE_FIELD_SEARCH, "%" + q + "%");
                    qb.orderByRaw(ServiceGroup.TABLE_FIELD_SERVICE_GROUP_NAME + " COLLATE NOCASE");
                    PreparedQuery<ServiceGroup> preparedQuery = qb.prepare();
                    return serviceGroupDao.query(preparedQuery);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new ArrayList<>();
        }

        @Override
        protected void onPostExecute(List<ServiceGroup> serviceGroups) {
            super.onPostExecute(serviceGroups);
            items = serviceGroups;
            adapter.notifyDataSetChanged();
        }
    }
}
