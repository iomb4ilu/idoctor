package com.staarlab.idoctor.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.CommonServiceDetailEntity;
import com.staarlab.idoctor.util.AppConst;

import java.util.List;

public class ServiceDetailAdapter extends BaseAdapter {
    static class ViewHolder {
        TextView txtCode;
        TextView txtName;
        TextView txtUnit;
        TextView txtCount;
        TextView txtObject;
    }

    private List<CommonServiceDetailEntity> mDataList;
    private Context mContext;
    private String mTabCode;

    public ServiceDetailAdapter(Context context, String tabCode) {
        mContext = context;
        mTabCode = tabCode;
    }

    @Override
    public int getCount() {
        return mDataList == null ? 0 : mDataList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public CommonServiceDetailEntity getItem(int position) {
        return mDataList == null || mDataList.size() <= position ? null : mDataList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CommonServiceDetailEntity entity = getItem(position);
        ViewHolder vh;
        if (convertView == null) {
            vh = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.adapter_service_detail_list_item, parent, false);
            vh.txtCode = (TextView) convertView.findViewById(R.id.txtCode);
            vh.txtName = (TextView) convertView.findViewById(R.id.txtName);
            vh.txtUnit = (TextView) convertView.findViewById(R.id.txtUnit);
            vh.txtCount = (TextView) convertView.findViewById(R.id.txtCount);
            vh.txtObject = (TextView) convertView.findViewById(R.id.txtObject);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }

        switch (mTabCode) {
            case AppConst.TAB_CODE_MEDI_REACT:
                mapMedReactInfo(vh, entity);
                break;
            default:
                mapGeneralInfo(vh, entity);
                break;
        }

        return convertView;
    }

    private void mapMedReactInfo(ServiceDetailAdapter.ViewHolder vh, CommonServiceDetailEntity entity) {
        vh.txtCode.setText(entity.getServiceCode());
        vh.txtName.setText(entity.getServiceName());
        vh.txtUnit.setText(entity.getServiceUnitName());
        vh.txtCount.setVisibility(View.GONE);
        vh.txtObject.setText(entity.getPatientTypeName());
    }

    private void mapGeneralInfo(ServiceDetailAdapter.ViewHolder vh, CommonServiceDetailEntity entity) {
        vh.txtCode.setText(entity.getServiceCode());
        vh.txtName.setText(entity.getServiceName());
        vh.txtUnit.setText(entity.getServiceUnitName());
        vh.txtCount.setText(String.valueOf(entity.getAmount()));
        vh.txtObject.setText(entity.getPatientTypeName());
    }

    public void refreshData(List<CommonServiceDetailEntity> dataList) {
        mDataList = dataList;
        notifyDataSetChanged();
    }
}
