package com.staarlab.idoctor.ui.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.j256.ormlite.dao.Dao;
import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.ICD;
import com.staarlab.idoctor.entity.PatientType;
import com.staarlab.idoctor.entity.ServicePATY;
import com.staarlab.idoctor.entity.ServiceType;
import com.staarlab.idoctor.entity.TechService;
import com.staarlab.idoctor.helper.share.MBSSharePreference;
import com.staarlab.idoctor.model.ResponseArrayModel;
import com.staarlab.idoctor.ui.activity.BaseActivity;
import com.staarlab.idoctor.util.StringUtils;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.WSFactory;

import java.sql.SQLException;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class SyncFragment extends BaseFragment implements View.OnClickListener {
    private final static String TAG = SyncFragment.class.getName();

    private View syncMainExamination;
    private View syncTechService;
    private View syncServiceType;
    private View syncPatientType;
    private View syncServicePATH;
    private HISService hisService;
    Dialog dialog;
    String param;

    public SyncFragment() {
        // Required empty public constructor
    }

    public static SyncFragment newInstance() {
        SyncFragment fragment = new SyncFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sync, container, false);

        hisService = WSFactory.createHisService(MBSSharePreference.get().getTokenCode());
        syncMainExamination = view.findViewById(R.id.sync_main_examination);
        syncTechService = view.findViewById(R.id.sync_tech_service);
        syncServiceType = view.findViewById(R.id.sync_tech_service_type);
        syncPatientType = view.findViewById(R.id.sync_patient_type);
        syncServicePATH = view.findViewById(R.id.sync_patient_paty);
        syncMainExamination.setOnClickListener(this);
        syncTechService.setOnClickListener(this);
        syncServiceType.setOnClickListener(this);
        syncPatientType.setOnClickListener(this);
        syncServicePATH.setOnClickListener(this);
        return view;
    }


    @Override
    public void onClick(View v) {
        dialog = new ProgressDialog(context);
        dialog.setTitle("LOADING");
        dialog.setCancelable(false);
        dialog.show();
        switch (v.getId()) {
            case R.id.sync_main_examination:
                param = "{'ApiData':{'IS_ACTIVE':1}}";
                param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
                hisService.getMainICD(param, new Callback<ResponseArrayModel<ICD>>() {
                    @Override
                    public void success(ResponseArrayModel<ICD> data, Response response) {
                        List<ICD> lstICD = data.getDataList();
                        if (lstICD != null && lstICD.size() > 0) {
                            try {
                                Dao<ICD, Integer> icdDao = ((BaseActivity) context).getHelper().getIcdDao();
                                for (ICD icd : lstICD) {
                                    icd.ICD_SEARCH = StringUtils.getEngStringFromUnicodeString(icd.ICD_CODE + " " + icd.ICD_NAME);
                                    icdDao.createOrUpdate(icd);
                                }
                            } catch (SQLException ex) {
                                Log.e(TAG, ex.getMessage());
                            }
                        }
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                });
                break;
            case R.id.sync_tech_service:
                param = "{'ApiData':{'IS_ACTIVE':1}}";
                param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
                hisService.getTechService(param, new Callback<ResponseArrayModel<TechService>>() {
                    @Override
                    public void success(ResponseArrayModel<TechService> data, Response response) {
                        List<TechService> lstTechService = data.getDataList();
                        if (lstTechService != null && lstTechService.size() > 0) {
                            try {
                                Dao<TechService, Integer> techServiceDao = ((BaseActivity) context).getHelper().getTechServiceDao();
                                for (TechService service : lstTechService) {
                                    service.SERVICE_SEARCH = StringUtils.getEngStringFromUnicodeString(service.SERVICE_CODE + " " + service.SERVICE_NAME);
                                    techServiceDao.createOrUpdate(service);
                                }
                            } catch (SQLException ex) {
                                Log.e(TAG, ex.getMessage());
                            }
                        }
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                });
                break;

            case R.id.sync_tech_service_type:
                param = "{'ApiData':{'IS_ACTIVE':1}}";
                param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
                hisService.getTechServiceType(param, new Callback<ResponseArrayModel<ServiceType>>() {
                    @Override
                    public void success(ResponseArrayModel<ServiceType> data, Response response) {
                        List<ServiceType> lstTechServiceType = data.getDataList();
                        if (lstTechServiceType != null && lstTechServiceType.size() > 0) {
                            try {
                                Dao<ServiceType, Integer> techServiceDao = ((BaseActivity) context).getHelper().getServiceTypeDao();
                                for (ServiceType type : lstTechServiceType) {
                                    type.SERVICE_TYPE_SEARCH = StringUtils.getEngStringFromUnicodeString(type.SERVICE_TYPE_CODE + " " + type.SERVICE_TYPE_NAME);
                                    techServiceDao.createOrUpdate(type);
                                }
                            } catch (SQLException ex) {
                                Log.e(TAG, ex.getMessage());
                            }
                        }
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                });
                break;

            case R.id.sync_patient_type:
                param = "{'ApiData':{'IS_ACTIVE':1}}";
                param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
                hisService.getPatientType(param, new Callback<ResponseArrayModel<PatientType>>() {
                    @Override
                    public void success(ResponseArrayModel<PatientType> data, Response response) {
                        List<PatientType> lstPatientType = data.getDataList();
                        if (lstPatientType != null && lstPatientType.size() > 0) {
                            try {
                                Dao<PatientType, Integer> patientTypeDao = ((BaseActivity) context).getHelper().getPatientTypeDao();
                                for (PatientType type : lstPatientType) {
                                    patientTypeDao.createOrUpdate(type);
                                }
                            } catch (SQLException ex) {
                                Log.e(TAG, ex.getMessage());
                            }
                        }
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                });
                break;

            case R.id.sync_patient_paty:
                param = "{'ApiData':{'IS_ACTIVE':1}}";
                param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
                hisService.getServicePATY(param, new Callback<ResponseArrayModel<ServicePATY>>() {
                    @Override
                    public void success(ResponseArrayModel<ServicePATY> data, Response response) {
                        List<ServicePATY> lstTechService = data.getDataList();
                        if (lstTechService != null && lstTechService.size() > 0) {
                            try {
                                Dao<ServicePATY, Integer> patientTypeDao = ((BaseActivity) context).getHelper().getServicePATYDao();
                                for (ServicePATY type : lstTechService) {
                                    patientTypeDao.createOrUpdate(type);
                                }
                            } catch (SQLException ex) {
                                Log.e(TAG, ex.getMessage());
                            }
                        }
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                    }
                });
                break;
        }
    }
}
