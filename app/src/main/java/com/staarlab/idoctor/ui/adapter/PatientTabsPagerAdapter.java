package com.staarlab.idoctor.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.Patient;
import com.staarlab.idoctor.entity.TreatmentSummaryInfo;
import com.staarlab.idoctor.ui.activity.PatientDetailActivity;
import com.staarlab.idoctor.ui.fragment.BedFragment;
import com.staarlab.idoctor.ui.fragment.CareFragment;
import com.staarlab.idoctor.ui.fragment.DeathFragment;
import com.staarlab.idoctor.ui.fragment.DebateFragment;
import com.staarlab.idoctor.ui.fragment.DhstFragment;
import com.staarlab.idoctor.ui.fragment.DiimFragment;
import com.staarlab.idoctor.ui.fragment.EndoFragment;
import com.staarlab.idoctor.ui.fragment.ExamFragment;
import com.staarlab.idoctor.ui.fragment.FuexFragment;
import com.staarlab.idoctor.ui.fragment.InfusionFragment;
import com.staarlab.idoctor.ui.fragment.MediReactFragment;
import com.staarlab.idoctor.ui.fragment.MisuFragment;
import com.staarlab.idoctor.ui.fragment.OtherFragment;
import com.staarlab.idoctor.ui.fragment.PatientInfoFragment;
import com.staarlab.idoctor.ui.fragment.PrescriptionFragment;
import com.staarlab.idoctor.ui.fragment.RehaFragment;
import com.staarlab.idoctor.ui.fragment.SuimFragment;
import com.staarlab.idoctor.ui.fragment.SurgFragment;
import com.staarlab.idoctor.ui.fragment.TestFragment;
import com.staarlab.idoctor.ui.fragment.TrackingFragment;
import com.staarlab.idoctor.ui.fragment.TranPatiFragment;
import com.staarlab.idoctor.util.AppConst;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: Lucero
 * Created: 12/29/16.
 */

public class PatientTabsPagerAdapter extends FragmentPagerAdapter {
    private Patient mPatient;
    private TreatmentSummaryInfo mTreatmentSummaryInfo;
    private List<String> tabTitles;
    private List<String> tabCodes;
    private Context mContext;

    public PatientTabsPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
        tabTitles = new ArrayList<>();
    }

    public void setPatient(Patient patient) {
        this.mPatient = patient;
    }

    public void setTreatmentCommonInfo(TreatmentSummaryInfo info) {
        Resources rs = mContext.getResources();
        mTreatmentSummaryInfo = info;
        tabTitles = new ArrayList<>();
        tabCodes = new ArrayList<>();
        tabTitles.add(rs.getString(R.string.tab_title_general));
        tabCodes.add(AppConst.TAB_CODE_GENERAL);
        if (info == null) return;
        for (int i = 0; i < AppConst.INFO_TABS.length; i++) {
            String code = AppConst.INFO_TABS[i];
            try {
                Method getMethod = TreatmentSummaryInfo.class.getMethod("get" + code);
                Float count = (Float) getMethod.invoke(info);
                if (count != null && count > 0) {
                    tabTitles.add(rs.getString(AppConst.INFO_TAB_TITLE_IDS[i]));
                    tabCodes.add(code);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        notifyDataSetChanged();
    }

    public int getTabIndexByCode(String tabCode) {
        return tabCodes.indexOf(tabCode);
    }

    @Override
    public Fragment getItem(int position) {
        long treatmentId = mTreatmentSummaryInfo.getId();
        if (position == 0) {
            PatientInfoFragment f = new PatientInfoFragment();
            Bundle args = new Bundle();
            args.putSerializable(PatientInfoFragment.ARG_PATIENT, mPatient);
            args.putLong(PatientInfoFragment.ARG_TREATMENT_ID, treatmentId);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_PRESCRIPTION)) {
            PrescriptionFragment f = new PrescriptionFragment();
            Bundle args = new Bundle();
            args.putLong(PrescriptionFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(DhstFragment.ARG_TAB_CODE, AppConst.TAB_CODE_PRESCRIPTION);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_EXAM)) {
            ExamFragment f = new ExamFragment();
            Bundle args = new Bundle();
            args.putLong(ExamFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(DhstFragment.ARG_TAB_CODE, AppConst.TAB_CODE_EXAM);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_DEATH)) {
            DeathFragment f = new DeathFragment();
            Bundle args = new Bundle();
            args.putLong(DeathFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(DhstFragment.ARG_TAB_CODE, AppConst.TAB_CODE_DEATH);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_TRAN_PATI)) {
            TranPatiFragment f = new TranPatiFragment();
            Bundle args = new Bundle();
            args.putLong(TranPatiFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(DhstFragment.ARG_TAB_CODE, AppConst.TAB_CODE_TRAN_PATI);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_DEBATE)) {
            DebateFragment f = new DebateFragment();
            Bundle args = new Bundle();
            args.putLong(DebateFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(DebateFragment.ARG_TAB_CODE, AppConst.TAB_CODE_DEBATE);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_MEDI_REACT)) {
            MediReactFragment f = new MediReactFragment();
            Bundle args = new Bundle();
            args.putLong(MediReactFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(MediReactFragment.ARG_TAB_CODE, AppConst.TAB_CODE_MEDI_REACT);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_TRACKING)) {
            TrackingFragment f = new TrackingFragment();
            Bundle args = new Bundle();
            args.putLong(TrackingFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(TrackingFragment.ARG_TAB_CODE, AppConst.TAB_CODE_TRACKING);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_INFUSION)) {
            InfusionFragment f = new InfusionFragment();
            Bundle args = new Bundle();
            args.putLong(InfusionFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(DhstFragment.ARG_TAB_CODE, AppConst.TAB_CODE_INFUSION);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_DHST)) {
            DhstFragment f = new DhstFragment();
            Bundle args = new Bundle();
            args.putLong(DhstFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(DhstFragment.ARG_TAB_CODE, AppConst.TAB_CODE_DHST);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_CARE)) {
            CareFragment f = new CareFragment();
            Bundle args = new Bundle();
            args.putLong(CareFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(DhstFragment.ARG_TAB_CODE, AppConst.TAB_CODE_CARE);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_REHA)) {
            RehaFragment f = new RehaFragment();
            Bundle args = new Bundle();
            args.putLong(RehaFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(DhstFragment.ARG_TAB_CODE, AppConst.TAB_CODE_REHA);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_OTHER)) {
            OtherFragment f = new OtherFragment();
            Bundle args = new Bundle();
            args.putLong(OtherFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(DhstFragment.ARG_TAB_CODE, AppConst.TAB_CODE_OTHER);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_SURG)) {
            SurgFragment f = new SurgFragment();
            Bundle args = new Bundle();
            args.putLong(SurgFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(DhstFragment.ARG_TAB_CODE, AppConst.TAB_CODE_SURG);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_SUIM)) {
            SuimFragment f = new SuimFragment();
            Bundle args = new Bundle();
            args.putLong(SuimFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(DhstFragment.ARG_TAB_CODE, AppConst.TAB_CODE_SUIM);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_ENDO)) {
            EndoFragment f = new EndoFragment();
            Bundle args = new Bundle();
            args.putLong(EndoFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(DhstFragment.ARG_TAB_CODE, AppConst.TAB_CODE_ENDO);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_BED)) {
            BedFragment f = new BedFragment();
            Bundle args = new Bundle();
            args.putLong(BedFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(DhstFragment.ARG_TAB_CODE, AppConst.TAB_CODE_BED);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_FUEX)) {
            FuexFragment f = new FuexFragment();
            Bundle args = new Bundle();
            args.putLong(FuexFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(DhstFragment.ARG_TAB_CODE, AppConst.TAB_CODE_FUEX);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_MISU)) {
            MisuFragment f = new MisuFragment();
            Bundle args = new Bundle();
            args.putLong(MisuFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(DhstFragment.ARG_TAB_CODE, AppConst.TAB_CODE_MISU);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_DIIM)) {
            DiimFragment f = new DiimFragment();
            Bundle args = new Bundle();
            args.putLong(DiimFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(DhstFragment.ARG_TAB_CODE, AppConst.TAB_CODE_DIIM);
            f.setArguments(args);
            return f;
        }
        if (tabCodes.get(position).equals(AppConst.TAB_CODE_TEST)) {
            TestFragment f = new TestFragment();
            Bundle args = new Bundle();
            args.putLong(TestFragment.ARG_TREATMENT_ID, treatmentId);
            args.putString(DhstFragment.ARG_TAB_CODE, AppConst.TAB_CODE_TEST);
            f.setArguments(args);
            return f;
        }
        return PatientDetailActivity.PlaceholderFragment.newInstance(position + 1);
    }

    @Override
    public int getCount() {
        return tabTitles.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles.get(position);
    }
}
