package com.staarlab.idoctor.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.Material;
import com.staarlab.idoctor.entity.Medicine;
import com.staarlab.idoctor.entity.MedicineMaterial;
import com.staarlab.idoctor.entity.MedicineUserForm;
import com.staarlab.idoctor.entity.PatientType;
import com.staarlab.idoctor.entity.PatientTypeAllow;
import com.staarlab.idoctor.entity.ServicePATY;
import com.staarlab.idoctor.entity.TreatmentCommonInfo;
import com.staarlab.idoctor.ui.activity.BaseActivity;
import com.staarlab.idoctor.ui.fragment.MedicineMaterialSpecifyFragment;
import com.staarlab.idoctor.util.HardCodeUtils;
import com.staarlab.idoctor.util.NumberFormatUtils;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.staarlab.idoctor.entity.ServicePATY.FIELD_NAME_SERVICE_PATY_PATIENT_TYPE_ID;
import static com.staarlab.idoctor.entity.ServicePATY.FIELD_NAME_SERVICE_PATY_SERVICE_ID;

/**
 * Created by PHAMHUNG on 1/1/2017.
 */

public class SelectMedicineMaterialDialog implements View.OnClickListener, AdapterView.OnItemSelectedListener, CheckBox.OnCheckedChangeListener {
    private static final String TAG = SelectMedicineMaterialDialog.class.getName();
    private OnNumberEnteredListener listener;
    protected Context context;
    private Dialog dialog;
    private MedicineMaterial medicineMaterial;
    private TreatmentCommonInfo treatmentCommonInfo;
    private long defaultPatientTypeId = -1;
    private long useFormId = -1;
    private String instruction = "";
    List<Long> lstPlacedServiceToday;
    long inTime, applyTime;
    private PatientType selectedPatientType;
    private MedicineUserForm selectedUseForm;
    private BigDecimal priceUnit = BigDecimal.ZERO;
    private BigDecimal priceUnitBKXH = null;
    private boolean isExpend;
    private boolean isWithBKXH;

    private Button btnOK;
    private Button btnCancel;
    private ImageButton imgButtonClose;
    private TextView tvAlreadyOrder;
    private TextView tvMedicineMaterialInfo;
    private TextView tvMedicineMaterialTitleName;
    private TextView tvMedicineMaterialTitleCode;
    private TextView tvAvailableStock;
    private TextView tvTotalStock;
    private Spinner spPatientType;
    private Spinner spUseForm;
    private View viewUseForm;
    private TextView txtMedicineMaterialCode;
    private TextView txtMedicineMaterialName;
    private EditText edtNumber;
    private TextView tvUOM;
    private TextView txtPriceUnit;
    private CheckBox chkExpend;
    private CheckBox chkWithBHXH;
    private View viewInstruction;
    private EditText edtInstruction;
    private double number = 0d;

    List<PatientType> lstPatientType = new ArrayList<>();
    List<MedicineUserForm> lstUseForm = new ArrayList<>();
    PatientTypeAdapter adapter;
    UseFormAdapter userFormAdapter;
    LayoutInflater layoutInflater;

    public SelectMedicineMaterialDialog(Context ctx, MedicineMaterial service, TreatmentCommonInfo commonInfo, List<Long> lstPlacedServiceToday, long applyTime) {
        this.context = ctx;
        this.medicineMaterial = service;
        this.treatmentCommonInfo = commonInfo;
        this.defaultPatientTypeId = treatmentCommonInfo.patientTypeAlter.PATIENT_TYPE_ID;
        this.inTime = treatmentCommonInfo.IN_TIME;
        this.lstPlacedServiceToday = lstPlacedServiceToday;
        this.applyTime = applyTime;

        this.dialog = new Dialog(this.context);
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.dialog.setContentView(R.layout.dialog_select_medicine_material);
        this.dialog.setCancelable(false);
        this.layoutInflater = LayoutInflater.from(context);

        initViews();
    }

    /*
        View Medicine
     */
    public SelectMedicineMaterialDialog(Context ctx, MedicineMaterialSpecifyFragment.MedicineMaterialHolder item, List<Long> lstPlacedServiceToday, long inTime, long applyTime) {
        this.context = ctx;
        this.medicineMaterial = item.medicineMaterial;
        this.defaultPatientTypeId = item.patientType.ID;
        this.priceUnit = item.priceUnit;
        this.isExpend = item.isExpend;
        this.isWithBKXH = item.isWithBHXH;
        this.number = item.number;
        this.inTime = inTime;
        this.lstPlacedServiceToday = lstPlacedServiceToday;
        this.applyTime = applyTime;
        this.useFormId = item.userForm != null && item.userForm.ID != null ? item.userForm.ID : 1;
        this.instruction = item.tutorial;

        this.dialog = new Dialog(this.context);
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.dialog.setContentView(R.layout.dialog_select_medicine_material);
        this.dialog.setCancelable(false);
        this.layoutInflater = LayoutInflater.from(context);

        initViews();
    }


    public void show() {
        dialog.show();
    }

    private void initViews() {
        tvAlreadyOrder = (TextView) dialog.findViewById(R.id.viewAlreadyOrder);
        btnOK = (Button) dialog.findViewById(R.id.btnOK);
        btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        tvMedicineMaterialInfo = (TextView) dialog.findViewById(R.id.tvMedicineMaterialInfo);
        tvMedicineMaterialTitleName = (TextView) dialog.findViewById(R.id.tvMedicineMaterialTitleName);
        tvMedicineMaterialTitleCode = (TextView) dialog.findViewById(R.id.tvMedicineMaterialTitleCode);
        txtMedicineMaterialCode = (TextView) dialog.findViewById(R.id.txtServiceCode);
        txtMedicineMaterialName = (TextView) dialog.findViewById(R.id.txtServiceName);
        tvAvailableStock = (TextView) dialog.findViewById(R.id.tvAvailableStock);
        tvTotalStock = (TextView) dialog.findViewById(R.id.tvTotalStock);
        edtNumber = (EditText) dialog.findViewById(R.id.edtNumber);
        tvUOM = (TextView) dialog.findViewById(R.id.tvUOM);
        txtPriceUnit = (TextView) dialog.findViewById(R.id.txtPriceUnit);
        chkExpend = (CheckBox) dialog.findViewById(R.id.chkExpend);
        chkWithBHXH = (CheckBox) dialog.findViewById(R.id.chkWithBHXH);
        imgButtonClose = (ImageButton) dialog.findViewById(R.id.btnClose);
        spPatientType = (Spinner) dialog.findViewById(R.id.spPatientType);
        spUseForm = (Spinner) dialog.findViewById(R.id.spUseForm);
        viewUseForm = dialog.findViewById(R.id.viewUseForm);
        viewInstruction = dialog.findViewById(R.id.viewInstruction);
        edtInstruction = (EditText) dialog.findViewById(R.id.edtInstruction);

        btnOK.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        imgButtonClose.setOnClickListener(this);
        spPatientType.setOnItemSelectedListener(this);
        spUseForm.setOnItemSelectedListener(this);
        chkExpend.setOnCheckedChangeListener(this);
        chkWithBHXH.setOnCheckedChangeListener(this);

        bindView();

    }

    private void bindView() {
        if (medicineMaterial instanceof Medicine) {
            txtMedicineMaterialName.setText(((Medicine) medicineMaterial).MedicineTypeName);
            txtMedicineMaterialCode.setText(((Medicine) medicineMaterial).MedicineTypeCode);
            tvMedicineMaterialInfo.setText(R.string.specify_medicine_material_medicine);
            tvMedicineMaterialTitleName.setText(R.string.specify_medicine_material_dialog_medicine_name);
            tvMedicineMaterialTitleCode.setText(R.string.specify_medicine_material_dialog_medicine_code);
            viewInstruction.setVisibility(View.VISIBLE);
            viewUseForm.setVisibility(View.VISIBLE);
            new LoadUseForm().execute();

        } else if (medicineMaterial instanceof Material) {
            txtMedicineMaterialName.setText(((Material) medicineMaterial).MaterialTypeName);
            txtMedicineMaterialCode.setText(((Material) medicineMaterial).MaterialTypeCode);
            tvMedicineMaterialInfo.setText(R.string.specify_medicine_material_material);
            tvMedicineMaterialTitleName.setText(R.string.specify_medicine_material_dialog_material_name);
            tvMedicineMaterialTitleCode.setText(R.string.specify_medicine_material_dialog_material_code);
            viewInstruction.setVisibility(View.GONE);
            viewUseForm.setVisibility(View.GONE);
        }
        tvUOM.setText(medicineMaterial.ServiceUnitName == null ? "" : medicineMaterial.ServiceUnitName);
        tvAvailableStock.setText(NumberFormatUtils.formatNumber(BigDecimal.valueOf(medicineMaterial.AvailableAmount)));
        tvTotalStock.setText(NumberFormatUtils.formatNumber(BigDecimal.valueOf(medicineMaterial.TotalAmount)));
        long temp = (long) number;
        edtNumber.setText(number == temp ? String.format("%d", temp) : String.valueOf(number));
        chkExpend.setChecked(isExpend);
        chkWithBHXH.setChecked(isWithBKXH);
        changePriceUnit(isExpend);
        if (lstPlacedServiceToday.contains(medicineMaterial.ServiceId)) {
            tvAlreadyOrder.setVisibility(View.VISIBLE);
        } else {
            tvAlreadyOrder.setVisibility(View.GONE);
        }
        edtInstruction.setText(instruction);
        ((BaseActivity) context).closeSoftKey();

        new LoadPatientType().execute();
    }

    void changePriceUnit(boolean isExpend) {
        if (!isExpend) {
            if (priceUnit.compareTo(BigDecimal.ZERO) == 0) {
                txtPriceUnit.setText(R.string.specify_service_zero_price);
                txtPriceUnit.setTextColor(ContextCompat.getColor(context, R.color.red));
            } else {
                txtPriceUnit.setText(NumberFormatUtils.formatMoney(priceUnit));
                txtPriceUnit.setTextColor(ContextCompat.getColor(context, R.color.Black87));
            }
        } else {
            txtPriceUnit.setText("0");
            txtPriceUnit.setTextColor(ContextCompat.getColor(context, R.color.Black87));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOK:
                sendData();
                break;
            case R.id.btnCancel:
                dialog.dismiss();
                break;
            case R.id.btnClose:
                dialog.dismiss();
                break;
        }
    }

    private void sendData() {
        try {
            double number = Double.parseDouble(edtNumber.getText().toString());
            if (number <= 0d) {
                Toast.makeText(context, R.string.warning_tech_medicine_material_dialog_number, Toast.LENGTH_SHORT).show();
                edtNumber.setError("");
                return;
            }


            boolean isExpend = chkExpend.isChecked();
            boolean isWithBHYT = chkWithBHXH.isChecked();
            String tutorial = edtInstruction.getText().toString();
            if (isWithBHYT && priceUnitBKXH != null && priceUnitBKXH.compareTo(priceUnit) > 0) {
                Toast.makeText(context, R.string.warning_tech_medicine_material_dialog_not_allow_with_BHYT, Toast.LENGTH_SHORT).show();
                return;
            }
            listener.onNumberEntered(number, medicineMaterial, selectedPatientType, selectedUseForm, isExpend, isWithBHYT, priceUnit, tutorial);
            dialog.dismiss();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.spPatientType) {
            selectedPatientType = lstPatientType.get(position);
            defaultPatientTypeId = selectedPatientType.ID;
            new CalculatePrice().execute(selectedPatientType);
        } else if (parent.getId() == R.id.spUseForm) {
            selectedUseForm = lstUseForm.get(position);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void setOnEnterNumberListener(OnNumberEnteredListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.chkExpend) {
            isExpend = isChecked;
            changePriceUnit(isExpend);
        } else if (buttonView.getId() == R.id.chkWithBHXH) {
            if (priceUnitBKXH == null) {
                new CalculatePriceForBHXH().execute();
            }
        }
    }

    // LOAD DATA LOCAL SQLITE
    class LoadPatientType extends AsyncTask<Void, Void, List<PatientType>> {

        @Override
        protected List<PatientType> doInBackground(Void... params) {
            Dao<PatientType, Integer> patientTypeDao = ((BaseActivity) context).getHelper().getPatientTypeDao();
            List<PatientType> lstPatientTypes = new ArrayList<>();
            QueryBuilder<PatientType, Integer> qb = patientTypeDao.queryBuilder();

            try {
                // Đối tượng thanh toán phải có giá -- Service PATY
                Dao<ServicePATY, Integer> servicePATies = ((BaseActivity) context).getHelper().getServicePATYDao();
                QueryBuilder<ServicePATY, Integer> qbServicePATY = servicePATies.queryBuilder();
                qbServicePATY.selectColumns(ServicePATY.FIELD_NAME_SERVICE_PATY_PATIENT_TYPE_ID);
                qbServicePATY.where().eq(ServicePATY.FIELD_NAME_SERVICE_PATY_SERVICE_ID, medicineMaterial.ServiceId);
                List<ServicePATY> patientTypes = servicePATies.query(qbServicePATY.prepare());

                // Đối tượng thanh toán phải được cho phép - Patient Type Allow
                Dao<PatientTypeAllow, Integer> patientTypeAllows = ((BaseActivity) context).getHelper().getPatientTypeAllow();
                QueryBuilder<PatientTypeAllow, Integer> qbPatientTypeAllow = patientTypeAllows.queryBuilder();
                qbPatientTypeAllow.selectColumns(PatientTypeAllow.FIELD_NAME_PATIENT_TYPE_ALLOW_ID);
                qbPatientTypeAllow.where().eq(PatientTypeAllow.FIELD_NAME_PATIENT_TYPE_ID, defaultPatientTypeId);
                List<PatientTypeAllow> lstPatientTypeAllow = patientTypeAllows.query(qbPatientTypeAllow.prepare());
                // Intersect 2 list
                Set<Long> patientTypeIds = new HashSet<>();
                List<Long> patientTypeIdFromServicePATY = new ArrayList<>();
                List<Long> patientTypeIdFromPatientTypeAllow = new ArrayList<>();
                for (ServicePATY sp : patientTypes) {
                    patientTypeIdFromServicePATY.add(sp.PATIENT_TYPE_ID);
                }
                for (PatientTypeAllow pta : lstPatientTypeAllow) {
                    patientTypeIdFromPatientTypeAllow.add(pta.PATIENT_TYPE_ALLOW_ID);
                }
                for (Long l : patientTypeIdFromServicePATY) {
                    if (patientTypeIdFromPatientTypeAllow.contains(l)) {
                        patientTypeIds.add(l);
                    }
                }
                for (Long l : patientTypeIdFromPatientTypeAllow) {
                    if (patientTypeIdFromServicePATY.contains(l)) {
                        patientTypeIds.add(l);
                    }
                }
                if (defaultPatientTypeId != -1 && !patientTypeIds.contains(defaultPatientTypeId)) {
                    patientTypeIds.add(defaultPatientTypeId);
                }

                qb.where().in(PatientType.FIELD_NAME_ID, patientTypeIds);
                qb.orderBy(PatientType.FIELD_NAME_PATIENT_TYPE_NAME, true);
                PreparedQuery<PatientType> preparedQuery = qb.prepare();
                lstPatientTypes = patientTypeDao.query(preparedQuery);

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return lstPatientTypes;
        }

        @Override
        protected void onPostExecute(List<PatientType> patientTypes) {
            super.onPostExecute(patientTypes);
            lstPatientType.clear();
            lstPatientType.addAll(patientTypes);
            adapter = new PatientTypeAdapter(context, R.layout.adapter_patient_type_spinner, R.id.txtPatientType);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spPatientType.setAdapter(adapter);
            if (defaultPatientTypeId != -1) {
                int selectedPosition = 0;
                for (; selectedPosition < lstPatientType.size(); selectedPosition++) {
                    if (lstPatientType.get(selectedPosition).ID == defaultPatientTypeId) {
                        spPatientType.setSelection(selectedPosition);
                        break;
                    }
                }

            }
            changePriceUnit(isExpend);
        }
    }

    class LoadUseForm extends AsyncTask<Void, Void, List<MedicineUserForm>> {

        @Override
        protected List<MedicineUserForm> doInBackground(Void... params) {
            Dao<MedicineUserForm, Integer> patientTypeDao = ((BaseActivity) context).getHelper().getMedicineUserFormDao();
            List<MedicineUserForm> lstUseForm = new ArrayList<>();
            QueryBuilder<MedicineUserForm, Integer> qb = patientTypeDao.queryBuilder();
            try {
                qb.orderBy(MedicineUserForm.FIELD_NAME_USE_FORM_CODE, true);
                PreparedQuery<MedicineUserForm> preparedQuery = qb.prepare();
                lstUseForm = patientTypeDao.query(preparedQuery);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return lstUseForm;
        }

        @Override
        protected void onPostExecute(List<MedicineUserForm> patientTypes) {
            super.onPostExecute(patientTypes);
            lstUseForm.clear();
            lstUseForm.addAll(patientTypes);
            userFormAdapter = new UseFormAdapter(context, R.layout.adapter_use_form_spinner, R.id.txtUseForm);
            userFormAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spUseForm.setAdapter(userFormAdapter);
            if (useFormId != -1) {
                int selectedPosition = 0;
                for (; selectedPosition < lstUseForm.size(); selectedPosition++) {
                    if (lstUseForm.get(selectedPosition).ID == useFormId) {
                        spUseForm.setSelection(selectedPosition);
                        break;
                    }
                }

            }
        }
    }

    class CalculatePrice extends AsyncTask<PatientType, Void, Double> {

        @Override
        protected Double doInBackground(PatientType... params) {
            PatientType patientType = params[0];
            List<ServicePATY> lstServicePATY = new ArrayList<>();
            Dao<ServicePATY, Integer> servicePATHDao = ((BaseActivity) context).getHelper().getServicePATYDao();
            QueryBuilder<ServicePATY, Integer> qb = servicePATHDao.queryBuilder();
            try {
                qb.where().eq(FIELD_NAME_SERVICE_PATY_SERVICE_ID, medicineMaterial.ServiceId).and().eq(FIELD_NAME_SERVICE_PATY_PATIENT_TYPE_ID, patientType.ID);
                PreparedQuery<ServicePATY> preparedQuery = qb.prepare();
                lstServicePATY = servicePATHDao.query(preparedQuery);
                if (lstServicePATY.size() == 0) return 0d;
                ServicePATY selectedPrice = null;
                for (ServicePATY pty : lstServicePATY) {
                    pty.FROM_TIME = pty.FROM_TIME != null ? pty.FROM_TIME : Long.MIN_VALUE;
                    pty.TO_TIME = pty.TO_TIME != null ? pty.TO_TIME : Long.MAX_VALUE;
                    pty.TREATMENT_FROM_TIME = pty.TREATMENT_FROM_TIME != null ? pty.TREATMENT_FROM_TIME : Long.MIN_VALUE;
                    pty.TREATMENT_TO_TIME = pty.TREATMENT_TO_TIME != null ? pty.TREATMENT_TO_TIME : Long.MAX_VALUE;
                    if ((pty.FROM_TIME < applyTime && applyTime < pty.TO_TIME) ||
                            (pty.TREATMENT_FROM_TIME < inTime && inTime < pty.TREATMENT_TO_TIME)) {
                        if (selectedPrice == null) {
                            selectedPrice = pty;
                        } else {
                            selectedPrice.PRIORITY = selectedPrice.PRIORITY != null ? selectedPrice.PRIORITY : 0;
                            pty.PRIORITY = pty.PRIORITY != null ? pty.PRIORITY : 0;
                            if (pty.PRIORITY > selectedPrice.PRIORITY) {
                                selectedPrice = pty;
                            } else if (pty.PRIORITY.equals(selectedPrice.PRIORITY)) {
                                if (pty.MODIFY_TIME > selectedPrice.MODIFY_TIME) {
                                    selectedPrice = pty;
                                }
                            }
                        }
                    }
                }
                return selectedPrice == null ? 0d : selectedPrice.PRICE;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return 0d;
        }

        @Override
        protected void onPostExecute(Double aDouble) {
            super.onPostExecute(aDouble);
            priceUnit = BigDecimal.valueOf(aDouble);
            changePriceUnit(isExpend);
        }
    }

    class CalculatePriceForBHXH extends AsyncTask<Void, Void, Double> {

        @Override
        protected Double doInBackground(Void... params) {
            List<ServicePATY> lstServicePATY = new ArrayList<>();
            Dao<ServicePATY, Integer> servicePATHDao = ((BaseActivity) context).getHelper().getServicePATYDao();
            QueryBuilder<ServicePATY, Integer> qb = servicePATHDao.queryBuilder();
            try {
                qb.where().eq(FIELD_NAME_SERVICE_PATY_SERVICE_ID, medicineMaterial.ServiceId).and().eq(FIELD_NAME_SERVICE_PATY_PATIENT_TYPE_ID, HardCodeUtils.PATIENT_TYPE_ID_BHXH);
                PreparedQuery<ServicePATY> preparedQuery = qb.prepare();
                lstServicePATY = servicePATHDao.query(preparedQuery);
                if (lstServicePATY.size() == 0) return 0d;
                ServicePATY selectedPrice = null;
                for (ServicePATY pty : lstServicePATY) {
                    pty.FROM_TIME = pty.FROM_TIME != null ? pty.FROM_TIME : Long.MIN_VALUE;
                    pty.TO_TIME = pty.TO_TIME != null ? pty.TO_TIME : Long.MAX_VALUE;
                    pty.TREATMENT_FROM_TIME = pty.TREATMENT_FROM_TIME != null ? pty.TREATMENT_FROM_TIME : Long.MIN_VALUE;
                    pty.TREATMENT_TO_TIME = pty.TREATMENT_TO_TIME != null ? pty.TREATMENT_TO_TIME : Long.MAX_VALUE;
                    if ((pty.FROM_TIME < applyTime && applyTime < pty.TO_TIME) ||
                            (pty.TREATMENT_FROM_TIME < inTime && inTime < pty.TREATMENT_TO_TIME)) {
                        if (selectedPrice == null) {
                            selectedPrice = pty;
                        } else {
                            selectedPrice.PRIORITY = selectedPrice.PRIORITY != null ? selectedPrice.PRIORITY : 0;
                            pty.PRIORITY = pty.PRIORITY != null ? pty.PRIORITY : 0;
                            if (pty.PRIORITY > selectedPrice.PRIORITY) {
                                selectedPrice = pty;
                            } else if (pty.PRIORITY.equals(selectedPrice.PRIORITY)) {
                                if (pty.MODIFY_TIME > selectedPrice.MODIFY_TIME) {
                                    selectedPrice = pty;
                                }
                            }
                        }
                    }
                }
                return selectedPrice == null ? 0d : selectedPrice.PRICE;
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return 0d;
        }

        @Override
        protected void onPostExecute(Double aDouble) {
            super.onPostExecute(aDouble);
            priceUnitBKXH = BigDecimal.valueOf(aDouble);

        }
    }

    class PatientTypeAdapter extends ArrayAdapter<PatientType> {

        public PatientTypeAdapter(Context context, int resource, int textViewResourceId) {
            super(context, resource, textViewResourceId);
        }

        @Override
        public int getCount() {
            return lstPatientType == null ? 0 : lstPatientType.size();
        }

        @Nullable
        @Override
        public PatientType getItem(int position) {
            return lstPatientType.get(position);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getView(position, convertView, parent);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            PatientType pt = lstPatientType.get(position);
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = layoutInflater.inflate(R.layout.adapter_patient_type_spinner, parent, false);
                viewHolder.txtName = (TextView) convertView.findViewById(R.id.txtPatientType);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.txtName.setText(pt.PATIENT_TYPE_NAME);
            return convertView;
        }
    }

    class UseFormAdapter extends ArrayAdapter<MedicineUserForm> {

        public UseFormAdapter(Context context, int resource, int textViewResourceId) {
            super(context, resource, textViewResourceId);
        }

        @Override
        public int getCount() {
            return lstUseForm == null ? 0 : lstUseForm.size();
        }

        @Nullable
        @Override
        public MedicineUserForm getItem(int position) {
            return lstUseForm.get(position);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getView(position, convertView, parent);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            MedicineUserForm u = lstUseForm.get(position);
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = layoutInflater.inflate(R.layout.adapter_use_form_spinner, parent, false);
                viewHolder.txtName = (TextView) convertView.findViewById(R.id.txtUseForm);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.txtName.setText(u.MEDICINE_USE_FORM_CODE + " - " + u.MEDICINE_USE_FORM_NAME);
            return convertView;
        }
    }

    private static class ViewHolder {
        TextView txtName;
    }

    public interface OnNumberEnteredListener {
        void onNumberEntered(double number, MedicineMaterial medicineMaterial, PatientType patientType, MedicineUserForm useForm, boolean isExpend, boolean isWithBHYT, BigDecimal priceUnit, String tutorial);
    }
}
