package com.staarlab.idoctor.ui.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.ExpMestTemplate;
import com.staarlab.idoctor.entity.ICD;
import com.staarlab.idoctor.entity.MedicalStock;
import com.staarlab.idoctor.entity.MedicineUserForm;
import com.staarlab.idoctor.entity.PatientType;
import com.staarlab.idoctor.entity.ServiceGroup;
import com.staarlab.idoctor.entity.ServicePATY;
import com.staarlab.idoctor.entity.ServiceRoom;
import com.staarlab.idoctor.entity.ServiceSegr;
import com.staarlab.idoctor.entity.ServiceType;
import com.staarlab.idoctor.entity.TechService;
import com.staarlab.idoctor.helper.share.MBSSharePreference;
import com.staarlab.idoctor.model.LoginModel;
import com.staarlab.idoctor.model.ResponseArrayModel;
import com.staarlab.idoctor.model.ResponseObjectModel;
import com.staarlab.idoctor.ui.dialog.SelectServerDialog;
import com.staarlab.idoctor.ui.service.SyncDataService;
import com.staarlab.idoctor.util.DateTimeUtils;
import com.staarlab.idoctor.util.StringUtils;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.LoginService;
import com.staarlab.idoctor.webservice.WSFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Callable;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class LoginActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = LoginActivity.class.getName();
    private EditText mUserNameView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private TextView tvSyncData;
    private ImageView imgChooseServer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mUserNameView = (EditText) findViewById(R.id.username);
        tvSyncData = (TextView) findViewById(R.id.tvSyncData);
        imgChooseServer = (ImageView) findViewById(R.id.imgChooseServer);
        imgChooseServer.setOnClickListener(this);
        String defaultUserName = MBSSharePreference.get().getUserLogin();
        mUserNameView.setText(defaultUserName);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button btnLogin = (Button) findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        showProgress(true);

        btnLogin.post(new Runnable() {
            @Override
            public void run() {
                if (!MBSSharePreference.get().getUserLogin().isEmpty()) {
                    showProgress(true);
                    checkAutoLogin();
                } else {
                    showProgress(false);
                }
            }
        });
    }

    private void checkAutoLogin() {
        LoginService loginService = WSFactory
                .createLoginService(MBSSharePreference.get().getTokenCode());
        loginService.getAuthenticated(new Callback<ResponseObjectModel<LoginModel>>() {
            @Override
            public void success(ResponseObjectModel<LoginModel> responLoginModel, Response response) {
                boolean success = responLoginModel.isSuccess();
                if (success) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            doUpdateIfAny();
                        }
                    });
                } else {
                    renewCode();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                renewCode();
            }
        });
    }

    private void doUpdateIfAny() {
        if (MBSSharePreference.get().getLastUpdate().isEmpty()) {
            tvSyncData.setVisibility(View.VISIBLE);
            Intent intent = new Intent(LoginActivity.this, SyncDataService.class);
            startService(intent);
        } else {
            tvSyncData.setVisibility(View.INVISIBLE);
            startMainActivity();
        }
    }

    private void renewCode() {
        LoginService loginService = WSFactory
                .createLoginServiceRenewCode(MBSSharePreference.get().getRenewCode());
        loginService.renew(new Callback<ResponseObjectModel<LoginModel>>() {
            @Override
            public void success(ResponseObjectModel<LoginModel> responLoginModel, Response response) {
                boolean success = responLoginModel.isSuccess();
                if (success) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            doUpdateIfAny();
                        }
                    });
                } else {
                    showProgress(false);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                showProgress(false);
                error.printStackTrace();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SyncDataService.ACTION_UPDATE_FAIL);
        intentFilter.addAction(SyncDataService.ACTION_UPDATE_SUCCESS);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(SyncDataService.ACTION_UPDATE_SUCCESS)) {
                showProgress(false);
                startMainActivity();
            } else if (intent.getAction().equalsIgnoreCase(SyncDataService.ACTION_UPDATE_FAIL)) {
                showProgress(false);
                Toast.makeText(LoginActivity.this, R.string.sync_data_fail, Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void attemptLogin() {
        closeSoftKey();
        // Reset errors.
        mUserNameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String userName = mUserNameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(userName)) {
            mUserNameView.setError(getString(R.string.error_field_required));
            focusView = mUserNameView;
            cancel = true;
        } else if (!isUserNameValid(userName)) {
            mUserNameView.setError(getString(R.string.login_error_invalid_username));
            focusView = mUserNameView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            showProgress(true);
            LoginService loginService = WSFactory.createLoginService(userName, password);
            loginService.login(new Callback<ResponseObjectModel<LoginModel>>() {
                @Override
                public void success(ResponseObjectModel<LoginModel> responseObjectModel, Response response) {
                    if (responseObjectModel.isSuccess()) {
                        MBSSharePreference.get().putUserLogin(responseObjectModel.getDataResult().getUser().getLoginName());
                        MBSSharePreference.get().putUserName(responseObjectModel.getDataResult().getUser().getUserName());
                        MBSSharePreference.get().putTokenCode(responseObjectModel.getDataResult().getTokenCode());
                        MBSSharePreference.get().putRenewToken(responseObjectModel.getDataResult().getRenewCode());
                        MBSSharePreference.get().putAppCode(responseObjectModel.getDataResult().getUser().getApplicationCode());
                        doUpdateIfAny();
                    } else {
                        showProgress(false);
                        Toast.makeText(LoginActivity.this, R.string.login_error_incorrect, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    showProgress(false);
                    Toast.makeText(LoginActivity.this, R.string.login_error_common, Toast.LENGTH_SHORT).show();
                    error.printStackTrace();
                }
            });
        }
    }

    private void startMainActivity() {
        Intent intent = new Intent(this, RoomListActivity.class);
        startActivity(intent);
    }

    private boolean isUserNameValid(String userName) {
        return userName != null && !"".equals(userName);
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgChooseServer:
                SelectServerDialog dialog = new SelectServerDialog(this);
                dialog.show();
                break;
            case R.id.btn_login:
                attemptLogin();
                break;
        }
    }
}