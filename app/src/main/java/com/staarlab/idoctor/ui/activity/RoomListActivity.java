package com.staarlab.idoctor.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.staarlab.idoctor.MBSApplication;
import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.Room;
import com.staarlab.idoctor.helper.layout.GeneralSwipeRefreshLayout;
import com.staarlab.idoctor.helper.share.MBSSharePreference;
import com.staarlab.idoctor.model.BodyModel;
import com.staarlab.idoctor.model.PostResponModel;
import com.staarlab.idoctor.model.ResponseArrayModel;
import com.staarlab.idoctor.ui.adapter.RoomAdapter;
import com.staarlab.idoctor.ui.fragment.MedicineMaterialListFragment;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.WSFactory;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RoomListActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, GeneralSwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemClickListener {
    private static final String TAG = RoomListActivity.class.getName();

    private GridView roomGridView;
    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    private GeneralSwipeRefreshLayout swipeRefresh;
    private int currentNavigationItemSelected = 0;
    private HISService hisService;

    private RoomAdapter roomAdapter;
    private TextView txtAppCode;
    private TextView txtUserName;
    List<Room> rooms;
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.room_list);
        currentNavigationItemSelected = R.id.room_list;

        View header = navigationView.getHeaderView(0);
        txtAppCode = (TextView) header.findViewById(R.id.txtAppCode);
        txtUserName = (TextView) header.findViewById(R.id.txtUserName);
        txtAppCode.setText(MBSSharePreference.get().getAppCode());
        txtUserName.setText(MBSSharePreference.get().getUserName());

        roomGridView = (GridView) findViewById(R.id.roomGridView);
        swipeRefresh = (GeneralSwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipeRefresh.setOnRefreshListener(this);

        hisService = WSFactory.createHisService(MBSSharePreference.get().getTokenCode());

        roomAdapter = new RoomAdapter(this, new ArrayList<Room>(0));
        roomGridView.setAdapter(roomAdapter);
        roomGridView.setOnItemClickListener(this);
        swipeRefresh.setOnChildScrollUpListener(new GeneralSwipeRefreshLayout.OnChildScrollUpListener() {
            @Override
            public boolean canChildScrollUp() {
                return roomGridView.getFirstVisiblePosition() != 0;
            }
        });
        swipeRefresh.post(new Runnable() {
            @Override
            public void run() {
                swipeRefresh.setRefreshing(false);
                swipeRefresh.setOnRefreshListener(RoomListActivity.this);
                checkIfHaveData();
            }
        });
    }

    private void checkIfHaveData() {
        swipeRefresh.setRefreshing(true);
        loadRooms();
    }

    private void loadRooms() {
        String loginName = MBSSharePreference.get().getUserLogin();
        String param = "{'ApiData':{'LOGINNAME':'" + loginName + "'}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getRoomByDoctor(param, new Callback<ResponseArrayModel<Room>>() {
            @Override
            public void success(ResponseArrayModel<Room> data, Response response) {
                if (data.isSuccess()) {
                    rooms = new ArrayList<>();
                    for (Room room : data.getDataList()) {
                        if (Room.ROOM_TYPE_GI.equalsIgnoreCase(room.getRoomTypeCode())) {
                            rooms.add(room);
                        }
                    }
                    roomAdapter.refreshData(rooms);
                    swipeRefresh.setRefreshing(false);
                } else {
                    Toast.makeText(RoomListActivity.this, R.string.room_list_load_fail, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(RoomListActivity.this, R.string.room_list_load_fail, Toast.LENGTH_SHORT).show();
                roomAdapter.refreshData(new ArrayList<Room>());
                swipeRefresh.setRefreshing(false);
            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == currentNavigationItemSelected) return false;

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        switch (id) {
            case R.id.settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.exit:
                MBSApplication.get().logout(this);
                break;
        }
        return true;
    }

    @Override
    public void onRefresh() {
        checkIfHaveData();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // POST working session info to server
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.update_work_space));
        }
        mProgressDialog.show();

        final Room item = roomAdapter.getItem(position);
        BodyModel<Long> data = new BodyModel<>(item.getRoomId());
        hisService.postWorkingSessionInfo(data, new Callback<PostResponModel>() {
            @Override
            public void success(PostResponModel postResponModel, Response response) {
                if (postResponModel.isSuccess()) {
                    Log.i(TAG, "Update working space successful");
                    Intent intent = new Intent(RoomListActivity.this, PatientListActivity.class);
                    intent.putExtra("room", item);
                    startActivity(intent);
                } else {
                    Log.i(TAG, "Update working space unsuccessful");
                    Toast.makeText(RoomListActivity.this, R.string.update_work_space_fail, Toast.LENGTH_SHORT).show();
                }
                if (mProgressDialog != null) mProgressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Log.i(TAG, "Update working space unsuccessful");
                Toast.makeText(RoomListActivity.this, R.string.update_work_space_fail, Toast.LENGTH_SHORT).show();
                if (mProgressDialog != null) mProgressDialog.dismiss();

            }
        });

    }

}