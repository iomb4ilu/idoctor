package com.staarlab.idoctor.ui.adapter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.Medicine;
import com.staarlab.idoctor.ui.fragment.ChooseMedicineFragment;

import java.util.List;

/**
 * Created by ducln on 17/11/2016.
 */

public class MedicineAdapter extends RecyclerView.Adapter<MedicineAdapter.ViewHolder> {
    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView txtName;
        final TextView txtAvailable;
        final TextView txtStock;
        final TextView txtHoatChat;

        public ViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtAvailable = (TextView) itemView.findViewById(R.id.txtAvailable);
            txtStock = (TextView) itemView.findViewById(R.id.txtStock);
            txtHoatChat = (TextView) itemView.findViewById(R.id.txtHoatChat);
        }
    }

    private Context mContext;
    private List<Medicine> medicineList;

    public MedicineAdapter(Context context, List<Medicine> medicineList) {
        super();
        mContext = context;
        this.medicineList = medicineList;
    }

    @Override
    public int getItemCount() {
        return medicineList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(mContext).inflate(R.layout.fragment_medicine_item, parent, false);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // mở màn hình chọn
                ChooseMedicineFragment chooseMedicineFragment = new ChooseMedicineFragment();
                chooseMedicineFragment.show(((AppCompatActivity) mContext).getFragmentManager(), "dialog");
            }
        });
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Medicine medicine = medicineList.get(position);

    }

    public void refresh(List<Medicine> medicineList, boolean notifyDataChanged) {
        this.medicineList = medicineList;
        if (notifyDataChanged) notifyDataSetChanged();
    }
}
