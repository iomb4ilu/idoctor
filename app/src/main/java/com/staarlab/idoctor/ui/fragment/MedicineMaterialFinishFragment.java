package com.staarlab.idoctor.ui.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.SimpleDialog;
import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.ICD;
import com.staarlab.idoctor.entity.Material;
import com.staarlab.idoctor.entity.MedicalStock;
import com.staarlab.idoctor.entity.Medicine;
import com.staarlab.idoctor.entity.TreatmentCommonInfo;
import com.staarlab.idoctor.helper.layout.DividerItemDecoration;
import com.staarlab.idoctor.helper.layout.HeaderRecyclerDividerItemDecorator;
import com.staarlab.idoctor.helper.share.MBSSharePreference;
import com.staarlab.idoctor.model.BodyModel;
import com.staarlab.idoctor.model.PostResponModel;
import com.staarlab.idoctor.ui.activity.BaseActivity;
import com.staarlab.idoctor.util.HardCodeUtils;
import com.staarlab.idoctor.util.NumberFormatUtils;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.WSFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ducln on 16/11/2016.
 */

public class MedicineMaterialFinishFragment extends BaseFragment {
    private static final String TAG = MedicineMaterialFinishFragment.class.getName();
    TreatmentCommonInfo treatment;
    private ICD icd;
    MedicineMaterialSpecifyFragment.MedicineMaterialHolder[] lstSelectedTechMedicineMaterial;
    private Long numberRemedy;
    private String advice;
    private long roomId;
    MedicalStock medicalStock;
    private long instructionTime, useTime;

    TextView tvTreatmentCode;
    TextView tvPatientCode;
    TextView tvPatientName;
    TextView tvPatientType;
    Toolbar toolbar;
    ProgressDialog mProgressDialog;

    HisPrescriptionSDO data;

    private RecyclerView rvSelectedMedicineMaterial;
    SelectedFinishAdapter adapter;

    private HISService hisService;

    public static MedicineMaterialFinishFragment newInstance(TreatmentCommonInfo treatmentCommonInfo, MedicalStock ms, long roomId, ICD icd, MedicineMaterialSpecifyFragment.MedicineMaterialHolder[] lstSelectedService,
                                                             Long numberRemedy, String advice, long instructionT, long useT) {
        MedicineMaterialFinishFragment fragment = new MedicineMaterialFinishFragment();
        Bundle args = new Bundle();
        args.putParcelable("tm", treatmentCommonInfo);
        args.putParcelable("ms", ms);
        args.putParcelable("icd", icd);
        args.putParcelableArray("lss", lstSelectedService);
        args.putLong("b", numberRemedy);
        args.putLong("l", roomId);
        args.putLong("li", instructionT);
        args.putLong("lu", useT);
        args.putString("str", advice);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            treatment = getArguments().getParcelable("tm");
            icd = getArguments().getParcelable("icd");
            medicalStock = getArguments().getParcelable("ms");
            Parcelable[] tempParcelable = getArguments().getParcelableArray("lss");
            lstSelectedTechMedicineMaterial = new MedicineMaterialSpecifyFragment.MedicineMaterialHolder[tempParcelable.length];
            for (int i = 0; i < tempParcelable.length; i++) {
                lstSelectedTechMedicineMaterial[i] = (MedicineMaterialSpecifyFragment.MedicineMaterialHolder) tempParcelable[i];
            }
            numberRemedy = getArguments().getLong("b");
            roomId = getArguments().getLong("l");
            instructionTime = getArguments().getLong("li");
            useTime = getArguments().getLong("lu");
            advice = getArguments().getString("str");
        }
        hisService = WSFactory.createHisService(MBSSharePreference.get().getTokenCode());
        adapter = new SelectedFinishAdapter();
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_medicine_material_finish, container, false);
        tvTreatmentCode = (TextView) view.findViewById(R.id.tvTreatmentCode);
        tvPatientCode = (TextView) view.findViewById(R.id.tvPatientCode);
        tvPatientName = (TextView) view.findViewById(R.id.tvPatientName);
        tvPatientType = (TextView) view.findViewById(R.id.tvPatientType);

        tvTreatmentCode.setText(treatment.TREATMENT_CODE == null ? "" : treatment.TREATMENT_CODE);
        tvPatientCode.setText(treatment.PATIENT_CODE == null ? "" : treatment.PATIENT_CODE);
        tvPatientName.setText(treatment.patient.getVirPatientName() == null ? "" : treatment.patient.getVirPatientName());
        tvPatientType.setText(treatment.patientTypeAlter.PATIENT_TYPE_NAME != null ? treatment.patientTypeAlter.PATIENT_TYPE_NAME : "");

        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        if (toolbar != null) {
            ((BaseActivity) getActivity()).setSupportActionBar(toolbar);
        }
        (getActivity()).setTitle(R.string.finish_medicine_material_title);

        rvSelectedMedicineMaterial = (RecyclerView) view.findViewById(R.id.rvSelectedService);
        rvSelectedMedicineMaterial.setHasFixedSize(true);
        rvSelectedMedicineMaterial.setLayoutManager(new LinearLayoutManager(context));
        rvSelectedMedicineMaterial.addItemDecoration(
                new HeaderRecyclerDividerItemDecorator(
                        context, DividerItemDecoration.VERTICAL_LIST, false, false,
                        R.drawable.divider_104));
        rvSelectedMedicineMaterial.setAdapter(adapter);

        hisService = WSFactory.createHisService(MBSSharePreference.get().getTokenCode());
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save) {
            postData();
        }
        return super.onOptionsItemSelected(item);
    }

    private void postData() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.sending_data));
        }
        mProgressDialog.show();
        collectData();
        BodyModel<HisPrescriptionSDO> body = new BodyModel<>(data);
        hisService.postMedicineMaterial(body, new Callback<PostResponModel>() {
            @Override
            public void success(PostResponModel response, Response response2) {
                if (response.isSuccess()) {
                    showDialogConfirmSuccess();
                } else {
                    String error = response.getParam().getMessages() != null && response.getParam().getMessages().size() > 0 ? response.getParam().getMessages().get(0) : getString(R.string.finish_medicine_material_post_fail);
                    Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                }
                if (mProgressDialog != null) mProgressDialog.dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(context, R.string.finish_medicine_material_post_fail, Toast.LENGTH_SHORT).show();
                if (mProgressDialog != null) mProgressDialog.dismiss();
            }
        });
    }

    private void collectData() {
        data = new HisPrescriptionSDO();
        data.ExpMest = new HIS_EXP_MEST();
        data.ExpMest.MEDI_STOCK_ID = medicalStock.MEDI_STOCK_ID != null ? medicalStock.MEDI_STOCK_ID : medicalStock.ID;
        data.ExpMest.DESCRIPTION = medicalStock.MEDI_STOCK_NAME;
        data.MediStockId = medicalStock.MEDI_STOCK_ID != null ? medicalStock.MEDI_STOCK_ID : medicalStock.ID;
        data.IcdId = icd.ID;
        data.IcdText = icd.ICD_TEXT;
        data.IcdMainText = icd.ICD_MAIN_NAME;
        data.PatientId = treatment.PATIENT_ID;
        data.TreatmentId = treatment.ID;
        data.InstructionTime = instructionTime;
        data.UseTime = useTime;
        data.RemedyCount = numberRemedy;
        data.Advise = advice;

        List<HisPrescriptionMedicineSDO> prescriptionMedicine = new ArrayList<>();
        List<HisPrescriptionMaterialSDO> prescriptionMaterial = new ArrayList<>();
        List<HIS_EXP_MEST_MATY> hisExpMestMaties = new ArrayList<>();
        List<HIS_EXP_MEST_METY> hisExpMestMeties = new ArrayList<>();

        for (MedicineMaterialSpecifyFragment.MedicineMaterialHolder mm : lstSelectedTechMedicineMaterial) {
            if (mm.medicineMaterial instanceof Medicine) {
                HisPrescriptionMedicineSDO medicineSDO = new HisPrescriptionMedicineSDO();
                medicineSDO.Tutorial = mm.tutorial;
                medicineSDO.PatientTypeId = mm.isWithBHXH ? HardCodeUtils.PATIENT_TYPE_ID_BHXH : mm.patientType != null && mm.patientType.ID != null ? mm.patientType.ID : treatment.patientTypeAlter.PATIENT_TYPE_ID;
                medicineSDO.EmbedPatientTypeId = mm.isWithBHXH ? mm.patientType != null && mm.patientType.ID != null ? mm.patientType.ID : null : null;
                medicineSDO.MedicineUseFormId = mm.userForm != null && mm.userForm.ID != null ? mm.userForm.ID : 1;
                medicineSDO.MedicineTypeId = mm.medicineMaterial.Id;
                medicineSDO.IsExpend = mm.isExpend ? 1 : 0;
                medicineSDO.Amount = BigDecimal.valueOf(mm.number);
                medicineSDO.UseTimeTo = useTime;

                prescriptionMedicine.add(medicineSDO);

//                HIS_EXP_MEST_METY hisExpMestMety = new HIS_EXP_MEST_METY();
//                hisExpMestMety.AMOUNT = BigDecimal.valueOf(mm.number);
//                hisExpMestMety.MEDICINE_TYPE_ID = mm.medicineMaterial.Id;
//                hisExpMestMety.MEDICINE_USE_FORM_ID = mm.userForm.ID;
//                hisExpMestMety.TUTORIAL = mm.tutorial;
//
//                hisExpMestMeties.add(hisExpMestMety);

            } else if (mm.medicineMaterial instanceof Material) {
                HisPrescriptionMaterialSDO materialSDO = new HisPrescriptionMaterialSDO();
                materialSDO.MaterialTypeId = mm.medicineMaterial.Id;
                materialSDO.PatientTypeId = mm.isWithBHXH ? HardCodeUtils.PATIENT_TYPE_ID_BHXH : mm.patientType != null && mm.patientType.ID != null ? mm.patientType.ID : treatment.patientTypeAlter.PATIENT_TYPE_ID;
                materialSDO.EmbedPatientTypeId = mm.isWithBHXH ? mm.patientType != null && mm.patientType.ID != null ? mm.patientType.ID : null : null;
                materialSDO.IsExpend = mm.isExpend ? 1 : 0;
                materialSDO.Amount = BigDecimal.valueOf(mm.number);

                prescriptionMaterial.add(materialSDO);

//                HIS_EXP_MEST_MATY mestMaty = new HIS_EXP_MEST_MATY();
//                mestMaty.MATERIAL_TYPE_ID = mm.medicineMaterial.Id;
//                mestMaty.AMOUNT = BigDecimal.valueOf(mm.number);
//                hisExpMestMaties.add(mestMaty);
            }
        }

        data.PrescriptionMedicines = prescriptionMedicine;
        data.PrescriptionMaterials = prescriptionMaterial;
        data.ExpMestMeties = hisExpMestMeties;
        data.ExpMestMaties = hisExpMestMaties;

    }

    private void showDialogConfirmSuccess() {
        Dialog.Builder builder = null;
        builder = new SimpleDialog.Builder(R.style.Material_App_Dialog_Simple_Light) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                super.onPositiveActionClicked(fragment);
                getActivity().finish();
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
            }
        };

        ((SimpleDialog.Builder) builder)
                .message(getString(R.string.finish_medicine_material_post_success))
                .title(getString(R.string.notify))
                .positiveAction(getString(R.string.confirm));
        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.setCancelable(false);
        fragment.show(getFragmentManager(), null);
    }

    class SelectedFinishAdapter extends RecyclerView.Adapter<SelectedFinishViewHolder> {

        @Override
        public SelectedFinishViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.adapter_specify_medicine_material_finish_list_item, parent, false);
            return new SelectedFinishViewHolder(view);
        }

        @Override
        public void onBindViewHolder(SelectedFinishViewHolder holder, int position) {
            MedicineMaterialSpecifyFragment.MedicineMaterialHolder item = lstSelectedTechMedicineMaterial[position];

            if (item.medicineMaterial instanceof Medicine) {
                holder.tvMedicineMaterialName.setText(((Medicine) item.medicineMaterial).MedicineTypeName);
                holder.tvMedicineMaterialCode.setText(((Medicine) item.medicineMaterial).MedicineTypeCode);
            } else if (item.medicineMaterial instanceof Material) {
                holder.tvMedicineMaterialName.setText(((Material) item.medicineMaterial).MaterialTypeName);
                holder.tvMedicineMaterialCode.setText(((Material) item.medicineMaterial).MaterialTypeCode);
            }
            holder.tvPatientType.setText(item.patientType.PATIENT_TYPE_NAME);
            long temp = (long) item.number;
            holder.tvNumber.setText(item.number == temp ? String.format("%d", temp) : String.valueOf(item.number));
            holder.tvExpend.setText(item.isExpend ? getString(R.string.yes) : getString(R.string.no));
            holder.tvWithBHYT.setText(item.isWithBHXH ? getString(R.string.yes) : getString(R.string.no));
            holder.tvPriceUnit.setText(item.isExpend ? "0" : item.priceUnit == null ? "0" : NumberFormatUtils.formatMoney(item.priceUnit));
        }

        @Override
        public int getItemCount() {
            return lstSelectedTechMedicineMaterial != null ? lstSelectedTechMedicineMaterial.length : 0;
        }
    }

    class SelectedFinishViewHolder extends RecyclerView.ViewHolder {

        TextView tvMedicineMaterialCode;
        TextView tvMedicineMaterialName;
        TextView tvPatientType;
        TextView tvNumber;
        TextView tvExpend;
        TextView tvWithBHYT;
        TextView tvPriceUnit;

        public SelectedFinishViewHolder(View itemView) {
            super(itemView);
            tvMedicineMaterialCode = (TextView) itemView.findViewById(R.id.tvServiceInfo);
            tvMedicineMaterialName = (TextView) itemView.findViewById(R.id.tvServiceName);
            tvPatientType = (TextView) itemView.findViewById(R.id.tvPatientType);
            tvNumber = (TextView) itemView.findViewById(R.id.tvNumber);
            tvExpend = (TextView) itemView.findViewById(R.id.tvExpend);
            tvWithBHYT = (TextView) itemView.findViewById(R.id.tvWithBHYT);
            tvPriceUnit = (TextView) itemView.findViewById(R.id.tvPriceUnit);
        }
    }

    // DATA
    public class HisPrescriptionSDO {
        public HIS_EXP_MEST ExpMest;
        public List<HisPrescriptionMedicineSDO> PrescriptionMedicines;
        public List<HisPrescriptionMaterialSDO> PrescriptionMaterials;
        public List<HIS_EXP_MEST_MATY> ExpMestMaties;
        public List<HIS_EXP_MEST_METY> ExpMestMeties;
        public String Advise;
        public Long ParentServiceReqId;
        public long MediStockId;
        public long IcdId;
        public String IcdText;
        public String IcdMainText;
        public Long PatientId;
        public long TreatmentId;
        public long InstructionTime;
        public long UseTime;
        public Long ExecuteGroupId;
        public Long RemedyCount;
    }

    public class HIS_EXP_MEST {
        public long MEDI_STOCK_ID;
        public String DESCRIPTION;
    }

    public class HisPrescriptionMedicineSDO {
        public Long SereServParentId;
        public String Tutorial;
        public long MedicineUseFormId;
        public long UseTimeTo;
        public long MedicineTypeId;
        public long PatientTypeId;
        public int IsExpend;
        public Long EmbedPatientTypeId;
        public boolean IsEmbedHeinPrice;
        public BigDecimal Amount;
        public int IsOutKtcFee;
    }

    public class HisPrescriptionMaterialSDO {
        public Long SereServParentId;
        public long MaterialTypeId;
        public long PatientTypeId;
        public int IsExpend;
        public Long EmbedPatientTypeId;
        public boolean IsEmbedHeinPrice;
        public BigDecimal Amount;
        public int IsOutKtcFee;
    }

    public class HIS_EXP_MEST_MATY {
        public long MATERIAL_TYPE_ID;
        public BigDecimal AMOUNT;
    }

    public class HIS_EXP_MEST_METY {
        public long MEDICINE_TYPE_ID;
        public BigDecimal AMOUNT;
        public String TUTORIAL;
        public Long MEDICINE_USE_FORM_ID;
    }
}
