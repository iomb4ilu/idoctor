package com.staarlab.idoctor.ui.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.ExpMestTemplate;
import com.staarlab.idoctor.entity.ICD;
import com.staarlab.idoctor.entity.MedicalStock;
import com.staarlab.idoctor.entity.Prescription;
import com.staarlab.idoctor.entity.Treatment;
import com.staarlab.idoctor.entity.TreatmentCommonInfo;
import com.staarlab.idoctor.helper.share.MBSSharePreference;
import com.staarlab.idoctor.model.ResponseObjectModel;
import com.staarlab.idoctor.ui.activity.BaseActivity;
import com.staarlab.idoctor.ui.dialog.SelectICDDialog;
import com.staarlab.idoctor.ui.dialog.SelectMedicalStockDialog;
import com.staarlab.idoctor.ui.dialog.SelectOldPrescription;
import com.staarlab.idoctor.ui.dialog.SelectTemplatePrescription;
import com.staarlab.idoctor.util.DateTimeUtils;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.WSFactory;

import java.util.Calendar;
import java.util.Date;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ducln on 22/11/2016.
 */

public class MedicineMaterialDiagnosticFragment extends BaseFragment implements View.OnClickListener, Toolbar.OnMenuItemClickListener, SelectICDDialog.OnICDSelectListener,
        SelectMedicalStockDialog.onMedicalStockSelected, SelectOldPrescription.onOldPrescriptionSelected, SelectTemplatePrescription.OnTemplatePrescriptionSelect {
    private HISService hisService;
    private Treatment treatment;
    private long roomId;
    private MedicalStock medicalStock;
    private TreatmentCommonInfo treatmentCommonInfo;
    private ICD icd;
    Date instructionTime, useTime;
    Prescription oldPrescription;
    ExpMestTemplate templateDescription;

    TextView tvTreatmentCode;
    TextView tvPatientCode;
    TextView tvPatientName;
    TextView tvPatientType;
    private EditText edtDate;
    private EditText edtTime;
    private EditText edtUseDate;
    private EditText edtUseTime;
    private EditText edtMainExaminationCode;
    private EditText edtMainExaminationNote;
    private EditText edtInventoryCode;
    private EditText edtInventoryName;
    private EditText edtSamplePrescription;
    private EditText edtOldPrescription;
    private EditText edtMinorExaminationNote;
    private EditText edtNumberRemedy;
    private EditText edtAdvice;
    private Button btnClearSamplePrescription;
    private Button btnClearOldPrescription;
    private Button btnClearICD;
    private Button btnClearInventory;

    public static MedicineMaterialDiagnosticFragment newInstance(Treatment tm, long roomId) {
        Bundle args = new Bundle();
        args.putParcelable("TREATMENT", tm);
        args.putLong("roomId", roomId);
        MedicineMaterialDiagnosticFragment fragment = new MedicineMaterialDiagnosticFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hisService = WSFactory.createHisService(MBSSharePreference.get().getTokenCode());
        if (getArguments() != null) {
            treatment = getArguments().getParcelable("TREATMENT");
            roomId = getArguments().getLong("roomId");
        }
        instructionTime = DateTimeUtils.currentDate();
        useTime = DateTimeUtils.currentDate();
        getPatientCommonInfo();
    }

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_medicine_material_diagnostic, container, false);
        tvTreatmentCode = (TextView) view.findViewById(R.id.tvTreatmentCode);
        tvPatientCode = (TextView) view.findViewById(R.id.tvPatientCode);
        tvPatientName = (TextView) view.findViewById(R.id.tvPatientName);
        tvPatientType = (TextView) view.findViewById(R.id.tvPatientType);
        edtNumberRemedy = (EditText) view.findViewById(R.id.edtNumberRemedy);
        edtAdvice = (EditText) view.findViewById(R.id.edtAdvice);
        edtDate = (EditText) view.findViewById(R.id.edtDate);
        edtTime = (EditText) view.findViewById(R.id.edtTime);
        edtUseDate = (EditText) view.findViewById(R.id.edtUseDate);
        edtUseTime = (EditText) view.findViewById(R.id.edtUseTime);
        edtMainExaminationCode = (EditText) view.findViewById(R.id.edtMainExaminationCode);
        edtMainExaminationNote = (EditText) view.findViewById(R.id.edtMainExaminationNote);
        edtMinorExaminationNote = (EditText) view.findViewById(R.id.edtMinorExaminationNote);
        edtInventoryCode = (EditText) view.findViewById(R.id.edtInventoryCode);
        edtInventoryName = (EditText) view.findViewById(R.id.edtInventoryNote);
        edtOldPrescription = (EditText) view.findViewById(R.id.edtOldPrescription);
        edtSamplePrescription = (EditText) view.findViewById(R.id.edtSamplePrescription);
        btnClearSamplePrescription = (Button) view.findViewById(R.id.btnClearSamplePrescription);
        btnClearOldPrescription = (Button) view.findViewById(R.id.btnClearOldPrescription);
        btnClearICD = (Button) view.findViewById(R.id.btnClearICD);
        btnClearInventory = (Button) view.findViewById(R.id.btnClearInventory);

        edtDate.setText(DateTimeUtils.formatDate(DateTimeUtils.currentDate()));
        edtTime.setText(DateTimeUtils.formatTime(DateTimeUtils.currentDate()));
        edtUseDate.setText(DateTimeUtils.formatDate(DateTimeUtils.currentDate()));
        edtUseTime.setText(DateTimeUtils.formatTime(DateTimeUtils.currentDate()));

        btnClearSamplePrescription.setOnClickListener(this);
        btnClearOldPrescription.setOnClickListener(this);
        edtDate.setOnClickListener(this);
        edtTime.setOnClickListener(this);
        edtUseDate.setOnClickListener(this);
        edtUseTime.setOnClickListener(this);
        edtMainExaminationCode.setOnClickListener(this);
        edtMainExaminationNote.setOnClickListener(this);
        tvTreatmentCode.setOnClickListener(this);
        edtInventoryCode.setOnClickListener(this);
        edtInventoryName.setOnClickListener(this);
        edtOldPrescription.setOnClickListener(this);
        edtSamplePrescription.setOnClickListener(this);
        btnClearICD.setOnClickListener(this);
        btnClearInventory.setOnClickListener(this);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp);
        toolbar.inflateMenu(R.menu.menu_next);
        toolbar.setOnMenuItemClickListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        tvTreatmentCode.setText(treatment.getTreatmentCode() == null ? "" : treatment.getTreatmentCode());
        tvPatientCode.setText(treatment.getPatientCode() == null ? "" : treatment.getPatientCode());
        tvPatientName.setText(treatment.getVirPatientName() == null ? "" : treatment.getVirPatientName());
        tvPatientType.setText(treatmentCommonInfo !=null && treatmentCommonInfo.patientTypeAlter!=null && treatmentCommonInfo.patientTypeAlter.PATIENT_TYPE_NAME != null ? treatmentCommonInfo.patientTypeAlter.PATIENT_TYPE_NAME : "");

        if (oldPrescription != null) {
            edtOldPrescription.setText(oldPrescription.getExpMestCode());
            edtOldPrescription.setPadding(0, 0, context.getResources().getDimensionPixelSize(R.dimen.dimen_large), 0);
            btnClearOldPrescription.setVisibility(View.VISIBLE);
        }
        if (templateDescription != null) {
            edtSamplePrescription.setText(templateDescription.EXP_MEST_TEMPLATE_NAME);
            edtSamplePrescription.setPadding(0, 0, context.getResources().getDimensionPixelSize(R.dimen.dimen_large), 0);
            btnClearSamplePrescription.setVisibility(View.VISIBLE);
        }

        if (icd != null) {
            edtMainExaminationCode.setText(icd.ICD_TEXT);
            edtMainExaminationNote.setText(icd.ICD_MAIN_NAME != null ? icd.ICD_MAIN_NAME : icd.ICD_NAME);
            edtMainExaminationNote.setPadding(0, 0, context.getResources().getDimensionPixelSize(R.dimen.dimen_large), 0);
            edtMainExaminationNote.setFocusableInTouchMode(true);
            edtMainExaminationNote.setFocusable(true);
            edtMinorExaminationNote.setText(icd.ICD_TEXT != null ? icd.ICD_TEXT : "");
            btnClearICD.setVisibility(View.VISIBLE);
        }

        if (medicalStock != null) {
            edtInventoryCode.setText(medicalStock.MEDI_STOCK_CODE);
            edtInventoryName.setText(medicalStock.MEDI_STOCK_NAME);
            edtInventoryName.setPadding(0, 0, context.getResources().getDimensionPixelSize(R.dimen.dimen_large), 0);
            btnClearInventory.setVisibility(View.VISIBLE);
        }
        return view;
    }

    private void bindView() {
        tvTreatmentCode.setText(treatment.getTreatmentCode() == null ? "" : treatment.getTreatmentCode());
        tvPatientCode.setText(treatment.getPatientCode() == null ? "" : treatment.getPatientCode());
        tvPatientName.setText(treatment.getVirPatientName() == null ? "" : treatment.getVirPatientName());
        tvPatientType.setText(treatmentCommonInfo.patientTypeAlter.PATIENT_TYPE_NAME != null ? treatmentCommonInfo.patientTypeAlter.PATIENT_TYPE_NAME : "");
        edtMainExaminationCode.setText(treatmentCommonInfo.ICD_CODE != null ? treatmentCommonInfo.ICD_CODE : "");
        edtMainExaminationNote.setText(treatmentCommonInfo.ICD_MAIN_TEXT == null ? treatmentCommonInfo.ICD_NAME != null ? treatmentCommonInfo.ICD_NAME : "" : treatmentCommonInfo.ICD_MAIN_TEXT);
        edtMinorExaminationNote.setText(treatmentCommonInfo.ICD_TEXT != null ? treatmentCommonInfo.ICD_TEXT : "");
        if (treatmentCommonInfo.ICD_CODE != null) {
            icd = new ICD();
            icd.ID = treatmentCommonInfo.ICD_ID;
            icd.ICD_NAME = treatmentCommonInfo.ICD_NAME;
            icd.ICD_CODE = treatmentCommonInfo.ICD_CODE;
            icd.ICD_TEXT = treatmentCommonInfo.ICD_TEXT != null ? treatmentCommonInfo.ICD_TEXT : "";
            edtMainExaminationNote.setFocusable(true);
            edtMainExaminationNote.setFocusableInTouchMode(true);
            edtMainExaminationNote.setPadding(0, 0, context.getResources().getDimensionPixelSize(R.dimen.dimen_large), 0);
            btnClearICD.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onBackPressed() {
        getActivity().finish();
        return true;
    }

    private ProgressDialog mProgressDialog;

    private void getPatientCommonInfo() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.diagnostic_get_patient_info));
        }
        mProgressDialog.show();

        String param = "{'ApiData':" + treatment.getTreatmentId() + "}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getHisTreatmentCommonInfo(param, new Callback<ResponseObjectModel<TreatmentCommonInfo>>() {
            @Override
            public void success(ResponseObjectModel<TreatmentCommonInfo> responseObjectModel, Response response) {
                if (mProgressDialog != null) mProgressDialog.dismiss();
                treatmentCommonInfo = responseObjectModel.getDataResult();
                bindView();
            }

            @Override
            public void failure(RetrofitError error) {
                if (mProgressDialog != null) mProgressDialog.dismiss();
                Toast.makeText(context, R.string.get_patient_info_fail, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onClick(final View view) {

        switch (view.getId()) {
            case R.id.edtDate: {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(instructionTime);
                android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(context, dateInstructionSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
                dialog.setCancelable(false);
                dialog.show();
            }
            break;
            case R.id.edtUseDate: {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(useTime);
                android.app.DatePickerDialog dialog = new android.app.DatePickerDialog(context, dateUseSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
                dialog.setCancelable(false);
                dialog.show();
            }
            break;
            case R.id.edtTime: {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(instructionTime);
                android.app.TimePickerDialog dialog = new android.app.TimePickerDialog(context, timeInstructionSetListener, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
                dialog.setCancelable(false);
                dialog.show();
            }
            break;
            case R.id.edtUseTime: {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(useTime);
                android.app.TimePickerDialog dialog = new android.app.TimePickerDialog(context, timeUseSetListener, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
                dialog.setCancelable(false);
                dialog.show();
            }

            break;
            case R.id.edtMainExaminationCode: {
                SelectICDDialog dialog = new SelectICDDialog(context);
                dialog.setICDSelectListener(this);
                dialog.show();
            }
            break;
            case R.id.edtMainExaminationNote: {
                if (!edtMainExaminationNote.isFocusable()) {
                    SelectICDDialog dialog = new SelectICDDialog(context);
                    dialog.setICDSelectListener(this);
                    dialog.show();
                } else {
                    edtMainExaminationNote.requestFocus();
                }
            }
            break;
            case R.id.edtInventoryCode:
            case R.id.edtInventoryNote: {
                SelectMedicalStockDialog dialog = new SelectMedicalStockDialog(context, roomId);
                dialog.setMedicalStockSelectListener(this);
                dialog.show();
            }
            break;
            case R.id.edtSamplePrescription: {
                if (oldPrescription != null) {
                    Toast.makeText(context, R.string.specify_medicine_material_choose_source_prescription, Toast.LENGTH_SHORT).show();
                } else {
                    SelectTemplatePrescription dialog = new SelectTemplatePrescription(context);
                    dialog.setTemplatePrescriptionSelectListener(this);
                    dialog.show();
                }
            }
            break;
            case R.id.edtOldPrescription: {
                if (templateDescription != null) {
                    Toast.makeText(context, R.string.specify_medicine_material_choose_source_prescription, Toast.LENGTH_SHORT).show();
                } else {
                    SelectOldPrescription dialog = new SelectOldPrescription(context, treatment.getTreatmentId());
                    dialog.setListener(this);
                    dialog.show();
                }
            }
            break;

            case R.id.btnClearSamplePrescription: {
                templateDescription = null;
                edtSamplePrescription.getText().clear();
                edtSamplePrescription.setPadding(0, 0, 0, 0);
                btnClearSamplePrescription.setVisibility(View.GONE);
            }
            break;
            case R.id.btnClearOldPrescription: {
                oldPrescription = null;
                edtOldPrescription.getText().clear();
                edtOldPrescription.setPadding(0, 0, 0, 0);
                btnClearOldPrescription.setVisibility(View.GONE);
            }
            break;
            case R.id.btnClearICD: {
                icd = null;
                edtMainExaminationCode.setText("");
                edtMainExaminationNote.setText("");
                edtMainExaminationNote.setFocusable(false);
                edtMainExaminationNote.setFocusableInTouchMode(false);
                edtMainExaminationNote.setPadding(0, 0, 0, 0);
                btnClearICD.setVisibility(View.GONE);
            }
            break;
            case R.id.btnClearInventory: {
                medicalStock = null;
                edtInventoryCode.setText("");
                edtInventoryName.setText("");
                edtInventoryName.setPadding(0, 0, 0, 0);
                btnClearInventory.setVisibility(View.GONE);
            }
            break;
        }

    }

    private android.app.DatePickerDialog.OnDateSetListener dateInstructionSetListener = new android.app.DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(instructionTime);
            calendar.set(year, monthOfYear, dayOfMonth);
            instructionTime = calendar.getTime();
            String date = DateTimeUtils.formatDate(instructionTime);
            edtDate.setText(date);
        }
    };

    private android.app.DatePickerDialog.OnDateSetListener dateUseSetListener = new android.app.DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(useTime);
            calendar.set(year, monthOfYear, dayOfMonth);
            useTime = calendar.getTime();
            String date = DateTimeUtils.formatDate(useTime);
            edtUseDate.setText(date);
        }
    };

    private android.app.TimePickerDialog.OnTimeSetListener timeInstructionSetListener = new android.app.TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(instructionTime);
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);
            instructionTime = calendar.getTime();
            String time = DateTimeUtils.formatTime(instructionTime);
            edtTime.setText(time);
        }
    };
    private android.app.TimePickerDialog.OnTimeSetListener timeUseSetListener = new android.app.TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(useTime);
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calendar.set(Calendar.MINUTE, minute);
            useTime = calendar.getTime();
            String time = DateTimeUtils.formatTime(useTime);
            edtUseTime.setText(time);
        }
    };

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (icd == null) {
            edtMainExaminationCode.setError("");
            edtMainExaminationNote.setError("");
            Toast.makeText(context, R.string.diagnostic_material_not_select_icd, Toast.LENGTH_SHORT).show();
            return true;
        }
        if (medicalStock == null) {
            edtInventoryCode.setError("");
            edtInventoryName.setError("");
            Toast.makeText(context, R.string.diagnostic_material_not_select_stock, Toast.LENGTH_SHORT).show();
            return true;
        }
        icd.ICD_MAIN_NAME = edtMainExaminationNote.getText().toString();
        icd.ICD_TEXT = edtMinorExaminationNote.getText().toString();
        long instructionT = instructionTime != null ? DateTimeUtils.formatLongTime(instructionTime) : DateTimeUtils.formatLongCurrentTime();
        long useT = useTime != null ? DateTimeUtils.formatLongTime(useTime) : DateTimeUtils.formatLongCurrentTime();
        Long remedy = edtNumberRemedy.getText().toString().isEmpty() ? 0l : Long.parseLong(edtNumberRemedy.getText().toString());
        String advice = edtAdvice.getText().toString().isEmpty() ? "" : edtAdvice.getText().toString();
        replaceCurrentFragment(MedicineMaterialSpecifyFragment.newInstance(treatmentCommonInfo, roomId, icd, medicalStock, remedy, advice,
                instructionT, useT, oldPrescription != null ? oldPrescription.getExpMestId() : 0l,
                templateDescription != null ? templateDescription.ID : 0l));

        return false;
    }

    @Override
    public void onSelected(ICD item) {
        edtMainExaminationCode.setText(item.ICD_CODE);
        edtMainExaminationNote.setText(item.ICD_NAME);
        edtMainExaminationCode.setError(null);
        edtMainExaminationNote.setError(null);
        edtMainExaminationNote.setFocusable(true);
        edtMainExaminationNote.setFocusableInTouchMode(true);
        edtMainExaminationNote.setPadding(0, 0, context.getResources().getDimensionPixelSize(R.dimen.dimen_large), 0);
        btnClearICD.setVisibility(View.VISIBLE);
        icd = item;
    }


    @Override
    public void onSelected(MedicalStock item) {
        edtInventoryCode.setText(item.MEDI_STOCK_CODE);
        edtInventoryName.setText(item.MEDI_STOCK_NAME);
        edtInventoryCode.setError(null);
        edtInventoryName.setError(null);
        edtInventoryName.setPadding(0, 0, context.getResources().getDimensionPixelSize(R.dimen.dimen_large), 0);
        btnClearInventory.setVisibility(View.VISIBLE);
        medicalStock = item;
    }

    @Override
    public void onSelected(Prescription prescription) {
        oldPrescription = prescription;
        edtOldPrescription.setText(prescription.getExpMestCode());
        edtOldPrescription.setPadding(0, 0, context.getResources().getDimensionPixelSize(R.dimen.dimen_large), 0);
        btnClearOldPrescription.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSelected(ExpMestTemplate item) {
        templateDescription = item;
        edtSamplePrescription.setText(item.EXP_MEST_TEMPLATE_NAME);
        edtSamplePrescription.setPadding(0, 0, context.getResources().getDimensionPixelSize(R.dimen.dimen_large), 0);
        btnClearSamplePrescription.setVisibility(View.VISIBLE);
    }
}
