package com.staarlab.idoctor.ui.activity;

import android.os.Bundle;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.Treatment;
import com.staarlab.idoctor.ui.fragment.TechServiceDiagnosticFragment;

public class TechServiceActivity extends BaseActivity {

    private Treatment treatment;
    private long roomId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tech_service);
        treatment = getIntent().getParcelableExtra("TREATMENT");
        roomId = getIntent().getLongExtra("roomId", 0l);
        replaceCurrentFragment(TechServiceDiagnosticFragment.newInstance(treatment, roomId));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
