package com.staarlab.idoctor.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.Prescription;
import com.staarlab.idoctor.util.CommonUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Lucero
 * Created: 12/29/16.
 */

public class PrescriptionListAdapter extends BaseAdapter {
    static class ViewHolder {
        TextView txtReqCode;
        TextView txtReqTypeName;
        TextView txtReqLoginName;
        TextView txtReqRoomName;
        TextView txtExeRoomName;
        TextView txtInstructionTime;
        TextView txtUseTime;
    }

    private List<Prescription> dataList;
    private Context mContext;

    public PrescriptionListAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return dataList == null ? 0 : dataList.size();
    }

    @Override
    public Prescription getItem(int i) {
        return dataList.get(i);
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        Prescription p = getItem(i);
        ViewHolder vh;
        if (convertView == null) {
            vh = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.adapter_prescription_list_item, viewGroup, false);
            vh.txtReqCode = (TextView) convertView.findViewById(R.id.txtC1);
            vh.txtReqTypeName = (TextView) convertView.findViewById(R.id.txtReqTypeName);
            vh.txtReqLoginName = (TextView) convertView.findViewById(R.id.txtReqLoginName);
            vh.txtReqRoomName = (TextView) convertView.findViewById(R.id.txtC5);
            vh.txtExeRoomName = (TextView)convertView.findViewById(R.id.txtExeRoomName);
            vh.txtInstructionTime = (TextView) convertView.findViewById(R.id.txtInTime);
            vh.txtUseTime = (TextView) convertView.findViewById(R.id.txtUseTime);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        vh.txtReqCode.setText(p.getExpMestCode());
        vh.txtReqTypeName.setText(p.getExpMestTypeName());
        vh.txtReqLoginName.setText(p.getRequestLoginName() + " - " + p.getRequestUserName());
        vh.txtReqRoomName.setText(p.getRequestRoomName());
        vh.txtExeRoomName.setText(p.getExecuteRoomName());
        vh.txtInstructionTime.setText(CommonUtil.hisString2DateTimeAsString(p.getIntructionTime()));
        vh.txtUseTime.setText(CommonUtil.hisString2DateTimeAsString(p.getUseTime()));
        return convertView;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void setDataList(List<Prescription> dataList) {
        if (this.dataList == null) {
            this.dataList = new ArrayList<>();
        }
        this.dataList.clear();
        this.dataList.addAll(dataList);
        notifyDataSetChanged();
    }
}
