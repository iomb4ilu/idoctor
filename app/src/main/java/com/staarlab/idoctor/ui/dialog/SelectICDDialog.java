package com.staarlab.idoctor.ui.dialog;


import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.ICD;
import com.staarlab.idoctor.helper.layout.DividerItemDecoration;
import com.staarlab.idoctor.helper.layout.HeaderRecyclerDividerItemDecorator;
import com.staarlab.idoctor.ui.activity.BaseActivity;
import com.staarlab.idoctor.util.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PhamHung on 12/27/16.
 */

public class SelectICDDialog implements View.OnClickListener {
    private static final String TAG = SelectICDDialog.class.getName();
    protected Context context;
    private Dialog dialog;

    private TextView txtTitle;
    private RecyclerView rvICD;
    private ImageButton imgSearch;
    private ImageButton imgClose;
    private EditText edtQuery;

    private LayoutInflater layoutInflater;
    private RecyclerView.LayoutManager layoutManager;
    private List<ICD> items = new ArrayList<>();
    private ICDAdapter adapter;

    private String query;
    private OnICDSelectListener listener;

    public SelectICDDialog(Context ctx) {
        this.context = ctx;
        this.layoutManager = new LinearLayoutManager(context);
        layoutInflater = LayoutInflater.from(context);
        this.dialog = new Dialog(this.context);
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.dialog.setContentView(R.layout.dialog_select);
        this.dialog.setCancelable(false);

        initViews();
    }

    private void initViews() {
        query = "";
        imgSearch = (ImageButton) dialog.findViewById(R.id.imgSearch);
        imgClose = (ImageButton) dialog.findViewById(R.id.btnClose);
        edtQuery = (EditText) dialog.findViewById(R.id.edtQuery);
        txtTitle = (TextView) dialog.findViewById(R.id.dialog_title);
        txtTitle.setText(R.string.select_icd);
        edtQuery.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtQuery.getWindowToken(), 0);
                if (!query.equalsIgnoreCase(edtQuery.getText().toString())) {
                    query = StringUtils.getEngStringFromUnicodeString(edtQuery.getText().toString().toLowerCase().trim());
                    doQueryDatabase(query);
                }
                return true;
            }
        });
        rvICD = (RecyclerView) dialog.findViewById(R.id.recycler_view);
        rvICD.setHasFixedSize(true);
        rvICD.setLayoutManager(layoutManager);
        rvICD.addItemDecoration(
                new HeaderRecyclerDividerItemDecorator(
                        context, DividerItemDecoration.VERTICAL_LIST, false, false,
                        R.drawable.divider_0));

        adapter = new ICDAdapter();
        rvICD.setAdapter(adapter);

        imgClose.setOnClickListener(this);
        imgSearch.setOnClickListener(this);
        new LoadICD().execute(query);

    }

    public void setICDSelectListener(OnICDSelectListener l) {
        this.listener = l;
    }

    public void show() {
        dialog.show();
    }

    public void dismiss() {
        dialog.dismiss();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnClose:
                dismiss();
                break;
            case R.id.imgSearch:
                InputMethodManager imm = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtQuery.getWindowToken(), 0);
                if (!query.equalsIgnoreCase(edtQuery.getText().toString())) {
                    query = StringUtils.getEngStringFromUnicodeString(edtQuery.getText().toString().toLowerCase().trim());
                    doQueryDatabase(query);
                }
                break;
        }
    }

    private void doQueryDatabase(String query) {
        new LoadICD().execute(query);
    }

    private View.OnClickListener onSelectItem = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getTag() instanceof ICD) {
                ICD item = (ICD) v.getTag();
                listener.onSelected(item);
            }
            dismiss();
        }
    };

    // LOAD DATA LOCAL
    class LoadICD extends AsyncTask<String, Void, List<ICD>> {

        @Override
        protected List<ICD> doInBackground(String... params) {
            String q = params[0];
            Dao<ICD, Integer> icdDao = ((BaseActivity) context).getHelper().getIcdDao();
            QueryBuilder<ICD, Integer> qb = icdDao.queryBuilder();
//            qb.orderBy(ICD.FIELD_NAME_ICD_CODE, true);
            qb.orderByRaw(ICD.FIELD_NAME_ICD_CODE + " COLLATE NOCASE");
            if (!((BaseActivity) context).getHelper().isOpen()) {
                Toast.makeText(context, "Database is closed", Toast.LENGTH_SHORT).show();
                return new ArrayList<>();
            }
            try {
                if (q.isEmpty()) {
                    PreparedQuery<ICD> preparedQuery = qb.prepare();
                    return icdDao.query(preparedQuery);
                } else {
                    qb.where().like(ICD.FIELD_NAME_ICD_SEARCH, "%" + q + "%");
                    PreparedQuery<ICD> preparedQuery = qb.prepare();
                    return icdDao.query(preparedQuery);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new ArrayList<>();
        }

        @Override
        protected void onPostExecute(List<ICD> icdLst) {
            super.onPostExecute(icdLst);
            items = icdLst;
            adapter.notifyDataSetChanged();
        }
    }

    private class ICDAdapter extends RecyclerView.Adapter {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = layoutInflater.inflate(R.layout.adapter_select_icd, parent, false);
            return new ICDViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ICDViewHolder vh = (ICDViewHolder) holder;
            ICD item = items.get(position);
            vh.tvName.setText(item.ICD_NAME);
            vh.tvCode.setText(item.ICD_CODE);
            vh.itemView.setTag(item);
            vh.itemView.setOnClickListener(onSelectItem);
        }

        @Override
        public int getItemCount() {
            if (items != null) {
                return items.size();
            }
            return 0;
        }
    }

    private class ICDViewHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        TextView tvCode;

        public ICDViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvCode = (TextView) itemView.findViewById(R.id.tvCode);

        }
    }

    public interface OnICDSelectListener {
        void onSelected(ICD item);
    }

}
