package com.staarlab.idoctor.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.query.In;
import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.PatientType;
import com.staarlab.idoctor.entity.PatientTypeAllow;
import com.staarlab.idoctor.entity.ServicePATY;
import com.staarlab.idoctor.ui.activity.BaseActivity;
import com.staarlab.idoctor.ui.fragment.TechServiceSpecifyFragment;
import com.staarlab.idoctor.util.HardCodeUtils;
import com.staarlab.idoctor.util.NumberFormatUtils;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.staarlab.idoctor.entity.ServicePATY.FIELD_NAME_SERVICE_PATY_PATIENT_TYPE_ID;
import static com.staarlab.idoctor.entity.ServicePATY.FIELD_NAME_SERVICE_PATY_SERVICE_ID;

/**
 * Created by PHAMHUNG on 1/1/2017.
 */

public class SelectServiceDialog implements View.OnClickListener, AdapterView.OnItemSelectedListener, CheckBox.OnCheckedChangeListener {
    private static final String TAG = SelectServiceDialog.class.getName();
    private OnNumberEnteredListener listener;
    protected Context context;
    private Dialog dialog;
    private TechServiceSpecifyFragment.ServiceCompound serviceCompound;
    private PatientType selectedPatientType;
    private BigDecimal priceUnit = BigDecimal.ZERO;
    private BigDecimal priceUnitBKXH = null;
    private boolean isExpend;
    private boolean isWithBKXH;

    private Button btnOK;
    private Button btnCancel;
    private ImageButton imgButtonClose;
    private TextView tvWarningPlacedService;
    private Spinner spPatientType;
    private TextView txtServiceCode;
    private TextView txtServiceName;
    private EditText edtNumber;
    private TextView txtPriceUnit;
    private CheckBox chkExpend;
    private CheckBox chkWithBHXH;
    private int number = 1;
    private long defaultPatientTypeId = -1;
    private long inTime;
    private long applyTime;
    private List<Long> lstPlacedServiceToday = new ArrayList<>();

    List<PatientType> lstPatientType = new ArrayList<>();
    PatientTypeAdapter adapter;
    LayoutInflater layoutInflater;

    public SelectServiceDialog(Context ctx, TechServiceSpecifyFragment.ServiceCompound service, long defaultPatientTypeId, List<Long> lstPlacedServiceToday, long inTime, long applyTime) {
        this.context = ctx;
        this.serviceCompound = service;
        this.defaultPatientTypeId = defaultPatientTypeId;
        this.lstPlacedServiceToday = lstPlacedServiceToday;
        this.inTime = inTime;
        this.applyTime = applyTime;

        this.dialog = new Dialog(this.context);
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.dialog.setContentView(R.layout.dialog_select_service);
        this.dialog.setCancelable(false);
        this.layoutInflater = LayoutInflater.from(context);

        initViews();
    }

    public SelectServiceDialog(Context ctx, TechServiceSpecifyFragment.TechServiceHolder item, List<Long> lstPlacedServiceToday, long inTime, long applyTime) {
        this.context = ctx;
        this.serviceCompound = item.service;
        this.selectedPatientType = item.patientType;
        this.defaultPatientTypeId = item.patientType.ID;
        this.priceUnit = item.priceUnit;
        this.isExpend = item.isExpend;
        this.isWithBKXH = item.isWithBHXH;
        this.number = item.number;
        this.lstPlacedServiceToday = lstPlacedServiceToday;
        this.inTime = inTime;
        this.applyTime = applyTime;

        this.dialog = new Dialog(this.context);
        this.dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.dialog.setContentView(R.layout.dialog_select_service);
        this.dialog.setCancelable(false);
        this.layoutInflater = LayoutInflater.from(context);

        initViews();
    }


    public void show() {
        dialog.show();
    }

    private void initViews() {
        btnOK = (Button) dialog.findViewById(R.id.btnOK);
        btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        txtServiceCode = (TextView) dialog.findViewById(R.id.txtServiceCode);
        txtServiceName = (TextView) dialog.findViewById(R.id.txtServiceName);
        edtNumber = (EditText) dialog.findViewById(R.id.edtNumber);
        txtPriceUnit = (TextView) dialog.findViewById(R.id.txtPriceUnit);
        chkExpend = (CheckBox) dialog.findViewById(R.id.chkExpend);
        chkWithBHXH = (CheckBox) dialog.findViewById(R.id.chkWithBHXH);
        imgButtonClose = (ImageButton) dialog.findViewById(R.id.btnClose);
        spPatientType = (Spinner) dialog.findViewById(R.id.spPatientType);
        tvWarningPlacedService = (TextView) dialog.findViewById(R.id.tvWarningPlacedService);

        btnOK.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        imgButtonClose.setOnClickListener(this);
        spPatientType.setOnItemSelectedListener(this);
        chkExpend.setOnCheckedChangeListener(this);
        chkWithBHXH.setOnCheckedChangeListener(this);

        bindView();

    }

    private void bindView() {
        txtServiceName.setText(serviceCompound.serviceName);
        txtServiceCode.setText(serviceCompound.serviceCode);
        edtNumber.setText(String.valueOf(number));
        chkExpend.setChecked(isExpend);
        chkWithBHXH.setChecked(isWithBKXH);

        changePriceUnit(isExpend);
        if (lstPlacedServiceToday.contains(serviceCompound.id)) {
            tvWarningPlacedService.setVisibility(View.VISIBLE);
        } else {
            tvWarningPlacedService.setVisibility(View.GONE);
        }
        ((BaseActivity) context).closeSoftKey();

        new LoadPatientType().execute();
    }

    private void changePriceUnit(boolean isExpend) {
        if (!isExpend) {
            if (priceUnit.compareTo(BigDecimal.ZERO) == 0) {
                txtPriceUnit.setText(R.string.specify_service_zero_price);
                txtPriceUnit.setTextColor(ContextCompat.getColor(context, R.color.red));
            } else {
                txtPriceUnit.setText(NumberFormatUtils.formatMoney(priceUnit));
                txtPriceUnit.setTextColor(ContextCompat.getColor(context, R.color.Black87));
            }
        } else {
            txtPriceUnit.setTextColor(ContextCompat.getColor(context, R.color.Black87));
            txtPriceUnit.setText("0");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOK:
                sendData();
                break;
            case R.id.btnCancel:
                dialog.dismiss();
                break;
            case R.id.btnClose:
                dialog.dismiss();
                break;
        }
    }

    private void sendData() {
        try {
            int number = Integer.parseInt(edtNumber.getText().toString());
            if (number < 1) {
                Toast.makeText(context, R.string.warning_tech_medicine_material_dialog_number, Toast.LENGTH_SHORT).show();
                return;
            }
            boolean isExpend = chkExpend.isChecked();
            boolean isWithBHYT = chkWithBHXH.isChecked();
            if (isWithBHYT && priceUnitBKXH != null && priceUnitBKXH.compareTo(priceUnit) > 0) {
                Toast.makeText(context, R.string.warning_tech_medicine_material_dialog_not_allow_with_BHYT, Toast.LENGTH_SHORT).show();
                return;
            }
            listener.onNumberEntered(number, serviceCompound, selectedPatientType, isExpend, isWithBHYT, priceUnit);
            dialog.dismiss();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        PatientType selectedPatientType = lstPatientType.get(position);
        this.selectedPatientType = selectedPatientType;
        new CalculatePrice().execute(selectedPatientType);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void setOnEnterNumberListener(OnNumberEnteredListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.chkExpend) {
            isExpend = isChecked;
            changePriceUnit(isExpend);
        } else if (buttonView.getId() == R.id.chkWithBHXH) {
            if (priceUnitBKXH == null) {
                new CalculatePriceForBHXH().execute();
            }
        }
    }

    // LOAD DATA LOCAL SQLITE
    class LoadPatientType extends AsyncTask<Void, Void, List<PatientType>> {
        long parentTypeId;

        @Override
        protected List<PatientType> doInBackground(Void... params) {
            Dao<PatientType, Integer> patientTypeDao = ((BaseActivity) context).getHelper().getPatientTypeDao();
            List<PatientType> lstPatientTypes = new ArrayList<>();
            QueryBuilder<PatientType, Integer> qb = patientTypeDao.queryBuilder();
            try {
                // Đối tượng thanh toán phải có giá -- Service PATY
                Dao<ServicePATY, Integer> servicePATies = ((BaseActivity) context).getHelper().getServicePATYDao();
                QueryBuilder<ServicePATY, Integer> qbServicePATY = servicePATies.queryBuilder();
                qbServicePATY.selectColumns(ServicePATY.FIELD_NAME_SERVICE_PATY_PATIENT_TYPE_ID);
                qbServicePATY.where().eq(ServicePATY.FIELD_NAME_SERVICE_PATY_SERVICE_ID, serviceCompound.id);
                List<ServicePATY> patientTypes = servicePATies.query(qbServicePATY.prepare());

                // Đối tượng thanh toán phải được cho phép - Patient Type Allow
                Dao<PatientTypeAllow, Integer> patientTypeAllows = ((BaseActivity) context).getHelper().getPatientTypeAllow();
                QueryBuilder<PatientTypeAllow, Integer> qbPatientTypeAllow = patientTypeAllows.queryBuilder();
                qbPatientTypeAllow.selectColumns(PatientTypeAllow.FIELD_NAME_PATIENT_TYPE_ALLOW_ID);
                qbPatientTypeAllow.where().eq(PatientTypeAllow.FIELD_NAME_PATIENT_TYPE_ID, defaultPatientTypeId);
                List<PatientTypeAllow> lstPatientTypeAllow = patientTypeAllows.query(qbPatientTypeAllow.prepare());
                // Intersect 2 list
                Set<Long> patientTypeIds = new HashSet<>();
                List<Long> patientTypeIdFromServicePATY = new ArrayList<>();
                List<Long> patientTypeIdFromPatientTypeAllow = new ArrayList<>();
                for (ServicePATY sp : patientTypes) {
                    patientTypeIdFromServicePATY.add(sp.PATIENT_TYPE_ID);
                }
                for (PatientTypeAllow pta : lstPatientTypeAllow) {
                    patientTypeIdFromPatientTypeAllow.add(pta.PATIENT_TYPE_ALLOW_ID);
                }
                for (Long l : patientTypeIdFromServicePATY) {
                    if (patientTypeIdFromPatientTypeAllow.contains(l)) {
                        patientTypeIds.add(l);
                    }
                }
                for (Long l : patientTypeIdFromPatientTypeAllow) {
                    if (patientTypeIdFromServicePATY.contains(l)) {
                        patientTypeIds.add(l);
                    }
                }
                if (defaultPatientTypeId != -1 && !patientTypeIds.contains(defaultPatientTypeId)) {
                    patientTypeIds.add(defaultPatientTypeId);
                }

                qb.where().in(PatientType.FIELD_NAME_ID, patientTypeIds);
                qb.orderByRaw(PatientType.FIELD_NAME_PATIENT_TYPE_NAME + " COLLATE NOCASE");
                PreparedQuery<PatientType> preparedQuery = qb.prepare();
                lstPatientTypes = patientTypeDao.query(preparedQuery);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return lstPatientTypes;
        }

        @Override
        protected void onPostExecute(List<PatientType> patientTypes) {
            super.onPostExecute(patientTypes);
            lstPatientType.clear();
            lstPatientType.addAll(patientTypes);
            adapter = new PatientTypeAdapter(context, R.layout.adapter_patient_type_spinner, R.id.txtPatientType);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spPatientType.setAdapter(adapter);
            parentTypeId = selectedPatientType != null ? selectedPatientType.ID : defaultPatientTypeId;
            int selectedPosition = 0;
            for (; selectedPosition < lstPatientType.size(); selectedPosition++) {
                if (lstPatientType.get(selectedPosition).ID == parentTypeId) {
                    spPatientType.setSelection(selectedPosition);
                    break;
                }
            }


        }
    }

    class CalculatePrice extends AsyncTask<PatientType, Void, Double> {

        @Override
        protected Double doInBackground(PatientType... params) {
            PatientType patientType = params[0];
            List<ServicePATY> lstServicePATY = new ArrayList<>();
            Dao<ServicePATY, Integer> servicePATHDao = ((BaseActivity) context).getHelper().getServicePATYDao();
            QueryBuilder<ServicePATY, Integer> qb = servicePATHDao.queryBuilder();
            try {
                qb.where().eq(FIELD_NAME_SERVICE_PATY_SERVICE_ID, serviceCompound.id).and().eq(FIELD_NAME_SERVICE_PATY_PATIENT_TYPE_ID, patientType.ID);
                PreparedQuery<ServicePATY> preparedQuery = qb.prepare();
                lstServicePATY = servicePATHDao.query(preparedQuery);
                if (lstServicePATY.size() == 0) return 0d;
                ServicePATY selectedPrice = null;
                for (ServicePATY pty : lstServicePATY) {
                    pty.FROM_TIME = pty.FROM_TIME != null ? pty.FROM_TIME : Long.MIN_VALUE;
                    pty.TO_TIME = pty.TO_TIME != null ? pty.TO_TIME : Long.MAX_VALUE;
                    pty.TREATMENT_FROM_TIME = pty.TREATMENT_FROM_TIME != null ? pty.TREATMENT_FROM_TIME : Long.MIN_VALUE;
                    pty.TREATMENT_TO_TIME = pty.TREATMENT_TO_TIME != null ? pty.TREATMENT_TO_TIME : Long.MAX_VALUE;
                    if ((pty.FROM_TIME < applyTime && applyTime < pty.TO_TIME) ||
                            (pty.TREATMENT_FROM_TIME < inTime && inTime < pty.TREATMENT_TO_TIME)) {
                        if (selectedPrice == null) {
                            selectedPrice = pty;
                        } else {
                            selectedPrice.PRIORITY = selectedPrice.PRIORITY != null ? selectedPrice.PRIORITY : 0;
                            pty.PRIORITY = pty.PRIORITY != null ? pty.PRIORITY : 0;
                            if (pty.PRIORITY > selectedPrice.PRIORITY) {
                                selectedPrice = pty;
                            } else if (pty.PRIORITY.equals(selectedPrice.PRIORITY)) {
                                if (pty.MODIFY_TIME > selectedPrice.MODIFY_TIME) {
                                    selectedPrice = pty;
                                }
                            }
                        }
                    }
                }
                return selectedPrice == null ? 0d : selectedPrice.PRICE;
            } catch (SQLException e) {
                e.printStackTrace();
                return 0d;
            }
        }

        @Override
        protected void onPostExecute(Double aDouble) {
            super.onPostExecute(aDouble);
            priceUnit = BigDecimal.valueOf(aDouble);
            changePriceUnit(isExpend);
        }
    }

    class CalculatePriceForBHXH extends AsyncTask<Void, Void, Double> {

        @Override
        protected Double doInBackground(Void... params) {
            List<ServicePATY> lstServicePATY = new ArrayList<>();
            Dao<ServicePATY, Integer> servicePATHDao = ((BaseActivity) context).getHelper().getServicePATYDao();
            QueryBuilder<ServicePATY, Integer> qb = servicePATHDao.queryBuilder();
            try {
                qb.where().eq(FIELD_NAME_SERVICE_PATY_SERVICE_ID, serviceCompound.id).and().eq(FIELD_NAME_SERVICE_PATY_PATIENT_TYPE_ID, HardCodeUtils.PATIENT_TYPE_ID_BHXH);
                PreparedQuery<ServicePATY> preparedQuery = qb.prepare();
                lstServicePATY = servicePATHDao.query(preparedQuery);
                if (lstServicePATY.size() == 0) return 0d;
                ServicePATY selectedPrice = null;
                for (ServicePATY pty : lstServicePATY) {
                    pty.FROM_TIME = pty.FROM_TIME != null ? pty.FROM_TIME : Long.MIN_VALUE;
                    pty.TO_TIME = pty.TO_TIME != null ? pty.TO_TIME : Long.MAX_VALUE;
                    pty.TREATMENT_FROM_TIME = pty.TREATMENT_FROM_TIME != null ? pty.TREATMENT_FROM_TIME : Long.MIN_VALUE;
                    pty.TREATMENT_TO_TIME = pty.TREATMENT_TO_TIME != null ? pty.TREATMENT_TO_TIME : Long.MAX_VALUE;
                    if ((pty.FROM_TIME < applyTime && applyTime < pty.TO_TIME) ||
                            (pty.TREATMENT_FROM_TIME < inTime && inTime < pty.TREATMENT_TO_TIME)) {
                        if (selectedPrice == null) {
                            selectedPrice = pty;
                        } else {
                            selectedPrice.PRIORITY = selectedPrice.PRIORITY != null ? selectedPrice.PRIORITY : 0;
                            pty.PRIORITY = pty.PRIORITY != null ? pty.PRIORITY : 0;
                            if (pty.PRIORITY > selectedPrice.PRIORITY) {
                                selectedPrice = pty;
                            } else if (pty.PRIORITY.equals(selectedPrice.PRIORITY)) {
                                if (pty.MODIFY_TIME > selectedPrice.MODIFY_TIME) {
                                    selectedPrice = pty;
                                }
                            }
                        }
                    }
                }
                return selectedPrice == null ? 0d : selectedPrice.PRICE;
            } catch (SQLException e) {
                e.printStackTrace();
                return 0d;
            }
        }

        @Override
        protected void onPostExecute(Double aDouble) {
            super.onPostExecute(aDouble);
            priceUnitBKXH = BigDecimal.valueOf(aDouble);
        }
    }

    class PatientTypeAdapter extends ArrayAdapter<PatientType> {

        public PatientTypeAdapter(Context context, int resource, int textViewResourceId) {
            super(context, resource, textViewResourceId);
        }

        @Override
        public int getCount() {
            return lstPatientType == null ? 0 : lstPatientType.size();
        }

        @Nullable
        @Override
        public PatientType getItem(int position) {
            return lstPatientType.get(position);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getView(position, convertView, parent);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            PatientType pt = lstPatientType.get(position);
            ViewHolder viewHolder;
            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = layoutInflater.inflate(R.layout.adapter_patient_type_spinner, parent, false);
                viewHolder.txtName = (TextView) convertView.findViewById(R.id.txtPatientType);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.txtName.setText(pt.PATIENT_TYPE_NAME);
            return convertView;
        }
    }

    private static class ViewHolder {
        TextView txtName;
    }

    public interface OnNumberEnteredListener {
        void onNumberEntered(int number, TechServiceSpecifyFragment.ServiceCompound serviceCompound, PatientType patientType, boolean isExpend, boolean isWithBHYT, BigDecimal priceUnit);
    }
}
