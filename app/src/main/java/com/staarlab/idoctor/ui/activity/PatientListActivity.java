package com.staarlab.idoctor.ui.activity;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;

import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.BedRoom;
import com.staarlab.idoctor.entity.Room;
import com.staarlab.idoctor.entity.Treatment;
import com.staarlab.idoctor.helper.layout.DividerItemDecoration;
import com.staarlab.idoctor.helper.layout.GeneralSwipeRefreshLayout;
import com.staarlab.idoctor.helper.layout.HeaderRecyclerDividerItemDecorator;
import com.staarlab.idoctor.helper.share.MBSSharePreference;
import com.staarlab.idoctor.model.ResponseArrayModel;
import com.staarlab.idoctor.ui.adapter.PatientRvAdapter;
import com.staarlab.idoctor.webservice.HISService;
import com.staarlab.idoctor.webservice.WSFactory;

import java.util.ArrayList;
import java.util.concurrent.RunnableFuture;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PatientListActivity extends BaseActivity implements GeneralSwipeRefreshLayout.OnRefreshListener {
    private RecyclerView mRvPatient;
    private GeneralSwipeRefreshLayout generalSwipeRefreshLayout;
    private PatientRvAdapter mAdapter;
    private Room room;
    private HISService hisService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_list);
        if (getIntent() != null) {
            room = (Room) getIntent().getSerializableExtra("room");
            setTitle(room.getRoomName());
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        generalSwipeRefreshLayout = (GeneralSwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        mRvPatient = (RecyclerView) findViewById(R.id.rvPatient);
        mAdapter = new PatientRvAdapter(this, room.getRoomId());
        mRvPatient.setLayoutManager(new LinearLayoutManager(this));
        mRvPatient.addItemDecoration(new HeaderRecyclerDividerItemDecorator(
                this, DividerItemDecoration.VERTICAL_LIST, false, false,
                R.drawable.divider_72));
        mRvPatient.setAdapter(mAdapter);

        hisService = WSFactory.createHisService(MBSSharePreference.get().getTokenCode());
        generalSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                generalSwipeRefreshLayout.setRefreshing(false);
                generalSwipeRefreshLayout.setOnRefreshListener(PatientListActivity.this);
                checkIfHaveData();
            }
        });

    }

    private void checkIfHaveData() {
        generalSwipeRefreshLayout.setRefreshing(true);
        loadData();
    }

    private void loadData() {
        String param = "{'ApiData':{'ROOM_ID':'" + room.getRoomId() + "'}}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getBedRoomByRoomId(param, new Callback<ResponseArrayModel<BedRoom>>() {
            @Override
            public void success(ResponseArrayModel<BedRoom> bedRoomResponseArrayModel, Response response) {
                if (bedRoomResponseArrayModel.getDataList() == null || bedRoomResponseArrayModel.getDataList().size() <= 0)
                    return;
                BedRoom bedRoom = bedRoomResponseArrayModel.getDataList().get(0);
                loadTreatment(bedRoom.getId());
            }

            @Override
            public void failure(RetrofitError error) {
                mAdapter.refresh(new ArrayList<Treatment>(), true);
                generalSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(PatientListActivity.this, R.string.patient_list_load_fail, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadTreatment(long bedRoomId) {
        String param = "{'ApiData':" + bedRoomId + "}";
        param = Base64.encodeToString(param.getBytes(), Base64.NO_WRAP);
        hisService.getTreatmentByBedRoom(param, new Callback<ResponseArrayModel<Treatment>>() {
            @Override
            public void success(ResponseArrayModel<Treatment> treatmentResponseArrayModel, Response response) {
                mAdapter.refresh(treatmentResponseArrayModel.getDataList(), true);
                if (mAdapter.getItemCount() > 0) {
                    setTitle(room.getRoomName() + " (" + mAdapter.getItemCount() + ")");
                }
                generalSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void failure(RetrofitError error) {
                mAdapter.refresh(new ArrayList<Treatment>(), true);
                generalSwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(PatientListActivity.this, R.string.patient_list_load_fail, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        checkIfHaveData();
    }
}
