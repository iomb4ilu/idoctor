package com.staarlab.idoctor.helper.share;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.staarlab.idoctor.MBSApplication;
import com.staarlab.idoctor.util.AppConst;
import com.staarlab.idoctor.webservice.WSFactory;

/**
 * Created by PhamHung on 12/22/16.
 */

public class MBSSharePreference {

    private static MBSSharePreference instance;
    private Context context;

    public synchronized static MBSSharePreference get() {
        if (instance == null) {
            instance = new MBSSharePreference(MBSApplication.get());
        }
        return instance;
    }

    private MBSSharePreference(Context context) {
        this.context = context;
    }

    private SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(AppConst.APP_PREF_NAME, Context.MODE_PRIVATE);
    }

    public void putLongValue(String key, long n) {
        SharedPreferences pref = getSharedPreferences();
        Editor editor = pref.edit();
        editor.putLong(key, n);
        editor.apply();
    }

    public long getLongValue(String key) {
        SharedPreferences pref = getSharedPreferences();
        return pref.getLong(key, 0);
    }

    public void putIntValue(String key, int n) {
        SharedPreferences pref = getSharedPreferences();
        Editor editor = pref.edit();
        editor.putInt(key, n);
        editor.apply();
    }

    public int getIntValue(String key, int intDefault) {
        SharedPreferences pref = getSharedPreferences();
        return pref.getInt(key, intDefault);
    }

    public void putStringValue(String key, String s) {
        SharedPreferences pref = getSharedPreferences();
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, s);
        editor.apply();
    }

    public String getStringValue(String key) {
        SharedPreferences pref = getSharedPreferences();
        return pref.getString(key, "");
    }

    public String getStringValue(String key, String defaultValue) {
        SharedPreferences pref = getSharedPreferences();
        return pref.getString(key, defaultValue);
    }

    public void putBooleanValue(String key, Boolean b) {
        SharedPreferences pref = getSharedPreferences();
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, b);
        editor.apply();
    }

    public boolean getBooleanValue(String key) {
        SharedPreferences pref = getSharedPreferences();
        return pref.getBoolean(key, false);
    }

    public void putFloatValue(String key, float f) {
        SharedPreferences pref = getSharedPreferences();
        Editor editor = pref.edit();
        editor.putFloat(key, f);
        editor.apply();
    }

    public float getFloatValue(String key) {
        SharedPreferences pref = getSharedPreferences();
        return pref.getFloat(key, 0.0f);
    }

    public boolean remove(String key) {
        SharedPreferences preferences = getSharedPreferences();

        Editor editor = preferences.edit();
        editor.remove(key);
        return editor.commit();
    }

    public void clear() {
        SharedPreferences pref = getSharedPreferences();
        Editor editor = pref.edit();
        editor.clear();
        editor.apply();
    }

    //========== Shortcut method ======

    public void putUserLogin(String str) {
        putStringValue(AppConst.PREF_KEY_LOGIN_NAME, str);
    }

    public void putUserName(String str) {
        putStringValue(AppConst.PREF_KEY_USER_NAME, str);
    }

    public void putTokenCode(String str) {
        putStringValue(AppConst.PREF_KEY_TOKEN_CODE, str);
    }

    public void putRenewToken(String str) {
        putStringValue(AppConst.PREF_KEY_RENEW_CODE, str);
    }

    public void putAppCode(String str) {
        putStringValue(AppConst.PREF_KEY_APP_CODE, str);
    }

    public void putLastUpdate(String str) {
        putStringValue(AppConst.PREF_KEY_LAST_UPDATE, str);
    }

    public void putServerAuthentication(String str) {
        putStringValue(AppConst.PREF_SERVER_AUTHENTICATION, str);
    }

    public void putServerAPI(String str) {
        putStringValue(AppConst.PREF_SERVER_API, str);
    }

    public String getUserLogin() {
        return getStringValue(AppConst.PREF_KEY_LOGIN_NAME, "");
    }

    public String getUserName() {
        return getStringValue(AppConst.PREF_KEY_USER_NAME);
    }

    public String getTokenCode() {
        return getStringValue(AppConst.PREF_KEY_TOKEN_CODE, "");
    }

    public String getRenewCode() {
        return getStringValue(AppConst.PREF_KEY_RENEW_CODE);
    }

    public String getLastUpdate() {
        return getStringValue(AppConst.PREF_KEY_LAST_UPDATE);
    }

    public String getAppCode() {
        return getStringValue(AppConst.PREF_KEY_APP_CODE);
    }

    public String getServerAuthentication() {
        return getStringValue(AppConst.PREF_SERVER_AUTHENTICATION, WSFactory.WEBSERVICE_AUTH_URL);
    }

    public String getServerAPI() {
        return getStringValue(AppConst.PREF_SERVER_API, WSFactory.WEBSERVICE_BUSINESS_URL);
    }

    public void putMMIntervalUpdate(int i) {
        putIntValue(AppConst.PREF_KEY_MEDICINE_MATERIAL_INTERVAL_UPDATE, i);
    }

    public int getMMIntervalUpdate() {
        return getIntValue(AppConst.PREF_KEY_MEDICINE_MATERIAL_INTERVAL_UPDATE, 15);
    }

}
