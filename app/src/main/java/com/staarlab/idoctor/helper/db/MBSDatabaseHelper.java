package com.staarlab.idoctor.helper.db;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.staarlab.idoctor.MBSApplication;
import com.staarlab.idoctor.R;
import com.staarlab.idoctor.entity.EmteMaterial;
import com.staarlab.idoctor.entity.EmteMedicine;
import com.staarlab.idoctor.entity.ExpMestTemplate;
import com.staarlab.idoctor.entity.ICD;
import com.staarlab.idoctor.entity.MedicalStock;
import com.staarlab.idoctor.entity.MedicineUserForm;
import com.staarlab.idoctor.entity.Patient;
import com.staarlab.idoctor.entity.PatientType;
import com.staarlab.idoctor.entity.PatientTypeAllow;
import com.staarlab.idoctor.entity.ServiceGroup;
import com.staarlab.idoctor.entity.ServicePATY;
import com.staarlab.idoctor.entity.ServiceRoom;
import com.staarlab.idoctor.entity.ServiceSegr;
import com.staarlab.idoctor.entity.ServiceType;
import com.staarlab.idoctor.entity.TechService;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by PHAMHUNG on 12/26/2016.
 */

public class MBSDatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String TAG = MBSDatabaseHelper.class.getName();

    private static final String DATABASE_NAME = "MBS.db";
    private static final int DATABASE_VERSION = 1;
    private static MBSDatabaseHelper instance;

    public static synchronized MBSDatabaseHelper getInstance() {
        if (instance == null) {
            instance = OpenHelperManager.getHelper(MBSApplication.get(), MBSDatabaseHelper.class);
        }
        return instance;
    }

    //    DAO
    private static Dao<ICD, Integer> icdDao;
    private static Dao<TechService, Integer> techServiceDao;
    private static Dao<ServiceType, Integer> serviceTypeDao;
    private static Dao<PatientType, Integer> patientTypeDao;
    private static Dao<ServicePATY, Integer> servicePATHDao;
    private static Dao<ServiceGroup, Integer> serviceGroupDao;
    private static Dao<ServiceSegr, Integer> serviceSegrDao;
    private static Dao<MedicalStock, Integer> medicalStockDao;
    private static Dao<MedicineUserForm, Integer> medicineUserFormDao;
    private static Dao<ServiceRoom, Integer> serviceRoomDao;
    private static Dao<ExpMestTemplate, Integer> expMestTemplateDao;
    private static Dao<EmteMedicine, Integer> emteMedicines;
    private static Dao<EmteMaterial, Integer> emteMaterials;
    private static Dao<PatientTypeAllow, Integer> patientTypeAllowsDao;

    public MBSDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            Log.i(TAG, "CREATE DATABASE");
            TableUtils.createTable(connectionSource, ICD.class);
            TableUtils.createTable(connectionSource, TechService.class);
            TableUtils.createTable(connectionSource, ServiceType.class);
            TableUtils.createTable(connectionSource, PatientType.class);
            TableUtils.createTable(connectionSource, ServicePATY.class);
            TableUtils.createTable(connectionSource, ServiceGroup.class);
            TableUtils.createTable(connectionSource, ServiceSegr.class);
            TableUtils.createTable(connectionSource, MedicalStock.class);
            TableUtils.createTable(connectionSource, MedicineUserForm.class);
            TableUtils.createTable(connectionSource, ServiceRoom.class);
            TableUtils.createTable(connectionSource, ExpMestTemplate.class);
            TableUtils.createTable(connectionSource, EmteMedicine.class);
            TableUtils.createTable(connectionSource, EmteMaterial.class);
            TableUtils.createTable(connectionSource, PatientTypeAllow.class);

        } catch (SQLException ex) {
            Log.e(TAG, ex.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(TAG, "onUpgrade");
            TableUtils.dropTable(connectionSource, ICD.class, true);
            TableUtils.dropTable(connectionSource, TechService.class, true);
            TableUtils.dropTable(connectionSource, ServiceType.class, true);
            TableUtils.dropTable(connectionSource, PatientType.class, true);
            TableUtils.dropTable(connectionSource, ServicePATY.class, true);
            TableUtils.dropTable(connectionSource, ServiceGroup.class, true);
            TableUtils.dropTable(connectionSource, ServiceSegr.class, true);
            TableUtils.dropTable(connectionSource, MedicalStock.class, true);
            TableUtils.dropTable(connectionSource, MedicineUserForm.class, true);
            TableUtils.dropTable(connectionSource, ExpMestTemplate.class, true);
            TableUtils.dropTable(connectionSource, EmteMedicine.class, true);
            TableUtils.dropTable(connectionSource, EmteMaterial.class, true);
            TableUtils.dropTable(connectionSource, PatientTypeAllow.class, true);
            // after we drop the old databases, we create the new ones
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(TAG, "Can't drop databases", e);
        }
    }


    // DAO
    public Dao<ICD, Integer> getIcdDao() {
        if (icdDao == null) {
            try {
                icdDao = getDao(ICD.class);
            } catch (SQLException ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
        return icdDao;
    }

    public Dao<TechService, Integer> getTechServiceDao() {
        if (techServiceDao == null) {
            try {
                techServiceDao = getDao(TechService.class);
            } catch (SQLException ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
        return techServiceDao;
    }

    public Dao<ServiceType, Integer> getServiceTypeDao() {
        if (serviceTypeDao == null) {
            try {
                serviceTypeDao = getDao(ServiceType.class);
            } catch (SQLException ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
        return serviceTypeDao;
    }

    public Dao<PatientType, Integer> getPatientTypeDao() {
        if (patientTypeDao == null) {
            try {
                patientTypeDao = getDao(PatientType.class);
            } catch (SQLException ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
        return patientTypeDao;
    }


    public Dao<ServiceSegr, Integer> getServiceSegrDao() {
        if (serviceSegrDao == null) {
            try {
                serviceSegrDao = getDao(ServiceSegr.class);
            } catch (SQLException ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
        return serviceSegrDao;
    }


    public Dao<ServiceGroup, Integer> getServiceGroupDao() {
        if (serviceGroupDao == null) {
            try {
                serviceGroupDao = getDao(ServiceGroup.class);
            } catch (SQLException ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
        return serviceGroupDao;
    }

    public Dao<ServicePATY, Integer> getServicePATYDao() {
        if (servicePATHDao == null) {
            try {
                servicePATHDao = getDao(ServicePATY.class);
            } catch (SQLException ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
        return servicePATHDao;
    }

    public Dao<MedicalStock, Integer> getMedicalStockDao() {
        if (medicalStockDao == null) {
            try {
                medicalStockDao = getDao(MedicalStock.class);
            } catch (SQLException ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
        return medicalStockDao;
    }

    public Dao<MedicineUserForm, Integer> getMedicineUserFormDao() {
        if (medicineUserFormDao == null) {
            try {
                medicineUserFormDao = getDao(MedicineUserForm.class);
            } catch (SQLException ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
        return medicineUserFormDao;
    }

    public Dao<ServiceRoom, Integer> getServiceRoomDao() {
        if (serviceRoomDao == null) {
            try {
                serviceRoomDao = getDao(ServiceRoom.class);
            } catch (SQLException ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
        return serviceRoomDao;
    }

    public Dao<ExpMestTemplate, Integer> getMestTemplateDao() {
        if (expMestTemplateDao == null) {
            try {
                expMestTemplateDao = getDao(ExpMestTemplate.class);
            } catch (SQLException ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
        return expMestTemplateDao;
    }

    public Dao<EmteMedicine, Integer> getEmteMedicine() {
        if (emteMedicines == null) {
            try {
                emteMedicines = getDao(EmteMedicine.class);
            } catch (SQLException ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
        return emteMedicines;
    }

    public Dao<EmteMaterial, Integer> getEmteMaterial() {
        if (emteMaterials == null) {
            try {
                emteMaterials = getDao(EmteMaterial.class);
            } catch (SQLException ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
        return emteMaterials;
    }

    public Dao<PatientTypeAllow, Integer> getPatientTypeAllow() {
        if (patientTypeAllowsDao == null) {
            try {
                patientTypeAllowsDao = getDao(PatientTypeAllow.class);
            } catch (SQLException ex) {
                Log.e(TAG, ex.getMessage());
            }
        }
        return patientTypeAllowsDao;
    }


    // --------------------CLEAR DATA TABLE
    public void clearTableICD() {
        try {
            TableUtils.clearTable(connectionSource, ICD.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearTableTechService() {
        try {
            TableUtils.clearTable(connectionSource, TechService.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearTableServiceType() {
        try {
            TableUtils.clearTable(connectionSource, ServiceType.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearTablePatientType() {
        try {
            TableUtils.clearTable(connectionSource, PatientType.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearTableServiceSegr() {
        try {
            TableUtils.clearTable(connectionSource, ServiceSegr.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearTableServiceGroup() {
        try {
            TableUtils.clearTable(connectionSource, ServiceGroup.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearTableServicePATY() {
        try {
            TableUtils.clearTable(connectionSource, ServicePATY.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearTableMedicalStock() {
        try {
            TableUtils.clearTable(connectionSource, MedicalStock.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearTableMedicineUseForm() {
        try {
            TableUtils.clearTable(connectionSource, MedicineUserForm.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearTableServiceRoom() {
        try {
            TableUtils.clearTable(connectionSource, ServiceRoom.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void clearTableExpTemplate() {
        try {
            TableUtils.clearTable(connectionSource, ExpMestTemplate.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearTableEmteMedicine() {
        try {
            TableUtils.clearTable(connectionSource, EmteMedicine.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearTableEmteMaterial() {
        try {
            TableUtils.clearTable(connectionSource, EmteMaterial.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearTablePatientTypeAllow() {
        try {
            TableUtils.clearTable(connectionSource, PatientTypeAllow.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
