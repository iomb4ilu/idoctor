package com.staarlab.idoctor.helper.layout;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.staarlab.idoctor.R;

/**
 * Created by PHAMHUNG on 10/9/2015.
 */
public class ViewEmptyStateLayout extends LinearLayout {
    public static final int VIEW_STATE_NORMAL = 0;
    public static final int VIEW_STATE_NETWORK_ERROR = 1;
    public static final int VIEW_STATE_EMPTY = 2;


    private ImageView imgView;
    private TextView tvNotice;


    public ViewEmptyStateLayout(Context context) {
        super(context);
        initializeViews(context);
    }

    public ViewEmptyStateLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initializeViews(context);
    }

    private void initializeViews(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_empty_state, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        imgView = (ImageView) findViewById(R.id.img_View);
        tvNotice = (TextView) findViewById(R.id.tv_Notice);
    }

    public void updateViewState(int state) {
        switch (state) {
            case VIEW_STATE_NORMAL: {
                this.setVisibility(GONE);
                break;
            }

            case VIEW_STATE_NETWORK_ERROR: {
                this.setVisibility(VISIBLE);
                Picasso.with(getContext()).load(R.drawable.img_network_error).into(imgView);
                tvNotice.setText(R.string.network_error);
                break;
            }

            case VIEW_STATE_EMPTY: {
                this.setVisibility(VISIBLE);
                Picasso.with(getContext()).load(R.drawable.img_empty_list_default).into(imgView);
                tvNotice.setText(R.string.empty_text);
                break;
            }
        }
    }
}
