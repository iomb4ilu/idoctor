package com.staarlab.idoctor.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ducln on 02/10/2016.
 */
public class ResponseObjectModel<T> {
    @SerializedName(value = "Data")
    private T data;
    @SerializedName(value = "Success")
    private boolean success;
    @SerializedName(value = "Param")
    private Param param;

    public T getDataResult() {
        return data;
    }

    public void setDataResult(T dataResult) {
        this.data = dataResult;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Param getParam() {
        return param;
    }

    public void setParam(Param param) {
        this.param = param;
    }
}
