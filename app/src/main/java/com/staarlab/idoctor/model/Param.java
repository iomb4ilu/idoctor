package com.staarlab.idoctor.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ducln on 02/10/2016.
 */
public class Param {
    @SerializedName(value = "Messages")
    private List<String> messages;
    @SerializedName(value = "BugCodes")
    private List<String> bugCodes;
    @SerializedName(value = "Start")
    private String start;
    @SerializedName(value = "Limit")
    private String limit;
    @SerializedName(value = "Count")
    private Integer count;
    @SerializedName(value = "ModuleCode")
    private String moduleCode;
    @SerializedName(value = "HasException")
    private boolean hasException;

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public List<String> getBugCodes() {
        return bugCodes;
    }

    public void setBugCodes(List<String> bugCodes) {
        this.bugCodes = bugCodes;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getModuleCode() {
        return moduleCode;
    }

    public void setModuleCode(String moduleCode) {
        this.moduleCode = moduleCode;
    }

    public boolean isHasException() {
        return hasException;
    }

    public void setHasException(boolean hasException) {
        this.hasException = hasException;
    }
}
