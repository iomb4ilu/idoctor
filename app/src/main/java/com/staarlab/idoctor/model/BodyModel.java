package com.staarlab.idoctor.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ducln on 20/10/2016.
 */
public class BodyModel<T> {
    @SerializedName(value = "ApiData")
    private T data;

    public BodyModel(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
