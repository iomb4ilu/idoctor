package com.staarlab.idoctor.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseArrayModel<T> {
    @SerializedName(value = "Data")
    private List<T> dataList;
    @SerializedName(value = "Success")
    private boolean success;
    @SerializedName(value = "Param")
    private Param param;

    public List<T> getDataList() {
        return dataList;
    }

    public void setDataList(List<T> dataList) {
        this.dataList = dataList;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Param getParam() {
        return param;
    }

    public void setParam(Param param) {
        this.param = param;
    }
}
