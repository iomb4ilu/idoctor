package com.staarlab.idoctor.model;

import com.google.gson.annotations.SerializedName;
import com.staarlab.idoctor.entity.User;

/**
 * Created by ducln on 02/10/2016.
 */
public class LoginModel {
    @SerializedName(value = "ValidAddress")
    private String validAddress;
    @SerializedName(value = "TokenCode")
    private String tokenCode;
    @SerializedName(value = "RenewCode")
    private String renewCode;
    @SerializedName(value = "LoginTime")
    private String loginTime;
    @SerializedName(value = "ExpireTime")
    private String expireTime;
    @SerializedName(value = "LoginAddress")
    private String loginAddress;
    @SerializedName(value = "User")
    private User user;

    public String getValidAddress() {
        return validAddress;
    }

    public void setValidAddress(String validAddress) {
        this.validAddress = validAddress;
    }

    public String getTokenCode() {
        return tokenCode;
    }

    public void setTokenCode(String tokenCode) {
        this.tokenCode = tokenCode;
    }

    public String getRenewCode() {
        return renewCode;
    }

    public void setRenewCode(String renewCode) {
        this.renewCode = renewCode;
    }

    public String getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(String loginTime) {
        this.loginTime = loginTime;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getLoginAddress() {
        return loginAddress;
    }

    public void setLoginAddress(String loginAddress) {
        this.loginAddress = loginAddress;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
