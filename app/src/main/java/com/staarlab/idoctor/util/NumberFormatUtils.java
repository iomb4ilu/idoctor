package com.staarlab.idoctor.util;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.util.Log;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Currency;

import static java.math.BigDecimal.ROUND_FLOOR;

@SuppressLint({"SimpleDateFormat", "DefaultLocale"})
public class NumberFormatUtils {

    public static String formatNumber(BigDecimal number) {
        if(number == null) number = BigDecimal.ZERO;
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
        otherSymbols.setDecimalSeparator(',');
        otherSymbols.setGroupingSeparator('.');

        df.setDecimalFormatSymbols(otherSymbols);
        return df.format(number);

    }

    public static String formatMoney(BigDecimal number) {
        if(number == null) number = BigDecimal.ZERO;
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(4);
        df.setMinimumFractionDigits(4);

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
        otherSymbols.setDecimalSeparator(',');
        otherSymbols.setGroupingSeparator('.');

        df.setDecimalFormatSymbols(otherSymbols);
        return df.format(number);

    }


    public static String formatMoneyWithRepresent(BigDecimal number, String currencyCode) {
        String formated = formatNumber(number);
        if (currencyCode != null && "VND".equalsIgnoreCase(currencyCode.trim())) {
            return formated + getCurrencyRepresent(currencyCode);
        }
        return getCurrencyRepresent(currencyCode) + formated;
    }

    public static String getCurrencyRepresent(String currencyCode) {
        if (TextUtils.isEmpty(currencyCode) || currencyCode == null) {
            return "";
        }

        currencyCode = currencyCode.trim();

        if ("ERO".equalsIgnoreCase(currencyCode) || "EUR".equalsIgnoreCase(currencyCode)) {
            return "€";
        } else if ("VND".equalsIgnoreCase(currencyCode)) {
            return "₫";
        } else if ("GBP".equalsIgnoreCase(currencyCode)) {
            return "₤";
        } else if ("USD".equalsIgnoreCase(currencyCode)) {
            return "$";
        }

        Currency c = null;
        try {
            c = Currency.getInstance(currencyCode.toUpperCase());
            return c.getSymbol();
        } catch (Exception e) {
            Log.e("Currency", "Cannot get currency code", e);
        }

        return currencyCode;
    }
}
