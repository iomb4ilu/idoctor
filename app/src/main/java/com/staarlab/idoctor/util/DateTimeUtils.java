package com.staarlab.idoctor.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateTimeUtils {

    public static Date currentDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    public static String formatDate(Date date) {
        return formatDate(date, "dd/MM/yyyy");
    }

    public static String formatTime(Date date) {
        return formatDate(date, "HH:mm");
    }


    public static String formatDateAndTime(Date date) {
        return formatDate(date, "dd/MM/yyyy HH:mm");
    }

    public static String formatDate(Date date, String format) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        return sdf.format(date);
    }


    public static Date truncTime(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    public static Date convertWithTimezone(TimeZone timeZone, Date localDate) {
        Calendar calLocal = Calendar.getInstance(timeZone);
        calLocal.setTime(localDate);

        Calendar calAfter = Calendar.getInstance();
        calAfter.set(Calendar.YEAR, calLocal.get(Calendar.YEAR));
        calAfter.set(Calendar.MONTH, calLocal.get(Calendar.MONTH));
        calAfter.set(Calendar.DAY_OF_MONTH, calLocal.get(Calendar.DAY_OF_MONTH));
        calAfter.set(Calendar.HOUR_OF_DAY, calLocal.get(Calendar.HOUR_OF_DAY));
        calAfter.set(Calendar.MINUTE, calLocal.get(Calendar.MINUTE));
        calAfter.set(Calendar.MILLISECOND, calLocal.get(Calendar.MILLISECOND));
        return calAfter.getTime();
    }

    public static String changeDateFormat(Date date, String strFormat) {
        if (strFormat == null) {
            strFormat = "";
        }
        if (date == null) return "";
        SimpleDateFormat timeFormat = new SimpleDateFormat(
                strFormat, Locale.getDefault());
        return timeFormat.format(date);
    }

    public static int compare(Date d1, Date d2) {
        Calendar c1 = Calendar.getInstance();
        c1.setTime(d1);
        Calendar c2 = Calendar.getInstance();
        c2.setTime(d2);
        if (c1.get(Calendar.YEAR) != c2.get(Calendar.YEAR))
            return c1.get(Calendar.YEAR) - c2.get(Calendar.YEAR);
        if (c1.get(Calendar.MONTH) != c2.get(Calendar.MONTH))
            return c1.get(Calendar.MONTH) - c2.get(Calendar.MONTH);
        return c1.get(Calendar.DAY_OF_MONTH) - c2.get(Calendar.DAY_OF_MONTH);
    }

    public static boolean isToday(Date date) {
        Calendar today = Calendar.getInstance();
        Calendar anotherDate = Calendar.getInstance();
        anotherDate.setTime(date);

        return today.get(Calendar.YEAR) == anotherDate.get(Calendar.YEAR)
                && today.get(Calendar.MONTH) == anotherDate.get(Calendar.MONTH)
                && today.get(Calendar.DAY_OF_MONTH) == anotherDate.get(Calendar.DAY_OF_MONTH);
    }

    public static long formatLongCurrentTime() {
        Date now = new Date();
        String formatStr = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH).format(now);
        long result = Long.valueOf(formatStr);
        return result;
    }

    public static long formatLongTime(Date date) {
        String formatStr = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH).format(date);
        long result = Long.valueOf(formatStr);
        return result;
    }

    public static long getBeginingTimeOfToday() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date beginningOfDay = calendar.getTime();
        String formatStr = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH).format(beginningOfDay);
        return Long.valueOf(formatStr);
    }

    public static long getEndTimeOfToday() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        Date endTimeOfDay = calendar.getTime();
        String formatStr = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH).format(endTimeOfDay);
        return Long.valueOf(formatStr);
    }

}
