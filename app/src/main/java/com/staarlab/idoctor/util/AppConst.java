package com.staarlab.idoctor.util;

import com.staarlab.idoctor.R;

/**
 * Author: Lucero
 * Created: 12/19/16.
 */

public class AppConst {
    // Web service
    public static final String API_TOKEN_CODE = "TokenCode";
    public static final String API_APP_CODE = "ApplicationCode";
    public static final String API_ACCEPT = "Accept";
    public static final String API_ACCEPT_VALUE = "application/json";

    public static final String APP_PREF_NAME = "com.staarlab.idoctor.prefs";
    public static final String PREF_KEY_LOGIN_NAME = "login_name";
    public static final String PREF_KEY_USER_NAME = "user_name";
    public static final String PREF_KEY_TOKEN_CODE = "token_code";
    public static final String PREF_KEY_RENEW_CODE = "renew_code";
    public static final String PREF_KEY_APP_CODE = "app_code";
    public static final String PREF_KEY_LAST_UPDATE = "last_update";
    public static final String PREF_KEY_MEDICINE_MATERIAL_INTERVAL_UPDATE = "interval_update";
    public static final String PREF_SERVER_AUTHENTICATION = "SERVER_AUTHENTICATION";
    public static final String PREF_SERVER_API = "SERVER_API";

    public static final String TAB_CODE_GENERAL = "General";
    public static final String TAB_CODE_DEATH = "CountDeath";
    public static final String TAB_CODE_TRAN_PATI = "CountTranPati";
    public static final String TAB_CODE_DEBATE = "CountDebate";
    public static final String TAB_CODE_MEDI_REACT = "CountMediReact";
    public static final String TAB_CODE_TRACKING = "CountTracking";
    public static final String TAB_CODE_INFUSION = "CountInfusion";
    public static final String TAB_CODE_DHST = "CountDhst";
    public static final String TAB_CODE_CARE = "CountCare";
    public static final String TAB_CODE_REHA = "CountReha";
    public static final String TAB_CODE_OTHER = "CountOther";
    public static final String TAB_CODE_SURG = "CountSurg";
    public static final String TAB_CODE_SUIM = "CountSuim";
    public static final String TAB_CODE_ENDO = "CountEndo";
    public static final String TAB_CODE_BED = "CountBed";
    public static final String TAB_CODE_PRESCRIPTION = "CountPres";
    public static final String TAB_CODE_FUEX = "CountFuex";
    public static final String TAB_CODE_MISU = "CountMisu";
    public static final String TAB_CODE_DIIM = "CountDiim";
    public static final String TAB_CODE_TEST = "CountTest";
    public static final String TAB_CODE_EXAM = "CountExam";

    public static final String[] INFO_TABS = new String[]{"CountDeath", "CountTranPati",
            "CountDebate", "CountMediReact", "CountTracking", "CountInfusion", "CountDhst",
            "CountCare", "CountReha", "CountOther", "CountSurg", "CountSuim", "CountEndo",
            "CountBed", "CountPres", "CountFuex", "CountMisu", "CountDiim", "CountTest",
            "CountExam"
    };
    public static final int[] INFO_TAB_TITLE_IDS = new int[]{R.string.tab_title_death, R.string.tab_title_tranpati,
            R.string.tab_title_debate, R.string.tab_title_medi_react, R.string.tab_title_tracking, R.string.tab_title_infusion, R.string.tab_title_dhst,
            R.string.tab_title_care, R.string.tab_title_reha, R.string.tab_title_other, R.string.tab_title_surg, R.string.tab_title_suim, R.string.tab_title_endo,
            R.string.tab_title_bed, R.string.tab_title_pres, R.string.tab_title_fuex, R.string.tab_title_misu, R.string.tab_title_diim, R.string.tab_title_test,
            R.string.tab_title_exam};
}
