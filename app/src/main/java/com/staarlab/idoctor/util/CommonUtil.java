package com.staarlab.idoctor.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Author: Lucero
 * Created: 12/21/16.
 */

public class CommonUtil {
    private static DecimalFormat numFormat = new DecimalFormat("###,###.000");

    public static String hisString2DateAsString(String hisDate) {
        try {
            String y = hisDate.substring(0, 4);
            String m = hisDate.substring(4, 6);
            String d = hisDate.substring(6, 8);
            return d + "/" + m + "/" + y;
        } catch (Exception e) {
            return "";
        }
    }

    public static String hisString2DateTimeAsString(String hisDateTime) {
        try {
            String y = hisDateTime.substring(0, 4);
            String m = hisDateTime.substring(4, 6);
            String d = hisDateTime.substring(6, 8);
            String hh = hisDateTime.substring(8, 10);
            String mm = hisDateTime.substring(10, 12);
            String ss = hisDateTime.substring(12, 14);
            return d + "/" + m + "/" + y + " " + hh + ":" + mm + ":" + ss;
        } catch (Exception e) {
            return "";
        }
    }


    public static String hisNumber2String(BigDecimal num) {
        if (num == null || num.compareTo(BigDecimal.ZERO) == 0) {
            return "";
        }
        return numFormat.format(num.doubleValue());
    }
}
